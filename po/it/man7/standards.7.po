# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Giulio Daprelà <giulio@pluto.it>, 2006.
# Elisabetta Galli <lab@kkk.it>, 2007-2008.
# Marco Curreli <marcocurreli@tiscali.it>, 2013-2018, 2020, 2021.
# Giuseppe Sacco <eppesuig@debian.org>, 2024
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2024-12-06 18:15+0100\n"
"PO-Revision-Date: 2024-06-22 17:30+0200\n"
"Last-Translator: Giuseppe Sacco <eppesuig@debian.org>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "standards"
msgstr "standards"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maggio 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "standards - C and UNIX Standards"
msgstr "standards - Standard C e UNIX"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The STANDARDS section that appears in many manual pages identifies various "
"standards to which the documented interface conforms.  The following list "
"briefly describes these standards."
msgstr ""
"La sezione \"CONFORME A\" che appare in molte pagine di manuale identifica "
"vari standard a cui si conformano le interfacce documentate. L'elenco "
"seguente descrive brevemente questi standard."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<V7>"
msgstr "B<V7>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Version 7 (also known as Seventh Edition) UNIX, released by AT&T/Bell Labs "
"in 1979.  After this point, UNIX systems diverged into two main dialects: "
"BSD and System V."
msgstr ""
"Versione 7 (noto anche come Seventh Edition) UNIX, rilasciata da  AT&T/Bell "
"Labs nel 1979. Da questo punto in poi, i sistemi UNIX si ramificarono in due "
"dialetti principali: BSD e System V."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<4.2BSD>"
msgstr "B<4.2BSD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is an implementation standard defined by the 4.2 release of the "
"I<Berkeley Software Distribution>, released by the University of California "
"at Berkeley.  This was the first Berkeley release that contained a TCP/IP "
"stack and the sockets API.  4.2BSD was released in 1983."
msgstr ""
"Questa è un'implementazione standard definita dal rilascio 4.2 della "
"I<Berkeley Software Distribution>, rilasciata dalla University of California "
"a Berkeley. Questo è stato il primo rilascio della Berkeley a contenere uno "
"stack TCP/IP e le API dei socket. La 4.2BSD è stata rilasciata nel 1983."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Earlier major BSD releases included I<3BSD> (1980), I<4BSD> (1980), and "
"I<4.1BSD> (1981)."
msgstr ""
"I principali rilasci precedenti di BSD inclusero I<3BSD> (1980), I<4BSD> "
"(1980), e I<4.1BSD> (1981)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<4.3BSD>"
msgstr "B<4.3BSD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The successor to 4.2BSD, released in 1986."
msgstr "Il successore della 4.2BSD, rilasciato nel 1986."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<4.4BSD>"
msgstr "B<4.4BSD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The successor to 4.3BSD, released in 1993.  This was the last major Berkeley "
"release."
msgstr ""
"Il successore della 4.3BSD, rilasciato nel 1993. Questo è stato l'ultimo "
"rilascio principale della Berkeley."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<System V>"
msgstr "B<System V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is an implementation standard defined by AT&T's milestone 1983 release "
"of its commercial System V (five) release.  The previous major AT&T release "
"was I<System III>, released in 1981."
msgstr ""
"Questa è un'implementazione standard definita dalla AT&T con il rilascio del "
"1983, pietra miliare del suo rilascio commerciale System V (cinque). Il "
"principale rilascio precedente della AT&T è stato I<System III>, rilasciato "
"nel 1981."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<System V release 2 (SVr2)>"
msgstr "B<System V release 2 (SVr2)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was the next System V release, made in 1985.  The SVr2 was formally "
"described in the I<System V Interface Definition version 1> (I<SVID 1>)  "
"published in 1985."
msgstr ""
"Questo fu il rilascio successivo di System V, uscito nel 1985. La SVr2 fu "
"formalmente descritta in I<System V Interface Definition version 1> (I<SVID "
"1>) pubblicato nel 1985."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<System V release 3 (SVr3)>"
msgstr "B<System V release 3 (SVr3)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was the successor to SVr2, released in 1986.  This release was formally "
"described in the I<System V Interface Definition version 2> (I<SVID 2>)."
msgstr ""
"Questa succedette alla SVr2, e fu rilasciata nel 1986. Questo rilascio fu "
"formalmente descritto in I<System V Interface Definition version 2> (I<SVID "
"2>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<System V release 4 (SVr4)>"
msgstr "B<System V release 4 (SVr4)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was the successor to SVr3, released in 1989.  This version of System V "
"is described in the \"Programmer's Reference Manual: Operating System API "
"(Intel processors)\" (Prentice-Hall 1992, ISBN 0-13-951294-2)  This release "
"was formally described in the I<System V Interface Definition version 3> "
"(I<SVID 3>), and is considered the definitive System V release."
msgstr ""
"Questa succedette alla SVr3, e fu rilasciata nel 1989. Questa versione di "
"System V è descritta nel \"Programmer's Reference Manual: Operating System "
"API (Intel processors)\" (Prentice-Hall 1992, ISBN 0-13-951294-2) Questo "
"rilascio fu formalmente descritto in I<System V Interface Definition version "
"3> (I<SVID 3>), ed è considerato il rilascio definitivo di System V."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SVID 4>"
msgstr "B<SVID 4>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"System V Interface Definition version 4, issued in 1995.  Available online "
"at E<.UR http://www.sco.com\\:/developers\\:/devspecs/> E<.UE .>"
msgstr ""
"System V Interface Definition versione 4, emesso nel 1995. Disponibile "
"online presso E<.UR http://www.sco.com\\:/developers\\:/devspecs/> E<.UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<C89>"
msgstr "B<C89>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was the first C language standard, ratified by ANSI (American National "
"Standards Institute) in 1989 (I<X3.159-1989>).  Sometimes this is known as "
"I<ANSI C>, but since C99 is also an ANSI standard, this term is ambiguous.  "
"This standard was also ratified by ISO (International Standards "
"Organization) in 1990 (I<ISO/IEC 9899:1990>), and is thus occasionally "
"referred to as I<ISO C90>."
msgstr ""
"Questo fu il primo linguaggio C standard, ratificato da ANSI (American "
"National Standards Institute) nel 1989 (I<X3.159-1989>). Talvolta esso è "
"noto come I<ANSI C>, ma, poiché a partire da C99 è anche uno standard ANSI, "
"questo termine è ambiguo. Questo standard fu anche ratificato da ISO "
"(International Standards Organization) nel 1990 (I<ISO/IEC 9899:1990>), e "
"quindi occasionalmente si fa riferimento a esso come I<ISO C90>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<C99>"
msgstr "B<C99>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This revision of the C language standard was ratified by ISO in 1999 (I<ISO/"
"IEC 9899:1999>).  Available online at E<.UR http://www.open-std.org\\:/"
"jtc1\\:/sc22\\:/wg14\\:/www\\:/standards> E<.UE .>"
msgstr ""
"Questa revisione del linguaggio C standard fu ratificata da ISO nel 1999 "
"(I<ISO/IEC 9899:1999>). È disponibile online presso E<.UR http://www.open-"
"std.org\\:/jtc1\\:/sc22\\:/wg14\\:/www\\:/standards> E<.UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<C11>"
msgstr "B<C11>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This revision of the C language standard was ratified by ISO in 2011 (I<ISO/"
"IEC 9899:2011>)."
msgstr ""
"Questa revisione del linguaggio C standard fu ratificata da ISO nel 2011 "
"(I<ISO/IEC 9899:2011>)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LFS>"
msgstr "B<LFS>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Large File Summit specification, completed in 1996.  This specification "
"defined mechanisms that allowed 32-bit systems to support the use of large "
"files (i.e., 64-bit file offsets).  See E<.UR https://www.opengroup.org\\:/"
"platform\\:/lfs.html> E<.UE .>"
msgstr ""
"La specifica del Large File Summit, completata nel 1996. Questa specifica ha "
"definito i meccanismi che consentono ai sistemi a 32 bit di supportare l'uso "
"di file di grandi dimensioni (cioè offset a 64 bit). Si veda E<.UR https://"
"www.opengroup.org\\:/platform\\:/lfs.html> E<.UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-1988>"
msgstr "B<POSIX.1-1988>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was the first POSIX standard, ratified by IEEE as IEEE Std 1003.1-1988, "
"and subsequently adopted (with minor revisions) as an ISO standard in 1990.  "
"The term \"POSIX\" was coined by Richard Stallman."
msgstr ""
"Questo è stato il primo standard POSIX, ratificato dall'IEEE come IEEE Std "
"1003.1-1988, e successivamente adottato (con revisioni minori) come standard "
"ISO nel 1990. Il termine \"POSIX\" fu coniato da Richard Stallman."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-1990>"
msgstr "B<POSIX.1-1990>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\"Portable Operating System Interface for Computing Environments\".  IEEE "
"1003.1-1990 part 1, ratified by ISO in 1990 (I<ISO/IEC 9945-1:1990>)."
msgstr ""
"\"Portable Operating System Interface for Computing Environments\". IEEE "
"1003.1-1990 parte 1, ratificato da ISO nel 1990 (I<ISO/IEC 9945-1:1990>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.2>"
msgstr "B<POSIX.2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"IEEE Std 1003.2-1992, describing commands and utilities, ratified by ISO in "
"1993 (I<ISO/IEC 9945-2:1993>)."
msgstr ""
"IEEE Std 1003.2-1992, che descrive comandi e utilità, ratificato da ISO nel "
"1993 (I<ISO/IEC 9945-2:1993>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1b> (formerly known as I<POSIX.4>)"
msgstr "B<POSIX.1b> (precedentemente conosciuto come I<POSIX.4>)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"IEEE Std 1003.1b-1993, describing real-time facilities for portable "
"operating systems, ratified by ISO in 1996 (I<ISO/IEC 9945-1:1996>)."
msgstr ""
"IEEE Std 1003.1b-1993, che descrive i servizi real-time per sistemi "
"operativi portabili, ratificato da ISO nel 1996 (I<ISO/IEC 9945-1:1996>)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1c> (formerly known as I<POSIX.4a>)"
msgstr "B<POSIX.1c> (precedentemente conosciuto come I<POSIX.4a>)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "IEEE Std 1003.1c-1995, which describes the POSIX threads interfaces."
msgstr "IEEE Std 1003.1c-1995, che descrive le interfacce thread POSIX."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1d>"
msgstr "B<POSIX.1d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "IEEE Std 1003.1d-1999, which describes additional real-time extensions."
msgstr "IEEE Std 1003.1d-1999, che descrive estensioni real-time aggiuntive."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1g>"
msgstr "B<POSIX.1g>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"IEEE Std 1003.1g-2000, which describes networking APIs (including sockets)."
msgstr "IEEE Std 1003.1g-2000, che descrive le API di rete (inclusi i socket)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1j>"
msgstr "B<POSIX.1j>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "IEEE Std 1003.1j-2000, which describes advanced real-time extensions."
msgstr "IEEE Std 1003.1j-2000, che descrive estensioni real-time avanzate."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-1996>"
msgstr "B<POSIX.1-1996>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A 1996 revision of POSIX.1 which incorporated POSIX.1b and POSIX.1c."
msgstr "Una revisione del 1996 di POSIX.1 che incorpora POSIX.1b e POSIX.1c."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<XPG3>"
msgstr "B<XPG3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Released in 1989, this was the first release of the X/Open Portability Guide "
"to be based on a POSIX standard (POSIX.1-1988).  This multivolume guide was "
"developed by the X/Open Group, a multivendor consortium."
msgstr ""
"Rilasciato nel 1989, questo fu il primo rilascio della guida I<X/Open "
"Portability Guide> ad essere basata su uno standard POSIX (POSIX.1-1988).  "
"Questa guida a più volumi sviluppata da X/Open Group, un consorzio di "
"aziende produttrici."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<XPG4>"
msgstr "B<XPG4>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A revision of the X/Open Portability Guide, released in 1992.  This revision "
"incorporated POSIX.2."
msgstr ""
"Una revisione della X/Open Portability Guide, rilasciata nel 1992. Questa "
"revisione ha incorporato POSIX.2."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<XPG4v2>"
msgstr "B<XPG4v2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A 1994 revision of XPG4.  This is also referred to as I<Spec 1170>, where "
"1170 referred to the number of interfaces defined by this standard."
msgstr ""
"Una revisione del 1994 di XPG4. Ad essa si fa anche riferimento come I<Spec "
"1170>, dove 1170 si riferisce al numero di interfacce definite da questo "
"standard."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUS (SUSv1)>"
msgstr "B<SUS (SUSv1)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Single UNIX Specification.  This was a repackaging of XPG4v2 and other X/"
"Open standards (X/Open Curses Issue 4 version 2, X/Open Networking Service "
"(XNS) Issue 4).  Systems conforming to this standard can be branded I<UNIX "
"95>."
msgstr ""
"Single UNIX Specification. Questo è un ripacchettamento di XPG4v2 e altri "
"standard X/Open (X/Open Curses Issue 4 versione 2, X/Open Networking Service "
"(XNS) Issue 4). I sistemi conformi a questo standard possono essere marcati "
"I<UNIX 95>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUSv2>"
msgstr "B<SUSv2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Single UNIX Specification version 2.  Sometimes also referred to "
"(incorrectly) as I<XPG5>.  This standard appeared in 1997.  Systems "
"conforming to this standard can be branded I<UNIX 98>.  See also E<.UR "
"http://www.unix.org\\:/version2/> E<.UE .)>"
msgstr ""
"Single UNIX Specification versione 2. Talvolta chiamata anche (erroneamente) "
"I<XPG5>. Questo standard apparve nel 1997. I sistemi conformi a questo "
"standard possono essere marcati I<UNIX 98>. Vedere anche E<.UR http://www."
"unix.org\\:/version2/> E<.UE .)>"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-2001>"
msgstr "B<POSIX.1-2001>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUSv3>"
msgstr "B<SUSv3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was a 2001 revision and consolidation of the POSIX.1, POSIX.2, and SUS "
"standards into a single document, conducted under the auspices of the Austin "
"Group E<.UR http://www.opengroup.org\\:/austin/> E<.UE .> The standard is "
"available online at E<.UR http://www.unix.org\\:/version3/> E<.UE .>"
msgstr ""
"Questa fu una revisione e consolidamento del 2001 degli standard POSIX.1, "
"POSIX.2 e SUS in un singolo documento, condotta sotto gli auspici del gruppo "
"Austin E<.UR http://www.opengroup.org\\:/austin/> E<.UE .> Lo standard è "
"disponibile in rete all'indirizzo E<.UR http://www.unix.org\\:/version3/> E<."
"UE .>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The standard defines two levels of conformance: I<POSIX conformance>, which "
"is a baseline set of interfaces required of a conforming system; and I<XSI "
"Conformance>, which additionally mandates a set of interfaces (the \"XSI "
"extension\") which are only optional for POSIX conformance.  XSI-conformant "
"systems can be branded I<UNIX 03>."
msgstr ""
"Lo standard definisce due livelli di conformità: I<POSIX conformance>, che è "
"l'insieme di interfacce basilari necessarie per un sistema conforme; e I<XSI "
"Conformance>, che in aggiunta impone una serie di interfacce (le \"XSI "
"extension\") che sono solo opzionali per la conformità POSIX. I sistemi "
"conformi XSI possono essere marcati I<UNIX 03>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The POSIX.1-2001 document is broken into four parts:"
msgstr "Il documento POSIX.1-2001 è diviso in quattro parti:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<XBD>: Definitions, terms, and concepts, header file specifications."
msgstr "B<XBD>: Definizioni, termini e concetti, specifiche dei file header."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<XSH>: Specifications of functions (i.e., system calls and library "
"functions in actual implementations)."
msgstr ""
"B<XSH>: Specifiche delle funzioni (cioè chiamate di sistema e funzioni di "
"libreria nelle implementazioni reali)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<XCU>: Specifications of commands and utilities (i.e., the area formerly "
"described by POSIX.2)."
msgstr ""
"B<XCU>: Specifiche di comandi e utilità (cioè l'area un tempo descritta da "
"POSIX.2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<XRAT>: Informative text on the other parts of the standard."
msgstr "B<XRAT>: Testo informativo sulle altre parti dello standard."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 is aligned with C99, so that all of the library functions "
"standardized in C99 are also standardized in POSIX.1-2001."
msgstr ""
"POSIX.1-2001 è allineato con C99, in questo modo tutte le funzioni di "
"libreria standardizzate in C99 sono anche standardizzate in POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Single UNIX Specification version 3 (SUSv3) comprises the Base "
"Specifications containing XBD, XSH, XCU, and XRAT as above, plus X/Open "
"Curses Issue 4 version 2 as an extra volume that is not in POSIX.1-2001."
msgstr ""
"La Single UNIX Specification versione 3 (SUSv3) comprende le Base "
"Specifications che contengono XBD, XSH, XCU e XRAT come precedentemente "
"descritto, più X/Open Curses Issue 4 versione 2 come volume extra che non è "
"in POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Two Technical Corrigenda (minor fixes and improvements)  of the original "
"2001 standard have occurred: TC1 in 2003 and TC2 in 2004."
msgstr ""
"Ci sono stati due Technical Corrigenda (piccole correzioni e miglioramenti) "
"dello standard originale del 2001: TC1 nel 2003, e TC2 nel 2004."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-2008>"
msgstr "B<POSIX.1-2008>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUSv4>"
msgstr "B<SUSv4>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Work on the next revision of POSIX.1/SUS was completed and ratified in "
"2008.  The standard is available online at E<.UR http://www.unix.org\\:/"
"version4/> E<.UE .>"
msgstr ""
"I lavori sulla prossima revisione di POSIX.1/SUS sono stati completati e "
"ratificati nel 2008. Lo standard è disponibile online all'indirizzo E<.UR "
"http://www.unix.org\\:/version4/> E<.UE .>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The changes in this revision are not as large as those that occurred for "
"POSIX.1-2001/SUSv3, but a number of new interfaces are added and various "
"details of existing specifications are modified.  Many of the interfaces "
"that were optional in POSIX.1-2001 become mandatory in the 2008 revision of "
"the standard.  A few interfaces that are present in POSIX.1-2001 are marked "
"as obsolete in POSIX.1-2008, or removed from the standard altogether."
msgstr ""
"I cambiamenti in questa revisione non sono tanti quanti quelli verificatisi "
"per POSIX.1-2001/SUSv3, ma sono state aggiunte molte nuove interfacce e sono "
"stati modificati vari dettagli di specifiche esistenti. Molte interfacce che "
"in POSIX.1-2001 erano opzionali diventano obbligatorie nella revisione 2008 "
"dello standard. Alcune interfacce presenti in POSIX.1-2001 sono marcate come "
"obsolete in POSIX.1-2008, o rimosse del tutto dallo standard."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The revised standard is structured in the same way as its predecessor.  The "
"Single UNIX Specification version 4 (SUSv4) comprises the Base "
"Specifications containing XBD, XSH, XCU, and XRAT, plus X/Open Curses Issue "
"7 as an extra volume that is not in POSIX.1-2008."
msgstr ""
"Lo standard rivisto è strutturato nello stesso modo del predecessore. La "
"Single UNIX Specification versione 3 (SUSv3) comprende le Base "
"Specifications che contengono XBD, XSH, XCU e XRAT, più X/Open Curses Issue "
"4 versione 2 come volume extra che non è in POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Again there are two levels of conformance: the baseline I<POSIX "
"Conformance>, and I<XSI Conformance>, which mandates an additional set of "
"interfaces beyond those in the base specification."
msgstr ""
"Anche in questo caso ci sono due livelli di conformità: la linea di fondo "
"I<POSIX Conformance>, e I<XSI Conformance>, che rende obbligatorio un "
"insieme di interfacce aggiuntive oltre a quelle delle specifiche di base."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In general, where the STANDARDS section of a manual page lists POSIX.1-2001, "
"it can be assumed that the interface also conforms to POSIX.1-2008, unless "
"otherwise noted."
msgstr ""
"In generale, quando la sezione CONFORME A di una pagina di manuale elenca "
"POSIX.1-2001, si può assumere che l'interfaccia sia conforme anche a "
"POSIX.1-2008, a meno che non sia indicato diversamente."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Technical Corrigendum 1 (minor fixes and improvements)  of this standard was "
"released in 2013."
msgstr ""
"L'errata-corrige (\"Technical Corrigendum 1\") di questo standard "
"(correzioni minori e miglioramenti) è stato rilasciata nel 2013."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Technical Corrigendum 2 of this standard was released in 2016."
msgstr ""
"L'errata-corrige (\"Technical Corrigendum 2\") di questo standard è stata "
"rilasciata nel 2016."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Further information can be found on the Austin Group web site, E<.UR http://"
"www.opengroup.org\\:/austin/> E<.UE .>"
msgstr ""
"Si possono trovare ulteriori informazioni nel sito web del gruppo di Austin, "
"E<.UR http://www.opengroup.org\\:/austin/> E<.UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUSv4 2016 edition>"
msgstr "B<SUSv4 edizione 2016 edition>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is equivalent to POSIX.1-2008, with the addition of Technical "
"Corrigenda 1 and 2 and the XCurses specification."
msgstr ""
"È equivalente a POSIX.1-2008, con l'aggiunta di Technical Corrigenda 1 e 2 e "
"le specifiche XCurses."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-2017>"
msgstr "B<POSIX.1--2017>"

# applied
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This revision of POSIX is technically identical to POSIX.1-2008 with "
"Technical Corrigenda 1 and 2 applied."
msgstr ""
"Questa resivsione di POSIX è tecnicamente identica a POSIX.1-2008 con "
"l'aggiunta di Technical Corrigenda 1 e 2."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUSv4 2018 edition>"
msgstr "B<SUSv4 2018 edition>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is equivalent to POSIX.1-2017, with the addition of the XCurses "
"specification."
msgstr ""
"È equivalente a  POSIX.1-2017, con l'aggiunta delle specifiche  XCurses."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The interfaces documented in POSIX.1/SUS are available as manual pages under "
"sections 0p (header files), 1p (commands), and 3p (functions); thus one can "
"write \"man 3p open\"."
msgstr ""
"Le interfacce documentate in POSIX.1/SUS sono disponibili come pagine di "
"manuale alla sezione 0p(file di intestazione), 1p (comandi) e 3p (funzioni); "
"perciò uno può scrivere \"man 3p open\"."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getconf>(1), B<confstr>(3), B<pathconf>(3), B<sysconf>(3), "
"B<attributes>(7), B<feature_test_macros>(7), B<libc>(7), B<posixoptions>(7), "
"B<system_data_types>(7)"
msgstr ""
"B<getconf>(1), B<confstr>(3), B<pathconf>(3), B<sysconf>(3), "
"B<attributes>(7), B<feature_test_macros>(7), B<libc>(7), B<posixoptions>(7), "
"B<system_data_types>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 ottobre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<LFS> The Large File Summit specification, completed in 1996.  This "
"specification defined mechanisms that allowed 32-bit systems to support the "
"use of large files (i.e., 64-bit file offsets).  See E<.UR https://www."
"opengroup.org\\:/platform\\:/lfs.html> E<.UE .>"
msgstr ""
"B<LFS> La specifica del Large File Summit, completata nel 1996. Questa "
"specifica ha definito i meccanismi che consentono ai sistemi a 32 bit di "
"supportare l'uso di file di grandi dimensioni (cioè offset a 64 bit). Si "
"veda  E<.UR https://www.opengroup.org\\:/platform\\:/lfs.html> E<.UE .>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<POSIX.1c  (formerly known as >I<POSIX.4a>B<)>"
msgstr "B<POSIX.1c (precedentemente conosciuto come>I<POSIX.4a>B<)>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<POSIX.1-2001, SUSv3>"
msgstr "B<POSIX.1-2001, SUSv3>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<POSIX.1-2008, SUSv4>"
msgstr "B<POSIX.1-2008, SUSv4>"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 ottobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (non rilasciato)"
