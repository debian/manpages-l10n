# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-09-06 18:27+0200\n"
"PO-Revision-Date: 2024-01-26 20:29+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sane-artec"
msgstr "sane-artec"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "11 Jul 2008"
msgstr "11 iulie 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE: „Scanner Access Now Easy”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sane-artec - SANE backend for Artec flatbed scanners"
msgstr "sane-artec - controlor SANE pentru scanerele plate Artec"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<sane-artec> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to Artec/Ultima SCSI flatbed scanners.  At "
"present, the following scanners are known to work at least partially with "
"this backend:"
msgstr ""
"Biblioteca B<sane-artec> implementează un controlor SANE (Scanner Access Now "
"Easy) care oferă acces la scanerele plate SCSI Artec/Ultima. În prezent, se "
"știe că următoarele scanere funcționează cel puțin parțial cu acest "
"controlor:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "* Artec A6000C"
msgstr "* Artec A6000C"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "* Artec A6000C PLUS"
msgstr "* Artec A6000C PLUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "* Artec ViewStation AT3"
msgstr "* Artec ViewStation AT3"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "* BlackWidow BW4800SP (rebadged Artec AT3)"
msgstr "* BlackWidow BW4800SP (Artec AT3 rebotezat)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "* Artec ViewStation AT6"
msgstr "* Artec ViewStation AT6"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "* Artec ViewStation AT12"
msgstr "* Artec ViewStation AT12"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "* Artec AM12S"
msgstr "* Artec AM12S"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "* Plustek 19200S (rebadged Artec AM12S)"
msgstr "* Plustek 19200S (Artec AM12S rebotezat)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Although this manual page is generally updated with each release, up-to-date "
"information on new releases and extraneous helpful hints are available from "
"the backend homepage: I<http://www4.infi.net/~cpinkham/sane>."
msgstr ""
"Deși această pagină de manual este în general actualizată cu fiecare "
"versiune, informații actualizate despre noile versiuni și alte sfaturi utile "
"sunt disponibile în pagina principală a controlorului: I<http://"
"www4.infi.net/~cpinkham/sane>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The contents of the I<artec.conf> file are a list of device names that "
"correspond to Artec scanners.  Empty lines and lines starting with a hash "
"mark (#) are ignored.  See B<sane-scsi>(5)  on details of what constitutes a "
"valid device name."
msgstr ""
"Conținutul fișierului I<artec.conf> este o listă de nume de dispozitive care "
"corespund scanerelor Artec. Liniile goale și liniile care încep cu un simbol "
"hash (#) sunt ignorate. A se vedea B<sane-scsi>(5) pentru detalii despre "
"ceea ce constituie un nume de dispozitiv valid."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Sample file:"
msgstr "Exemplu de fișier:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"# artec.conf\n"
"#\n"
"# this is a comment.\n"
"#\n"
"# this line says search for any SCSI devices which are scanners and have\n"
"#     a vendor string of 'ULTIMA'\n"
"scsi ULTIMA\n"
"#\n"
"# the next line forces the backend to assume the next scanner found has\n"
"#     the specified vendor string (useful for testing rebadged models).\n"
"vendor ULTIMA\n"
"#\n"
"# the next line forces the backend to assume the next scanner found has\n"
"#     the specified model string (useful for testing rebadged models).\n"
"model AT3\n"
"#\n"
"# now a line that actually specifies a device.  The backend is going to\n"
"#     assume this is an Artec/Ultima AT3 because we forced the vendor and\n"
"#     model above.\n"
"/dev/scanner\n"
"#\n"
"# once we hit a scanner device line, the forced vendor and model\n"
"# string are\n"
"#     'forgotten', so the vendor and model for this next device will be\n"
"#     determined from the result of a SCSI inquiry.\n"
"/dev/sge\n"
"#\n"
msgstr ""
"# artec.conf\n"
"#\n"
"# acesta este un comentariu.\n"
"#\n"
"# această linie spune că trebuie să caute orice dispozitive SCSI care sunt scanere și care au\n"
"#     un șir de fabricant „ULTIMA”\n"
"scsi ULTIMA\n"
"#\n"
"# următoarea linie forțează controlorul să presupună că următorul scaner găsit are\n"
"# șirul de fabricant specificat (util pentru testarea modelelor rebotezate).\n"
"vendor ULTIMA\n"
"#\n"
"# următoarea linie forțează controlorul să presupună că următorul scaner găsit are\n"
"#     șirul de modele specificat (util pentru testarea modelelor rebotezate).\n"
"model AT3\n"
"#\n"
"# acum o linie care specifică de fapt un dispozitiv.  Controlorul va\n"
"#     presupune că acesta este un Artec/Ultima AT3 deoarece am forțat fabricantul și\n"
"# modelul de mai sus.\n"
"/dev/scanner\n"
"#\n"
"# odată ce am ajuns la o linie de dispozitiv de scanare, fabricantul și modelul forțat\n"
"# sunt\n"
"#     „uitate”, astfel încât fabricantul și modelul pentru următorul dispozitiv vor fi\n"
"#     determinate din rezultatul unei interogări SCSI.\n"
"/dev/sge\n"
"#\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SCSI ADAPTER TIPS"
msgstr "SFATURI PENTRU ADAPTORUL SCSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some Artec scanners come with an included SCSI adapter.  If your scanner "
"came with a DTC ISA SCSI cards, you can probably use it with recent (E<gt>= "
"2.2.0)  kernels using the generic NCR5380 support.  You must pass the "
"following boot argument to the kernel: \"dtc3181e=0x2c0,0\""
msgstr ""
"Unele scanere Artec vin cu un adaptor SCSI inclus. Dacă scanerul dvs. a fost "
"livrat cu o placă DTC ISA SCSI, probabil că îl puteți utiliza cu nuclee "
"recente (E<gt>= 2.2.0) folosind suportul generic NCR5380. Trebuie să "
"transmiteți nucleului următorul argument de pornire: „dtc3181e=0x2c0,0”"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I do not have any information on the PCI SCSI adapter included with some "
"newer Artec scanners."
msgstr ""
"Nu am nicio informație despre adaptorul PCI SCSI inclus cu unele scanere "
"Artec mai noi."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/sane.d/artec.conf>"
msgstr "I</etc/sane.d/artec.conf>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The backend configuration file (see also description of B<SANE_CONFIG_DIR> "
"below)."
msgstr ""
"Fișierul de configurare al controlorului (a se vedea, de asemenea, "
"descrierea B<SANE_CONFIG_DIR> de mai jos)."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-artec.a>"
msgstr "I</usr/lib/sane/libsane-artec.a>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Biblioteca statică care implementează acest controlor."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-artec.so>"
msgstr "I</usr/lib/sane/libsane-artec.so>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Biblioteca partajată care implementează acest controlor (prezentă pe "
"sistemele care acceptă încărcare dinamică)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MEDIU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_CONFIG_DIR>"
msgstr "B<SANE_CONFIG_DIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This environment variable specifies the list of directories that may contain "
"the configuration file.  On *NIX systems, the directories are separated by a "
"colon (`:'), under OS/2, they are separated by a semi-colon (`;').  If this "
"variable is not set, the configuration file is searched in two default "
"directories: first, the current working directory (\".\") and then in I</etc/"
"sane.d>.  If the value of the environment variable ends with the directory "
"separator character, then the default directories are searched after the "
"explicitly specified directories.  For example, setting B<SANE_CONFIG_DIR> "
"to \"/tmp/config:\" would result in directories I<tmp/config>, I<.>, and I</"
"etc/sane.d> being searched (in this order)."
msgstr ""
"Această variabilă de mediu specifică lista de directoare care pot conține "
"fișierul de configurare. Pe sistemele *NIX, directoarele sunt separate prin "
"două puncte („:”), în cazul sistemelor OS/2, ele sunt separate prin punct și "
"virgulă („;”). Dacă această variabilă nu este definită, fișierul de "
"configurare este căutat în două directoare implicite: mai întâi, în "
"directorul de lucru curent („.”) și apoi în I</etc/sane.d>. Dacă valoarea "
"variabilei de mediu se termină cu caracterul separator de directoare, atunci "
"directoarele implicite sunt căutate după directoarele specificate explicit. "
"De exemplu, dacă se definește B<SANE_CONFIG_DIR> la „/tmp/config:”, se vor "
"căuta (în această ordine) directoarele I<tmp/config>, I<.> și I</etc/sane.d>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_ARTEC>"
msgstr "B<SANE_DEBUG_ARTEC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend. E.g., a value of 128 "
"requests all debug output to be printed. Smaller levels reduce verbosity: "
"B<SANE_DEBUG_ARTEC> values:"
msgstr ""
"Dacă biblioteca a fost compilată cu suportul de depanare activat, această "
"variabilă de mediu controlează nivelul de depanare pentru acest controlor. "
"De exemplu, o valoare de 128 solicită imprimarea tuturor datelor de "
"depanare. Nivelurile mai mici reduc nivelul de detalii; valorile "
"B<SANE_DEBUG_ARTEC>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f(CRNumber  Remark\n"
" 0       print important errors\n"
" 1       print errors\n"
" 2       print sense\n"
" 3       print warnings\n"
" 4       print scanner-inquiry\n"
" 5       print information\n"
" 6       print less important information\n"
" 7       print major called procedures\n"
" 8       print all called procedures\n"
" 9       print procedure info/data messages\n"
" 10      print called sane-init-routines\n"
" 11      print called sane-procedures\n"
" 12      print sane infos\n"
" 13      print sane option-control messages\n"
" 50      print verbose data/debug messages\n"
" == 100  print software RGB calibration data\n"
" == 101  print raw data from scanner to artec.data.raw file\n"
" == 128  print out all messages\\fR\n"
msgstr ""
"\\f(CRNumăr   Observație\n"
" 0       imprimă erori importante\n"
" 1       imprimă erori\n"
" 2       imprimă sensul\n"
" 3       imprimă avertismente\n"
" 4       imprimă scaner-intrebare\n"
" 5       imprimă informații\n"
" 6       imprimă informații mai puțin importante\n"
" 7       imprimă procedurile majore apelate\n"
" 8       imprimă toate procedurile apelate\n"
" 9       imprimă mesajele de informații/date ale procedurii\n"
" 10      imprimă rutinele sane-init apelate\n"
" 11      imprimă procedurile sane apelate\n"
" 12      imprimă informații sane\n"
" 13      imprimă mesaje de control al opțiunilor sane\n"
" 50      imprimă mesaje detaliate de date/depanare\n"
" == 100  imprimă datele de calibrare RGB ale software-ului\n"
" == 101  imprimă datele brute de la scanner în fișierul artec.data.raw\n"
" == 128  imprimă toate mesajele\\fR\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Example: export SANE_DEBUG_ARTEC=13"
msgstr "Exemplu: export SANE_DEBUG_ARTEC=13"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Known bugs in this release: A6000C+ users with firmware v1.92 or earlier "
"have problems with the backend, the cause has not been determined.  "
"Sometimes the backend is not particularly robust, you can possibly lock up "
"the SCSI bus (and/or machine) by not having patience enough when scanning.  "
"3-channel gamma correction is not implemented and single-channel gamma "
"correction is not totally working on models other than the AT3."
msgstr ""
"Erori cunoscute în această versiune: utilizatorii A6000C+ cu firmware v1.92 "
"sau anterior au probleme cu controlorul, cauza nu a fost determinată. "
"Uneori, controlorul nu este deosebit de robust, este posibil să blocați "
"magistrala SCSI (și/sau mașina) dacă nu aveți suficientă răbdare la scanare. "
"Corecția gamma pe 3 canale nu este implementată, iar corecția gamma pe un "
"singur canal nu funcționează în totalitate pe alte modele decât AT3."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<sane>(7)B<,> B<sane-scsi>(5)"
msgstr "B<sane>(7)B<,> B<sane-scsi>(5)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Chris Pinkham I<E<lt>cpinkham@corp.infi.netE<gt>>"
msgstr "Chris Pinkham I<E<lt>cpinkham@corp.infi.netE<gt>>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-artec.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-artec.a>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-artec.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-artec.so>"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-artec.a>"
msgstr "I</usr/lib64/sane/libsane-artec.a>"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-artec.so>"
msgstr "I</usr/lib64/sane/libsane-artec.so>"
