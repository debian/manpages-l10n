# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 17:55+0100\n"
"PO-Revision-Date: 2024-09-09 13:21+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "BUILDINFO"
msgstr "BUILDINFO"

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-11-27"
msgstr "27 noiembrie 2024"

#. type: TH
#: archlinux fedora-41 fedora-rawhide
#, no-wrap
msgid "Pacman 7\\&.0\\&.0"
msgstr "Pacman 7\\&.0\\&.0"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Pacman Manual"
msgstr "Manualul Pacman"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "BUILDINFO - Makepkg package build information file"
msgstr ""
"BUILDINFO - fișier cu informații despre construcția pachetului cu «makepkg»"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"This manual page describes the format of a BUILDINFO file found in the root "
"of a package created by makepkg\\&. The file contains a description of the "
"package\\(cqs build environment\\&. The information is formatted in key-"
"value pairs separated by a I<=>, one value per line\\&. Arrays are "
"represented multiple keys with the same value\\&."
msgstr ""
"Această pagină de manual descrie formatul unui fișier „BUILDINFO” care se "
"găsește în rădăcina unui pachet creat de «makepkg»\\&. Fișierul conține o "
"descriere a mediului de construire a pachetului\\&. Informațiile sunt "
"formatate în perechi cheie-valoare separate de un I<=>, câte o valoare pe "
"linie\\&. Matricele sunt reprezentate de chei multiple cu aceeași valoare\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide
msgid ""
"This is a description of the contents of version I<2> of the BUILDINFO file "
"format\\&."
msgstr ""
"Aceasta este o descriere a conținutului versiunii I<2> a formatului de "
"fișier „BUILDINFO”\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<format>"
msgstr "B<format>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Denotes the file format version, represented by a plain positive integer\\&."
msgstr ""
"Indică versiunea formatului de fișier, reprezentată printr-un număr întreg "
"pozitiv simplu\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<pkgname>"
msgstr "B<pkgname>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The name of the package\\&."
msgstr "Numele pachetului\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<pkgbase>"
msgstr "B<pkgbase>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"The base name of a package, usually the same as the pkgname except for split "
"packages\\&."
msgstr ""
"Numele de bază al unui pachet, de obicei același cu numele pachetului, cu "
"excepția pachetelor divizate&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<pkgver>"
msgstr "B<pkgver>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The version of the package including pkgrel and epoch\\&."
msgstr "Versiunea pachetului care include B<pkgrel> și B<epoch>\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<pkgarch>"
msgstr "B<pkgarch>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The architecture of the package\\&."
msgstr "Arhitectura pachetului\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<pkgbuild_sha256sum>"
msgstr "B<pkgbuild_sha256sum>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"The sha256sum in hex format of the PKGBUILD used to build the package\\&."
msgstr ""
"Suma sha256sum în format hexazecimal a „PKGBUILD” utilizat pentru a crea "
"pachetul\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<packager>"
msgstr "B<packager>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The details of the packager that built the package\\&."
msgstr "Detalii despre împachetatorul care a construit pachetul\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<builddate>"
msgstr "B<builddate>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The build date of the package in epoch\\&."
msgstr "Data de construire a pachetului în raport cu epoca\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<builddir>"
msgstr "B<builddir>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The directory where the package was built\\&."
msgstr "Directorul în care a fost construit pachetul\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<startdir>"
msgstr "B<startdir>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The directory from which makepkg was executed\\&."
msgstr "Directorul din care a fost executat «makepkg»\\&."

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide
msgid "B<buildtool>"
msgstr "B<buildtool>"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide
msgid ""
"The name of the tool ecosystem used to set up the build environment\\&. Used "
"for defining a spec for reproducible builds, e\\&.g\\&. the source of the "
"B<makepkg.conf>(5)  used\\&."
msgstr ""
"Numele ecosistemului de instrumente utilizat pentru a configura mediul de "
"construcție. Utilizat pentru definirea unei specificații pentru compilări "
"reproductibile, de exemplu, sursa B<makepkg.conf>(5) utilizată\\&."

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide
msgid "B<buildtoolver>"
msgstr "B<buildtoolver>"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide
msgid ""
"The full version of the I<buildtool>, for example: \"$pkgver-$pkgrel-"
"$pkgarch\"\\&."
msgstr ""
"Versiunea completă a I<buildtool>, de exemplu: „$pkgver-$pkgrel-$pkgarch”\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<buildenv (array)>"
msgstr "B<buildenv (serie de variabile de mediu)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The build environment specified in makepkg\\&.conf\\&."
msgstr "Mediul de construcție specificat în makepkg\\&.conf\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<options (array)>"
msgstr "B<options (serie de opțiuni)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The options set specified when building the package\\&."
msgstr "Setul de opțiuni specificat la crearea pachetului\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<installed (array)>"
msgstr "B<installed (serie de pachete)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"The installed packages at build time including the version information of "
"the package\\&. Formatted as \"$pkgname-$pkgver-$pkgrel-$pkgarch\"\\&."
msgstr ""
"Pachetele instalate în momentul compilării, inclusiv informațiile despre "
"versiunea pachetului\\&. Formatate ca „$pkgname-$pkgver-$pkgrel-$pkgarch”\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<makepkg>(8), B<pacman>(8), B<makepkg.conf>(5)"
msgstr "B<makepkg>(8), B<pacman>(8), B<makepkg.conf>(5)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"See the pacman website at https://archlinux\\&.org/pacman/ for current "
"information on pacman and its related tools\\&."
msgstr ""
"Consultați situl web pacman la https://archlinux\\&.org/pacman/ pentru "
"informații actuale despre «pacman» și instrumentele sale conexe\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide
msgid ""
"Bugs? You must be kidding; there are no bugs in this software\\&. But if we "
"happen to be wrong, please report them to the issue tracker at https://"
"gitlab\\&.archlinux\\&.org/pacman/pacman/-/issues with specific information "
"such as your command-line, the nature of the bug, and even the package "
"database if it helps\\&."
msgstr ""
"Hibe? Probabil că glumiți; nu există nicio hibă în acest software&. Dar dacă "
"se întâmplă să fie ceva greșit, depuneți o cerere de rezolvare a problemei "
"cu cât mai multe detalii posibile la: https://gitlab\\&.archlinux\\&.org/"
"pacman/pacman/-/issues\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

# R-GB, scrie:
# cred că la fel de bine, mesajul ar putea fi
# tradus ca:
# „Menținătorii actuali:”
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Current maintainers:"
msgstr "Responsabilii actuali:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Allan McRae E<lt>allan@archlinux\\&.orgE<gt>"
msgstr "Allan McRae E<lt>allan@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Andrew Gregory E<lt>andrew\\&.gregory\\&.8@gmail\\&.comE<gt>"
msgstr "Andrew Gregory E<lt>andrew\\&.gregory\\&.8@gmail\\&.comE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Morgan Adamiec E<lt>morganamilo@archlinux\\&.orgE<gt>"
msgstr "Morgan Adamiec E<lt>morganamilo@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Past major contributors:"
msgstr "Contribuitori importanți din trecut:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Judd Vinet E<lt>jvinet@zeroflux\\&.orgE<gt>"
msgstr "Judd Vinet E<lt>jvinet@zeroflux\\&.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Aurelien Foret E<lt>aurelien@archlinux\\&.orgE<gt>"
msgstr "Aurelien Foret E<lt>aurelien@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Aaron Griffin E<lt>aaron@archlinux\\&.orgE<gt>"
msgstr "Aaron Griffin E<lt>aaron@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Dan McGee E<lt>dan@archlinux\\&.orgE<gt>"
msgstr "Dan McGee E<lt>dan@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Xavier Chantry E<lt>shiningxc@gmail\\&.comE<gt>"
msgstr "Xavier Chantry E<lt>shiningxc@gmail\\&.comE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Nagy Gabor E<lt>ngaba@bibl\\&.u-szeged\\&.huE<gt>"
msgstr "Nagy Gabor E<lt>ngaba@bibl\\&.u-szeged\\&.huE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Dave Reisner E<lt>dreisner@archlinux\\&.orgE<gt>"
msgstr "Dave Reisner E<lt>dreisner@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Eli Schwartz E<lt>eschwartz@archlinux\\&.orgE<gt>"
msgstr "Eli Schwartz E<lt>eschwartz@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"For additional contributors, use git shortlog -s on the pacman\\&.git "
"repository\\&."
msgstr ""
"Pentru contribuitori suplimentari, folosiți «git shortlog -s» în depozitul "
"\\&.git pacman\\&."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-11-21"
msgstr "21 noiembrie 2022"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Pacman 6\\&.0\\&.2"
msgstr "Pacman 6\\&.0\\&.2"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This is a description of the contents of version I<1> of the BUILDINFO file "
"format\\&."
msgstr ""
"Aceasta este o descriere a conținutului versiunii I<1> a formatului de "
"fișier „BUILDINFO”\\&."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Bugs? You must be kidding; there are no bugs in this software\\&. But if we "
"happen to be wrong, submit a bug report with as much detail as possible at "
"the Arch Linux Bug Tracker in the Pacman section\\&."
msgstr ""
"Hibe (erori)? Glumiți; nu există erori în acest software\\&. Dar dacă se "
"întâmplă să apară greșeli, trimiteți un raport de eroare cu cât mai multe "
"detalii posibil la sistemul de urmărire a erorilor al Arch Linux (Arch Linux "
"Bug Tracker) din secțiunea Pacman\\&."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2024-10-29"
msgstr "29 octombrie 2024"

#. type: TH
#: fedora-41 fedora-rawhide
#, no-wrap
msgid "2024-07-18"
msgstr "18 iulie 2024"
