# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-09-06 18:27+0200\n"
"PO-Revision-Date: 2024-01-27 23:37+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sane-ma1509"
msgstr "sane-ma1509"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "13 Jul 2008"
msgstr "13 iulie 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE: „Scanner Access Now Easy”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sane-ma1509 - SANE backend for Mustek BearPaw 1200F USB scanner"
msgstr "sane-ma1509 - controlor SANE pentru scanerul USB Mustek BearPaw 1200F"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<sane-ma1509> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to the Mustek BearPaw 1200F USB flatbed "
"scanner. This scanner is based on the MA-1509 chipset. Other scanners that "
"use this chip (if they exist) may also work."
msgstr ""
"Biblioteca B<sane-ma1509> implementează un controlor SANE (Scanner Access "
"Now Easy) care oferă acces la scanerul plat USB Mustek BearPaw 1200F. Acest "
"scaner se bazează pe chipset-ul MA-1509. Este posibil să funcționeze și alte "
"scanere care utilizează acest cip (dacă există)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This backend is ALPHA software.  Be careful and remove the power plug "
"immediately if your hear unusual noises."
msgstr ""
"Acest controlor este un software ALPHA. Fiți atenți și scoateți imediat "
"ștecherul din priză dacă auziți zgomote neobișnuite."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"More details can be found on the B<sane-ma1509> backend homepage I<http://"
"www.meier-geinitz.de/sane/ma1509-backend/>."
msgstr ""
"Mai multe detalii pot fi găsite pe pagina principală a controlorului B<sane-"
"ma1509> I<http://www.meier-geinitz.de/sane/ma1509-backend/>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Other Mustek USB scanners are supported by the B<sane-mustek_usb>(5), B<sane-"
"gt68xx>(5)  and B<sane-plustek>(5)  backends."
msgstr ""
"Alte scanere Mustek USB sunt acceptate de controlorii B<sane-mustek_usb>(5), "
"B<sane-gt68xx>(5) și B<sane-plustek>(5)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This backend can only work with scanners that are already detected by the "
"operating system. See B<sane-usb>(5)  for details."
msgstr ""
"Acest controlor poate funcționa numai cu scanerele care sunt deja detectate "
"de sistemul de operare. Pentru detalii, consultați B<sane-usb>(5)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you own a scanner other than the Mustek BearPaw 1200F that works with "
"this backend, please let me know this by sending the scanner's exact model "
"name and the USB vendor and device ids (e.g. from I</proc/bus/usb/devices> "
"or syslog) to me."
msgstr ""
"Dacă dețineți un scaner, altul decât Mustek BearPaw 1200F, care funcționează "
"cu acest controlor, vă rog să-mi comunicați acest lucru trimițându-mi numele "
"exact al modelului de scaner și ID-urile fabricantului și dispozitivului USB "
"(de exemplu, din I</proc/bus/usb/devices> sau syslog)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DEVICE NAMES"
msgstr "NUME DE DISPOZITIVE"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This backend expects device names of the form:"
msgstr "Acest controlor așteaptă nume de dispozitive de forma:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<special>"
msgstr "I<special>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Where I<special> is a path-name for the special device that corresponds to a "
"USB scanner.  With Linux, such a device name could be I</dev/usb/scanner0> "
"or I<libusb:001:002>, for example."
msgstr ""
"Unde I<special> este un nume de rută pentru dispozitivul special care "
"corespunde unui scaner USB. În cazul Linux, un astfel de nume de dispozitiv "
"poate fi I</dev/usb/scanner0> sau I<libusb:001:002>, de exemplu."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The contents of the I<ma1509.conf> file is a list of options and device "
"names that correspond to Mustek BearPaw 1200F scanners.  Empty lines and "
"lines starting with a hash mark (#) are ignored."
msgstr ""
"Conținutul fișierului I<ma1509.conf> este o listă de opțiuni și nume de "
"dispozitive care corespund scanerelor Mustek BearPaw 1200F. Liniile goale și "
"liniile care încep cu un simbol hash (#) sunt ignorate."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Instead of using the device name, the scanner can be autodetected by B<usb "
"vendor_id product_id> statements which are already included into "
"I<ma1509.conf>.  This is only supported with Linux 2.4.8 and higher and all "
"systems that support libsub. \"vendor_id\" and \"product_id\" are "
"hexadecimal numbers that identify the scanner. If this doesn't work, a "
"device name must be placed in I<ma1509.conf> as described above."
msgstr ""
"În loc să se utilizeze numele dispozitivului, scanerul poate fi detectat "
"automat prin declarațiile B<usb vendor_id product_id> care sunt deja incluse "
"în I<ma1509.conf>. Acest lucru este valabil numai pentru Linux 2.4.8 și "
"versiunile ulterioare și pentru toate sistemele care acceptă „libsub”. "
"„vendor_id” și „product_id” sunt numere hexazecimale care identifică "
"scanerul. Dacă acest lucru nu funcționează, trebuie să se plaseze un nume de "
"dispozitiv în I<ma1509.conf>, așa cum este descris mai sus."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To set the time the lamp needs for warm-up, use B<option> B<warmup-time> in "
"I<ma1509.conf>.  The time is given in seconds after the option. The default "
"is 30 seconds."
msgstr ""
"Pentru a stabili timpul de care lampa are nevoie pentru încălzire, utilizați "
"B<opțiunea> B<warmup-time> în I<ma1509.conf>. Timpul este indicat în secunde "
"după opțiune. Valoarea implicită este de 30 de secunde."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/sane.d/ma1509.conf>"
msgstr "I</etc/sane.d/ma1509.conf>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The backend configuration file (see also description of B<SANE_CONFIG_DIR> "
"below)."
msgstr ""
"Fișierul de configurare al controlorului (a se vedea, de asemenea, "
"descrierea B<SANE_CONFIG_DIR> de mai jos)."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-ma1509.a>"
msgstr "I</usr/lib/sane/libsane-ma1509.a>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Biblioteca statică care implementează acest controlor."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-ma1509.so>"
msgstr "I</usr/lib/sane/libsane-ma1509.so>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Biblioteca partajată care implementează acest controlor (prezentă pe "
"sistemele care acceptă încărcare dinamică)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MEDIU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_CONFIG_DIR>"
msgstr "B<SANE_CONFIG_DIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This environment variable specifies the list of directories that may contain "
"the configuration file.  On *NIX systems, the directories are separated by a "
"colon (`:'), under OS/2, they are separated by a semi-colon (`;').  If this "
"variable is not set, the configuration file is searched in two default "
"directories: first, the current working directory (\".\") and then in I</etc/"
"sane.d>.  If the value of the environment variable ends with the directory "
"separator character, then the default directories are searched after the "
"explicitly specified directories.  For example, setting B<SANE_CONFIG_DIR> "
"to \"/tmp/config:\" would result in directories I<tmp/config>, I<.>, and I</"
"etc/sane.d> being searched (in this order)."
msgstr ""
"Această variabilă de mediu specifică lista de directoare care pot conține "
"fișierul de configurare. Pe sistemele *NIX, directoarele sunt separate prin "
"două puncte („:”), în cazul sistemelor OS/2, ele sunt separate prin punct și "
"virgulă („;”). Dacă această variabilă nu este definită, fișierul de "
"configurare este căutat în două directoare implicite: mai întâi, în "
"directorul de lucru curent („.”) și apoi în I</etc/sane.d>. Dacă valoarea "
"variabilei de mediu se termină cu caracterul separator de directoare, atunci "
"directoarele implicite sunt căutate după directoarele specificate explicit. "
"De exemplu, dacă se definește B<SANE_CONFIG_DIR> la „/tmp/config:”, se vor "
"căuta (în această ordine) directoarele I<tmp/config>, I<.> și I</etc/sane.d>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_MA1509>"
msgstr "B<SANE_DEBUG_MA1509>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend.  Higher debug levels "
"increase the verbosity of the output."
msgstr ""
"Dacă biblioteca a fost compilată cu suportul de depanare activat, această "
"variabilă de mediu controlează nivelul de depanare pentru acest controlor. "
"Nivelurile mai mari de depanare cresc cantitatea de detalii informative a "
"ieșirii."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<sane>(7), B<sane-usb>(5), B<sane-gt68xx>(5), B<sane-plustek>(5), B<sane-"
"mustek_usb>(5), B<sane-mustek>(5), B<sane-mustek_pp>(5),"
msgstr ""
"B<sane>(7), B<sane-usb>(5), B<sane-gt68xx>(5), B<sane-plustek>(5), B<sane-"
"mustek_usb>(5), B<sane-mustek>(5), B<sane-mustek_pp>(5),"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<http://www.meier-geinitz.de/sane/ma1509-backend/>"
msgstr "I<http://www.meier-geinitz.de/sane/ma1509-backend/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Henning Meier-Geinitz E<lt>I<henning@meier-geinitz.de>E<gt>"
msgstr "Henning Meier-Geinitz E<lt>I<henning@meier-geinitz.de>E<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Resolutions higher than 600 dpi don't work"
msgstr "Rezoluțiile mai mari de 600 dpi nu funcționează"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Transparency adapter and automatic document feeder is not supported yet"
msgstr ""
"Adaptorul de transparență și alimentatorul automat de documente nu sunt încă "
"acceptate"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "No support for \"high-speed\" mode (jpeg)"
msgstr "Nu există suport pentru modul „de mare viteză” (jpeg)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"More detailed bug information is available at the MA-1509 backend homepage "
"I<http://www.meier-geinitz.de/sane/ma1509-backend/>."
msgstr ""
"Informații mai detaliate despre erori sunt disponibile pe pagina principală "
"a controlorului MA-1509 I<http://www.meier-geinitz.de/sane/ma1509-backend/>."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-ma1509.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-ma1509.a>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-ma1509.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-ma1509.so>"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-ma1509.a>"
msgstr "I</usr/lib64/sane/libsane-ma1509.a>"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-ma1509.so>"
msgstr "I</usr/lib64/sane/libsane-ma1509.so>"
