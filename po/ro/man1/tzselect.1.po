# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-06-27 19:59+0200\n"
"PO-Revision-Date: 2023-05-20 11:05+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "TZSELECT"
msgstr "TZSELECT"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "12 June 1998"
msgstr "12 iunie 1998"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Debian"
msgstr "Debian"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Debian Timezone Configuration"
msgstr "Configurarea fusului orar în Debian"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "tzselect - view timezones"
msgstr "tzselect - vizualizează fusurile orare"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<tzselect>"
msgstr "B<tzselect>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This manual page explains how you can use the B<tzselect> utility to view "
"the installed timezone. It comes handy when you want to know what time it is "
"in other countries, or if you just wonder what timezones exist."
msgstr ""
"Această pagină de manual explică modul în care puteți utiliza instrumentul "
"B<tzselect> pentru a vizualiza fusul orar instalat. Este util atunci când "
"doriți să știți cât este ora în alte țări sau dacă pur și simplu vă "
"întrebați ce fuse orare există."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<tzselect> is called without any parameters from the shell. It shows a list "
"of about one dozen geographic areas one can roughly recognize as continents. "
"After choosing a geographic area by number, a list of countries and cities "
"in this area will be shown."
msgstr ""
"B<tzselect> este apelat fără parametrii din shell. Acesta afișează o listă "
"de aproximativ o duzină de zone geografice care pot fi recunoscute drept "
"continente. După ce se alege o zonă geografică prin număr, se afișează o "
"listă de țări și orașe din această zonă."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"You can press the B<Enter> key to reprint the list. To choose a timezone, "
"just press the number left to it.  If your input is invalid, the list will "
"be reprinted."
msgstr ""
"Puteți apăsa tasta B<Enter> pentru a reimprima lista. Pentru a alege un fus "
"orar, trebuie doar să apăsați numărul din stânga acestuia.  Dacă "
"introducerea dvs. nu este validă, lista va fi reimprimată."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "You may press B<Ctrl-C> to interrupt the script at any time."
msgstr ""
"Puteți să apăsați combinația de taste B<Ctrl-C> pentru a întrerupe scriptul "
"în orice moment."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Note that B<tzselect> will not actually change the timezone for you. Use "
"'dpkg-reconfigure tzdata' to achieve this."
msgstr ""
"Rețineți că B<tzselect> nu va schimba efectiv fusul orar pentru "
"dumneavoastră. Folosiți «dpkg-reconfigure tzdata» pentru a realiza acest "
"lucru."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "I</usr/share/zoneinfo/>"
msgstr "I</usr/share/zoneinfo/>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<hwclock>(8)"
msgstr "B<hwclock>(8)"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Copyright 1998 Marcus Brinkmann E<lt>brinkmd@debian.orgE<gt>"
msgstr "Drepturi de autor 1998 Marcus Brinkmann E<lt>brinkmd@debian.orgE<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Please see nroff source for legal notice."
msgstr "Consultați sursa nroff pentru avizul juridic."
