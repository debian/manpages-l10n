# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-12-06 18:10+0100\n"
"PO-Revision-Date: 2024-11-27 21:34+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_setcancelstate"
msgstr "pthread_setcancelstate"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"pthread_setcancelstate, pthread_setcanceltype - set cancelability state and "
"type"
msgstr ""
"pthread_setcancelstate, pthread_setcanceltype - stabilește starea și tipul "
"anulabilității"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr "Biblioteca de fire de execuție POSIX (I<libpthread>, I<-lpthread>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr "B<#include E<lt>pthread.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int pthread_setcancelstate(int >I<state>B<, int *>I<oldstate>B<);>\n"
"B<int pthread_setcanceltype(int >I<type>B<, int *>I<oldtype>B<);>\n"
msgstr ""
"B<int pthread_setcancelstate(int >I<state>B<, int *>I<oldstate>B<);>\n"
"B<int pthread_setcanceltype(int >I<type>B<, int *>I<oldtype>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_setcancelstate>()  sets the cancelability state of the calling "
"thread to the value given in I<state>.  The previous cancelability state of "
"the thread is returned in the buffer pointed to by I<oldstate>.  The "
"I<state> argument must have one of the following values:"
msgstr ""
"B<pthread_setcancelstate>() stabilește starea de anulabilitate a firului "
"apelant la valoarea indicată în I<state>. Starea anterioară de anulare a "
"firului este returnată în memoria tampon indicată de I<oldstate>. Argumentul "
"I<state> trebuie să aibă una dintre următoarele valori:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PTHREAD_CANCEL_ENABLE>"
msgstr "B<PTHREAD_CANCEL_ENABLE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The thread is cancelable.  This is the default cancelability state in all "
"new threads, including the initial thread.  The thread's cancelability type "
"determines when a cancelable thread will respond to a cancelation request."
msgstr ""
"Firul este anulabil. Aceasta este starea implicită de anulare în toate "
"firele noi, inclusiv în firul inițial. Tipul de anulabilitate al firului "
"determină momentul în care un fir anulabil va răspunde la o cerere de "
"anulare."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PTHREAD_CANCEL_DISABLE>"
msgstr "B<PTHREAD_CANCEL_DISABLE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The thread is not cancelable.  If a cancelation request is received, it is "
"blocked until cancelability is enabled."
msgstr ""
"Firul nu este anulabil. Dacă se primește o cerere de anulare, acesta este "
"blocat până la activarea posibilității de anulare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_setcanceltype>()  sets the cancelability type of the calling "
"thread to the value given in I<type>.  The previous cancelability type of "
"the thread is returned in the buffer pointed to by I<oldtype>.  The I<type> "
"argument must have one of the following values:"
msgstr ""
"B<pthread_setcanceltype>() stabilește tipul de anulabilitate al firului "
"apelant la valoarea indicată în I<type>. Tipul anterior de anulabilitate al "
"firului este returnat în memoria tampon indicată de I<oldtype>. Argumentul "
"I<type> trebuie să aibă una dintre următoarele valori:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PTHREAD_CANCEL_DEFERRED>"
msgstr "B<PTHREAD_CANCEL_DEFERRED>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A cancelation request is deferred until the thread next calls a function "
"that is a cancelation point (see B<pthreads>(7)).  This is the default "
"cancelability type in all new threads, including the initial thread."
msgstr ""
"O cerere de anulare este amânată până când firul următor apelează o funcție "
"care este un punct de anulare (a se vedea B<pthreads>(7)). Acesta este tipul "
"implicit de anulare în toate firele noi, inclusiv în firul inițial."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Even with deferred cancelation, a cancelation point in an asynchronous "
"signal handler may still be acted upon and the effect is as if it was an "
"asynchronous cancelation."
msgstr ""
"Chiar și în cazul anulării amânate, un punct de anulare dintr-un gestionar "
"de semnal asincron poate fi totuși luat în considerare, iar efectul este ca "
"și cum ar fi fost o anulare asincronă."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PTHREAD_CANCEL_ASYNCHRONOUS>"
msgstr "B<PTHREAD_CANCEL_ASYNCHRONOUS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The thread can be canceled at any time.  (Typically, it will be canceled "
"immediately upon receiving a cancelation request, but the system doesn't "
"guarantee this.)"
msgstr ""
"Firul poate fi anulat în orice moment; (în mod normal, acesta va fi anulat "
"imediat după primirea unei cereri de anulare, dar sistemul nu garantează "
"acest lucru)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The set-and-get operation performed by each of these functions is atomic "
"with respect to other threads in the process calling the same function."
msgstr ""
"Operația „set-and-get” efectuată de fiecare dintre aceste funcții este "
"atomică în ceea ce privește alte fire din proces care apelează la aceeași "
"funcție."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, these functions return 0; on error, they return a nonzero error "
"number."
msgstr ""
"În caz de succes, aceste funcții returnează 0; în caz de eroare, ele "
"returnează un număr de eroare diferit de zero."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The B<pthread_setcancelstate>()  can fail with the following error:"
msgstr "B<pthread_setcancelstate>() poate eșua cu următoarea eroare:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Invalid value for I<state>."
msgstr "Valoare nevalidă pentru I<state>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The B<pthread_setcanceltype>()  can fail with the following error:"
msgstr "B<pthread_setcanceltype>() poate eșua cu următoarea eroare:"

#. #-#-#-#-#  archlinux: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH VERSIONS
#.  Available since glibc 2.0
#. type: Plain text
#. #-#-#-#-#  debian-unstable: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-41: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Invalid value for I<type>."
msgstr "Valoare nevalidă pentru I<type>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pthread_setcancelstate>(),\n"
"B<pthread_setcanceltype>()"
msgstr ""
"B<pthread_setcancelstate>(),\n"
"B<pthread_setcanceltype>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Async-cancel safety"
msgstr "Async-cancel safety"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AC-Safe"
msgstr "AC-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.0 POSIX.1-2001."
msgstr "glibc 2.0 POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For details of what happens when a thread is canceled, see B<\\"
"%pthread_cancel>(3)."
msgstr ""
"Pentru detalii despre ce se întâmplă atunci când un fir este anulat, "
"consultați B<\\%pthread_cancel>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Briefly disabling cancelability is useful if a thread performs some critical "
"action that must not be interrupted by a cancelation request.  Beware of "
"disabling cancelability for long periods, or around operations that may "
"block for long periods, since that will render the thread unresponsive to "
"cancelation requests."
msgstr ""
"Dezactivarea pentru scurt timp a capacității de anulare este utilă în cazul "
"în care un fir de execuție efectuează o acțiune critică care nu trebuie "
"întreruptă de o cerere de anulare. Atenție la dezactivarea capacității de "
"anulare pentru perioade lungi de timp sau în jurul unor operații care se pot "
"bloca pentru perioade lungi de timp, deoarece acest lucru va face ca firul "
"să nu răspundă la cererile de anulare."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Asynchronous cancelability"
msgstr "Anulabilitate asincronă"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Setting the cancelability type to B<PTHREAD_CANCEL_ASYNCHRONOUS> is rarely "
"useful.  Since the thread could be canceled at I<any> time, it cannot safely "
"reserve resources (e.g., allocating memory with B<malloc>(3)), acquire "
"mutexes, semaphores, or locks, and so on.  Reserving resources is unsafe "
"because the application has no way of knowing what the state of these "
"resources is when the thread is canceled; that is, did cancelation occur "
"before the resources were reserved, while they were reserved, or after they "
"were released? Furthermore, some internal data structures (e.g., the linked "
"list of free blocks managed by the B<malloc>(3)  family of functions) may be "
"left in an inconsistent state if cancelation occurs in the middle of the "
"function call.  Consequently, clean-up handlers cease to be useful."
msgstr ""
"Stabilirea tipului de anulare la B<PTHREAD_CANCEL_ASYNCHRONOUS> este rareori "
"utilă. Deoarece firul poate fi anulat în orice moment, acesta nu poate "
"rezerva resurse în siguranță (de exemplu, alocarea de memorie cu "
"B<malloc>(3)), nu poate achiziționa mutex-uri, semafoare sau blocaje și așa "
"mai departe. Rezervarea resurselor este nesigură deoarece aplicația nu are "
"cum să știe care este starea acestor resurse atunci când firul este anulat; "
"adică, anularea a avut loc înainte ca resursele să fie rezervate, în timp ce "
"erau rezervate sau după ce au fost eliberate? În plus, unele structuri "
"interne de date (de exemplu, lista legată a blocurilor libere gestionată de "
"familia de funcții B<malloc>(3)) poate fi lăsată într-o stare inconsistentă "
"dacă anularea are loc în mijlocul apelului funcției. În consecință, "
"gestionarii de curățare încetează să mai fie utili."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Functions that can be safely asynchronously canceled are called I<async-"
"cancel-safe functions>.  POSIX.1-2001 and POSIX.1-2008 require only that "
"B<pthread_cancel>(3), B<pthread_setcancelstate>(), and "
"B<pthread_setcanceltype>()  be async-cancel-safe.  In general, other library "
"functions can't be safely called from an asynchronously cancelable thread."
msgstr ""
"Funcțiile care pot fi anulate în mod sigur în mod asincron sunt denumite "
"I<funcții asincrone de anulare sigură (async-cancel-safe)>. POSIX.1-2001 și "
"POSIX.1-2008 cer doar ca B<pthread_cancel>(3), B<pthread_setcancelstate>() "
"și B<pthread_setcanceltype>() să fie async-cancel-safe. În general, alte "
"funcții de bibliotecă nu pot fi apelate în siguranță de la un fir de "
"execuție cu anulare asincronă."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"One of the few circumstances in which asynchronous cancelability is useful "
"is for cancelation of a thread that is in a pure compute-bound loop."
msgstr ""
"Una dintre puținele circumstanțe în care anularea asincronă este utilă este "
"anularea unui fir care se află într-o buclă legată pur de calcul."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Portability notes"
msgstr "Note privind portabilitatea"

#.  It looks like at least Solaris, FreeBSD and Tru64 support this.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Linux threading implementations permit the I<oldstate> argument of "
"B<pthread_setcancelstate>()  to be NULL, in which case the information about "
"the previous cancelability state is not returned to the caller.  Many other "
"implementations also permit a NULL I<oldstat> argument, but POSIX.1 does not "
"specify this point, so portable applications should always specify a non-"
"NULL value in I<oldstate>.  A precisely analogous set of statements applies "
"for the I<oldtype> argument of B<pthread_setcanceltype>()."
msgstr ""
"Implementările threading Linux permit ca argumentul I<oldstate> din "
"B<pthread_setcancelstate>() să fie NULL, caz în care informațiile despre "
"starea anterioară de anulare nu sunt returnate apelantului. Multe alte "
"implementări permit, de asemenea, un argument I<oldstat> NULL, dar POSIX.1 "
"nu specifică acest punct, astfel încât aplicațiile portabile ar trebui să "
"specifice întotdeauna o valoare non-NULL în I<oldstate>. Un set exact analog "
"de declarații se aplică pentru argumentul I<oldtype> al "
"B<pthread_setcanceltype>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "See B<pthread_cancel>(3)."
msgstr "Consultați B<pthread_cancel>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pthread_cancel>(3), B<pthread_cleanup_push>(3), B<pthread_testcancel>(3), "
"B<pthreads>(7)"
msgstr ""
"B<pthread_cancel>(3), B<pthread_cleanup_push>(3), B<pthread_testcancel>(3), "
"B<pthreads>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 decembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
msgid ""
"For details of what happens when a thread is canceled, see "
"B<pthread_cancel>(3)."
msgstr ""
"Pentru detalii despre ce se întâmplă atunci când un fir este anulat, "
"consultați B<pthread_cancel>(3)."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
