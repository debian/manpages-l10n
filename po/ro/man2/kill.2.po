# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:03+0100\n"
"PO-Revision-Date: 2024-01-25 00:06+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "kill"
msgstr "kill"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "kill - send signal to a process"
msgstr "kill - trimite un semnal către un proces"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int kill(pid_t >I<pid>B<, int >I<sig>B<);>\n"
msgstr "B<int kill(pid_t >I<pid>B<, int >I<sig>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<kill>():"
msgstr "B<kill>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE\n"
msgstr "    _POSIX_C_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<kill>()  system call can be used to send any signal to any process "
"group or process."
msgstr ""
"Apelul de sistem B<kill>() poate fi utilizat pentru a trimite orice semnal "
"către orice grup de procese sau proces."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<pid> is positive, then signal I<sig> is sent to the process with the ID "
"specified by I<pid>."
msgstr ""
"Dacă I<pid> este pozitiv, atunci semnalul I<sig> este trimis către procesul "
"cu ID-ul specificat de I<pid>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<pid> equals 0, then I<sig> is sent to every process in the process "
"group of the calling process."
msgstr ""
"Dacă I<pid> este egal cu 0, atunci I<sig> este trimis la fiecare proces din "
"grupul de procese al procesului apelant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<pid> equals -1, then I<sig> is sent to every process for which the "
"calling process has permission to send signals, except for process 1 "
"(I<init>), but see below."
msgstr ""
"Dacă I<pid> este egal cu -1, atunci I<sig> este trimis la fiecare proces "
"pentru care procesul apelant are permisiunea de a trimite semnale, cu "
"excepția procesului 1 (I<init>), dar a se vedea mai jos."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<pid> is less than -1, then I<sig> is sent to every process in the "
"process group whose ID is I<-pid>."
msgstr ""
"Dacă I<pid> este mai mic decât -1, atunci I<sig> este trimis fiecărui proces "
"din grupul de procese al cărui ID este I<-pid>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<sig> is 0, then no signal is sent, but existence and permission checks "
"are still performed; this can be used to check for the existence of a "
"process ID or process group ID that the caller is permitted to signal."
msgstr ""
"Dacă I<sig> este 0, atunci nu se trimite niciun semnal, dar se efectuează în "
"continuare verificări de existență și de permisiune; acest lucru poate fi "
"utilizat pentru a verifica existența unui ID de proces sau a unui ID de grup "
"de procese pe care apelantul este autorizat să le semnalizeze."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For a process to have permission to send a signal, it must either be "
"privileged (under Linux: have the B<CAP_KILL> capability in the user "
"namespace of the target process), or the real or effective user ID of the "
"sending process must equal the real or saved set-user-ID of the target "
"process.  In the case of B<SIGCONT>, it suffices when the sending and "
"receiving processes belong to the same session.  (Historically, the rules "
"were different; see NOTES.)"
msgstr ""
"Pentru ca un proces să aibă permisiunea de a trimite un semnal, acesta "
"trebuie fie să fie privilegiat (în Linux: să aibă capacitatea B<CAP_KILL> în "
"spațiul de nume al utilizatorului procesului țintă), fie ID-ul de utilizator "
"real sau efectiv al procesului de trimitere trebuie să fie egal cu ID-ul de "
"utilizator real sau salvat al procesului țintă.  În cazul lui B<SIGCONT>, "
"este suficient ca procesele de trimitere și de primire să aparțină aceleiași "
"sesiuni; (din punct de vedere istoric, regulile erau diferite; a se vedea "
"NOTE)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success (at least one signal was sent), zero is returned.  On error, -1 "
"is returned, and I<errno> is set to indicate the error."
msgstr ""
"În caz de succes (cel puțin un semnal a fost trimis), se returnează zero. În "
"caz de eroare, se returnează -1, iar I<errno> este configurată pentru a "
"indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An invalid signal was specified."
msgstr "A fost specificat un semnal nevalid."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The calling process does not have permission to send the signal to any of "
"the target processes."
msgstr ""
"Procesul apelant nu are permisiunea de a trimite semnalul către niciunul "
"dintre procesele țintă."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The target process or process group does not exist.  Note that an existing "
"process might be a zombie, a process that has terminated execution, but has "
"not yet been B<wait>(2)ed for."
msgstr ""
"Procesul sau grupul de procese țintă nu există. Rețineți că un proces "
"existent ar putea fi un proces zombi, un proces care și-a încheiat execuția, "
"dar care nu a fost încă B<wait>(2) (așteptat)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux notes"
msgstr "Note Linux"

#.  In the 0.* kernels things chopped and changed quite
#.  a bit - MTK, 24 Jul 02
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Across different kernel versions, Linux has enforced different rules for the "
"permissions required for an unprivileged process to send a signal to another "
"process.  In Linux 1.0 to 1.2.2, a signal could be sent if the effective "
"user ID of the sender matched effective user ID of the target, or the real "
"user ID of the sender matched the real user ID of the target.  From Linux "
"1.2.3 until 1.3.77, a signal could be sent if the effective user ID of the "
"sender matched either the real or effective user ID of the target.  The "
"current rules, which conform to POSIX.1, were adopted in Linux 1.3.78."
msgstr ""
"În diferite versiuni de nucleu, Linux a impus reguli diferite pentru "
"permisiunile necesare pentru ca un proces neprivilegiat să trimită un semnal "
"unui alt proces. În Linux 1.0 până la 1.2.2, un semnal putea fi trimis dacă "
"ID-ul efectiv de utilizator al expeditorului se potrivea cu ID-ul efectiv de "
"utilizator al țintei sau dacă ID-ul real de utilizator al expeditorului se "
"potrivea cu ID-ul real de utilizator al țintei. De la Linux 1.2.3 până la "
"1.3.77, se putea trimite un semnal dacă ID-ul efectiv de utilizator al "
"expeditorului corespundea fie cu ID-ul real, fie cu ID-ul efectiv de "
"utilizator al țintei. Regulile actuale, care sunt conforme cu POSIX.1, au "
"fost adoptate în Linux 1.3.78."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The only signals that can be sent to process ID 1, the I<init> process, are "
"those for which I<init> has explicitly installed signal handlers.  This is "
"done to assure the system is not brought down accidentally."
msgstr ""
"Singurele semnale care pot fi trimise către procesul ID 1, procesul I<init>, "
"sunt cele pentru care I<init> a instalat în mod explicit gestionari de "
"semnal. Acest lucru se face pentru a se asigura că sistemul nu este oprit "
"din greșeală."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1 requires that I<kill(-1,sig)> send I<sig> to all processes that the "
"calling process may send signals to, except possibly for some implementation-"
"defined system processes.  Linux allows a process to signal itself, but on "
"Linux the call I<kill(-1,sig)> does not signal the calling process."
msgstr ""
"POSIX.1 cere ca I<kill(-1,sig)> să trimită I<sig> la toate procesele cărora "
"procesul apelant le poate trimite semnale, cu excepția, eventual, a unor "
"procese de sistem definite de implementare.  Linux permite unui proces să se "
"semnalizeze singur, dar pe Linux apelul I<kill(-1,sig)> nu semnalizează "
"procesul apelant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1 requires that if a process sends a signal to itself, and the sending "
"thread does not have the signal blocked, and no other thread has it "
"unblocked or is waiting for it in B<sigwait>(3), at least one unblocked "
"signal must be delivered to the sending thread before the B<kill>()  returns."
msgstr ""
"POSIX.1 prevede că, în cazul în care un proces își trimite un semnal către "
"el însuși, iar firul expeditor nu are semnalul blocat și nici un alt fir nu "
"îl are deblocat sau nu îl așteaptă în B<sigwait>(3), cel puțin un semnal "
"deblocat trebuie să fie livrat firului expeditor înainte ca B<kill>() să se "
"întoarcă."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In Linux 2.6 up to and including Linux 2.6.7, there was a bug that meant "
"that when sending signals to a process group, B<kill>()  failed with the "
"error B<EPERM> if the caller did not have permission to send the signal to "
"I<any> (rather than I<all>) of the members of the process group.  "
"Notwithstanding this error return, the signal was still delivered to all of "
"the processes for which the caller had permission to signal."
msgstr ""
"În Linux 2.6 până la Linux 2.6.7 inclusiv, a existat o eroare care făcea ca "
"atunci când se trimiteau semnale către un grup de procese, B<kill>() să "
"eșueze cu eroarea B<EPERM> dacă apelantul nu avea permisiunea de a trimite "
"semnalul către I<oricare> (mai degrabă decât I<toți>) dintre membrii "
"grupului de procese. În pofida acestei erori de returnare, semnalul a fost "
"totuși transmis tuturor proceselor pentru care apelantul avea permisiunea de "
"a transmite semnalul."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<kill>(1), B<_exit>(2), B<pidfd_send_signal>(2), B<signal>(2), B<tkill>(2), "
"B<exit>(3), B<killpg>(3), B<sigqueue>(3), B<capabilities>(7), "
"B<credentials>(7), B<signal>(7)"
msgstr ""
"B<kill>(1), B<_exit>(2), B<pidfd_send_signal>(2), B<signal>(2), B<tkill>(2), "
"B<exit>(3), B<killpg>(3), B<sigqueue>(3), B<capabilities>(7), "
"B<credentials>(7), B<signal>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 decembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
