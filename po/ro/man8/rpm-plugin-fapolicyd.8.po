# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-09-18 20:37+0200\n"
"PO-Revision-Date: 2024-11-06 13:54+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.4\n"

#. type: TH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "RPM-FAPOLICYD"
msgstr "RPM-FAPOLICYD"

#. type: TH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "28 Jan 2021"
msgstr "28 ianuarie 2021"

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "rpm-plugin-fapolicyd - Fapolicyd plugin for the RPM Package Manager"
msgstr ""
"rpm-plugin-fapolicyd - modul fapolicyd pentru gestionarul de pachete RPM"

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Descriere"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"The plugin gathers metadata of currently installed files.  It sends the "
"information about files and about ongoing rpm transaction to the fapolicyd "
"daemon.  The information is written to Linux pipe which is placed in /var/"
"run/fapolicyd/fapolicyd.fifo."
msgstr ""
"Modulul adună metadatele fișierelor instalate în prezent. Acesta trimite "
"informațiile despre fișiere și despre tranzacția rpm în curs către demonul "
"fapolicyd. Informațiile sunt scrise în conducta Linux care este plasată în „/"
"var/run/fapolicyd/fapolicyd.fifo”."

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Configuration"
msgstr "Configurare"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"There are currently no options for this plugin in particular.  See \\f[B]rpm-"
"plugins\\f[R](8) on how to control plugins in general.\\fR"
msgstr ""
"În prezent nu există opțiuni pentru acest modul în special. Consultați "
"\\f[B]rpm-plugins\\f[R](8) despre cum să controlați modulele în general."

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "\\f[B]fapolicyd\\f[R](8), \\f[B]rpm-plugins\\f[R](8)\\fR"
msgstr "\\f[B]fapolicyd\\f[R](8), \\f[B]rpm-plugins\\f[R](8)\\fR"
