# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:06+0100\n"
"PO-Revision-Date: 2024-02-25 13:31+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Title"
msgstr "Title"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NFSDCLTRACK 8"
msgstr "NFSDCLTRACK 8"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NFSDCLTRACK"
msgstr "NFSDCLTRACK"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "2012-10-24"
msgstr "24 octombrie 2012"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "nfsdcltrack - NFSv4 Client Tracking Callout Program"
msgstr "nfsdcltrack - program de urmărire a clienților NFSv4"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Header"
msgstr "Antet"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"nfsdcltrack [-d] [-f] [-s stable storage dir] E<lt>commandE<gt> "
"E<lt>args...E<gt>"
msgstr ""
"nfsdcltrack [-d] [-f] [-s director-de-stocare-stabil] E<lt>comandaE<gt> "
"E<lt>argumente...E<gt>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"nfsdcltrack is the NFSv4 client tracking callout program. It is not "
"necessary to install this program on machines that are not acting as NFSv4 "
"servers."
msgstr ""
"nfsdcltrack este programul de urmărire a clienților NFSv4. Nu este necesar "
"să instalați acest program pe mașinile care nu acționează ca servere NFSv4."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When a network partition is combined with a server reboot, there are edge "
"conditions that can cause the server to grant lock reclaims when other "
"clients have taken conflicting locks in the interim. A more detailed "
"explanation of this issue is described in \\s-1RFC\\s0 3530, section 8.6.3 "
"and in \\s-1RFC\\s0 5661, section 8.4.3."
msgstr ""
"Atunci când o partiție de rețea este combinată cu o repornire a serverului, "
"există condiții de limitare care pot face ca serverul să acorde revendicări "
"de blocare atunci când alți clienți au luat între timp blocări conflictuale. "
"O explicație mai detaliată a acestei probleme este descrisă în „RFC” 3530, "
"secțiunea 8.6.3 și în „RFC” 5661, secțiunea 8.4.3."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In order to prevent these problems, the server must track a small amount of "
"per-client information on stable storage. This program provides the "
"userspace piece of that functionality. When the kernel needs to manipulate "
"the database that stores this info, it will execute this program to handle "
"it."
msgstr ""
"Pentru a preveni aceste probleme, serverul trebuie să urmărească o cantitate "
"mică de informații pentru fiecare client în parte, pe un spațiu de stocare "
"stabil. Acest program asigură partea de spațiu de utilizator a acestei "
"funcționalități. Atunci când nucleul trebuie să manipuleze baza de date care "
"stochează aceste informații, el va executa acest program pentru a se ocupa "
"de acest lucru."

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--debug>"
msgstr "B<-d>, B<--debug>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Item"
msgstr "Element"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-d, --debug"
msgstr "-d, --debug"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Enable debug level logging."
msgstr "Activează jurnalizarea la nivel de depanare."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--foreground>"
msgstr "B<-f>, B<--foreground>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-f, --foreground"
msgstr "-f, --foreground"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Log to stderr instead of syslog."
msgstr "Scrie registrul la ieșirea de eroare standard în loc de în syslog."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-s> I<storagedir>, B<--storagedir>=I<storage_dir>"
msgstr "B<-s> I<director-stocare>, B<--storagedir>=I<director-stocare>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-s storagedir, --storagedir=storage_dir"
msgstr "-s director-stocare, --storagedir=director-stocare"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Directory where stable storage information should be kept. The default value "
"is I</var/lib/nfs/nfsdcltrack>."
msgstr ""
"Directorul în care trebuie păstrate informațiile privind stocarea stabilă. "
"Valoarea implicită este I</var/lib/nfs/nfsdcltrack>."

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COMMANDS"
msgstr "COMENZI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"nfsdcltrack requires a command for each invocation. Supported commands are:"
msgstr ""
"«nfsdcltrack» necesită o comandă pentru fiecare invocare. Comenzile "
"acceptate sunt:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<init>"
msgstr "B<init>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "init"
msgstr "init"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Initialize the database. This command requires no argument."
msgstr ""
"Inițializează baza de date. Această comandă nu necesită niciun argument."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<create>"
msgstr "B<create>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "create"
msgstr "create"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Create a new client record (or update the timestamp on an existing one). "
"This command requires a hex-encoded nfs_client_id4 as an argument."
msgstr ""
"Creează o nouă înregistrare de client (sau actualizează data și ora unei "
"înregistrări existente). Această comandă necesită ca argument un "
"nfs_client_id4 codificat în hexazecimal."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<remove>"
msgstr "B<remove>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "remove"
msgstr "remove"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Remove a client record from the database. This command requires a hex-"
"encoded nfs_client_id4 as an argument."
msgstr ""
"Elimină o înregistrare de client din baza de date. Această comandă necesită "
"ca argument un nfs_client_id4 codificat în hexazecimal."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<check>"
msgstr "B<check>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "check"
msgstr "check"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Check to see if a nfs_client_id4 is allowed to reclaim. This command "
"requires a hex-encoded nfs_client_id4 as an argument."
msgstr ""
"Verifică dacă un nfs_client_id4 are voie să revendice. Această comandă "
"necesită ca argument un nfs_client_id4 codificat în hexazecimal."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<gracedone>"
msgstr "B<gracedone>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "gracedone"
msgstr "gracedone"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Remove any unreclaimed client records from the database. This command "
"requires a epoch boot time as an argument."
msgstr ""
"Elimină din baza de date înregistrările de clienți nerevendicate. Această "
"comandă necesită ca argument o oră de pornire de la epocă."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXTERNAL CONFIGURATION"
msgstr "CONFIGURAȚIA EXTERNĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The directory for stable storage information can be set via the file B</etc/"
"nfs.conf> by setting the B<storagedir> value in the B<nfsdcltrack> section.  "
"For example:"
msgstr ""
"Directorul pentru informațiile de stocare stabilă poate fi definit prin "
"intermediul fișierului B</etc/nfs.conf> prin definirea valorii B<storagedir> "
"în secțiunea B<nfsdcltrack>.  De exemplu:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "[nfsdcltrack]"
msgstr "[nfsdcltrack]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "  storagedir = /shared/nfs/nfsdcltrack\n"
msgstr "  storagedir = /shared/nfs/nfsdcltrack\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Debugging to syslog can also be enabled by setting \"debug = 1\" in this "
"file."
msgstr ""
"Depanarea în syslog poate fi, de asemenea, activată prin definirea „debug = "
"1” în acest fișier."

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LEGACY TRANSITION MECHANISM"
msgstr "MECANISMUL DE TRANZIȚIE VECHI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Linux kernel NFSv4 server has historically tracked this information on "
"stable storage by manipulating information on the filesystem directly, in "
"the directory to which I</proc/fs/nfsd/nfsv4recoverydir> points. If the "
"kernel passes the correct information, then nfsdcltrack can use it to allow "
"a seamless transition from the old client tracking scheme to the new one."
msgstr ""
"Serverul NFSv4 al nucleului Linux a urmărit în mod istoric aceste informații "
"din stocarea stabilă prin manipularea directă a informațiilor din sistemul "
"de fișiere, în directorul către care indică I</proc/fs/nfsd/"
"nfsv4recoverydir>. Dacă nucleul transmite informațiile corecte, atunci "
"«nfsdcltrack» le poate utiliza pentru a permite o tranziție fără probleme de "
"la vechea schemă de urmărire a clienților la cea nouă."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On a B<check> operation, if there is no record of the client in the "
"database, nfsdcltrack will look to see if the "
"B<\\s-1NFSDCLTRACK_LEGACY_RECDIR\\s0> environment variable is set. If it is, "
"then it will fetch that value and see if a directory exists by that name. If "
"it does, then the check operation will succeed and the directory will be "
"removed."
msgstr ""
"La o operație B<check>, dacă nu există nicio înregistrare a clientului în "
"baza de date, «nfsdcltrack» va verifica dacă variabila de mediu "
"B<„NFSDCLTRACK_LEGACY_RECDIR”> este definită. Dacă este, atunci va prelua "
"această valoare și va vedea dacă există un director cu acest nume. În caz "
"afirmativ, operația de verificare va reuși și directorul va fi eliminat."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On a B<gracedone> operation, nfsdcltrack will look to see if the "
"\\&B<\\s-1NFSDCLTRACK_LEGACY_TOPDIR\\s0> environment variable is set. If it "
"is, then it will attempt to clean out that directory prior to exiting."
msgstr ""
"La o operație B<gracedone>, «nfsdcltrack» va verifica dacă variabila de "
"mediu \\&B<„NFSDCLTRACK_LEGACY_TOPDIR”> este definită. Dacă este, atunci va "
"încerca să curețe acel director înainte de a ieși."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that this transition is one-way. If the machine subsequently reboots "
"back into an older kernel that does not support the nfsdcltrack upcall then "
"the clients will not be able to recover their state."
msgstr ""
"Rețineți că această tranziție este unidirecțională. Dacă mașina repornește "
"ulterior într-un nucleu mai vechi care nu acceptă apelul «nfsdcltrack», "
"atunci clienții nu își vor putea recupera starea."

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This program requires a kernel that supports the nfsdcltrack usermodehelper "
"upcall. This support was first added to mainline kernels in 3.8."
msgstr ""
"Acest program necesită un nucleu care acceptă apelul „nfsdcltrack "
"usermodehelper upcall”. Acest suport a fost adăugat pentru prima dată în "
"nucleele din linia principală în versiunea 3.8."

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "nfsdcltrack was developed by Jeff Layton E<lt>jlayton@redhat.comE<gt>."
msgstr ""
"«nfsdcltrack» a fost dezvoltat de Jeff Layton E<lt>jlayton@redhat.comE<gt>."

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Debuging to syslog can also be enabled by setting \"debug = 1\" in this file."
msgstr ""
"Depanarea în syslog poate fi, de asemenea, activată prin definirea „debug = "
"1” în acest fișier."
