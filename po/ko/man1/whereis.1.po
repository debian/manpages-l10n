# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-29 09:57+0100\n"
"PO-Revision-Date: 2000-04-13 08:57+0900\n"
"Last-Translator: Unknown <>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "WHEREIS"
msgstr "WHEREIS"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "2022년 5월 11일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "사용자 명령"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: debian-bookworm
msgid ""
"whereis - locate the binary, source, and manual page files for a command"
msgstr ""
"whereis - 명령의 실행 파일, 소스, 매뉴얼 페이지가 어디 있는지 보여준다."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<whereis> [ B<-bmsu> ] [ B<-BMS> I<directory>.\\|.\\|.  B<-f> ] "
#| "I<filename>\\| \\&.\\|.\\|."
msgid "B<whereis> [options] [B<-BMS> I<directory>... B<-f>] I<name>..."
msgstr ""
"B<whereis> [ B<-bmsu> ] [ B<-BMS> I<경로>.\\|.\\|.  B<-f> ] I<파일이름>\\| "
"\\&.\\|.\\|."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<whereis> locates source/binary and manuals sections for specified "
#| "files.  The supplied names are first stripped of leading pathname "
#| "components and any (single) trailing extension of the form B<.>I<ext,> "
#| "for example, B<.c>.  Prefixes of B<s.> resulting from use of source code "
#| "control are also dealt with.  B<whereis> then attempts to locate the "
#| "desired program in a list of standard Linux places."
msgid ""
"B<whereis> locates the binary, source and manual files for the specified "
"command names. The supplied names are first stripped of leading pathname "
"components. Prefixes of B<s.> resulting from use of source code control are "
"also dealt with. B<whereis> then attempts to locate the desired program in "
"the standard Linux places, and in the places specified by B<$PATH> and "
"B<$MANPATH>."
msgstr ""
"B<whereis> 명령은 지정한 I<파일이름>의 실행파일, 소스, 매뉴얼 페이지가 어디 "
"있는지 알려준다.  지정할 I<파일이름>에는 확장자가 사용될 수 있다. 이때, `."
"c', `.1' 이런 소스 파일과 매뉴얼 페이지 확장자가 사용될 수 있는데, 이러면 특"
"별히 지정하지 않으면 그 실행파일까지도 찾아준다.  이때 B<whereis> 명령은 따"
"로 특별히 찾을 경로를 지정하지 않으면, 다음 경로를 바탕으로 찾는다:"

#. type: Plain text
#: debian-bookworm
msgid ""
"The search restrictions (options B<-b>, B<-m> and B<-s>) are cumulative and "
"apply to the subsequent I<name> patterns on the command line. Any new search "
"restriction resets the search mask. For example,"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<whereis -bm ls tr -m gcc>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"searches for \"ls\" and \"tr\" binaries and man pages, and for \"gcc\" man "
"pages only."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The options B<-B>, B<-M> and B<-S> reset search paths for the subsequent "
"I<name> patterns. For example,"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<whereis -m ls -M /usr/share/man/man1 -f cal>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"searches for \"B<ls>\" man pages in all default paths, but for \"cal\" in "
"the I</usr/share/man/man1> directory only."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "옵션"

#. type: Plain text
#: debian-bookworm
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "Search only for binaries."
msgid "Search for binaries."
msgstr "실행 파일만 찾는다."

#. type: Plain text
#: debian-bookworm
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "Search only for manual sections."
msgid "Search for manuals."
msgstr "매뉴얼 페이지만 찾는다."

#. type: Plain text
#: debian-bookworm
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "Search only for sources."
msgid "Search for sources."
msgstr "소스만 찾는다."

#. type: Plain text
#: debian-bookworm
msgid "B<-u>"
msgstr "B<-u>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "Search for unusual entries.  A file is said to be unusual if it does not "
#| "have one entry of each requested type.  Thus `B<whereis\\ \\ -m\\ \\ -u\\ "
#| "\\ *>' asks for those files in the current directory which have no "
#| "documentation."
msgid ""
"Only show the command names that have unusual entries. A command is said to "
"be unusual if it does not have just one entry of each explicitly requested "
"type. Thus \\(aqB<whereis -m -u *>\\(aq asks for those files in the current "
"directory which have no documentation file, or more than one."
msgstr ""
"일반적이지 않은 항목을 위한 찾기.  이 옵션은 일반적으로 특정 파일을 제외하는"
"데 사용된다.  즉, `B<whereis\\ \\ -m\\ \\ -u\\ \\ *>' 명령은 현재 경로에서 메"
"뉴얼 페이지가 없는 파일을 조사한다."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<-l>, B<--list>"
msgid "B<-B> I<list>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "Change or otherwise limit the places where B<whereis> searches for "
#| "binaries."
msgid ""
"Limit the places where B<whereis> searches for binaries, by a whitespace-"
"separated list of directories."
msgstr "실행 파일을 찾을 경로를 지정한다."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<-l>, B<--list>"
msgid "B<-M> I<list>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the places where B<whereis> searches for manuals and documentation in "
"Info format, by a whitespace-separated list of directories."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<-l>, B<--list>"
msgid "B<-S> I<list>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "Change or otherwise limit the places where B<whereis> searches for "
#| "sources."
msgid ""
"Limit the places where B<whereis> searches for sources, by a whitespace-"
"separated list of directories."
msgstr "소스 파일을 찾을 경로를 지정한다."

#. type: Plain text
#: debian-bookworm
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "Terminate the last directory list and signals the start of file names, "
#| "and I<must> be used when any of the B<-B>, B<-M>, or B<-S> options are "
#| "used."
msgid ""
"Terminates the directory list and signals the start of filenames. It I<must> "
"be used when any of the B<-B>, B<-M>, or B<-S> options is used."
msgstr ""
"경로 지정이 끝나고 다음에 오는 것은 찾을 파일 이름을 알리는 옵션이다.  이 옵"
"션은 B<-B>, B<-M>, B<-S> 옵션들이 사용될 때에 사용해야 한다."

#. type: Plain text
#: debian-bookworm
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Output the list of effective lookup paths that B<whereis> is using. When "
"none of B<-B>, B<-M>, or B<-S> is specified, the option will output the hard-"
"coded paths that the command was able to find on the system."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "도움말을 보여주고 마친다."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "버전 정보를 보여주고 마친다."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILE SEARCH PATHS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"By default B<whereis> tries to find files from hard-coded paths, which are "
"defined with glob patterns. The command attempts to use the contents of "
"B<$PATH> and B<$MANPATH> environment variables as default search path. The "
"easiest way to know what paths are in use is to add the B<-l> listing "
"option. Effects of the B<-B>, B<-M>, and B<-S> are displayed with B<-l>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr "환경"

#. type: Plain text
#: debian-bookworm
msgid "B<WHEREIS_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "enables debug output."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLES"
msgstr "폐제"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "Find all files in B</usr/bin> which are not documented in B</usr/man/"
#| "man1> with source in B</usr/src>:"
msgid ""
"To find all files in I</usr/bin> which are not documented in I</usr/man/"
"man1> or have no source in I</usr/src>:"
msgstr ""
"B</usr/bin> 경로 안에 있지만, B</usr/man/man1> 경로 안에 그 해당 매뉴얼이 없"
"고, B</usr/src> 경로 안에 그 해당 소스가 없는 모든 파일을 찾는 예제는:"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<example% cd /usr/bin\n"
#| "example% whereis -u -M /usr/man/man1 -S /usr/src -f *>\n"
msgid "B<cd /usr/bin> B<whereis -u -ms -M /usr/man/man1 -S /usr/src -f *>"
msgstr ""
"B<example% cd /usr/bin\n"
"example% whereis -u -M /usr/man/man1 -S /usr/src -f *>\n"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "버그 보고"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "가용성"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<whereis> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
