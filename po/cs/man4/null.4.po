# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jiří Pavlovský <pavlovsk@ff.cuni.cz>, 2001.
# Pavel Heimlich <tropikhajma@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:06+0100\n"
"PO-Revision-Date: 2009-02-09 20:06+0100\n"
"Last-Translator: Pavel Heimlich <tropikhajma@gmail.com>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "null"
msgstr "null"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15. června 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "null, zero - data sink"
msgstr "null, zero - \"propadliště\" dat"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "Data written on a B<null> or B<zero> special file is discarded."
msgid ""
"Data written to the I</dev/null> and I</dev/zero> special files is discarded."
msgstr "Data zapsaná do speciálních souborů B<null> nebo B<zero> jsou zničena."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Reads from the B<null> special file always return end of file, whereas "
#| "reads from B<zero> always return \\e0 characters."
msgid ""
"Reads from I</dev/null> always return end of file (i.e., B<read>(2)  returns "
"0), whereas reads from I</dev/zero> always return bytes containing zero (\\"
"[aq]\\[rs]0\\[aq] characters)."
msgstr ""
"Čtení ze speciálního souboru B<null> vždy vrací konec souboru, čtení ze "
"speciálního souboru B<zero> vždy vrací znaky \\e0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "B<null> and B<zero> are typically created by:"
msgid "These devices are typically created by:"
msgstr "B<null> a B<zero> jsou vytvářeny takto:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 666 /dev/null c 1 3\n"
"mknod -m 666 /dev/zero c 1 5\n"
"chown root:root /dev/null /dev/zero\n"
msgstr ""
"mknod -m 666 /dev/null c 1 3\n"
"mknod -m 666 /dev/zero c 1 5\n"
"chown root:root /dev/null /dev/zero\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "SOUBORY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/null>"
msgstr "I</dev/null>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/zero>"
msgstr "I</dev/zero>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "POZNÁMKY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If these devices are not writable and readable for all users, many programs "
"will act strangely."
msgstr ""
"Nejsou-li tyto speciální soubory čitelné nebo zapisovatelné pro všechny "
"uživatele, mnoho programů nefunguje tak, jak by mělo."

#.  commit 2b83868723d090078ac0e2120e06a1cc94dbaef0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since Linux 2.6.31, reads from I</dev/zero> are interruptible by signals.  "
"(This change was made to help with bad latencies for large reads from I</dev/"
"zero>.)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chown>(1), B<mknod>(1), B<full>(4)"
msgstr "B<chown>(1), B<mknod>(1), B<full>(4)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. února 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "Reads from the B<null> special file always return end of file, whereas "
#| "reads from B<zero> always return \\e0 characters."
msgid ""
"Reads from I</dev/null> always return end of file (i.e., B<read>(2)  returns "
"0), whereas reads from I</dev/zero> always return bytes containing zero (\\"
"[aq]\\e0\\[aq] characters)."
msgstr ""
"Čtení ze speciálního souboru B<null> vždy vrací konec souboru, čtení ze "
"speciálního souboru B<zero> vždy vrací znaky \\e0."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. října 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (nevydané)"
