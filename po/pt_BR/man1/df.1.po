# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Ricardo C.O.Freitas <english.quest@best-service.com>, 2000.
# Rafael Fontenelle <rafaelff@gnome.org>, 2021-2024.
#
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 17:56+0100\n"
"PO-Revision-Date: 2024-09-26 13:31-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 46.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DF"
msgstr "DF"

#. type: TH
#: archlinux
#, no-wrap
msgid "August 2024"
msgstr "Agosto de 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comandos de usuário"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "df - report file system space usage"
msgstr "df - relata o uso de espaço do sistema de arquivos"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<df> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<df> [I<\\,OPÇÃO\\/>]... [I<\\,ARQUIVO\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page documents the GNU version of B<df>.  B<df> displays the "
"amount of space available on the file system containing each file name "
"argument.  If no file name is given, the space available on all currently "
"mounted file systems is shown.  Space is shown in 1K blocks by default, "
"unless the environment variable POSIXLY_CORRECT is set, in which case 512-"
"byte blocks are used."
msgstr ""
"Esta página de manual documenta a versão GNU de B<df>. B<df> exibe a "
"quantidade de espaço em disco disponível no sistema de arquivos contendo "
"cada argumento de nome de arquivo. Se nenhum nome de arquivo for fornecido, "
"o espaço disponível em todos os sistemas de arquivos montados atualmente é "
"mostrado. O espaço em disco é mostrado em blocos de 1K por padrão, a menos "
"que a variável de ambiente POSIXLY_CORRECT seja definida, caso em que blocos "
"de 512 bytes são usados."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If an argument is the absolute file name of a device node containing a "
"mounted file system, B<df> shows the space available on that file system "
"rather than on the file system containing the device node.  This version of "
"B<df> cannot show the space available on unmounted file systems, because on "
"most kinds of systems doing so requires non-portable intimate knowledge of "
"file system structures."
msgstr ""
"Se um argumento for o nome de arquivo absoluto de um nó de dispositivo de "
"dispositivo que contém um sistema de arquivos montado, B<df> mostra o espaço "
"disponível nesse sistema de arquivos em vez do sistema de arquivos que "
"contém o nó de dispositivo. Esta versão do B<df> não pode mostrar o espaço "
"disponível em sistemas de arquivos não montados, porque na maioria dos tipos "
"de sistemas, fazer isso requer um conhecimento íntimo não portável das "
"estruturas do sistema de arquivos."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPÇÕES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Show information about the file system on which each FILE resides, or all "
"file systems by default."
msgstr ""
"Mostra informações sobre os sistemas de arquivos nos quais cada ARQUIVO "
"reside ou, por padrão, sobre todos os sistemas de arquivos."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumentos obrigatórios para opções longas também o são para opções curtas."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "include pseudo, duplicate, inaccessible file systems"
msgstr "inclui pseudo sistemas de arquivos, além de duplicados e inacessíveis"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>, B<--block-size>=I<\\,SIZE\\/>"
msgstr "B<-B>, B<--block-size>=I<\\,TAM\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"scale sizes by SIZE before printing them; e.g., \\&'-BM' prints sizes in "
"units of 1,048,576 bytes; see SIZE format below"
msgstr ""
"o tamanho considera blocos de TAM bytes; por exemplo, \\&\"-BM\" emite "
"tamanhos em unidades de 1.048.576 bytes; veja o formato de TAM abaixo"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--human-readable>"
msgstr "B<-h>, B<--human-readable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print sizes in powers of 1024 (e.g., 1023M)"
msgstr "emite tamanhos na potência de 1024 (p.ex.: 1023M)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>, B<--si>"
msgstr "B<-H>, B<--si>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print sizes in powers of 1000 (e.g., 1.1G)"
msgstr "emite tamanhos na potência de 1000 (p.ex.: 1.1G)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--inodes>"
msgstr "B<-i>, B<--inodes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "list inode information instead of block usage"
msgstr "lista informações do inode, em vez de uso de bloco"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "like B<--block-size>=I<\\,1K\\/>"
msgstr "o mesmo que B<--block-size>=I<\\,1K\\/>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--local>"
msgstr "B<-l>, B<--local>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "limit listing to local file systems"
msgstr "limita a listagem a sistemas de arquivos locais"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-sync>"
msgstr "B<--no-sync>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "do not invoke sync before getting usage info (default)"
msgstr "não invoca \"sync\" ao obter informações de uso (padrão)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--output>[=I<\\,FIELD_LIST\\/>]"
msgstr "B<--output>[=I<\\,LISTA_CAMPOS\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use the output format defined by FIELD_LIST, or print all fields if "
"FIELD_LIST is omitted."
msgstr ""
"usa o formato de saída definido por LISTA_CAMPOS ou exibe todos os campos, "
"se LISTA_CAMPOS for omitido."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--portability>"
msgstr "B<-P>, B<--portability>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use the POSIX output format"
msgstr "usa o formato de saída POSIX"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--sync>"
msgstr "B<--sync>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "invoke sync before getting usage info"
msgstr "invoca \"sync\" antes de obter informações de uso"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--total>"
msgstr "B<--total>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"elide all entries insignificant to available space, and produce a grand total"
msgstr ""
"suprime todas as entradas insignificantes para o espaço disponível e produz "
"um total geral"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--type>=I<\\,TYPE\\/>"
msgstr "B<-t>, B<--type>=I<\\,TIPO\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "limit listing to file systems of type TYPE"
msgstr "limita a listagem a sistemas de arquivo de tipo TIPO"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--print-type>"
msgstr "B<-T>, B<--print-type>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print file system type"
msgstr "emite o tipo de sistema de arquivo"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--exclude-type>=I<\\,TYPE\\/>"
msgstr "B<-x>, B<--exclude-type>=I<\\,TIPO\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "limit listing to file systems not of type TYPE"
msgstr "limita a listagem a sistemas de arquivo que não sejam do tipo TIPO"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "(ignored)"
msgstr "(ignorado)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "mostra esta ajuda e sai"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "informa a versão e sai"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Display values are in units of the first available SIZE from B<--block-"
"size>, and the DF_BLOCK_SIZE, BLOCK_SIZE and BLOCKSIZE environment "
"variables.  Otherwise, units default to 1024 bytes (or 512 if "
"POSIXLY_CORRECT is set)."
msgstr ""
"Valores exibidos estão em unidades da primeiro TAM disponível de B<--block-"
"size>, e das variáveis de ambiente DF_BLOCK_SIZE, BLOCK_SIZE e BLOCKSIZE. Do "
"contrário, as unidades são definidas com 1024 bytes (ou 512 se "
"POSIXLY_CORRECT estiver definido)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y,R,Q (powers of 1024) or KB,MB,... "
"(powers of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"O argumento TAM é uma unidade opcional e inteiro (exemplo: 10K é 10*1024). "
"As unidades são K,M,G,T,P,E,Z,Y,R,Q (vezes 1024) ou KB,MB,... (vezes 1000). "
"Prefixos binários também podem ser usados: KiB=K, MiB=M e assim por diante."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"FIELD_LIST is a comma-separated list of columns to be included.  Valid field "
"names are: 'source', 'fstype', 'itotal', 'iused', 'iavail', 'ipcent', "
"\\&'size', 'used', 'avail', 'pcent', 'file' and 'target' (see info page)."
msgstr ""
"LISTA_CAMPOS é uma lista separada por vírgula de colunas a serem incluídas. "
"Nomes válidos de campos são: \"source\", \"fstype\", \"itotal\", \"iused\", "
"\"iavail\", \"ipcent\", \\&\"size\", \"used\", \"avail\", \"pcent\", "
"\"file\" e \"target\" (veja a página info)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Torbjorn Granlund, David MacKenzie, and Paul Eggert."
msgstr "Escrito por Torbjorn Granlund, David MacKenzie e Paul Eggert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RELATANDO PROBLEMAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Página de ajuda do GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Relate erros de tradução para E<lt>https://translationproject.org/team/pt_BR."
"htmlE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DIREITOS AUTORAIS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc. Licença GPLv3+: GNU GPL "
"versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Este é um software livre: você é livre para alterá-lo e redistribuí-lo. NÃO "
"HÁ QUALQUER GARANTIA, na máxima extensão permitida em lei."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/dfE<gt>"
msgstr ""
"Documentação completa E<lt>https://www.gnu.org/software/coreutils/dfE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) df invocation\\(aq"
msgstr "ou disponível localmente via: info \\(aq(coreutils) df invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "Setembro de 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
msgid ""
"If an argument is the absolute file name of a device node containing a "
"mounted file system, B<df> shows the space available on that file system "
"rather than on the file system containing the device node.  This version of "
"B<df> cannot show the space available on unmounted file systems, because on "
"most kinds of systems doing so requires very nonportable intimate knowledge "
"of file system structures."
msgstr ""
"Se um argumento for o nome de arquivo absoluto de um nó de dispositivo de "
"disco que contém um sistema de arquivos montado, B<df> mostra o espaço "
"disponível nesse sistema de arquivos em vez do sistema de arquivos que "
"contém o nó de dispositivo. Esta versão do B<df> não pode mostrar o espaço "
"disponível em sistemas de arquivos não montados, porque na maioria dos tipos "
"de sistemas, fazer isso requer um conhecimento íntimo não portável das "
"estruturas do sistema de arquivos."

#. type: Plain text
#: debian-bookworm
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers "
"of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"O argumento TAM é uma unidade opcional e inteiro (exemplo: 10K é 10*1024). "
"As unidades são K,M,G,T,P,E,Z,Y (vezes 1024) ou KB,MB,... (vezes 1000). "
"Prefixos binários também podem ser usados: KiB=K, MiB=M e assim por diante."

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licença GPLv3+: GNU GPL "
"versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "October 2023"
msgid "October 2024"
msgstr "Outubro de 2023"

#. type: TH
#: fedora-41 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "September 2022"
msgid "September 2024"
msgstr "Setembro de 2022"

#. type: TP
#: fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--direct>"
msgstr "B<--direct>"

#. type: Plain text
#: fedora-41 fedora-rawhide
msgid "show statistics for a file instead of mount point"
msgstr "mostra estatísticas para um arquivo em vez do ponto de montagem"

#. type: TH
#: fedora-rawhide
#, fuzzy, no-wrap
#| msgid "September 2022"
msgid "November 2024"
msgstr "Setembro de 2022"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "Agosto de 2023"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Licença GPLv3+: GNU GPL "
"versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "Janeiro de 2024"
