# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Ricardo C.O.Freitas <english.quest@best-service.com>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:07+0100\n"
"PO-Revision-Date: 2000-06-02 19:20-0300\n"
"Last-Translator: Ricardo C.O.Freitas <english.quest@best-service.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "personality"
msgstr "personality"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maio 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "personality - set the process execution domain"
msgstr "personality - seleciona o domínio de execução do processo"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>sys/personality.hE<gt>>"
msgid "B<#include E<lt>sys/personality.hE<gt>>\n"
msgstr "B<#include E<lt>sys/personality.hE<gt>>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int personality(unsigned long >I<persona>B<);>"
msgid "B<int personality(unsigned long >I<persona>B<);>\n"
msgstr "B<int personality(unsigned long >I<persona>B<);>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Linux supports different execution domains, or personalities, for each "
"process.  Among other things, execution domains tell Linux how to map signal "
"numbers into signal actions.  The execution domain system allows Linux to "
"provide limited support for binaries compiled under other UNIX-like "
"operating systems."
msgstr ""
"O Linux suporta diferentes domínios de execução, ou personalidades, para "
"cada processo. Entre outras coisas, domínios de execução dizem ao Linux como "
"mapear números de sinalização dentro de ações sinalizadas. Este sistema "
"permite ao Linux fornecer suporte limitado para executáveis compilados "
"dentro de outros sistemas operacionais compatíveis com o UNIX."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<persona> is not 0xffffffff, then B<personality>()  sets the caller's "
"execution domain to the value specified by I<persona>.  Specifying "
"I<persona> as 0xffffffff provides a way of retrieving the current persona "
"without changing it."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A list of the available execution domains can be found in I<E<lt>sys/"
"personality.hE<gt>>.  The execution domain is a 32-bit value in which the "
"top three bytes are set aside for flags that cause the kernel to modify the "
"behavior of certain system calls so as to emulate historical or "
"architectural quirks.  The least significant byte is a value defining the "
"personality the kernel should assume.  The flag values are as follows:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_COMPAT_LAYOUT> (since Linux 2.6.9)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "With this flag set, provide legacy virtual address space layout."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_NO_RANDOMIZE> (since Linux 2.6.12)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "With this flag set, disable address-space-layout randomization."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_LIMIT_32BIT> (since Linux 2.2)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Limit the address space to 32 bits."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_LIMIT_3GB> (since Linux 2.4.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"With this flag set, use 0xc0000000 as the offset at which to search a "
"virtual memory chunk on B<mmap>(2); otherwise use 0xffffe000.  Applies to 32-"
"bit x86 processes only."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<FDPIC_FUNCPTRS> (since Linux 2.6.11)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"User-space function pointers to signal handlers point to descriptors.  "
"Applies only to ARM if BINFMT_ELF_FDPIC and SuperH."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<MMAP_PAGE_ZERO> (since Linux 2.4.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Map page 0 as read-only (to support binaries that depend on this SVr4 "
"behavior)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<READ_IMPLIES_EXEC> (since Linux 2.6.8)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "With this flag set, B<PROT_READ> implies B<PROT_EXEC> for B<mmap>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SHORT_INODE> (since Linux 2.4.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "No effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<STICKY_TIMEOUTS> (since Linux 1.2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"With this flag set, B<select>(2), B<pselect>(2), and B<ppoll>(2)  do not "
"modify the returned timeout argument when interrupted by a signal handler."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<UNAME26> (since Linux 3.1)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Have B<uname>(2)  report a 2.6.(40+x) version number rather than a MAJOR.x "
"version number.  Added as a stopgap measure to support broken applications "
"that could not handle the kernel version-numbering switch from Linux 2.6.x "
"to Linux 3.x."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<WHOLE_SECONDS> (since Linux 1.2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "The field descriptions are:"
msgid "The available execution domains are:"
msgstr "As descrições dos campos são:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_BSD> (since Linux 1.2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "BSD. (No effects.)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_HPUX> (since Linux 2.4)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Support for 32-bit HP/UX.  This support was never complete, and was dropped "
"so that since Linux 4.0, this value has no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_IRIX32> (since Linux 2.2)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"IRIX 5 32-bit.  Never fully functional; support dropped in Linux 2.6.27.  "
"Implies B<STICKY_TIMEOUTS>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_IRIX64> (since Linux 2.2)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "IRIX 6 64-bit.  Implies B<STICKY_TIMEOUTS>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_IRIXN32> (since Linux 2.2)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "IRIX 6 new 32-bit.  Implies B<STICKY_TIMEOUTS>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_ISCR4> (since Linux 1.2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Implies B<STICKY_TIMEOUTS>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX> (since Linux 1.2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX32> (since Linux 2.2)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<uname>(2)  returns the name of the 32-bit architecture in the I<machine> "
"field (\"i686\" instead of \"x86_64\", &c.)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Under ia64 (Itanium), processes with this personality don't have the "
"O_LARGEFILE B<open>(2)  flag forced."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Under 64-bit ARM, setting this personality is forbidden if B<execve>(2)ing a "
"32-bit process would also be forbidden (cf. the allow_mismatched_32bit_el0 "
"kernel parameter and I<Documentation/arm64/asymmetric-32bit.rst>)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX32_3GB> (since Linux 2.4)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Same as B<PER_LINUX32>, but implies B<ADDR_LIMIT_3GB>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX_32BIT> (since Linux 2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Same as B<PER_LINUX>, but implies B<ADDR_LIMIT_32BIT>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX_FDPIC> (since Linux 2.6.11)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Same as B<PER_LINUX>, but implies B<FDPIC_FUNCPTRS>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_OSF4> (since Linux 2.4)"
msgstr ""

#.  commit 987f20a9dcce3989e48d87cff3952c095c994445
#.  Following is from a comment in arch/alpha/kernel/osf_sys.c
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"OSF/1 v4.  No effect since Linux 6.1, which removed a.out binary support.  "
"Before, on alpha, would clear top 32 bits of iov_len in the user's buffer "
"for compatibility with old versions of OSF/1 where iov_len was defined as.  "
"I<int>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_OSR5> (since Linux 2.4)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"SCO OpenServer 5.  Implies B<STICKY_TIMEOUTS> and B<WHOLE_SECONDS>; "
"otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_RISCOS> (since Linux 2.3.7; macro since Linux 2.3.13)"
msgstr ""

#.  commit 125ec7b4e90cbae4eed5a7ff1ee479cc331dcf3c
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Acorn RISC OS/Arthur (MIPS).  No effect.  Up to Linux v4.0, would set the "
"emulation altroot to I</usr/gnemul/riscos> (cf.\\& B<PER_SUNOS>, below).  "
"Before then, up to Linux 2.6.3, just Arthur emulation."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SCOSVR3> (since Linux 1.2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"SCO UNIX System V Release 3.  Same as B<PER_OSR5>, but also implies "
"B<SHORT_INODE>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SOLARIS> (since Linux 2.4)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Solaris.  Implies B<STICKY_TIMEOUTS>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SUNOS> (since Linux 2.4.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sun OS.  Same as B<PER_BSD>, but implies B<STICKY_TIMEOUTS>.  Prior to Linux "
"2.6.26, diverted library and dynamic linker searches to I</usr/gnemul>.  "
"Buggy, largely unmaintained, and almost entirely unused."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SVR3> (since Linux 1.2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"AT&T UNIX System V Release 3.  Implies B<STICKY_TIMEOUTS> and "
"B<SHORT_INODE>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SVR4> (since Linux 1.2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"AT&T UNIX System V Release 4.  Implies B<STICKY_TIMEOUTS> and "
"B<MMAP_PAGE_ZERO>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_UW7> (since Linux 2.4)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"UnixWare 7.  Implies B<STICKY_TIMEOUTS> and B<MMAP_PAGE_ZERO>; otherwise no "
"effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_WYSEV386> (since Linux 1.2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"WYSE UNIX System V/386.  Implies B<STICKY_TIMEOUTS> and B<SHORT_INODE>; "
"otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_XENIX> (since Linux 1.2.0)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"XENIX.  Implies B<STICKY_TIMEOUTS> and B<SHORT_INODE>; otherwise no effect."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, the previous I<persona> is returned.  On error, -1 is returned, "
"and I<errno> is set to indicate the error."
msgstr ""
"Em caso de sucesso, zero é retornado. Caso contrário, -1 é retornado, e "
"I<errno> é selecionado adequadamente."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The kernel was unable to change the personality."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTÓRICO"

#.  (and thus first in a stable kernel release with Linux 1.2.0)
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux 1.1.20, glibc 2.3."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<setarch>(8)"
msgstr "B<setarch>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 dezembro 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"With this flag set, use 0xc0000000 as the offset at which to search a "
"virtual memory chunk on B<mmap>(2); otherwise use 0xffffe000."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"User-space function pointers to signal handlers point (on certain "
"architectures) to descriptors."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "No effects(?)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Have B<uname>(2)  report a 2.6.40+ version number rather than a 3.x version "
"number.  Added as a stopgap measure to support broken applications that "
"could not handle the kernel version-numbering switch from Linux 2.6.x to "
"Linux 3.x."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "IRIX 6 64-bit.  Implies B<STICKY_TIMEOUTS>; otherwise no effects."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "IRIX 6 new 32-bit.  Implies B<STICKY_TIMEOUTS>; otherwise no effects."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Implies B<STICKY_TIMEOUTS>; otherwise no effects."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "[To be documented.]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Implies B<ADDR_LIMIT_3GB>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Implies B<ADDR_LIMIT_32BIT>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Implies B<FDPIC_FUNCPTRS>."
msgstr ""

#.  Following is from a comment in arch/alpha/kernel/osf_sys.c
#. type: Plain text
#: debian-bookworm
msgid ""
"OSF/1 v4.  On alpha, clear top 32 bits of iov_len in the user's buffer for "
"compatibility with old versions of OSF/1 where iov_len was defined as.  "
"I<int>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Implies B<STICKY_TIMEOUTS> and B<WHOLE_SECONDS>; otherwise no effects."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<PER_RISCOS> (since Linux 2.2)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Implies B<STICKY_TIMEOUTS>, B<WHOLE_SECONDS>, and B<SHORT_INODE>; otherwise "
"no effects."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Implies B<STICKY_TIMEOUTS>.  Divert library and dynamic linker searches to "
"I</usr/gnemul>.  Buggy, largely unmaintained, and almost entirely unused; "
"support was removed in Linux 2.6.26."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Implies B<STICKY_TIMEOUTS> and B<SHORT_INODE>; otherwise no effects."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Implies B<STICKY_TIMEOUTS> and B<MMAP_PAGE_ZERO>; otherwise no effects."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSÕES"

#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#: debian-bookworm
msgid ""
"This system call first appeared in Linux 1.1.20 (and thus first in a stable "
"kernel release with Linux 1.2.0); library support was added in glibc 2.3."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<personality>()  is Linux-specific and should not be used in programs "
"intended to be portable."
msgstr ""
"B<personality>() é específca do Linux, e não deveria ser usada em programas "
"que pretendam ser portáveis."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 outubro 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (não lançado)"
