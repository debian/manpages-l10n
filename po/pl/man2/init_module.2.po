# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2001.
# Michał Kułach <michal.kulach@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:01+0100\n"
"PO-Revision-Date: 2024-07-03 22:12+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "init_module"
msgstr "init_module"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maja 2024 r."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "init_module, finit_module - load a kernel module"
msgstr "init_module, finit_module - ładuje moduł jądra"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/module.hE<gt>>    /* Definition of B<MODULE_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>     /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>linux/module.hE<gt>>    /* Definicja stałych B<MODULE_*> */\n"
"B<#include E<lt>sys/syscall.hE<gt>>     /* Definicja stałych B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_init_module, void >I<module_image>B<[.>I<len>B<], unsigned long >I<len>B<,>\n"
"B<            const char *>I<param_values>B<);>\n"
"B<int syscall(SYS_finit_module, int >I<fd>B<,>\n"
"B<            const char *>I<param_values>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int syscall(SYS_init_module, void >I<module_image>B<[.>I<len>B<], unsigned long >I<len>B<,>\n"
"B<            const char *>I<param_values>B<);>\n"
"B<int syscall(SYS_finit_module, int >I<fd>B<,>\n"
"B<            const char *>I<param_values>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrappers for these system calls, necessitating "
"the use of B<syscall>(2)."
msgstr ""
"I<Uwaga>: glibc nie udostępnia opakowania dla tych wywołań systemowych, co "
"wymusza użycie B<syscall>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<init_module>()  loads an ELF image into kernel space, performs any "
"necessary symbol relocations, initializes module parameters to values "
"provided by the caller, and then runs the module's I<init> function.  This "
"system call requires privilege."
msgstr ""
"B<init_module>() ładuje obraz ELF do przestrzeni jądra, przeprowadzając "
"wszelkie niezbędne przesunięcia symboli, inicjuje parametry modułu zgodnie z "
"wartościami przekazanymi przez wywołującego, a następnie uruchamia funkcję "
"I<init> modułu. To wywołanie systemowe wymaga uprzywilejowania."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<module_image> argument points to a buffer containing the binary image "
"to be loaded; I<len> specifies the size of that buffer.  The module image "
"should be a valid ELF image, built for the running kernel."
msgstr ""
"Argument I<module_image> wskazuje na bufor zawierający obraz binarny, który "
"ma być załadowany; I<len> określa rozmiar tego bufora. Obraz modułu powinien "
"być prawidłowym obrazem ELF, zbudowanym do aktualnie działającego jądra."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<param_values> argument is a string containing space-delimited "
"specifications of the values for module parameters (defined inside the "
"module using B<module_param>()  and B<module_param_array>()).  The kernel "
"parses this string and initializes the specified parameters.  Each of the "
"parameter specifications has the form:"
msgstr ""
"Argument I<param_values> jest łańcuchem zawierającym wartości parametrów "
"modułu (zdefiniowanych wewnątrz modułu za pomocą B<module_param>() i "
"B<module_param_array>()), rozdzielone spacją. Jądra analizuje ten łańcuch i "
"inicjuje podane parametry. Określenie każdego z parametrów ma postać:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<name>[ B<=>I<value> [B<,>I<value>...]]"
msgstr "I<nazwa>[ B<=>I<wartość> [B<,>I<wartość>...]]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The parameter I<name> is one of those defined within the module using "
"I<module_param>()  (see the Linux kernel source file I<include/linux/"
"moduleparam.h>).  The parameter I<value> is optional in the case of I<bool> "
"and I<invbool> parameters.  Values for array parameters are specified as a "
"comma-separated list."
msgstr ""
"Parametr I<nazwa> jest jednym ze zdefiniowanych wewnątrz modułu za pomocą "
"I<module_param>() (zob. plik źródeł jądra Linux I<include/linux/moduleparam."
"h>). Parametr I<wartość> jest opcjonalny w przypadku parametrów I<bool> i "
"I<invbool>. Wartości parametrów tablicy są podawane w liście rozdzielonej "
"przecinkami."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "finit_module()"
msgstr "finit_module()"

#.  commit 34e1169d996ab148490c01b65b4ee371cf8ffba2
#.  https://lwn.net/Articles/519010/
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<finit_module>()  system call is like B<init_module>(), but reads the "
"module to be loaded from the file descriptor I<fd>.  It is useful when the "
"authenticity of a kernel module can be determined from its location in the "
"filesystem; in cases where that is possible, the overhead of using "
"cryptographically signed modules to determine the authenticity of a module "
"can be avoided.  The I<param_values> argument is as for B<init_module>()."
msgstr ""
"Wywołanie systemowe B<finit_module>() jest podobne do B<init_module>(), lecz "
"odczytuje moduł, który ma być załadowany, z deskryptora pliku I<fd>. Ma to "
"zastosowanie, gdy autentyczność modułu jądra można określić na podstawie "
"jego położenia w systemie plików; w przypadku gdy jest to możliwe, można w "
"ten sposób uniknąć narzutu wynikającego z używania modułów podpisanych "
"kryptograficznie. Argument I<param_values> jest taki sam jak do "
"B<init_module>()."

#.  commit 2f3238aebedb243804f58d62d57244edec4149b2
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<flags> argument modifies the operation of B<finit_module>().  It is a "
"bit mask value created by ORing together zero or more of the following flags:"
msgstr ""
"Argument I<flags> modyfikuje działanie B<finit_module>(). Jest maską bitową "
"wartości, utworzoną przez zsumowanie (OR) zera lub więcej poniższych "
"znaczników:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<MODULE_INIT_IGNORE_MODVERSIONS>"
msgstr "B<MODULE_INIT_IGNORE_MODVERSIONS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Ignore symbol version hashes."
msgstr "Ignoruje skróty wersji symboli."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<MODULE_INIT_IGNORE_VERMAGIC>"
msgstr "B<MODULE_INIT_IGNORE_VERMAGIC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Ignore kernel version magic."
msgstr "Ignoruje magię wersji jądra."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<MODULE_INIT_COMPRESSED_FILE> (since Linux 5.17)"
msgstr "B<MODULE_INIT_COMPRESSED_FILE> (od Linuksa 5.17)"

#.  commit b1ae6dc41eaaa98bb75671e0f3665bfda248c3e7
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Use in-kernel module decompression."
msgstr "Używa modułu dekompresji w jądrze."

#.  http://www.tldp.org/HOWTO/Module-HOWTO/basekerncompat.html
#.  is dated, but informative
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There are some safety checks built into a module to ensure that it matches "
"the kernel against which it is loaded.  These checks are recorded when the "
"module is built and verified when the module is loaded.  First, the module "
"records a \"vermagic\" string containing the kernel version number and "
"prominent features (such as the CPU type).  Second, if the module was built "
"with the B<CONFIG_MODVERSIONS> configuration option enabled, a version hash "
"is recorded for each symbol the module uses.  This hash is based on the "
"types of the arguments and return value for the function named by the "
"symbol.  In this case, the kernel version number within the \"vermagic\" "
"string is ignored, as the symbol version hashes are assumed to be "
"sufficiently reliable."
msgstr ""
"W module są wbudowane pewne mechanizmy bezpieczeństwa zapewniające, że "
"będzie on pasował do jądra, do którego jest załadowywany. Są one zapisywane "
"przy budowaniu modułu i weryfikowane przy jego ładowaniu. Po pierwsze, moduł "
"zapisuje łańcuch \\[Bq]vermagic\\[rq] zawierający łańcuch z numerem wersji "
"jądra i głównymi cechami (takimi jak typ procesora). Po drugie, jeśli moduł "
"zbudowano z włączoną opcją konfiguracyjną B<CONFIG_MODVERSIONS>, to "
"zapisywany jest skrót (hash) wersji każdego symbolu, którego używa moduł. "
"Skrót ten zależy od typów argumentów i zwracanej przez funkcję, nazwaną "
"przez symbol, wartości. W niniejszym przypadku, numer wersji jądra w "
"\\[Bq]vermagic\\[rq] jest ignorowany przyjmując, że skróty wersji symboli są "
"wystarczająco wiarygodne."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Using the B<MODULE_INIT_IGNORE_VERMAGIC> flag indicates that the "
"\"vermagic\" string is to be ignored, and the "
"B<MODULE_INIT_IGNORE_MODVERSIONS> flag indicates that the symbol version "
"hashes are to be ignored.  If the kernel is built to permit forced loading "
"(i.e., configured with B<CONFIG_MODULE_FORCE_LOAD>), then loading continues, "
"otherwise it fails with the error B<ENOEXEC> as expected for malformed "
"modules."
msgstr ""
"Użycie znacznika B<MODULE_INIT_IGNORE_VERMAGIC> wskazuje, że łańcuch "
"\\[Bq]vermagic\\[rq] ma być zignorowany, natomiast znacznik "
"B<MODULE_INIT_IGNORE_MODVERSIONS> wskazuje, że skróty wersji symboli mają "
"być zignorowane. Jeśli jądro zbudowano w sposób umożliwiający wymuszone "
"ładowanie modułów (tj. skonfigurowano je z opcją "
"B<CONFIG_MODULE_FORCE_LOAD>), to ładowanie będzie kontynuowane, w przeciwnym "
"przypadku zawiedzie z błędem B<ENOEXEC>, jak jest to spodziewane wobec "
"nieprawidłowych modułów."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If the kernel was build with B<CONFIG_MODULE_DECOMPRESS>, the in-kernel "
"decompression feature can be used.  User-space code can check if the kernel "
"supports decompression by reading the I</sys/module/compression> attribute.  "
"If the kernel supports decompression, the compressed file can directly be "
"passed to B<finit_module>()  using the B<MODULE_INIT_COMPRESSED_FILE> flag.  "
"The in-kernel module decompressor supports the following compression "
"algorithms:"
msgstr ""
"Jeśli jądro zbudowano z opcją B<CONFIG_MODULE_DECOMPRESS>, można skorzystać "
"z funkcji dekompresji w jądrze. Kod w przestrzeni użytkownika może "
"sprawdzić, czy jądro obsługuje dekompresję odczytując atrybut I</sys/module/"
"compression>. Jeśli jądro obsługuje dekompresję, skompresowany plik można "
"podać bezpośrednio do B<finit_module>() za pomocą znacznika "
"B<MODULE_INIT_COMPRESSED_FILE>. Moduł dekompresji w jądrze obsługuje "
"następujące algorytmy kompresji:"

#. type: IP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<gzip> (since Linux 5.17)"
msgstr "I<gzip> (od Linuksa 5.17)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<xz> (since Linux 5.17)"
msgstr "I<xz> (od Linuksa 5.17)"

#.  commit 169a58ad824d896b9e291a27193342616e651b82
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<zstd> (since Linux 6.2)"
msgstr "I<zstd> (od Linuksa 6.2)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The kernel only implements a single decompression method.  This is selected "
"during module generation accordingly to the compression method chosen in the "
"kernel configuration."
msgstr ""
"Jądro implementuje jedynie pojedynczą metodę dekompresji. Wybiera się ją "
"przy generowaniu modułu, zgodnie z metodą kompresji użytą w konfiguracji "
"jądra."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, these system calls return 0.  On error, -1 is returned and "
"I<errno> is set to indicate the error."
msgstr ""
"W przypadku powodzenia, te wywołania zwracają 0. W razie wystąpienia błędu "
"zwracane jest -1 i ustawiane I<errno> wskazując błąd."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADMSG> (since Linux 3.7)"
msgstr "B<EBADMSG> (od Linuksa 3.7)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Module signature is misformatted."
msgstr "Podpis modułu jest nieprawidłowo sformatowany."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBUSY>"
msgstr "B<EBUSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Timeout while trying to resolve a symbol reference by this module."
msgstr ""
"Nastąpiło przeterminowanie, przy próbie rozwiązania referencji symbolu przez "
"ten moduł."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An address argument referred to a location that is outside the process's "
"accessible address space."
msgstr ""
"Argument adresu odnosi się do położenia, znajdującego się poza dostępną "
"przestrzenią adresową procesu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOKEY> (since Linux 3.7)"
msgstr "B<ENOKEY> (od Linuksa 3.7)"

#.  commit 48ba2462ace6072741fd8d0058207d630ce93bf1
#.  commit 1d0059f3a468825b5fc5405c636a2f6e02707ffa
#.  commit 106a4ee258d14818467829bf0e12aeae14c16cd7
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Module signature is invalid or the kernel does not have a key for this "
"module.  This error is returned only if the kernel was configured with "
"B<CONFIG_MODULE_SIG_FORCE>; if the kernel was not configured with this "
"option, then an invalid or unsigned module simply taints the kernel."
msgstr ""
"Podpis modułu jest nieprawidłowy albo jądro nie ma klucza do tego modułu. "
"Błąd jest zwracany tylko, jeśli jądro skonfigurowano z opcją "
"B<CONFIG_MODULE_SIG_FORCE>; w przeciwnym wypadku nieprawidłowy lub "
"niepodpisany moduł jedynie zatruwa (taint) jądro."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Out of memory."
msgstr "Brak pamięci."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The caller was not privileged (did not have the B<CAP_SYS_MODULE> "
"capability), or module loading is disabled (see I</proc/sys/kernel/"
"modules_disabled> in B<proc>(5))."
msgstr ""
"Wywołujący nie był uprzywilejowany (nie posiadał przywileju (ang. "
"capability) B<CAP_SYS_MODULE>) lub ładowanie modułów jest wyłączone (zob.  "
"I</proc/sys/kernel/modules_disabled> w B<proc>(5))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following errors may additionally occur for B<init_module>():"
msgstr "Mogą wystąpić następujące dodatkowe błędy dla B<init_module>():"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "A module with this name is already loaded."
msgstr "Załadowano już moduł o takiej nazwie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#.  .TP
#.  .BR EINVAL " (Linux 2.4 and earlier)"
#.  Some
#.  .I image
#.  slot is filled in incorrectly,
#.  .I image\->name
#.  does not correspond to the original module name, some
#.  .I image\->deps
#.  entry does not correspond to a loaded module,
#.  or some other similar inconsistency.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<param_values> is invalid, or some part of the ELF image in I<module_image> "
"contains inconsistencies."
msgstr ""
"I<param_values> jest nieprawidłowe albo pewne fragmenty obrazu "
"I<module_image> są niespójne."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOEXEC>"
msgstr "B<ENOEXEC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The binary image supplied in I<module_image> is not an ELF image, or is an "
"ELF image that is invalid or for a different architecture."
msgstr ""
"Obraz binarny podany w I<module_image> nie jest obrazem ELF albo jest "
"nieprawidłowym lub przeznaczonym do innej architektury obrazem ELF."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following errors may additionally occur for B<finit_module>():"
msgstr "Mogą wystąpić następujące dodatkowe błędy dla B<finit_module>():"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The file referred to by I<fd> is not opened for reading."
msgstr "Plik, do którego odnosi się I<fd> , nie jest otwarty do odczytu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFBIG>"
msgstr "B<EFBIG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The file referred to by I<fd> is too large."
msgstr "Plik, do którego odnosi się I<fd>, jest zbyt duży."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<flags> is invalid."
msgstr "Znaczniki I<flags> są nieprawidłowe."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The decompressor sanity checks failed, while loading a compressed module "
"with flag B<MODULE_INIT_COMPRESSED_FILE> set."
msgstr ""
"Przy ładowaniu spakowanego modułu ze znacznikiem "
"B<MODULE_INIT_COMPRESSED_FILE>, zawiodło sprawdzanie poprawności "
"przeprowadzone przez dekompresor."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<fd> does not refer to an open file."
msgstr "I<fd> nie odnosi się do otwartego pliku."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP> (since Linux 5.17)"
msgstr "B<EOPNOTSUPP> (od Linuksa 5.17)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The flag B<MODULE_INIT_COMPRESSED_FILE> is set to load a compressed module, "
"and the kernel was built without B<CONFIG_MODULE_DECOMPRESS>."
msgstr ""
"Ustawiono znacznik B<MODULE_INIT_COMPRESSED_FILE> w celu załadowania "
"spakowanego modułu, lecz jądro zbudowano bez B<CONFIG_MODULE_DECOMPRESS>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ETXTBSY> (since Linux 4.7)"
msgstr "B<ETXTBSY> (od Linuksa 4.7)"

#.  commit 39d637af5aa7577f655c58b9e55587566c63a0af
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The file referred to by I<fd> is opened for read-write."
msgstr "Plik, do którego odnosi się I<fd>, jest otwarty do odczytu i zapisu."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In addition to the above errors, if the module's I<init> function is "
"executed and returns an error, then B<init_module>()  or B<finit_module>()  "
"fails and I<errno> is set to the value returned by the I<init> function."
msgstr ""
"Oprócz powyższych błędów, jeśli wykonana funkcja I<init> modułu zwróci błąd, "
"to B<init_module>() lub B<finit_module>() zawiodą, a I<errno> zostanie "
"ustawione na wartość zwróconą przez funkcję I<init>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<finit_module>()"
msgstr "B<finit_module>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Linux 3.8."
msgstr "Linux 3.8."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<init_module>()  system call is not supported by glibc.  No declaration "
"is provided in glibc headers, but, through a quirk of history, glibc "
"versions before glibc 2.23 did export an ABI for this system call.  "
"Therefore, in order to employ this system call, it is (before glibc 2.23) "
"sufficient to manually declare the interface in your code; alternatively, "
"you can invoke the system call using B<syscall>(2)."
msgstr ""
"Wywołanie systemowe B<init_module>() nie jest obsługiwane przez glibc. W "
"nagłówkach glibc nie ma jego deklaracji, ale z powodów pewnych zaszłości "
"historycznych wersje glibc przed glibc 2.23 eksportowały ABI dla tego "
"wywołania systemowego. Z tego powodu, aby go użyć wystarczy (przed glibc "
"2.23) manualnie zadeklarować interfejs w swoim kodzie; alternatywnie można "
"wywołać to wywołanie systemowe za pomocą B<syscall>(2)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux 2.4 and earlier"
msgstr "Linux 2.4 i wcześniejsze"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In Linux 2.4 and earlier, the B<init_module>()  system call was rather "
"different:"
msgstr ""
"W Linuksie 2.4 i wcześniejszych wywołanie systemowe B<init_module>() było "
"wyraźnie odmienne:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<#include E<lt>linux/module.hE<gt>>"
msgstr "B<#include E<lt>linux/module.hE<gt>>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B< int init_module(const char *>I<name>B<, struct module *>I<image>B<);>"
msgstr ""
"B< int init_module(const char *>I<name>B<, struct module *>I<image>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"(User-space applications can detect which version of B<init_module>()  is "
"available by calling B<query_module>(); the latter call fails with the error "
"B<ENOSYS> on Linux 2.6 and later.)"
msgstr ""
"(Aplikacje w przestrzeni użytkownika mogą wykryć dostępną wersję "
"B<init_module>() przez wywołanie B<query_module>(); to ostatnie wywołanie "
"zawiedzie z błędem B<ENOSYS> w Linuksie 2.6 i późniejszych)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The older version of the system call loads the relocated module image "
"pointed to by I<image> into kernel space and runs the module's I<init> "
"function.  The caller is responsible for providing the relocated image "
"(since Linux 2.6, the B<init_module>()  system call does the relocation)."
msgstr ""
"Starsza wersja tego wywołania systemowego ładowała przesunięty obraz jądra, "
"na który wskazywało I<image> do przestrzeni jądra i uruchamiało funkcję "
"I<init> modułu. To wywołujący był odpowiedzialny za udostępnienie "
"przesuniętego obrazu (od Linuksa 2.6, to wywołanie systemowe "
"B<init_module>() dokonuje przesunięcia)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The module image begins with a module structure and is followed by code and "
"data as appropriate.  Since Linux 2.2, the module structure is defined as "
"follows:"
msgstr ""
"Obraz modułu rozpoczyna się od struktury modułu, po której następują, "
"odpowiednio, kod i dane. Od Linuksa 2.2 struktura modułu jest zdefiniowana "
"następująco:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct module {\n"
"    unsigned long         size_of_struct;\n"
"    struct module        *next;\n"
"    const char           *name;\n"
"    unsigned long         size;\n"
"    long                  usecount;\n"
"    unsigned long         flags;\n"
"    unsigned int          nsyms;\n"
"    unsigned int          ndeps;\n"
"    struct module_symbol *syms;\n"
"    struct module_ref    *deps;\n"
"    struct module_ref    *refs;\n"
"    int                 (*init)(void);\n"
"    void                (*cleanup)(void);\n"
"    const struct exception_table_entry *ex_table_start;\n"
"    const struct exception_table_entry *ex_table_end;\n"
"#ifdef __alpha__\n"
"    unsigned long gp;\n"
"#endif\n"
"};\n"
msgstr ""
"struct module {\n"
"    unsigned long         size_of_struct;\n"
"    struct module        *next;\n"
"    const char           *name;\n"
"    unsigned long         size;\n"
"    long                  usecount;\n"
"    unsigned long         flags;\n"
"    unsigned int          nsyms;\n"
"    unsigned int          ndeps;\n"
"    struct module_symbol *syms;\n"
"    struct module_ref    *deps;\n"
"    struct module_ref    *refs;\n"
"    int                 (*init)(void);\n"
"    void                (*cleanup)(void);\n"
"    const struct exception_table_entry *ex_table_start;\n"
"    const struct exception_table_entry *ex_table_end;\n"
"#ifdef __alpha__\n"
"    unsigned long gp;\n"
"#endif\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"All of the pointer fields, with the exception of I<next> and I<refs>, are "
"expected to point within the module body and be initialized as appropriate "
"for kernel space, that is, relocated with the rest of the module."
msgstr ""
"Wszystkie pola wskazujące, oprócz I<next> i I<refs>, powinny wskazywać na "
"adresy w ciele modułu i zostać zainicjalizowane odpowiednio dla przestrzeni "
"adresowej jądra, tzn. przesunięte wraz z resztą modułu."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Information about currently loaded modules can be found in I</proc/modules> "
"and in the file trees under the per-module subdirectories under I</sys/"
"module>."
msgstr ""
"Informacje o obecnie załadowanych modułach są dostępne w I</proc/modules> "
"oraz w strukturze plików zawartej w podkatalogach, przypisanych "
"poszczególnym modułom, w katalogu I</sys/module>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See the Linux kernel source file I<include/linux/module.h> for some useful "
"background information."
msgstr ""
"Więcej przydatnych informacji \\[Bq]od kuchni\\[rq] znajduje się w pliku "
"źródeł jądra Linux I<include/linux/module.h>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<create_module>(2), B<delete_module>(2), B<query_module>(2), B<lsmod>(8), "
"B<modprobe>(8)"
msgstr ""
"B<create_module>(2), B<delete_module>(2), B<query_module>(2), B<lsmod>(8), "
"B<modprobe>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 grudnia 2022 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "WERSJE"

#. type: Plain text
#: debian-bookworm
msgid "B<finit_module>()  is available since Linux 3.8."
msgstr "B<finit_module>() jest dostępne od Linuksa 3.8."

#. type: Plain text
#: debian-bookworm
msgid "B<init_module>()  and B<finit_module>()  are Linux-specific."
msgstr "B<init_module>() i B<finit_module>() są typowo linuksowe."

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
