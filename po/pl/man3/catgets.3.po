# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Adam Byrtek <alpha@irc.pl>, 1998.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2001.
# Robert Luberda <robert@debian.org>, 2013, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2014, 2016, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-12-06 17:55+0100\n"
"PO-Revision-Date: 2024-12-12 08:10+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "catgets"
msgstr "catgets"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 czerwca 2024 r."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "catgets - get message from a message catalog"
msgstr "catgets - pobiera komunikat z katalogu komunikatów"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>nl_types.hE<gt>>\n"
msgstr "B<#include E<lt>nl_types.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *catgets(nl_catd >I<catalog>B<, int >I<set_number>B<, int >I<message_number>B<,>\n"
"B<              const char *>I<message>B<);>\n"
msgstr ""
"B<char *catgets(nl_catd >I<katalog>B<, int >I<numer_zbioru>B<, int >I<numer_komunikatu>B<,>\n"
"B<              const char *>I<komunikat>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<catgets>()  reads the message I<message_number>, in set I<set_number>, "
"from the message catalog identified by I<catalog>, where I<catalog> is a "
"catalog descriptor returned from an earlier call to B<catopen>(3).  The "
"fourth argument, I<message>, points to a default message string which will "
"be returned by B<catgets>()  if the identified message catalog is not "
"currently available.  The message-text is contained in an internal buffer "
"area and should be copied by the application if it is to be saved or "
"modified.  The return string is always terminated with a null byte "
"(\\[aq]\\[rs]0\\[aq])."
msgstr ""
"Funkcja B<catgets>() odczytuje komunikat o numerze I<numer_komunikatu> z "
"zestawu I<numer_zestawu> z katalogu komunikatów identyfikowanego przez "
"deskryptor I<katalog>, zwrócony wcześniej przez B<catopen>(3). Czwarty "
"argument I<komunikat> wskazuje na domyślny komunikat, który zostanie "
"zwrócony przez B<catgets>(), jeśli katalog komunikatów nie jest obecnie "
"dostępny. Tekst komunikatu znajduje się w wewnętrznym buforze, więc jeśli "
"aplikacja chce go przechować lub zmodyfikować, powinna go najpierw "
"skopiować. Zwracamy łańcuch zawsze kończy się bajtem null "
"(\\[Bq]\\[rs]0\\[rq])."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<catgets>()  returns a pointer to an internal buffer area "
"containing the null-terminated message string.  On failure, B<catgets>()  "
"returns the value I<message>."
msgstr ""
"Jeśli funkcja B<catgets>() zakończy się pomyślnie, zwraca wskaźnik do "
"obszaru bufora zawierającego łańcuch z komunikatem zakończonym bajtem null. "
"Jeśli funkcja B<catgets>() zawiedzie, zwraca wartość I<komunikat>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<catgets>()"
msgstr "B<catgets>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-bezpieczne"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<catgets>()  function is available only in libc.so.4.4.4c and above."
msgstr "Funkcja B<catgets>() jest dostępna tylko w libc.so.4.4.4c i wyższych."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Jan 1987 X/Open Portability Guide specifies a more subtle error return: "
"I<message> is returned if the message catalog specified by I<catalog> is not "
"available, while an empty string is returned when the message catalog is "
"available but does not contain the specified message.  These two possible "
"error returns seem to be discarded in SUSv2 in favor of always returning "
"I<message>."
msgstr ""
"Dokument X/Open Portability Guide ze stycznia 1987 określa bardziej "
"wyrafinowany sposób zwracania wartości w razie wystąpienia błędu: "
"I<komunikat> jest zwracany, jeśli katalog wyznaczony przez I<katalog> jest "
"niedostępny. Natomiast gdy katalog jest dostępny, ale nie zawiera wskazanego "
"komunikatu, zwracany jest pusty łańcuch.  Zasada zwracania dwóch różnych "
"wartości w przypadkach błędów została jednak porzucona w SUSv2 na rzecz "
"zwracania zawsze wartości I<komunikat>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<catopen>(3), B<setlocale>(3)"
msgstr "B<catopen>(3), B<setlocale>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"B<catgets>()  reads the message I<message_number>, in set I<set_number>, "
"from the message catalog identified by I<catalog>, where I<catalog> is a "
"catalog descriptor returned from an earlier call to B<catopen>(3).  The "
"fourth argument, I<message>, points to a default message string which will "
"be returned by B<catgets>()  if the identified message catalog is not "
"currently available.  The message-text is contained in an internal buffer "
"area and should be copied by the application if it is to be saved or "
"modified.  The return string is always terminated with a null byte "
"(\\[aq]\\e0\\[aq])."
msgstr ""
"Funkcja B<catgets>() odczytuje komunikat o numerze I<numer_komunikatu> z "
"zestawu I<numer_zestawu> z katalogu komunikatów identyfikowanego przez "
"deskryptor I<katalog>, zwrócony wcześniej przez B<catopen>(3). Czwarty "
"argument I<komunikat> wskazuje na domyślny komunikat, który zostanie "
"zwrócony przez B<catgets>(), jeśli katalog komunikatów nie jest obecnie "
"dostępny. Tekst komunikatu znajduje się w wewnętrznym buforze, więc jeśli "
"aplikacja chce go przechować lub zmodyfikować, powinna go najpierw "
"skopiować. Zwracamy łańcuch zawsze kończy się bajtem null (\\[Bq]\\e0\\[rq])."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<catgets>()  function is available only in libc.so.4.4.4c and above.  "
"The Jan 1987 X/Open Portability Guide specifies a more subtle error return: "
"I<message> is returned if the message catalog specified by I<catalog> is not "
"available, while an empty string is returned when the message catalog is "
"available but does not contain the specified message.  These two possible "
"error returns seem to be discarded in SUSv2 in favor of always returning "
"I<message>."
msgstr ""
"Funkcja B<catgets>() jest dostępna jedynie w libc.so.4.4.4c i wyższych. "
"Dokument X/Open Portability Guide ze stycznia 1987 określa bardziej "
"wyrafinowany sposób zwracania wartości w razie wystąpienia błędu: "
"I<komunikat> jest zwracany, jeśli katalog wyznaczony przez I<katalog> jest "
"niedostępny. Natomiast gdy katalog jest dostępny, ale nie zawiera wskazanego "
"komunikatu, zwracany jest pusty łańcuch.  Zasada zwracania dwóch różnych "
"wartości w przypadkach błędów została jednak porzucona w SUSv2 na rzecz "
"zwracania zawsze wartości I<komunikat>."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
