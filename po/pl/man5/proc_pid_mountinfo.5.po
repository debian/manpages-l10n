# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1998.
# Robert Luberda <robert@debian.org>, 2006, 2012.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2016, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-12-06 18:09+0100\n"
"PO-Revision-Date: 2024-03-31 18:15+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_mountinfo"
msgstr "proc_pid_mountinfo"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maja 2024 r."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/mountinfo - mount information"
msgstr "/proc/pid/mountinfo - informacje o montowaniu"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</mountinfo> (since Linux 2.6.26)"
msgstr "I</proc/>pidI</mountinfo> (od Linuksa 2.6.26)"

#.  This info adapted from Documentation/filesystems/proc.txt
#.  commit 2d4d4864ac08caff5c204a752bd004eed4f08760
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file contains information about mounts in the process's mount namespace "
"(see B<mount_namespaces>(7)).  It supplies various information (e.g., "
"propagation state, root of mount for bind mounts, identifier for each mount "
"and its parent) that is missing from the (older)  I</proc/>pidI</mounts> "
"file, and fixes various other problems with that file (e.g., "
"nonextensibility, failure to distinguish per-mount versus per-superblock "
"options)."
msgstr ""
"Plik zawiera informacje o montowaniach w przestrzeni nazw montowań procesu "
"(zob. B<mount_namespaces>(7)). Zapewnia wiele informacji (np. stan "
"propagacji, korzeń montowania w przypadku montowań za pomocą podpięcia, "
"identyfikator każdego montowania i jego rodzica), których brakuje w "
"(starszym) pliku I</proc/>pidI</mounts> i poprawia różne inne problemy, na "
"jakie tamten plik cierpi (np. nierozszerzalność, niemożność rozróżnienia "
"opcji odnoszących się do montowania, od tych dotyczących superbloku)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The file contains lines of the form:"
msgstr "Plik posiada wiersze w postaci:"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - ext3 /dev/root rw,errors=continue\n"
"(1)(2)(3)   (4)   (5)      (6)      (7)   (8) (9)   (10)         (11)\n"
msgstr ""
"36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - ext3 /dev/root rw,errors=continue\n"
"(1)(2)(3)   (4)   (5)      (6)      (7)   (8) (9)   (10)         (11)\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The numbers in parentheses are labels for the descriptions below:"
msgstr "Liczby w nawiasach są etykietami poniższych opisów:"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(1)"
msgstr "(1)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "mount ID: a unique ID for the mount (may be reused after B<umount>(2))."
msgstr ""
"ID montowania: unikatowy identyfikator montowania (może zostać użyty "
"ponownie po wykonaniu B<umount>(2))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(2)"
msgstr "(2)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"parent ID: the ID of the parent mount (or of self for the root of this mount "
"namespace's mount tree)."
msgstr ""
"ID rodzica: identyfikator rodzica (lub siebie samego, jeśli montowanie "
"znajduje się w korzeniu drzewa montowań tej przestrzeni nazw montowań)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If a new mount is stacked on top of a previous existing mount (so that it "
"hides the existing mount) at pathname P, then the parent of the new mount is "
"the previous mount at that location.  Thus, when looking at all the mounts "
"stacked at a particular location, the top-most mount is the one that is not "
"the parent of any other mount at the same location.  (Note, however, that "
"this top-most mount will be accessible only if the longest path subprefix of "
"P that is a mount point is not itself hidden by a stacked mount.)"
msgstr ""
"Jeśli nowe montowanie jest ułożone na poprzednio istniejącym punkcie "
"montowania (czym go przesłania), ze ścieżką P, to rodzicem nowego montowania "
"jest poprzednie montowanie w tym położeniu. Z tego względu, przy sprawdzaniu "
"wszystkich montowań ułożonych w danej lokalizacji, najwyższe montowanie jest "
"tym, które nie jest rodzicem żadnego z pozostałych montowań w tej "
"lokalizacji (proszę jednak zauważyć, że to najwyższe montowanie będzie "
"dostępne jedynie wtedy, gdy najdłuższy podprzedrostek P jest tym punktem "
"montowania, które nie jest ukryte przez ułożone montowania)."

#.  Miklos Szeredi, Nov 2017: The hidden one is the initramfs, I believe
#.  mtk: In the initial mount namespace, this hidden ID has the value 0
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the parent mount lies outside the process's root directory (see "
"B<chroot>(2)), the ID shown here won't have a corresponding record in "
"I<mountinfo> whose mount ID (field 1) matches this parent mount ID (because "
"mounts that lie outside the process's root directory are not shown in "
"I<mountinfo>).  As a special case of this point, the process's root mount "
"may have a parent mount (for the initramfs filesystem) that lies outside the "
"process's root directory, and an entry for that mount will not appear in "
"I<mountinfo>."
msgstr ""
"Jeśli montowanie macierzyste leży poza głównym katalogiem procesu (zob. "
"B<chroot>(2)), to pokazywany tu identyfikator nie będzie miał odpowiedniego "
"wpisu w I<mountinfo>, którego ID montowania (pole 1) pasowałby do "
"identyfikatora montowania macierzystego (ponieważ montowania, które leżą "
"poza głównym katalogiem procesu nie są pokazywane w I<mountinfo>). Jako "
"przypadek specjalny w tym punkcie, korzeń montowania procesu może mieć "
"montowanie macierzyste (jak w przypadku systemu plików initramfs) położone "
"poza katalogiem głównym procesu, a wpis dla tego montowania nie pojawi się w "
"I<mountinfo>."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(3)"
msgstr "(3)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"major:minor: the value of I<st_dev> for files on this filesystem (see "
"B<stat>(2))."
msgstr ""
"główny:poboczny: wartość pola I<st_dev> (patrz B<stat>(2)) dla plików w "
"systemie plików."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(4)"
msgstr "(4)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"root: the pathname of the directory in the filesystem which forms the root "
"of this mount."
msgstr ""
"root: ścieżka katalogu w systemie plików, która tworzy korzeń (katalog "
"główny) tego montowania."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(5)"
msgstr "(5)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"mount point: the pathname of the mount point relative to the process's root "
"directory."
msgstr ""
"punkt montowania: ścieżka punktu montowania w odniesieniu do katalogu "
"głównego procesów."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(6)"
msgstr "(6)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "mount options: per-mount options (see B<mount>(2))."
msgstr ""
"opcje montowania: opcje montowania dla każdego montowania (patrz "
"B<mount>(2))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(7)"
msgstr "(7)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"optional fields: zero or more fields of the form \"tag[:value]\"; see below."
msgstr ""
"pola opcjonalne: zero lub więcej pól w postaci \\[Bq]znacznik[:"
"wartość]\\[rq]; zob. niżej."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(8)"
msgstr "(8)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "separator: the end of the optional fields is marked by a single hyphen."
msgstr "separator: koniec pól opcjonalnych jest oznaczony pojedynczym dywizem."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(9)"
msgstr "(9)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "filesystem type: the filesystem type in the form \"type[.subtype]\"."
msgstr ""
"typ systemu plików: typ systemu plików w postaci \\[Bq]typ[.podtyp]\\[rq]."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(10)"
msgstr "(10)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "mount source: filesystem-specific information or \"none\"."
msgstr ""
"źródło montowania: informacja zależna od systemu plików lub \\[Bq]none\\[rq]."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(11)"
msgstr "(11)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "super options: per-superblock options (see B<mount>(2))."
msgstr "super opcje: opcje dla superbloku (patrz B<mount>(2))."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Currently, the possible optional fields are I<shared>, I<master>, "
"I<propagate_from>, and I<unbindable>.  See B<mount_namespaces>(7)  for a "
"description of these fields.  Parsers should ignore all unrecognized "
"optional fields."
msgstr ""
"Obecnie, możliwe pola opcjonalne to: I<shared>, I<master>, I<propagate_from> "
"i I<unbindable>. Opis tych pól znajduje się w podręczniku "
"B<mount_namespaces>(7). Wszelkie nierozpoznane pola opcjonalne powinny być "
"ignorowane przy ich przetwarzaniu."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For more information on mount propagation see I<Documentation/filesystems/"
"sharedsubtree.rst> (or I<Documentation/filesystems/sharedsubtree.txt> before "
"Linux 5.8)  in the Linux kernel source tree."
msgstr ""
"Aby dowiedzieć się więcej o propagacji montowań, proszę zapoznać się z "
"I<Documentation/filesystems/sharedsubtree.rst> (lub I<Documentation/"
"filesystems/sharedsubtree.txt> przed Linuksem 5.8) w drzewie źródeł jądra "
"Linux."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-11-24"
msgstr "24 listopada 2023 r."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
