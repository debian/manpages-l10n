# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Michał Kułach <michal.kulach@gmail.com>, 2014, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-06-27 19:31+0200\n"
"PO-Revision-Date: 2024-02-25 12:08+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: Dd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "$Mdocdate: February 10 2020 $"
msgstr "$Mdocdate: February 10 2020 $"

#. type: Dt
#: debian-bookworm debian-unstable
#, no-wrap
msgid "INETD 8"
msgstr "INETD 8"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm inetd>, E<.Nm inetd.conf>"
msgstr "E<.Nm inetd>, E<.Nm inetd.conf>"

#. type: Nd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "internet super-server"
msgstr "superserwer internetowy"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm inetd> E<.Op Fl d> E<.Op Fl E> E<.Op Fl i> E<.Op Fl l> E<.Op Fl q Ar "
"length> E<.Op Fl R Ar rate> E<.Op Ar configuration_file>"
msgstr ""
"E<.Nm inetd> E<.Op Fl d> E<.Op Fl E> E<.Op Fl i> E<.Op Fl l> E<.Op Fl q Ar "
"długość> E<.Op Fl R Ar częstość> E<.Op Ar plik-konfiguracyjny>"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm inetd> listens for connections on certain internet sockets.  When a "
"connection is found on one of its sockets, it decides what service the "
"socket corresponds to, and invokes a program to service the request.  After "
"the program is finished, it continues to listen on the socket (except in "
"some cases which will be described below).  Essentially, E<.Nm inetd> allows "
"running one daemon to invoke several others, reducing load on the system."
msgstr ""
"E<.Nm Inetd> nasłuchuje połączeń na określonych gniazdach internetowych. Gdy "
"na jednym z gniazd zaistnieje połączenie, decyduje on, jakiej usłudze to "
"gniazdo odpowiada i wywołuje program, który obsłuży żądanie. Po zakończeniu "
"programu, inetd kontynuuje nasłuchiwania gniazda (poza niektórymi wypadkami, "
"opisanymi poniżej). Ogólnie, E<.Nm inetd> umożliwia używanie jednego demona "
"do wywoływania wielu innych, zmniejszając wymagane obciążenie systemu."

#
#. type: Plain text
#: debian-bookworm debian-unstable
msgid "The options are as follows:"
msgstr "Dostępne są następujące opcje:"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl d"
msgstr "Fl d"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Turns on debugging."
msgstr "Włącza debugowanie."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl E"
msgstr "Fl E"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Prevents E<.Nm inetd> from laundering the environment.  Without this option "
"a selection of potentially harmful environment variables, including E<.Pa "
"PATH>, will be removed and not inherited by services."
msgstr ""
"Zapobiega ignorowaniu przez E<.Nm inetd> środowiska. Bez tej opcji część "
"potencjalnie niebezpiecznych zmiennych środowiskowych, w tym E<.Pa PATH> "
"zostanie usuniętych i nie będzie dziedziczone przez usługi."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl i"
msgstr "Fl i"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Makes the program not daemonize itself."
msgstr "Program nie przechodzi w tryb demona."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl l"
msgstr "Fl l"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Turns on libwrap connection logging and access control.  Internal services "
"cannot be wrapped.  When enabled, E<.Pa /usr/sbin/tcpd> is silently not "
"executed even if present in E<.Pa /etc/inetd.conf> and instead libwrap is "
"called directly by inetd."
msgstr ""
"Włącza logowanie połączeń i kontrolę dostępu przez libwrap. Usługi "
"wewnętrzne nie mogą być opakowywane. Gdy opcja jest aktywna, po cichu nie "
"dokonuje się wykonania E<.Pa /usr/sbin/tcpd> nawet gdy jest ono obecne w "
"pliku E<.Pa /etc/inetd.conf>, a w zamian inetd bezpośrednio wywołuje libwrap."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl q Ar length"
msgstr "Fl q Ar długość"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Specify the length of the E<.Xr listen 2> connections queue; the default is "
"128."
msgstr "Określa długość kolejki połączeń E<.Xr listen 2> (domyślnie 128)."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl R Ar rate"
msgstr "Fl R Ar częstość"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Specify the maximum number of times a service can be invoked in one minute; "
"the default is 256.  If a service exceeds this limit, E<.Nm> will log the "
"problem and stop servicing requests for the specific service for ten "
"minutes.  See also the wait/nowait configuration fields below."
msgstr ""
"Określa maksymalną częstość, z jaką usługa może być wywołana w ciągu minuty "
"(domyślnie 256). Jeśli usługa przekroczy ten limit, E<.Nm> zapisze problem "
"do dziennika i zaprzestanie obsługi żądań dla danej usługi przez dziesięć "
"minut. Więcej informacji można przeczytać też w poniższym opisie pól wait/"
"nowait."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Upon execution, E<.Nm inetd> reads its configuration information from a "
"configuration file which, by default, is E<.Pa /etc/inetd.conf>.  There must "
"be an entry for each field of the configuration file, with entries for each "
"field separated by a tab or a space.  Comments are denoted by a E<.Dq #> at "
"the beginning of a line.  The fields of the configuration file are as "
"follows:"
msgstr ""
"Podczas uruchamiania, E<.Nm inetd> odczytuje swoją konfigurację z pliku "
"konfiguracyjnego, którym domyślnie jest E<.Pa /etc/inetd.conf>. Musi tam być "
"wpis dla każdego pola pliku konfiguracyjnego, z poszczególnymi wpisami dla "
"danego pola; wpisy są oddzielane znakiem tabulacji lub spacji. Komentarze są "
"zaznaczane przez E<.Dq #> na początku wiersza. Pola pliku konfiguracyjnego "
"są następujące:"

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"service name\n"
"socket type\n"
"protocol[,sndbuf=size][,rcvbuf=size]\n"
"wait/nowait[.max]\n"
"user[.group] or user[:group]\n"
"server program\n"
"server program arguments\n"
msgstr ""
"nazwa usługi (service name)\n"
"rodzaj gniazda (socket type)\n"
"protokół[,sndbuf=rozmiar][,rcvbuf=rozmiar]\n"
"\t\t\t\t\t(protocol[,sndbuf=size][,rcvbuf=size])\n"
"określenie, czy usługa ma \"zwlekać\" (wait/nowait[.max])\n"
"użytkownik[.grupa] lub użytkownik[:grupa]\n"
"\t\t\t\t\t(user[.group] lub user[:group])\n"
"program serwera (server program)\n"
"argumenty programu serwera (server program arguments)\n"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"To specify a Sun-RPC based service, the entry would contain these fields."
msgstr ""
"Aby podać usługę opartą o E<.Em Sun-RPC > , wpis powinien zawierać te pola:"

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"service name/version\n"
"socket type\n"
"rpc/protocol[,sndbuf=size][,rcvbuf=size]\n"
"wait/nowait[.max]\n"
"user[.group] or user[:group]\n"
"server program\n"
"server program arguments\n"
msgstr ""
"nazwa usługi/wersja (service name/version)\n"
"rodzaj gniazda (socket type)\n"
"rpc/protokół[,sndbuf=rozmiar][,rcvbuf=rozmiar]\n"
" \t\t\t\t\t(rpc/protocol[,sndbuf=size][,rcvbuf=size])\n"
"zwłoka (wait/nowait[.max])\n"
"użytkownik[.grupa] lub użytkownik[:grupa]\n"
"\t\t\t\t\t(user[.group] or user[:group])\n"
"program serwera (server program)\n"
"argumenty programu serwera (server program arguments)\n"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"For internet services, the first field of the line may also have a host "
"address specifier prefixed to it, separated from the service name by a "
"colon.  If this is done, the string before the colon in the first field "
"indicates what local address E<.Nm> should use when listening for that "
"service.  Multiple local addresses can be specified on the same line, "
"separated by commas.  Numeric IP addresses in dotted-quad notation can be "
"used as well as symbolic hostnames.  Symbolic hostnames are looked up using "
"E<.Fn getaddrinfo>.  If a hostname has multiple address mappings, inetd "
"creates a socket to listen on each address."
msgstr ""
"W przypadku usług internetowych, pierwsze pole w wierszu może posiadać "
"również wyrażenie określające adres hosta, będące przedrostkiem oddzielonym "
"dwukropkiem. Łańcuch będący w pierwszym polu przed dwukropkiem określa "
"wówczas którego adresu lokalnego ma użyć E<.Nm> przy nasłuchiwaniu dla tej "
"usługi. W jednym wierszu można podać wiele adresów lokalnych, oddzielonych "
"dwukropkiem. Można wpisać adresy w postaci numeru IP (cztery liczby "
"oddzielone kropkami) lub nazw symbolicznych domen. Nazwy domenowe są "
"sprawdzane za pomocą E<.Fn getaddrinfo>. Jeśli dana nazwa ma przypisanych "
"kilka adresów, inetd tworzy gniazda do nasłuchu na każdym adresie."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The single character E<.Dq \\&*> indicates E<.Dv INADDR_ANY>, meaning E<.Dq "
"all local addresses>.  To avoid repeating an address that occurs frequently, "
"a line with a host address specifier and colon, but no further fields, "
"causes the host address specifier to be remembered and used for all further "
"lines with no explicit host specifier (until another such line or the end of "
"the file).  A line"
msgstr ""
"Pojedynczy znak  E<.Dq \\&*> oznacza E<.Dv INADDR_ANY>, czyli E<.Dq "
"wszystkie adresy lokalne>. Aby zapobiec powtarzaniu adresów występujących "
"wielokrotnie, wiersz z wyrażeniami określającymi adresy hosta i dwukropkiem, "
"bez kolejnych pól, powoduje że adresy są zapamiętywane i używane do "
"wszystkich kolejnych wierszy bez bezpośrednio podanego wyrażenia "
"określającego adres (do momentu napotkania kolejnego tak skonstruowanego "
"wiersza lub dotarcia do końca pliku). Wiersz"

#. type: Dl
#: debian-bookworm debian-unstable
#, no-wrap
msgid "*:"
msgstr "*:"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"is implicitly provided at the top of the file; thus, traditional "
"configuration files (which have no host address specifiers) will be "
"interpreted in the traditional manner, with all services listened for on all "
"local addresses.  If the protocol is E<.Dq unix>, this value is ignored."
msgstr ""
"jest bezpośrednio udostępniony na początku pliku, z tego powodu tradycyjne "
"pliki konfiguracyjne (bez wyrażeń określających adres hosta) będą "
"interpretowane w tradycyjny sposób, czyli wszystkie usługi będą nasłuchiwać "
"na wszystkich adresach lokalnych. Jeśli protokół to E<.Dq unix>, to ta "
"wartość jest ignorowana."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Em service name> entry is the name of a valid service in the file E<."
"Pa /etc/services> or a port number.  For E<.Dq internal> services (discussed "
"below), the service name E<.Em must> be the official name of the service "
"(that is, the first entry in E<.Pa /etc/services>).  When used to specify a "
"Sun-RPC based service, this field is a valid RPC service name in the file E<."
"Pa /etc/rpc>.  The part on the right of the E<.Dq /> is the RPC version "
"number.  This can simply be a single numeric argument or a range of "
"versions.  A range is bounded by the low version to the high version - E<.Dq "
"rusers/1\\-3>.  For E<.Ux Ns -domain> sockets this field specifies the path "
"name of the socket."
msgstr ""
"Wpis E<.Em nazwa-usługi> jest nazwą prawidłowej usługi, zdefiniowanej w "
"pliku E<.Pa /etc/services> lub portem. Dla usług E<.Dq wewnętrznych> "
"(internal) (opisanych niżej), nazwa usługi musi być oficjalną nazwą usługi "
"(to znaczy pierwszym wpisem w E<.Pa /etc/services>). Podczas podawania "
"usługi opartej o Sun-RPC, pole to jest prawidłową nazwą usługi RPC, "
"zdefiniowaną w pliku E<.Pa /etc/rpc>. Część na prawo od E<.Dq /> jest "
"numerem wersji RPC. Może to być zwyczajny argument numeryczny, lub zakres "
"wersji. Zakres jest obramowany od niższej wersji do wyższej - E<.Dq "
"rusers/1-3>. W przypadku gniazd E<.Ux Ns -domain> pole to określa ścieżkę "
"gniazda."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Em socket type> should be one of E<.Dq stream> or E<.Dq dgram>, "
"depending on whether the socket is a stream or datagram socket."
msgstr ""
"Wpis E<.Em rodzaj gniazda> powinien być jednym z E<.Dq stream> lub E<.Dq "
"dgram>, zależnie od tego, czy gniazdo jest strumieniowe (stream) czy "
"datagramowe (datagram)."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Em protocol> must be a valid protocol as given in E<.Pa /etc/"
"protocols or> E<.Dq unix>.  Examples might be E<.Dq tcp> or E<.Dq udp>.  RPC "
"based services are specified with the E<.Dq rpc/tcp> or E<.Dq rpc/udp> "
"service type.  E<.Dq tcp> and E<.Dq udp> will be recognized as E<.Dq TCP or "
"UDP over both IPv4 and IPv6.> If you need to specify IPv4 or IPv6 "
"explicitly, use something like E<.Dq tcp4> or E<.Dq udp6>.  A E<.Em "
"protocol> of E<.Dq unix> is used to specify a socket in the E<.Ux Ns -"
"domain>."
msgstr ""
"E<.Em Protokół> musi być prawidłowym protokołem, podanym w pliku E<.Pa /etc/"
"protocols> lub w E<.Dq unix>. Przykładami mogą być E<.Dq tcp> lub E<.Dq "
"udp>. Usługi oparte na RPC są podawane z typem usługi E<.Dq rpc/tcp> lub E<."
"Dq rpc/udp>. E<.Dq tcp> i E<.Dq udp> będą rozpoznawane jako E<.Dq TCP lub "
"UDP zarówno w IPv4 jak i w IPv6>. Aby bezpośrednio wskazać IPv4 lub IPv6 "
"należy zastosować zapis taki jak w przykładach: E<.Dq tcp4>, E<.Dq udp6>. E<."
"Em Protokół> równy E<.Dq unix> służy do wskazania gniazda w E<.Ux Ns -"
"domain>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"In addition to the protocol, the configuration file may specify the send and "
"receive socket buffer sizes for the listening socket.  This is especially "
"useful for E<.Tn TCP> as the window scale factor, which is based on the "
"receive socket buffer size, is advertised when the connection handshake "
"occurs, thus the socket buffer size for the server must be set on the listen "
"socket.  By increasing the socket buffer sizes, better E<.Tn TCP> "
"performance may be realized in some situations.  The socket buffer sizes are "
"specified by appending their values to the protocol specification as follows:"
msgstr ""
"Oprócz protokołu, plik konfiguracyjny może określać rozmiary buforów "
"nasłuchujących gniazd do wysyłania i otrzymywania danych. Jest to "
"szczególnie przydatne przy E<.Tn TCP>, jako współczynnik skalujący okna, co "
"bazuje na fakcie, że rozmiar bufora gniazda danych otrzymywanych jest "
"ogłaszany przy nawiązaniu połączenia, a zatem rozmiar bufora gniazda serwera "
"musi być ustawiony na gnieździe nasłuchującym. Zwiększając rozmiary buforów "
"gniazda, w pewnych sytuacjach można uzyskać lepszą wydajność E<.Tn TCP>. "
"Rozmiar buforów gniazda są podawane przez dołączanie ich wartości do "
"określenia protokołów, jak poniżej:"

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"tcp,rcvbuf=16384\n"
"tcp,sndbuf=64k\n"
"tcp,rcvbuf=64k,sndbuf=1m\n"
msgstr ""
"tcp,rcvbuf=16384\n"
"tcp,sndbuf=64k\n"
"tcp,rcvbuf=64k,sndbuf=1m\n"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A literal value may be specified, or modified using E<.Sq k> to indicate "
"kilobytes or E<.Sq m> to indicate megabytes."
msgstr ""
"Można podać wartość dosłowną lub zmodyfikować ją podając E<.Sq k> do "
"wskazania kilobajtów lub E<.Sq m> - jeśli chodzi o megabajty."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Em wait/nowait> entry is used to tell E<.Nm> if it should wait for "
"the server program to return, or continue processing connections on the "
"socket.  If a datagram server connects to its peer, freeing the socket so E<."
"Nm inetd> can receive further messages on the socket, it is said to be a E<."
"Dq multi-threaded> server, and should use the E<.Dq nowait> entry.  For "
"datagram servers which process all incoming datagrams on a socket and "
"eventually time out, the server is said to be E<.Dq single-threaded> and "
"should use a E<.Dq wait> entry.  E<.Xr comsat 8> E<.Pq Xr biff 1> and E<.Xr "
"talkd 8> are both examples of the latter type of datagram server.  The "
"optional E<.Dq max> suffix (separated from E<.Dq wait> or E<.Dq nowait> by a "
"dot) specifies the maximum number of times a service can be invoked in one "
"minute; the default is 256.  If a service exceeds this limit, E<.Nm> will "
"log the problem and stop servicing requests for the specific service for ten "
"minutes.  See also the E<.Fl R> option above."
msgstr ""
"Wpis E<.Em wait/nowait> (zwłoka) jest używany do przekazania E<.Nm> czy "
"powinien on czekać na powrót programu serwera, czy kontynuować obsługę "
"połączeń na gnieździe. Jeśli serwer datagramowy łączy się ze swoim rozmówcą, "
"zwalniając gniazdo w ten sposób, że E<.Nm inetd> może odbierać dalsze "
"wiadomości z tego gniazda, to mówi się o nim jako o serwerze E<.Dq "
"wielowątkowym> (multi-threaded) i powinno się używać wpisu E<.Dq nowait> "
"Serwery datagramowe, które przetwarzają wszystkie nadchodzące do gniazda "
"datagram, które ostatecznie przedawniają się, nazywa się E<.Dq "
"jednowątkowymi> (single threaded) i powinno używać się dla nich wpisu E<.Dq "
"wait>. E<.Xr comsat 8> E<.Pq Xr biff 1> i E<.Xr talkd 8> są przykładami tego "
"drugiego rodzaju serwerów datagramowych. Opcjonalny przyrostek E<.Dq max> "
"(oddzielony od E<.Dq wait> lub E<.Dq nowait> kropką) określa maksymalną "
"liczbę instancji serwera, jakie mogą zostać postawione przez E<.Nm> w czasie "
"60 sekund, domyślnie wynosi 256. Jeśli usługa przekroczy ten limit, E<.Nm> "
"zapisze ten problem do dziennika i zaprzestanie obsługi żądań dla danej "
"usługi przez dziesięć minut. Proszę sprawdzić też opis opcji E<.Fl R> "
"(powyżej)."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Stream servers are usually marked as E<.Dq nowait> but if a single server "
"process is to handle multiple connections, it may be marked as E<.Dq wait>.  "
"The master socket will then be passed as fd 0 to the server, which will then "
"need to accept the incoming connection.  The server should eventually time "
"out and exit when no more connections are active.  E<.Nm> will continue to "
"listen on the master socket for connections, so the server should not close "
"it when it exits."
msgstr ""
"Serwery strumieniowe są zwykle oznaczane jako E<.Dq nowait>, lecz jeśli "
"pojedynczy serwer strumieniowy ma obsługiwać wiele połączeń, można go "
"oznaczyć E<.Dq wait>. Główne gniazdo zostanie wówczas przekazane jako fd 0 "
"do serwera, który następnie będzie musiał akceptować połączenia "
"przychodzące. Serwer powinien ostatecznie przedawnić się i wyjść gdy nie "
"będzie już aktywnych połączeń. E<.Nm> będzie kontynuował nasłuch na głównym "
"gnieździe czekając na połączenia, więc serwer nie powinien zamykać go przy "
"wychodzeniu."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Em user> entry should contain the user name of the user as whom the "
"server should run.  This allows for servers to be given less permission than "
"root.  An optional group name can be specified by appending a dot to the "
"user name followed by the group name.  This allows for servers to run with a "
"different (primary) group ID than specified in the password file.  If a "
"group is specified and user is not root, the supplementary groups associated "
"with that user will still be set."
msgstr ""
"Wpis E<.Em użytkownik> powinien zawierać nazwę użytkownika, pod którym "
"powinien uruchamiać się serwer. Umożliwia to serwerom posiadanie mniejszych "
"praw niż prawa roota. Opcjonalnie, po dodaniu kropki do nazwy użytkownika, "
"można podać w tym polu nazwę grupy. Umożliwia to serwerom pracę z innym "
"(podstawowym) identyfikatorem grupy niż ten, podany w pliku z hasłami. Jeśli "
"grupa jest podana, a użytkownik nie jest rootem, to uzupełniające grupy "
"związane z użytkownikiem wciąż będą ustawione."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Em server program> entry should contain the pathname of the program "
"which is to be executed by E<.Nm inetd> when a request is found on its "
"socket.  If E<.Nm inetd> provides this service internally, this entry should "
"be E<.Dq internal>."
msgstr ""
"Wpis E<.Em program serwera> powinien zawierać ścieżkę programu, który ma być "
"wywoływany przez E<.Nm inetd> po otrzymaniu żądania na gnieździe. Jeśli E<."
"Nm inetd> udostępnia tę usługę wewnętrznie, to wpis ten powinien być wpisem "
"E<.Dq internal>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Em server program arguments> should be just as arguments normally "
"are, starting with argv[0], which is the name of the program.  If the "
"service is provided internally, the word E<.Dq internal> should take the "
"place of this entry."
msgstr ""
"Wpis E<.Em argumenty programu serwera> powinien wyglądać tak jak zwykłe "
"argumenty, poczynając od argv[0], który jest nazwą programu. Jeśli usługa "
"jest udostępniana wewnętrznie, to wpis powinien przyjąć nazwę E<.Dq internal."
">"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm inetd> provides several E<.Dq trivial> services internally by use of "
"routines within itself.  These services are E<.Dq echo>, E<.Dq discard>, E<."
"Dq chargen> (character generator), E<.Dq daytime> (human readable time), and "
"E<.Dq time> (machine readable time, in the form of the number of seconds "
"since midnight, January 1, 1900).  All of these services are TCP based.  For "
"details of these services, consult the appropriate RFC from the Network "
"Information Center."
msgstr ""
"Program E<.Nm inetd> udostępnia wiele E<.Dq trywialnych> usług wewnętrznie, "
"używając do tego swoich własnych procedur. Tymi usługami są E<.Dq echo>, E<."
"Dq discard>, E<.Dq chargen> (generator znaków), E<.Dq daytime> (odczytywalny "
"przez człowieka czas) oraz E<.Dq time> (czas odczytywalny przez maszynę, "
"liczba sekund od północy 1 stycznia 1900).  Wszystkie te usługi są oparte o "
"tcp. Dla dalszych szczegółów o tych usługach, skonsultuj się z odpowiednim "
"RFC z Centrum Informacji Sieci (Network Information Center)."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm inetd> rereads its configuration file when it receives a hangup "
"signal, E<.Dv SIGHUP>.  Services may be added, deleted or modified when the "
"configuration file is reread."
msgstr ""
"E<.Nm Inetd> odczytuje swój plik konfiguracyjny od nowa gdy otrzyma sygnał "
"zawieszenia (hangup), czyli E<.Dv SIGHUP>. Usługi mogą być tak dodawane, "
"kasowane lub modyfikowane."

#. type: Ss
#: debian-bookworm debian-unstable
#, no-wrap
msgid "libwrap"
msgstr "libwrap"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Support for E<.Tn TCP> wrappers is included with E<.Nm> to provide built-in "
"tcpd-like access control functionality.  An external tcpd program is not "
"needed.  You do not need to change the E<.Pa /etc/inetd.conf> server-program "
"entry to enable this capability.  E<.Nm> uses E<.Pa /etc/hosts.allow> and E<."
"Pa /etc/hosts.deny> for access control facility configurations, as described "
"in E<.Xr hosts_access 5>."
msgstr ""
"Obsługa opakowań E<.Tn TCP> jest włączona w program E<.Nm> w celu "
"zapewnienia wbudowanej funkcji kontroli dostępu podobnej do tcpd. Zewnętrzny "
"program tcpd nie jest wymagany. Nie ma potrzeby zmian wpisu programu serwera "
"w E<.Pa /etc/inetd.conf> do włączenia tej funkcji. E<.Nm> używa E<.Pa /etc/"
"hosts.allow> i E<.Pa /etc/hosts.deny> do konfiguracji usług kontroli "
"dostępu, zgodnie z opisem w podręczniku E<.Xr hosts_access 5>."

#. type: Ss
#: debian-bookworm debian-unstable
#, no-wrap
msgid "IPv6 TCP/UDP behavior"
msgstr "Zachowanie TCP/UDP w IPv6"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The default is to run two servers: one for IPv4 and one for IPv6 traffic.  "
"If you have different requirements then you may specify one or two separate "
"lines in E<.Pa inetd.conf>, for E<.Dq tcp4> and E<.Dq tcp6>."
msgstr ""
"Domyślnie uruchamiane są dwa serwery: jeden do obsługi ruchu IPv4, a drugi "
"do IPv6. Przy innych wymaganiach, może być konieczne podanie jednego lub "
"dwóch odrębnych wierszy w pliku E<.Pa inetd.conf>, dla E<.Dq tcp4> i E<.Dq "
"tcp6>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Under various combinations of IPv4/v6 daemon settings, E<.Nm> will behave as "
"follows:"
msgstr ""
"W zależności od różnych kombinacji ustawień demona IPv4/IPv6 E<.Nm> będzie "
"wykazywał następujące zachowanie:"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If you have only one server on E<.Dq tcp4>, IPv4 traffic will be routed to "
"the server.  IPv6 traffic will not be accepted."
msgstr ""
"Jeśli ma się jedynie jeden serwer - E<.Dq tcp4>, ruch IPv4 będzie "
"przekierowany na serwer. Ruch IPv6 nie będzie akceptowany."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If you have two servers on E<.Dq tcp4> and E<.Dq tcp6>, IPv4 traffic will be "
"routed to the server on E<.Dq tcp4>, and IPv6 traffic will go to the server "
"on E<.Dq tcp6>, which is identical to the default behaviour when only E<.Dq "
"tcp> is specified."
msgstr ""
"Jeśli ma się dwa serwery, tzn. E<.Dq tcp4> oraz E<.Dq tcp6>, to ruch IPv4 "
"będzie przekierowany na serwer E<.Dq tcp4>, a ruch IPv6 będzie przekierowany "
"na serwer E<.Dq tcp6>, co jest identyczne z domyślnym rozwiązaniem "
"stosowanym gdy poda się wyłącznie E<.Dq tcp>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If you have only one server on E<.Dq tcp6>, only IPv6 traffic will be routed "
"to the server."
msgstr ""
"Jeśli ma się jedynie jeden serwer - E<.Dq tcp6>, jedynie ruch IPv6 będzie "
"przekierowany na serwer."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The special E<.Dq tcp46> parameter can be used for obsolete servers which "
"require to receive IPv4 connections mapped in an IPv6 socket. Its usage is "
"discouraged."
msgstr ""
"Specjalny parametr E<.Dq tcp46> można wykorzystać przy przestarzałych "
"serwerach, które wymagają połączenia IPv4 przepisanego na gniazdo IPv6. Nie "
"zaleca się używania tego parametru."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Pa /etc/inetd.conf"
msgstr "Pa /etc/inetd.conf"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Xr fingerd 8>, E<.Xr ftpd 8>, E<.Xr identd 8>, E<.Xr talkd 8>"
msgstr "E<.Xr fingerd 8>, E<.Xr ftpd 8>, E<.Xr identd 8>, E<.Xr talkd 8>"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Nm> command appeared in E<.Bx 4.3>.  Support for Sun-RPC based "
"services is modelled after that provided by SunOS 4.1.  IPv6 support was "
"added by the KAME project in 1999."
msgstr ""
"Polecenie E<.Nm> pojawiło się w E<.Bx 4.3>. Obsługa usług opartych na Sun-"
"RPC została utworzona wg udostępnionej przez Sun-OS 4.1. Obsługę IPv6 dodano "
"w projekcie KAME w 1999."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Marco d'Itri ported this code from OpenBSD in summer 2002 and added socket "
"buffers tuning and libwrap support from the NetBSD source tree."
msgstr ""
"Marco d'Itri przeniósł ten kod z OpenBSD latem 2002 roku oraz dodał "
"możliwość modyfikacji buforów gniazd oraz obsługę libwrap z drzewa źródeł "
"NetBSD."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "BUGS"
msgstr "USTERKI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"On Linux systems, the daemon cannot reload its configuration and needs to be "
"restarted when the host address for a service is changed between E<.Dq \\&*> "
"and a specific address."
msgstr ""
"Na systemach linuksowych demon nie może przeładować swojej konfiguracji. "
"Trzeba go zrestartować jeśli zmienił się adres hosta dla usługi pomiędzy E<."
"Dq \\&*> i wyrażeniem określającym adres."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Server programs used with E<.Dq dgram> E<.Dq udp> E<.Dq nowait> must read "
"from the network socket, or E<.Nm inetd> will spawn processes until the "
"maximum is reached."
msgstr ""
"Programy serwera używane z E<.Dq dgram> E<.Dq udp> E<.Dq nowait> muszą "
"czytać z gniazd sieciowych lub E<.Nm inetd> będzie mnożył procesy aż do "
"osiągnięcia limitu."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Host address specifiers, while they make conceptual sense for RPC services, "
"do not work entirely correctly.  This is largely because the portmapper "
"interface does not provide a way to register different ports for the same "
"service on different local addresses.  Provided you never have more than one "
"entry for a given RPC service, everything should work correctly.  (Note that "
"default host address specifiers do apply to RPC lines with no explicit "
"specifier.)"
msgstr ""
"Wyrażenia określające adres hosta, choć ich koncepcja ma sens przy usługach "
"RPC, nie działają do końca poprawnie. Dzieje się tak w dużej części z powodu "
"faktu, iż interfejs portmappera nie udostępnia metody rejestracji różnych "
"portów dla tej samej usługi na różnym adresie lokalnym. Jeśli nie będzie się "
"używało więcej niż jednego wpisu dla danej usługi RPC, to wszystko powinno "
"działać poprawnie (proszę zauważyć, że do wierszy RPC bez bezpośredniego "
"określenia adresu ma zastosowanie domyślne wyrażenie określające adres "
"hosta)."
