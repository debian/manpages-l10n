# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Tuukka Forssell <taf@jytol.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-09-06 18:18+0200\n"
"PO-Revision-Date: 1998-04-10 11:01+0200\n"
"Last-Translator: Tuukka Forssell <taf@jytol.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "locate"
msgstr "locate"

#. type: TH
#: archlinux
#, no-wrap
msgid "Sep 2012"
msgstr "Syyskuuta 2012"

#. type: TH
#: archlinux
#, no-wrap
msgid "mlocate"
msgstr "mlocate"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: archlinux
msgid "locate - find files by name"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: Plain text
#: archlinux
msgid "B<locate> [I<OPTION>]... I<PATTERN>..."
msgstr "B<locate> [I<VALITSIN>]... I<HAHMO>..."

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: archlinux
msgid ""
"B<locate> reads one or more databases prepared by B<updatedb>(8)  and writes "
"file names matching at least one of the I<PATTERN>s to standard output, one "
"per line."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"If B<--regex> is not specified, I<PATTERN>s can contain globbing "
"characters.  If any I<PATTERN> contains no globbing characters, B<locate> "
"behaves as if the pattern were B<*>I<PATTERN>B<*>."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"By default, B<locate> does not check whether files found in database still "
"exist (but it does require all parent directories to exist if the database "
"was built with B<--require-visibility no>).  B<locate> can never report "
"files created after the most recent update of the relevant database."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "EXIT STATUS"
msgstr "POISTUMISEN TILA"

#. type: Plain text
#: archlinux
msgid ""
"B<locate> exits with status 0 if any match was found or if B<locate> was "
"invoked with one of the B<--limit 0>, B<--help>, B<--statistics> or B<--"
"version> options.  If no match was found or a fatal error was encountered, "
"B<locate> exits with status 1."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Errors encountered while reading a database are not fatal, search continues "
"in other specified databases, if any."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "VALITSIMET"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-A>, B<--all>"
msgstr "B<-A>, B<--all>"

#. type: Plain text
#: archlinux
msgid ""
"Print only entries that match all I<PATTERN>s instead of requiring only one "
"of them to match."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-b>, B<--basename>"
msgstr "B<-b>, B<--basename>"

#. type: Plain text
#: archlinux
msgid ""
"Match only the base name against the specified patterns.  This is the "
"opposite of B<--wholename>."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-c>, B<--count>"
msgstr "B<-c>, B<--count>"

#. type: Plain text
#: archlinux
msgid ""
"Instead of writing file names on standard output, write the number of "
"matching entries only."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-d, --database> I<DBPATH>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Replace the default database with I<DBPATH>.  I<DBPATH> is a B<:>-separated "
"list of database file names.  If more than one B<--database> option is "
"specified, the resulting path is a concatenation of the separate paths."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"An empty database file name is replaced by the default database.  A database "
"file name B<-> refers to the standard input.  Note that a database can be "
"read from the standard input only once."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-e>, B<--existing>"
msgstr "B<-e>, B<--existing>"

#. type: Plain text
#: archlinux
msgid ""
"Print only entries that refer to files existing at the time B<locate> is run."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-L>, B<--follow>"
msgstr "B<-L>, B<--follow>"

#. type: Plain text
#: archlinux
msgid ""
"When checking whether files exist (if the B<--existing> option is "
"specified), follow trailing symbolic links.  This causes broken symbolic "
"links to be omitted from the output."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"This is the default behavior.  The opposite can be specified using B<--"
"nofollow>."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "Print a summary of the options to B<locate> and exit."
msgid ""
"Write a summary of the available options to standard output and exit "
"successfully."
msgstr "Tulostaa ohjeen komennosta B<locate> ja poistuu ohjelmasta."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: archlinux
msgid "Ignore case distinctions when matching patterns."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-l>, B<--limit>, B<-n> I<LIMIT>"
msgstr "B<-l>, B<--limit>, B<-n> I<RAJA>"

#. type: Plain text
#: archlinux
msgid ""
"Exit successfully after finding I<LIMIT> entries.  If the B<--count> option "
"is specified, the resulting count is also limited to I<LIMIT>."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-m>, B<--mmap>"
msgstr "B<-m>, B<--mmap>"

#. type: Plain text
#: archlinux
msgid "Ignored, for compatibility with E<.SM BSD> and E<.SM GNU> B<locate>."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-P>, B<--nofollow>, B<-H>"
msgstr "B<-P>, B<--nofollow>, B<-H>"

#. type: Plain text
#: archlinux
msgid ""
"When checking whether files exist (if the B<--existing> option is "
"specified), do not follow trailing symbolic links.  This causes broken "
"symbolic links to be reported like other files."
msgstr ""

#. type: Plain text
#: archlinux
msgid "This is the opposite of B<--follow>."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-0>, B<--null>"
msgstr "B<-0>, B<--null>"

#. type: Plain text
#: archlinux
msgid ""
"Separate the entries on output using the E<.SM ASCII NUL> character instead "
"of writing each entry on a separate line.  This option is designed for "
"interoperability with the B<--null> option of E<.SM GNU> B<xargs>(1)."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-S>, B<--statistics>"
msgstr "B<-S>, B<--statistics>"

#. type: Plain text
#: archlinux
msgid ""
"Write statistics about each read database to standard output instead of "
"searching for files and exit successfully."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: archlinux
msgid ""
"Write no messages about errors encountered while reading and processing "
"databases."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-r>, B<--regexp> I<REGEXP>"
msgstr "B<-r>, B<--regexp> I<SÄÄNNLAUS>"

#. type: Plain text
#: archlinux
msgid ""
"Search for a basic regexp I<REGEXP>.  No I<PATTERN>s are allowed if this "
"option is used, but this option can be specified multiple times."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--regex>"
msgstr "B<--regex>"

#. type: Plain text
#: archlinux
msgid "Interpret all I<PATTERN>s as extended regexps."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-s>, B<--stdio>"
msgstr "B<-s>, B<--stdio>"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid ""
"Write information about the version and license of B<locate> on standard "
"output and exit successfully."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-w>, B<--wholename>"
msgstr "B<-w>, B<--wholename>"

#. type: Plain text
#: archlinux
msgid "Match only the whole path name against the specified patterns."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"This is the default behavior.  The opposite can be specified using B<--"
"basename>."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "EXAMPLES"
msgstr "ESIMERKKEJÄ"

#. type: Plain text
#: archlinux
msgid "To search for a file named exactly I<NAME> (not B<*>I<NAME>B<*>), use"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<locate -b> B<'\\e>I<NAME>B<'>"
msgstr "B<locate -b> B<'\\e>I<NIMI>B<'>"

#. type: Plain text
#: archlinux
msgid ""
"Because B<\\e> is a globbing character, this disables the implicit "
"replacement of I<NAME> by B<*>I<NAME>B<*>."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "FILES"
msgstr "TIEDOSTOT"

#. type: TP
#: archlinux
#, no-wrap
msgid "B</var/lib/mlocate/mlocate.db>"
msgstr "B</var/lib/mlocate/mlocate.db>"

#. type: Plain text
#: archlinux
msgid "The database searched by default."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "ENVIRONMENT"
msgstr "YMPÄRISTÖ"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<LOCATE_PATH>"
msgstr "B<LOCATE_PATH>"

#. type: Plain text
#: archlinux
msgid ""
"Path to additional databases, added after the default database or the "
"databases specified using the B<--database> option."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NOTES"
msgstr "HUOMAUTUKSET"

#. type: Plain text
#: archlinux
msgid ""
"The order in which the requested databases are processed is unspecified, "
"which allows B<locate> to reorder the database path for security reasons."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"B<locate> attempts to be compatible to B<slocate> (without the options used "
"for creating databases) and E<.SM GNU> B<locate>, in that order.  This is "
"the reason for the impractical default B<--follow> option and for the "
"confusing set of B<--regex> and B<--regexp> options."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The short spelling of the B<-r> option is incompatible to E<.SM GNU> "
"B<locate>, where it corresponds to the B<--regex> option.  Use the long "
"option names to avoid confusion."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The B<LOCATE_PATH> environment variable replaces the default database in E<."
"SM BSD> and E<.SM GNU> B<locate>, but it is added to other databases in this "
"implementation and B<slocate>."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHOR"
msgstr "TEKIJÄ"

#. type: Plain text
#: archlinux
msgid "Miloslav Trmac E<lt>mitr@redhat.comE<gt>"
msgstr "Miloslav Trmac E<lt>mitr@redhat.comE<gt>"

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "KATSO MYÖS"

#. type: Plain text
#: archlinux
msgid "B<updatedb>(8)"
msgstr "B<updatedb>(8)"
