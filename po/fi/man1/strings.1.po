# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Janne Viitala <janne.viitala@cc.tut.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:15+0100\n"
"PO-Revision-Date: 1998-04-10 11:01+0200\n"
"Last-Translator: Janne Viitala <janne.viitala@cc.tut.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. #-#-#-#-#  archlinux: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  debian-bookworm: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  debian-unstable: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  fedora-41: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Required to disable full justification in groff 1.23.0.
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  fedora-rawhide: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Required to disable full justification in groff 1.23.0.
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  mageia-cauldron: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  opensuse-leap-16-0: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  opensuse-tumbleweed: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Title"
msgstr "Title"

#. #-#-#-#-#  archlinux: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  debian-bookworm: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  debian-unstable: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  fedora-41: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Required to disable full justification in groff 1.23.0.
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  fedora-rawhide: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Required to disable full justification in groff 1.23.0.
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  mageia-cauldron: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  opensuse-leap-16-0: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#. #-#-#-#-#  opensuse-tumbleweed: strings.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  ========================================================================
#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STRINGS 1"
msgstr "STRINGS 1"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STRINGS"
msgstr "STRINGS"

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-08-05"
msgstr "5. elokuuta 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "binutils-2.43.0"
msgstr "binutils-2.43.0"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GNU Development Tools"
msgstr "GNU kehitystyökalut"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "strings - print the strings of printable characters in files"
msgid "strings - print the sequences of printable characters in files"
msgstr "strings - tulosta selväkieliset merkkijonot tiedostoista"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Header"
msgstr "Header"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"strings [B<-afovV>] [B<->I<min-len>]\n"
"        [B<-n> I<min-len>] [B<--bytes=>I<min-len>]\n"
"        [B<-t> I<radix>] [B<--radix=>I<radix>]\n"
"        [B<-e> I<encoding>] [B<--encoding=>I<encoding>]\n"
"        [B<-U> I<method>] [B<--unicode=>I<method>]\n"
"        [B<->] [B<--all>] [B<--print-file-name>]\n"
"        [B<-T> I<bfdname>] [B<--target=>I<bfdname>]\n"
"        [B<-w>] [B<--include-all-whitespace>]\n"
"        [B<-s>] [B<--output-separator> I<sep_string>]\n"
"        [B<--help>] [B<--version>] I<file>...\n"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "For each I<file> given, \\s-1GNU\\s0 B<strings> prints the printable "
#| "character sequences that are at least 4 characters long (or the number "
#| "given with the options below) and are followed by an unprintable "
#| "character."
msgid ""
"For each I<file> given, GNU B<strings> prints the printable character "
"sequences that are at least 4 characters long (or the number given with the "
"options below) and are followed by an unprintable character."
msgstr ""
"\\s-1GNU\\s0 B<strings> tulostaa jokaisesta annetusta I<tiedostosta> "
"selväkieliset merkkijonot jotka ovat vähintään neljä merkkiä pitkiä (pituus "
"voidaan määrätä alempana kuvatuilla optioilla) ja joita seuraa ei-"
"tulostettavia merkkejä."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Depending upon how the strings program was configured it will default to "
"either displaying all the printable sequences that it can find in each file, "
"or only those sequences that are in loadable, initialized data sections.  If "
"the file type is unrecognizable, or if strings is reading from stdin then it "
"will always display all of the printable sequences that it can find."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For backwards compatibility any file that occurs after a command-line option "
"of just B<-> will also be scanned in full, regardless of the presence of any "
"B<-d> option."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\&B<strings> is mainly useful for determining the contents of non-text "
"files."
msgstr ""
"\\&B<strings> on hyödyllinen lähinnä tutkittaessa muiden kuin "
"tekstitiedostojen sisältöä."

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "VALITSIMET"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Item"
msgstr "Item"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-a"
msgstr "-a"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--all>"
msgstr "B<--all>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--all"
msgstr "--all"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<->"
msgstr "B<->"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-"
msgstr "-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Scan the whole file, regardless of what sections it contains or whether "
"those sections are loaded or initialized.  Normally this is the default "
"behaviour, but strings can be configured so that the \\&B<-d> is the default "
"instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<-> option is position dependent and forces strings to perform full "
"scans of any file that is mentioned after the B<-> on the command line, even "
"if the B<-d> option has been specified."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-d"
msgstr "-d"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--data>"
msgstr "B<--data>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--data"
msgstr "--data"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Only print strings from initialized, loaded data sections in the file.  This "
"may reduce the amount of garbage in the output, but it also exposes the "
"strings program to any security flaws that may be present in the BFD library "
"used to scan and load sections.  Strings can be configured so that this "
"option is the default behaviour.  In such cases the B<-a> option can be used "
"to avoid using the BFD library and instead just print all of the strings "
"found in the file."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-f"
msgstr "-f"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--print-file-name>"
msgstr "B<--print-file-name>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--print-file-name"
msgstr "--print-file-name"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print the name of the file before each string."
msgstr "Tulosta tiedoston nimi ennen jokaista löydettyä merkkijonoa."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--help"
msgstr "--help"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Print a summary of the options to B<strings> on the standard output and "
#| "exit."
msgid "Print a summary of the program usage on the standard output and exit."
msgstr "Tulosta yhteenveto B<strings> ohjelman optioista ja poistu ohjelmasta."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<->I<min-len>"
msgstr "B<->I<minimi-pituus>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-min-len"
msgstr "-minimi-pituus"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-n> I<min-len>"
msgstr "B<-n> I<minimi-pituus>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-n min-len"
msgstr "-n minimi-pituus"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--bytes=>I<min-len>"
msgstr "B<--bytes=>I<minimi-pituus>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--bytes=min-len"
msgstr "--bytes=minimi-pituus"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Print sequences of displayable characters that are at least \\&I<min-len> "
"characters long.  If not specified a default minimum length of 4 is used.  "
"The distinction between displayable and non-displayable characters depends "
"upon the setting of the \\&B<-e> and B<-U> options.  Sequences are always "
"terminated at control characters such as new-line and carriage-return, but "
"not the tab character."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>"
msgstr "B<-o>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-o"
msgstr "-o"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Like B<-t o>.  Some other versions of B<strings> have B<-o> act like B<-t d> "
"instead.  Since we can not be compatible with both ways, we simply chose one."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-t> I<radix>"
msgstr "B<-t> I<radix>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-t radix"
msgstr "-t radix"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--radix=>I<radix>"
msgstr "B<--radix=>I<radix>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--radix=radix"
msgstr "--radix=radix"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Print the offset within the file before each string.  The single "
#| "character argument specifies the radix of the offset\\(emoctal, "
#| "hexadecimal, or decimal."
msgid ""
"Print the offset within the file before each string.  The single character "
"argument specifies the radix of the offset---B<o> for octal, B<x> for "
"hexadecimal, or B<d> for decimal."
msgstr ""
"Tulosta merkkijonon siirros (offset) tiedoston sisällä ennen jokaista "
"merkkijonoa.  Optiota seuraava merkki määrää tulostuksessa käytettävän "
"lukutyypin\\(emoktaali, heksadesimaali tai desimaali."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-e> I<encoding>"
msgstr "B<-e> I<merkkien_koodaus>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-e encoding"
msgstr "-e merkkien_koodaus"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--encoding=>I<encoding>"
msgstr "B<--encoding=>I<merkkien_koodaus>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--encoding=encoding"
msgstr "--encoding=merkkien_koodaus"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Select the character encoding of the strings that are to be found.  Possible "
"values for I<encoding> are: B<s> = single-7-bit-byte characters (default), "
"B<S> = single-8-bit-byte characters, B<b> = 16-bit bigendian, B<l> = 16-bit "
"littleendian, B<B> = 32-bit bigendian, B<L> = 32-bit littleendian.  Useful "
"for finding wide character strings. (B<l> and B<b> apply to, for example, "
"Unicode UTF-16/UCS-2 encodings)."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-U> I<[d|i|l|e|x|h]>"
msgstr "B<-U> I<[d|i|l|e|x|h]>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-U [d|i|l|e|x|h]"
msgstr "-U [d|i|l|e|x|h]"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--unicode=>I<[default|invalid|locale|escape|hex|highlight]>"
msgstr "B<--unicode=>I<[default|invalid|locale|escape|hex|highlight]>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--unicode=[default|invalid|locale|escape|hex|highlight]"
msgstr "--unicode=[default|invalid|locale|escape|hex|highlight]"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Controls the display of UTF-8 encoded multibyte characters in strings.  The "
"default (B<--unicode=default>) is to give them no special treatment, and "
"instead rely upon the setting of the \\&B<--encoding> option.  The other "
"values for this option automatically enable B<--encoding=S>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<--unicode=invalid> option treats them as non-graphic characters and "
"hence not part of a valid string.  All the remaining options treat them as "
"valid string characters."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<--unicode=locale> option displays them in the current locale, which "
"may or may not support UTF-8 encoding.  The \\&B<--unicode=hex> option "
"displays them as hex byte sequences enclosed between I<E<lt>E<gt>> "
"characters.  The B<--unicode=escape> option displays them as escape "
"sequences (I<\\euxxxx>) and the \\&B<--unicode=highlight> option displays "
"them as escape sequences highlighted in red (if supported by the output "
"device).  The colouring is intended to draw attention to the presence of "
"unicode sequences where they might not be expected."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-T> I<bfdname>"
msgstr "B<-T> I<bfdnimi>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-T bfdname"
msgstr "-T bfdnimi"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--target=>I<bfdname>"
msgstr "B<--target=>I<bfdnimi>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--target=bfdname"
msgstr "--target=bfdnimi"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Specify an object code format other than your system's default format."
msgstr ""
"Jos objektikoodin formaatti on joku muu kuin järjestelmän oletusformaatti, "
"se on määriteltävä tällä optiolla."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-v"
msgstr "-v"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-V"
msgstr "-V"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--version"
msgstr "--version"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Print the version number of B<strings> on the standard output and exit."
msgid "Print the program version number on the standard output and exit."
msgstr "Tulosta B<strings> ohjelman versionumero ja poistu ohjelmasta."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>"
msgstr "B<-w>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-w"
msgstr "-w"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--include-all-whitespace>"
msgstr "B<--include-all-whitespace>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--include-all-whitespace"
msgstr "--include-all-whitespace"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"By default tab and space characters are included in the strings that are "
"displayed, but other whitespace characters, such a newlines and carriage "
"returns, are not.  The B<-w> option changes this so that all whitespace "
"characters are considered to be part of a string."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>"
msgstr "B<-s>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-s"
msgstr "-s"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--output-separator>"
msgstr "B<--output-separator>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "--output-separator"
msgstr "--output-separator"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"By default, output strings are delimited by a new-line. This option allows "
"you to supply any string to be used as the output record separator.  Useful "
"with --include-all-whitespace where strings may contain new-lines internally."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<@>I<file>"
msgstr "B<@>I<tiedosto>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "@file"
msgstr "@tiedosto"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Read command-line options from I<file>.  The options read are inserted in "
"place of the original @I<file> option.  If I<file> does not exist, or cannot "
"be read, then the option will be treated literally, and not removed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Options in I<file> are separated by whitespace.  A whitespace character may "
"be included in an option by surrounding the entire option in either single "
"or double quotes.  Any character (including a backslash) may be included by "
"prefixing the character to be included with a backslash.  The I<file> may "
"itself contain additional @I<file> options; any such options will be "
"processed recursively."
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "KATSO MYÖS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\&B<ar>\\|(1), B<nm>\\|(1), B<objdump>\\|(1), B<ranlib>\\|(1), B<readelf>\\|"
"(1)  and the Info entries for I<binutils>."
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "TEKIJÄNOIKEUDET"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Copyright (c) 1991-2024 Free Software Foundation, Inc."
msgstr "Copyright (c) 1991-2024 Free Software Foundation, Inc."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Permission is granted to copy, distribute and/or modify this document under "
"the terms of the GNU Free Documentation License, Version 1.3 or any later "
"version published by the Free Software Foundation; with no Invariant "
"Sections, with no Front-Cover Texts, and with no Back-Cover Texts.  A copy "
"of the license is included in the section entitled \"GNU Free Documentation "
"License\"."
msgstr ""

#. type: ds C+
#: debian-bookworm
#, no-wrap
msgid "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"
msgstr "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"

#. type: ds :
#: debian-bookworm
#, no-wrap
msgid "\\k:\\h'-(\\n(.wu*8/10-\\*(#H+.1m+\\*(#F)'\\v'-\\*(#V'\\z.\\h'.2m+\\*(#F'.\\h'|\\n:u'\\v'\\*(#V'"
msgstr "\\k:\\h'-(\\n(.wu*8/10-\\*(#H+.1m+\\*(#F)'\\v'-\\*(#V'\\z.\\h'.2m+\\*(#F'.\\h'|\\n:u'\\v'\\*(#V'"

#. type: ds 8
#: debian-bookworm
#, no-wrap
msgid "\\h'\\*(#H'\\(*b\\h'-\\*(#H'"
msgstr "\\h'\\*(#H'\\(*b\\h'-\\*(#H'"

#. type: ds o
#: debian-bookworm
#, no-wrap
msgid "\\k:\\h'-(\\n(.wu+\\w'\\(de'u-\\*(#H)/2u'\\v'-.3n'\\*(#[\\z\\(de\\v'.3n'\\h'|\\n:u'\\*(#]"
msgstr "\\k:\\h'-(\\n(.wu+\\w'\\(de'u-\\*(#H)/2u'\\v'-.3n'\\*(#[\\z\\(de\\v'.3n'\\h'|\\n:u'\\*(#]"

#. type: ds d-
#: debian-bookworm
#, no-wrap
msgid "\\h'\\*(#H'\\(pd\\h'-\\w'~'u'\\v'-.25m'I<\\(hy>\\v'.25m'\\h'-\\*(#H'"
msgstr "\\h'\\*(#H'\\(pd\\h'-\\w'~'u'\\v'-.25m'I<\\(hy>\\v'.25m'\\h'-\\*(#H'"

#. type: ds D-
#: debian-bookworm
#, no-wrap
msgid "D\\k:\\h'-\\w'D'u'\\v'-.11m'\\z\\(hy\\v'.11m'\\h'|\\n:u'"
msgstr "D\\k:\\h'-\\w'D'u'\\v'-.11m'\\z\\(hy\\v'.11m'\\h'|\\n:u'"

#. type: ds th
#: debian-bookworm
#, no-wrap
msgid "\\*(#[\\v'.3m'\\s+1I\\s-1\\v'-.3m'\\h'-(\\w'I'u*2/3)'\\s-1o\\s+1\\*(#]"
msgstr "\\*(#[\\v'.3m'\\s+1I\\s-1\\v'-.3m'\\h'-(\\w'I'u*2/3)'\\s-1o\\s+1\\*(#]"

#. type: ds Th
#: debian-bookworm
#, no-wrap
msgid "\\*(#[\\s+2I\\s-2\\h'-\\w'I'u*3/5'\\v'-.3m'o\\v'.3m'\\*(#]"
msgstr "\\*(#[\\s+2I\\s-2\\h'-\\w'I'u*3/5'\\v'-.3m'o\\v'.3m'\\*(#]"

#. type: ds ae
#: debian-bookworm
#, no-wrap
msgid "a\\h'-(\\w'a'u*4/10)'e"
msgstr "a\\h'-(\\w'a'u*4/10)'e"

#. type: ds Ae
#: debian-bookworm
#, no-wrap
msgid "A\\h'-(\\w'A'u*4/10)'E"
msgstr "A\\h'-(\\w'A'u*4/10)'E"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-14"
msgstr "14. tammikuuta 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "binutils-2.40.00"
msgstr "binutils-2.40.00"

#. type: Plain text
#: debian-bookworm
msgid ""
"For each I<file> given, \\s-1GNU\\s0 B<strings> prints the printable "
"character sequences that are at least 4 characters long (or the number given "
"with the options below) and are followed by an unprintable character."
msgstr ""
"\\s-1GNU\\s0 B<strings> tulostaa jokaisesta annetusta I<tiedostosta> "
"selväkieliset merkkijonot jotka ovat vähintään neljä merkkiä pitkiä (pituus "
"voidaan määrätä alempana kuvatuilla optioilla) ja joita seuraa ei-"
"tulostettavia merkkejä."

#. type: Plain text
#: debian-bookworm
msgid ""
"Only print strings from initialized, loaded data sections in the file.  This "
"may reduce the amount of garbage in the output, but it also exposes the "
"strings program to any security flaws that may be present in the "
"\\s-1BFD\\s0 library used to scan and load sections.  Strings can be "
"configured so that this option is the default behaviour.  In such cases the "
"B<-a> option can be used to avoid using the \\s-1BFD\\s0 library and instead "
"just print all of the strings found in the file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Select the character encoding of the strings that are to be found.  Possible "
"values for I<encoding> are: B<s> = single-7-bit-byte characters (default), "
"B<S> = single-8-bit-byte characters, B<b> = 16-bit bigendian, B<l> = 16-bit "
"littleendian, B<B> = 32-bit bigendian, B<L> = 32-bit littleendian.  Useful "
"for finding wide character strings. (B<l> and B<b> apply to, for example, "
"Unicode \\s-1UTF-16/UCS-2\\s0 encodings)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Controls the display of \\s-1UTF-8\\s0 encoded multibyte characters in "
"strings.  The default (B<--unicode=default>) is to give them no special "
"treatment, and instead rely upon the setting of the \\&B<--encoding> "
"option.  The other values for this option automatically enable B<--"
"encoding=S>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<--unicode=locale> option displays them in the current locale, which "
"may or may not support \\s-1UTF-8\\s0 encoding.  The \\&B<--unicode=hex> "
"option displays them as hex byte sequences enclosed between I<E<lt>E<gt>> "
"characters.  The B<--unicode=escape> option displays them as escape "
"sequences (I<\\euxxxx>) and the \\&B<--unicode=highlight> option displays "
"them as escape sequences highlighted in red (if supported by the output "
"device).  The colouring is intended to draw attention to the presence of "
"unicode sequences where they might not be expected."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Copyright (c) 1991-2023 Free Software Foundation, Inc."
msgstr "Copyright (c) 1991-2023 Free Software Foundation, Inc."

#. type: Plain text
#: debian-bookworm
msgid ""
"Permission is granted to copy, distribute and/or modify this document under "
"the terms of the \\s-1GNU\\s0 Free Documentation License, Version 1.3 or any "
"later version published by the Free Software Foundation; with no Invariant "
"Sections, with no Front-Cover Texts, and with no Back-Cover Texts.  A copy "
"of the license is included in the section entitled \\*(L\"\\s-1GNU\\s0 Free "
"Documentation License\\*(R\"."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2024-12-04"
msgstr "4. joulukuuta 2024"

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "binutils-2.43.50"
msgstr "binutils-2.43.50"

#. type: TH
#: fedora-41
#, no-wrap
msgid "2024-09-09"
msgstr "9. syyskuuta 2024"

#. type: TH
#: fedora-41 mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "binutils-2.43.1"
msgstr "binutils-2.43.1"

#. type: Plain text
#: fedora-41 fedora-rawhide
#, fuzzy
#| msgid ""
#| "Specify an object code format other than your system's default format."
msgid ""
"Specify an object code format other than your system\\*(Aqs default format."
msgstr ""
"Jos objektikoodin formaatti on joku muu kuin järjestelmän oletusformaatti, "
"se on määriteltävä tällä optiolla."

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "2024-11-26"
msgstr "26. marraskuuta 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2024-10-25"
msgstr "25. lokakuuta 2024"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "2024-08-28"
msgstr "28. elokuuta 2024"
