# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jussi Larjo <larjo@cc.tut.fi>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-09-06 18:09+0200\n"
"PO-Revision-Date: 1999-10-11 13:30+0200\n"
"Last-Translator: Jussi Larjo <larjo@cc.tut.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AT"
msgstr "AT"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2009-11-14"
msgstr "14. marraskuuta 2009"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"at, batch, atq, atrm - queue, examine, or delete jobs for later execution"
msgstr ""
"at, batch, atq, atrm - määritä, listaa tai poista myöhemmin suoritettavia "
"komentoja"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<batch> [B<-V>] [B<-q> I<queue>] [B<-f> I<file>] [B<-mv>] [B<TIME>]"
msgid ""
"B<at> [B<-V>] [B<-q> I<queue>] [B<-f> I<file>] [B<-u> I<username>] [B<-"
"mMlv>] I<timespec> ...\""
msgstr ""
"B<batch> [B<-V>] [B<-q> I<jono>] [B<-f> I<tiedosto>] [B<-mv>] [B<AIKA>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<batch> [B<-V>] [B<-q> I<queue>] [B<-f> I<file>] [B<-mv>] [B<TIME>]"
msgid ""
"B<at> [B<-V>] [B<-q> I<queue>] [B<-f> I<file>] [B<-u> I<username>] [B<-"
"mMkv>] [B<-t> I<time>]"
msgstr ""
"B<batch> [B<-V>] [B<-q> I<jono>] [B<-f> I<tiedosto>] [B<-mv>] [B<AIKA>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<at -c> I<job> [...\\&]"
msgstr "B<at -c> I<työnumero> [...\\&]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<at> [B<-V>] [B<-q> I<queue>] [B<-f> I<file>] [B<-mldbv>] B<TIME>"
msgid "B<at> [B<-V>] -l [B<-o> I<timeformat>] I<[job> I<...>]"
msgstr "B<at> [B<-V>] [B<-q> I<jono>] [B<-f> I<tiedosto>] [B<-mldbv>] B<AIKA>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<at> [B<-V>] [B<-q> I<queue>] [B<-f> I<file>] [B<-mldbv>] B<TIME>"
msgid "B<atq> [B<-V>] [B<-q> I<queue>] [B<-o> I<timeformat>] I<[job> I<...>]"
msgstr "B<at> [B<-V>] [B<-q> I<jono>] [B<-f> I<tiedosto>] [B<-mldbv>] B<AIKA>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<at> [B<-rd>] I<job> [...\\&]"
msgstr "B<at> [B<-rd>] I<työnumero> [...\\&]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<atrm> [B<-V>] I<job> [...\\&]"
msgstr "B<atrm> [B<-V>] I<työnumero> [...\\&]"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<batch>"
msgstr "B<batch>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<at -b>"
msgstr "B<at -b>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<at> and B<batch> read commands from standard input or a specified file "
"which are to be executed at a later time, using B</bin/sh>."
msgstr ""
"B<at> ja B<batch> lukevat komentoja standardisyötteestä tai nimetystä "
"tiedostosta. Komennot suoritetaan myöhemmin B</bin/sh> -komentotulkkia "
"käyttäen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<at>"
msgstr "B<at>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "executes commands at a specified time."
msgstr "suorittaa komennot määritettynä ajankohtana."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<atq>"
msgstr "B<atq>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
msgid ""
"lists the user's pending jobs, unless the user is the superuser; in that "
"case, everybody's jobs are listed.  The format of the output lines (one for "
"each job) is: Job number, date, hour, queue, and username."
msgstr ""
"listaa käyttäjän määrittämät työt. Pääkäyttäjälle tulostetaan kaikkien työt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<atrm>"
msgstr "B<atrm>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
msgid "deletes jobs, identified by their job number."
msgstr "poistaa työt."

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"executes commands when system load levels permit; in other words, when the "
"load average drops below 0.8, or the value specified in the invocation of "
"B<atd>."
msgstr ""
"suorittaa komennot, kun systeemin kuormitus on alhainen, eli kun "
"keskimääräinen kuormitus laskee alle arvon 0.8 (oletus), tai B<atd> -"
"komennolla määritetyn arvon."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<At> allows fairly complex time specifications, extending the POSIX.2 "
#| "standard.  It accepts times of the form B<HH:MM> to run a job at a "
#| "specific time of day.  (If that time is already past, the next day is "
#| "assumed.)  You may also specify B<midnight,> B<noon,> or B<teatime> "
#| "(4pm)  and you can have a time-of-day suffixed with B<AM> or B<PM> for "
#| "running in the morning or the evening.  You can also say what day the job "
#| "will be run, by giving a date in the form B<month-name> B<day> with an "
#| "optional B<year,> or giving a date of the form B<MMDDYY> or B<MM/DD/YY> "
#| "or B<DD.MM.YY.> The specification of a date I<must> follow the "
#| "specification of the time of day.  You can also give times like B<now> "
#| "B<\\+> I<count> I<time-units,> where the time-units can be B<minutes,> "
#| "B<hours,> B<days,> or B<weeks> and you can tell B<at> to run the job "
#| "today by suffixing the time with B<today> and to run the job tomorrow by "
#| "suffixing the time with B<tomorrow.>"
msgid ""
"B<At> allows fairly complex time specifications, extending the POSIX.2 "
"standard.  It accepts times of the form B<HH:MM> to run a job at a specific "
"time of day.  (If that time is already past, the next day is assumed.)  You "
"may also specify B<midnight,> B<noon,> or B<teatime> (4pm)  and you can have "
"a time-of-day suffixed with B<AM> or B<PM> for running in the morning or the "
"evening.  You can also say what day the job will be run, by giving a date in "
"the form B<month-name> B<day> with an optional B<year,> or giving a date of "
"the form I<MMDD>[I<CC>]I<YY>, I<MM>/I<DD>/[I<CC>]I<YY>, I<DD>.I<MM>."
"[I<CC>]I<YY> or [I<CC>]I<YY>-I<MM>-I<DD>.  The specification of a date "
"I<must> follow the specification of the time of day.  You can also give "
"times like B<now> B<+> I<count> I<time-units,> where the time-units can be "
"B<minutes,> B<hours,> B<days,> or B<weeks> and you can tell B<at> to run the "
"job today by suffixing the time with B<today> and to run the job tomorrow by "
"suffixing the time with B<tomorrow.>"
msgstr ""
"B<At> ymmärtää monia aikamäärityksiä, mm. POSIX.2 -standardin mukaiset.  "
"Aikamääritys B<TT:MM> (tunnit:minuutit) ajaa työn tiettyyn kellonaikaan.  "
"Voidaan myös ilmoittaa B<midnight> (keskiyö), B<noon> (keskipäivä)  tai "
"B<teatime> (16:00)  ja ajan perässä voi olla B<AM> (aamupäivä) tai B<PM> "
"(iltapäivä).  Ajopäivämäärä voidaan myös määrittää; joko antamalla peräkkäin "
"B<month-name> (englanninkielinen kuukauden nimi)  B<päivämäärä> sekä "
"valinnaisesti B<vuosiluku,> tai antamalla päiväys muodossa B<KKPPVV> tai "
"B<KK/PP/VV> tai B<PP.KK.VV.> Päivämäärän määritys seuraa I<aina> kellonajan "
"määritystä.  Suoritus voidaan määrittää myös ajettavaksi tietyn ajan "
"kuluttua seuraavasti: B<now> (heti)  B<\\+> I<luku> I<aikayksikkö,> "
"aikayksikkö voi olla B<minutes> (minuuttia), B<hours> (tuntia), B<days> "
"(päivää)  tai B<weeks> (viikkoa).  Työ voidaan määrätä ajettavaksi tänään "
"sanalla B<today> tai huomenna sanalla B<tomorrow.>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"For example, to run a job at 4pm three days from now, you would do B<at 4pm "
"+ 3 days,> to run a job at 10:00am on July 31, you would do B<at 10am Jul "
"31> and to run a job at 1am tomorrow, you would do B<at 1am tomorrow.>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If you specify a job to absolutely run at a specific time and date in the "
"past, the job will run as soon as possible.  For example, if it is 8pm and "
"you do a B<at 6pm today,> it will run more likely at 8:05pm."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "The exact definition of the time specification can be found in I</usr/doc/"
#| "at/timespec>."
msgid ""
"The definition of the time specification can be found in I</usr/share/doc/at/"
"timespec>."
msgstr ""
"Tarkka formaatti aja määrittelyyn on tiedostossa I</usr/doc/at/timespec>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"For both B<at> and B<batch>, commands are read from standard input or the "
"file specified with the B<-f> option and executed.  The working directory, "
"the environment (except for the variables B<BASH_VERSINFO>, B<DISPLAY>, "
"B<EUID>, B<GROUPS>, B<SHELLOPTS>, B<TERM>, B<UID>, and B<_>)  and the umask "
"are retained from the time of invocation."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"As B<at> is currently implemented as a setuid program, other environment "
"variables (e.g., B<LD_LIBRARY_PATH> or B<LD_PRELOAD>)  are also not "
"exported.  This may change in the future.  As a workaround, set these "
"variables explicitly in your job."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid ""
#| "For both B<at> and B<batch>, commands are read from standard input or the "
#| "file specified with the B<-f> option and executed.  The working "
#| "directory, the environment (except for the variables B<TERM>, B<DISPLAY> "
#| "and B<_>)  and the umask are retained from the time of invocation.  An "
#| "B<at >- or B<batch >- command invoked from a B<su(1)> shell will retain "
#| "the current userid.  The user will be mailed standard error and standard "
#| "output from his commands, if any.  Mail will be sent using the command B</"
#| "usr/lib/sendmail>.  If B<at> is executed from a B<su(1)> shell, the owner "
#| "of the login shell will receive the mail."
msgid ""
"An B<at >- or B<batch >- command invoked from a B<su>(1)  shell will retain "
"the current userid.  The user will be mailed standard error and standard "
"output from his commands, if any.  Mail will be sent using the command B</"
"usr/bin/sendmail>.  If B<at> is executed from a B<su>(1)  shell, the owner "
"of the login shell will receive the mail."
msgstr ""
"Sekä B<at> että B<batch> -komennoilla suoritettavat komennot luetaan joko "
"standardisyötteestä tai B<-f> -optiolla nimetystä tiedostosta.  "
"Suoritusaikana oletushakemisto, ympäristö (paitsi muuttujia B<TERM>, "
"B<DISPLAY> ja B<_>)  sekä B<umask> ovat samat kuin B<at> -komentoa "
"annettaessa.  Jos B<at >- tai B<batch >- komento annetaan B<su(1)> -komennon "
"jälkeen, ajo suoritetaan su:n omaksumalla käyttäjätunnuksella.  Käyttäjälle "
"lähetetään sähköpostina komentojen mahdollinen standardituloste ja "
"virheilmoitukset.  Viesti lähetetään komennolla B</usr/lib/sendmail>.  Jos "
"B<at> suoritetaan B<su(1)> :n jälkeen, viesti lähetetään alkuperäiselle "
"tunnukselle."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The superuser may use these commands in any case.  For other users, "
#| "permission to use at is determined by the files I</etc/at.allow> and I</"
#| "etc/at.deny>."
msgid ""
"The superuser may use these commands in any case.  For other users, "
"permission to use at is determined by the files I</etc/at.allow> and I</etc/"
"at.deny>.  See B<at.allow>(5)  for details."
msgstr ""
"Pääkäyttäjä saa aina käyttää näitä komentoja.  Muille käyttäjille oikeudet "
"määritellään tiedostoilla I</etc/at.allow> ja I</etc/at.deny>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "VALITSIMET"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "prints the version number to standard error."
msgid "prints the version number to standard error and exit successfully."
msgstr "tulostaa versionumeron ja poistuu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-q>I< queue>"
msgstr "B<-q>I< jono>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"uses the specified queue.  A queue designation consists of a single letter; "
"valid queue designations range from B<a> to B<z> and B<A> to B<Z>.  The B<a> "
"queue is the default for B<at> and the B<b> queue for B<batch>.  Queues with "
"higher letters run with increased niceness.  The special queue \"=\" is "
"reserved for jobs which are currently running."
msgstr ""
"käyttää määritettyä jonoa.  Jonomääritys on yksi merkki väleillä B<a> - B<z> "
"tai B<A> - B<Z>.  B<a> -jono on oletus komennolle B<at> ja B<b> -jono "
"komennolle B<batch>.  Suuremmilla kirjainarvoilla määritetyt jonot ajetaan "
"alemmalla prioriteetilla.  Jonomääritys \"=\" sisältää parhaillaan ajossa "
"olevat työt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If a job is submitted to a queue designated with an uppercase letter, it "
#| "is treated as if it had been submitted to batch at that time.  If B<atq> "
#| "is given a specific queue, it will only show jobs pending in that queue."
msgid ""
"If a job is submitted to a queue designated with an uppercase letter, the "
"job is treated as if it were submitted to batch at the time of the job.  "
"Once the time is reached, the batch processing rules with respect to load "
"average apply.  If B<atq> is given a specific queue, it will only show jobs "
"pending in that queue."
msgstr ""
"Jos jonomääritys on iso kirjain, työtä käsitellään B<batch> -komennon "
"mukaisesti.  Jos B<atq> -komennolle määritetään jono, vain kyseisen jonon "
"työt listataan."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Send mail to the user when the job has completed even if there was no output."
msgstr "Lähetä viesti työn päätyttyä, vaikka mitään tulostetta ei syntyisi."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-M>"
msgstr "B<-M>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Never send mail to the user."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-q>I< queue>"
msgid "B<-u>I< username>"
msgstr "B<-q>I< jono>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Sends mail to I<username> rather than the current user."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-f>I< file>"
msgstr "B<-f>I< tiedosto>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
msgid "Reads the job from I<file> rather than standard input."
msgstr "Lue komennot nimetystä tiedostosta."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-f>I< file>"
msgid "B<-t>I< time>"
msgstr "B<-f>I< tiedosto>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "run the job at I<time>, given in the format [[CC]YY]MMDDhhmm[.ss]"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Is an alias for B<atq.>"
msgstr "Sama kuin B<atq.>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-r>"
msgstr "B<-r>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Is an alias for B<atrm.>"
msgstr "Sama kuin B<atrm.>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "is an alias for B<batch>."
msgstr "Sama kuin B<batch>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
msgid "Shows the time the job will be executed before reading the job."
msgstr ""
"B<atq> -komento tulostaa sellaiset ajetut työt, joita ei ole vielä poistettu "
"jonosta; muutoin näytetään kullekin työlle määritetty ajoaika."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Times displayed will be in the format \"Thu Feb 20 14:50:00 1997\"."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-c>"
msgstr "B<-c>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "cats the jobs listed on the command line to standard output."
msgstr "tulostaa määritettyjen töiden komennot standarditulosteeseen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-f>I< file>"
msgid "B<-o>I< fmt>"
msgstr "B<-f>I< tiedosto>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "strftime-like time format used for the job list"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "TIEDOSTOT"

#. type: Plain text
#: archlinux
msgid "I</var/spool/atd>"
msgstr "I</var/spool/atd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I</proc/loadavg>"
msgstr "I</proc/loadavg>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I</var/run/utmp>"
msgstr "I</var/run/utmp>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I</etc/at.allow>"
msgstr "I</etc/at.allow>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I</etc/at.deny>"
msgstr "I</etc/at.deny>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "KATSO MYÖS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<at.allow>(5), B<at.deny>(5), B<atd>(8), B<cron>(1), B<nice>(1), B<sh>(1), "
"B<umask>(2)."
msgstr ""
"B<at.allow>(5), B<at.deny>(5), B<atd>(8), B<cron>(1), B<nice>(1), B<sh>(1), "
"B<umask>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGIT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The correct operation of B<batch> for Linux depends on the presence of a "
"I<proc>- type directory mounted on I</proc>."
msgstr ""
"Jotta B<batch> toimisi oikein Linuxissa, tulee I<proc>- tyypin "
"pseudolaitteen olla kytkettynä hakemistoon I</proc>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If the file I</var/run/utmp> is not available or corrupted, or if the user "
"is not logged on at the time B<at> is invoked, the mail is sent to the "
"userid found in the environment variable B<LOGNAME>.  If that is undefined "
"or empty, the current userid is assumed."
msgstr ""
"Jos tiedosto I</var/run/utmp> ei ole luettavissa tai on turmeltunut, tai jos "
"käyttäjä ei ole kirjattu sisään B<at> -komennon ajoaikana, komennosta "
"syntyvät viestit lähetetään ympäristömuuttujan B<LOGNAME> nimeämälle "
"käyttäjälle. Jos sekin on tyhjä, käytetään userid:n mukaista tunnusta."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<At> and B<batch> as presently implemented are not suitable when users are "
"competing for resources.  If this is the case for your site, you might want "
"to consider another batch system, such as B<nqs>."
msgstr ""
"B<At> ja B<batch> on toteutettu tavalla, joka soveltuu huonosti "
"resurssikilpailuun.  Tällaisiin tilanteisiin sopii paremmin eräajosysteemi, "
"kuten B<nqs>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TEKIJÄ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "At was mostly written by Thomas Koenig."
msgstr "Suurimmaksi osaksi Thomas König."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"executes commands when system load levels permit; in other words, when the "
"load average drops below 1.5, or the value specified in the invocation of "
"B<atd>."
msgstr ""
"suorittaa komennot, kun systeemin kuormitus on alhainen, eli kun "
"keskimääräinen kuormitus laskee alle arvon 1.5 (oletus), tai B<atd> -"
"komennolla määritetyn arvon."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "For both B<at> and B<batch>, commands are read from standard input or the "
#| "file specified with the B<-f> option and executed.  The working "
#| "directory, the environment (except for the variables B<TERM>, B<DISPLAY> "
#| "and B<_>)  and the umask are retained from the time of invocation.  An "
#| "B<at >- or B<batch >- command invoked from a B<su(1)> shell will retain "
#| "the current userid.  The user will be mailed standard error and standard "
#| "output from his commands, if any.  Mail will be sent using the command B</"
#| "usr/lib/sendmail>.  If B<at> is executed from a B<su(1)> shell, the owner "
#| "of the login shell will receive the mail."
msgid ""
"An B<at >- or B<batch >- command invoked from a B<su>(1)  shell will retain "
"the current userid.  The user will be mailed standard error and standard "
"output from his commands, if any.  Mail will be sent using the command B</"
"usr/sbin/sendmail>.  If B<at> is executed from a B<su>(1)  shell, the owner "
"of the login shell will receive the mail."
msgstr ""
"Sekä B<at> että B<batch> -komennoilla suoritettavat komennot luetaan joko "
"standardisyötteestä tai B<-f> -optiolla nimetystä tiedostosta.  "
"Suoritusaikana oletushakemisto, ympäristö (paitsi muuttujia B<TERM>, "
"B<DISPLAY> ja B<_>)  sekä B<umask> ovat samat kuin B<at> -komentoa "
"annettaessa.  Jos B<at >- tai B<batch >- komento annetaan B<su(1)> -komennon "
"jälkeen, ajo suoritetaan su:n omaksumalla käyttäjätunnuksella.  Käyttäjälle "
"lähetetään sähköpostina komentojen mahdollinen standardituloste ja "
"virheilmoitukset.  Viesti lähetetään komennolla B</usr/lib/sendmail>.  Jos "
"B<at> suoritetaan B<su(1)> :n jälkeen, viesti lähetetään alkuperäiselle "
"tunnukselle."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "I</var/spool/cron/atjobs>"
msgstr "I</var/spool/cron/atjobs>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "I</var/spool/cron/atspool>"
msgstr "I</var/spool/cron/atspool>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid "I</var/spool/at>"
msgstr "I</var/spool/at>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid "I</var/spool/at/spool>"
msgstr "I</var/spool/at/spool>"

#. type: Plain text
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "executes commands when system load levels permit; in other words, when "
#| "the load average drops below 0.8, or the value specified in the "
#| "invocation of B<atrun>."
msgid ""
"executes commands when system load levels permit; in other words, when the "
"load average drops below 0.8, or the value specified in the invocation of "
"B<atd>.  Note that because of the load meaning on Linux, this number is "
"multiplied by the amount of CPUs when compared to the system loadavg."
msgstr ""
"suorittaa komennot, kun systeemin kuormitus on alhainen, eli kun "
"keskimääräinen kuormitus laskee alle arvon 0.8 (oletus), tai B<atrun> -"
"komennolla määritetyn arvon."

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"The definition of the time specification can be found in I</usr/share/doc/"
"packages/at/timespec>."
msgstr ""
"Tarkka formaatti aja määrittelyyn on tiedostossa I</usr/share/doc/packages/"
"at/timespec>."

#. type: Plain text
#: opensuse-tumbleweed
msgid "I</var/spool/atjobs>"
msgstr "I</var/spool/atjobs>"

#. type: Plain text
#: opensuse-tumbleweed
msgid "I</var/spool/atspool>"
msgstr "I</var/spool/atspool>"
