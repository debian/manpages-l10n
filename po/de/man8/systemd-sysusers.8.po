# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2016.
# Helge Kreutzmann <debian@helgefjell.de>, 2018-2019, 2021, 2022, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.1\n"
"POT-Creation-Date: 2024-12-22 07:44+0100\n"
"PO-Revision-Date: 2024-09-06 22:23+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-SYSUSERS"
msgstr "SYSTEMD-SYSUSERS"

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr "systemd 257"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "systemd-sysusers"
msgstr "systemd-sysusers"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"systemd-sysusers, systemd-sysusers.service - Allocate system users and groups"
msgstr ""
"systemd-sysusers, systemd-sysusers.service - Systembenutzer und -gruppen "
"zuweisen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<systemd-sysusers> [OPTIONS...] [I<CONFIGFILE>...]"
msgstr "B<systemd-sysusers> [OPTIONEN …] [I<KONFIGURATIONSDATEI> …]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-sysusers\\&.service"
msgstr "systemd-sysusers\\&.service"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd-sysusers> creates system users and groups, based on files in the "
"format described in B<sysusers.d>(5)\\&."
msgstr ""
"B<Systemd-sysusers> legt, basierend auf Dateien in dem in B<sysusers.d>(5) "
"beschriebenen Format, Systembenutzer und -gruppen an\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"If invoked with no arguments, directives from the configuration files found "
"in the directories specified by B<sysusers.d>(5)  are executed\\&. When "
"invoked with positional arguments, if option B<--replace=>I<PATH> is "
"specified, arguments specified on the command line are used instead of the "
"configuration file I<PATH>\\&. Otherwise, just the configuration specified "
"by the command line arguments is executed\\&. If the string \"-\" is "
"specified instead of a filename, the configuration is read from standard "
"input\\&. If the argument is a file name (without any slashes), all "
"configuration directories are searched for a matching file and the file "
"found that has the highest priority is executed\\&. If the argument is a "
"path, that file is used directly without searching the configuration "
"directories for any other matching file\\&."
msgstr ""
"Beim Aufruf ohne Argumente werden alle Direktiven aus den "
"Konfigurationsdateien in den durch B<sysusers.d>(5) festgelegten "
"Verzeichnissen ausgeführt\\&. Falls die Option B<--replace=>I<PFAD> "
"angegeben wird und der Aufruf mit positionsbezogenen Argumenten erfolgt, "
"werden die auf der Befehlszeile angegebenen Argumente statt der "
"Konfigurationsdatei I<PFAD> verwandt\\&. Andernfalls wird nur die auf der "
"Befehlszeile angegebene Konfiguration ausgeführt\\&. Falls die Zeichenkette "
"»-« anstelle eines Dateinamens angegeben wird, dann wird die Konfiguration "
"aus der Standardeingabe gelesen\\&. Falls das Argument ein Dateiname (ohne "
"Schrägstriche) ist, werden alle Konfigurationsverzeichnisse auf passende "
"Dateien durchsucht und die gefundene Datei mit der höchsten Priorität wird "
"ausgeführt\\&. Falls das Agument ein Pfad ist, wird diese Datei direkt "
"verwandt, ohne in den Konfigurationsdateien nach anderen passenden Dateien "
"zu suchen\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following options are understood:"
msgstr "Die folgenden Optionen werden verstanden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--root=>I<root>"
msgstr "B<--root=>I<Wurzel>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Takes a directory path as an argument\\&. All paths will be prefixed with "
"the given alternate I<root> path, including config search paths\\&."
msgstr ""
"Akzeptiert einen Verzeichnispfad als Argument\\&. Allen Pfaden wird der "
"angegebene alternative I<Wurzel>-Pfad vorangestellt, einschließlich der "
"Suchpfade für die Konfiguration\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Added in version 215\\&."
msgstr "Hinzugefügt in Version 215\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--image=>I<image>"
msgstr "B<--image=>I<Abbild>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Takes a path to a disk image file or block device node\\&. If specified all "
"operations are applied to file system in the indicated disk image\\&. This "
"is similar to B<--root=> but operates on file systems stored in disk images "
"or block devices\\&. The disk image should either contain just a file system "
"or a set of file systems within a GPT partition table, following the "
"\\m[blue]B<Discoverable Partitions Specification>\\m[]"
"\\&\\s-2\\u[1]\\d\\s+2\\&. For further information on supported disk images, "
"see B<systemd-nspawn>(1)\\*(Aqs switch of the same name\\&."
msgstr ""
"Akzeptiert einen Pfad zu einer Plattenabbilddatei oder einem "
"Blockgerätenamen\\&. Falls angegeben, werden alle Aktionen auf das "
"Dateisystem in dem angegebenen Plattenabbild angewandt\\&. Dies ist ähnlich "
"zu B<--root=>, agiert aber auf Dateisystemen, die in Plattenabbildern oder "
"Blockgeräten gespeichert sind\\&. Das Plattenabbild sollte entweder nur ein "
"Dateisystem oder eine Reihe von Dateisystemen innerhalb einer GPT-"
"Partitionstabelle enthalten, die der \\m[blue]B<Spezifikation für "
"auffindbare Partitionen>\\m[]\\&\\s-2\\u[1]\\d\\s+2 folgt\\&. Für weitere "
"Informationen über unterstützte Plattenabbilder, siehe den Schalter von "
"B<systemd-nspawn>(1) mit dem gleichen Namen\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Added in version 247\\&."
msgstr "Hinzugefügt in Version 247\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--image-policy=>I<policy>"
msgstr "B<--image-policy=>I<Richtlinie>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Takes an image policy string as argument, as per B<systemd.image-"
"policy>(7)\\&. The policy is enforced when operating on the disk image "
"specified via B<--image=>, see above\\&. If not specified defaults to the "
"\"*\" policy, i\\&.e\\&. all recognized file systems in the image are "
"used\\&."
msgstr ""
"Akzeptiert gemäß B<systemd.image-policy>(7) eine "
"Abbildrichtlinienzeichenkette als Argument\\&. Die Richtlinie wird bei "
"Aktionen auf dem mittels B<--image=> angegebenen Plattenabbild durchgesetzt, "
"siehe oben\\&. Falls nicht angegeben ist die Vorgabe die Richtlinie »*«, "
"d\\&.h\\&. alle erkannten Dateisysteme im Abbild werden verwandt\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--replace=>I<PATH>"
msgstr "B<--replace=>I<PFAD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When this option is given, one or more positional arguments must be "
"specified\\&. All configuration files found in the directories listed in "
"B<sysusers.d>(5)  will be read, and the configuration given on the command "
"line will be handled instead of and with the same priority as the "
"configuration file I<PATH>\\&."
msgstr ""
"Wird diese Option angegeben, müssen eine oder mehrere positionsbezogene "
"Argumente festgelegt werden\\&. Alle in den in B<sysusers.d>(5) aufgeführten "
"Verzeichnissen gefundenen Konfigurationsdateien werden gelesen und die auf "
"der Befehlszeile übergebene Konfiguration wird statt der Konfigurationsdatei "
"I<PFAD> mit der gleichen Priorität wie diese verwandt\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This option is intended to be used when package installation scripts are "
"running and files belonging to that package are not yet available on disk, "
"so their contents must be given on the command line, but the admin "
"configuration might already exist and should be given higher priority\\&."
msgstr ""
"Wenn die Installationsskripte laufen und zum Paket gehörende Dateien noch "
"nicht verfügbar sind, muss der Inhalt der Skripte auf der Befehlszeile "
"übergeben werden\\&. Diese Option sorgt dafür, dass die "
"Konfigurationsskripte des Administrators eine höhere Priorität erhalten, "
"sofern diese bereits existieren\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<Example\\ \\&1.\\ \\&RPM installation script for radvd>"
msgstr "B<Beispiel\\ \\&1.\\ \\&RPM-Installationsskript für Radvd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"echo \\*(Aqu radvd - \"radvd daemon\"\\*(Aq | \\e\n"
"          systemd-sysusers --replace=/usr/lib/sysusers\\&.d/radvd\\&.conf -\n"
msgstr ""
"echo \\*(Aqu radvd - \"radvd daemon\"\\*(Aq | \\e\n"
"          systemd-sysusers --replace=/usr/lib/sysusers\\&.d/radvd\\&.conf -\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This will create the radvd user as if /usr/lib/sysusers\\&.d/radvd\\&.conf "
"was already on disk\\&. An admin might override the configuration specified "
"on the command line by placing /etc/sysusers\\&.d/radvd\\&.conf or even /etc/"
"sysusers\\&.d/00-overrides\\&.conf\\&."
msgstr ""
"Dies erzeugt den Benutzer radvd, als ob /usr/lib/sysusers\\&.d/radvd\\&.conf "
"bereits auf Platte wäre\\&. Ein Administrator könnte die auf der "
"Befehlszeile übergebene Konfiguration außer Kraft setzen, indem er /etc/"
"sysusers\\&.d/radvd\\&.conf oder sogar /etc/sysusers\\&.d/00-"
"overrides\\&.conf erzeugt\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that this is the expanded form, and when used in a package, this would "
"be written using a macro with \"radvd\" and a file containing the "
"configuration line as arguments\\&."
msgstr ""
"Beachten Sie, dass dies dies die expandierte Form ist\\&. Wird dies in einem "
"Paket verwandt, würde es mit einem Makro mit »radvd« und einer Datei, die "
"die Konfigurationszeile als Argumente enthält, geschrieben\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Added in version 238\\&."
msgstr "Hinzugefügt in Version 238\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--dry-run>"
msgstr "B<--dry-run>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Process the configuration and figure out what entries would be created, but "
"don\\*(Aqt actually write anything\\&."
msgstr ""
"Verarbeitet die Konfiguration und ermittelt, welche Einträge erstellt "
"würden, aber schreibt nichts wirklich\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Added in version 250\\&."
msgstr "Hinzugefügt in Version 250\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--inline>"
msgstr "B<--inline>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Treat each positional argument as a separate configuration line instead of a "
"file name\\&."
msgstr ""
"Behandelt jedes positionsabhängige Argument als separate Konfigurationszeile "
"statt als einen Dateinamen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--cat-config>"
msgstr "B<--cat-config>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Copy the contents of config files to standard output\\&. Before each file, "
"the filename is printed as a comment\\&."
msgstr ""
"Kopiert den Inhalt der Konfigurationsdateien in die Standardausgabe\\&. Vor "
"jeder Datei wird der Dateiname als Kommentar ausgegeben\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--tldr>"
msgstr "B<--tldr>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Copy the contents of config files to standard output\\&. Only the "
"\"interesting\" parts of the configuration files are printed, comments and "
"empty lines are skipped\\&. Before each file, the filename is printed as a "
"comment\\&."
msgstr ""
"Kopiert den Inhalt der Konfigurationsdateien in die Standardausgabe\\&. Nur "
"der »interessante« Teil der Konfigurationsdateien wird ausgegeben, "
"Kommentare und leere Zeilen werden übersprungen\\&. Vor jeder Datei wird der "
"Dateiname als Kommentar ausgegeben\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--no-pager>"
msgstr "B<--no-pager>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Do not pipe output into a pager\\&."
msgstr "Leitet die Ausgabe nicht an ein Textanzeigeprogramm weiter\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr "Zeigt einen kurzen Hilfetext an und beendet das Programm\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print a short version string and exit\\&."
msgstr "Zeigt eine kurze Versionszeichenkette an und beendet das Programm\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CREDENTIALS"
msgstr "ZUGANGSDATEN"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<systemd-sysusers> supports the service credentials logic as implemented by "
"I<ImportCredential=>/I<LoadCredential=>/I<SetCredential=> (see "
"B<systemd.exec>(5)  for details)\\&. The following credentials are used when "
"passed in:"
msgstr ""
"B<systemd-sysusers> unterstützt die Dienste-Zugangsdatenlogik, wie sie durch "
"I<ImportCredential=>/I<LoadCredential=>/I<SetCredential=> implementiert wird "
"(siehe B<systemd.exec>(5) für Details)\\&. Die folgenden Anmeldedaten werden "
"verwandt, wenn sie hereingereicht werden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<passwd\\&.hashed-password\\&.>I<user>"
msgstr "I<passwd\\&.hashed-password\\&.>I<Benutzer>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A UNIX hashed password string to use for the specified user, when creating "
"an entry for it\\&. This is particularly useful for the \"root\" user as it "
"allows provisioning the default root password to use via a unit file drop-in "
"or from a container manager passing in this credential\\&. Note that setting "
"this credential has no effect if the specified user account already "
"exists\\&. This credential is hence primarily useful in first boot scenarios "
"or systems that are fully stateless and come up with an empty /etc/ on every "
"boot\\&."
msgstr ""
"Eine gehashte UNIX-Passwortzeichenkette, die für den festgelegten Benutzer "
"verwandt werden soll, wenn für ihn ein Eintrag erstellt wird\\&. Dies ist "
"insbesondere für den Benutzer »root« hilfreich, da er die Bereitstellung "
"eines Vorgabe-Passworts für root über eine Unit-Dateiergänzung oder einen "
"Container-Verwalter, der dieses Zugangsdatum weitergibt, erlaubt\\&. "
"Beachten Sie, dass das Setzen dieses Zugangsdatums keine Auswirkung hat, "
"falls das festgelegte Benutzerkonto bereits existiert\\&. Dieses "
"Zugangsdatum ist daher hauptsächlich für Erst-Systemstart-Szenarien oder "
"Systeme, die vollständig zustandslos sind, und bei jedem Systemstart mit "
"einem leeren /etc/ hochkommen, nützlich\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Added in version 249\\&."
msgstr "Hinzugefügt in Version 249\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<passwd\\&.plaintext-password\\&.>I<user>"
msgstr "I<passwd\\&.plaintext-password\\&.>I<Benutzer>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Similar to \"passwd\\&.hashed-password\\&.I<user>\" but expect a literal, "
"plaintext password, which is then automatically hashed before used for the "
"user account\\&. If both the hashed and the plaintext credential are "
"specified for the same user the former takes precedence\\&. It\\*(Aqs "
"generally recommended to specify the hashed version; however in test "
"environments with weaker requirements on security it might be easier to pass "
"passwords in plaintext instead\\&."
msgstr ""
"Ähnlich zu »passwd\\&.hashed-password\\&.I<Benutzer>«, erwartet aber ein "
"direktes Klartextpasswort, das automatisch gehasht wird, bevor es für das "
"Benutzerkonto verwandt wird\\&. Falls für den gleichen Benutzer sowohl die "
"Klartext- als auch die gehashten Zugangsdaten festgelegt werden, haben "
"erstere Vorrang\\&. Es wird im Allgemeinen empfohlen, die gehashte Version "
"zu verwenden; in Testumgebungen mit geringeren Sicherheitsanforderungen mag "
"es leichter sein, stattdessen Passwörter direkt im Klartext zu übergeben\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<passwd\\&.shell\\&.>I<user>"
msgstr "I<passwd\\&.shell\\&.>I<Benutzer>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifies the shell binary to use for the specified account when creating "
"it\\&."
msgstr ""
"Gibt das Shell-Programm an, das für das festgelegte Konto bei der Erstellung "
"verwandt werden soll\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<sysusers\\&.extra>"
msgstr "I<sysusers\\&.extra>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The contents of this credential may contain additional lines to operate "
"on\\&. The credential contents should follow the same format as any other "
"sysusers\\&.d/ drop-in\\&. If this credential is passed it is processed "
"after all of the drop-in files read from the file system\\&."
msgstr ""
"Der Inhalt dieser Zugangsberechtigung darf zusätzliche Zeilen enthalten, auf "
"die agiert wird\\&. Die Zugangsberechtigungsinhalte sollten dem gleichen "
"Format wie jede andere Ergänzung für sysusers\\&.d/ folgen\\&. Falls die "
"Zugangsberechtigung hereingereicht wird, wird sie nach allen anderen, aus "
"dem Dateisystem gelesenen Ergänzungsdateien verarbeitet\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Added in version 252\\&."
msgstr "Hinzugefügt in Version 252\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that by default the systemd-sysusers\\&.service unit file is set up to "
"inherit the \"passwd\\&.hashed-password\\&.root\", \"passwd\\&.plaintext-"
"password\\&.root\", \"passwd\\&.shell\\&.root\" and \"sysusers\\&.extra\" "
"credentials from the service manager\\&. Thus, when invoking a container "
"with an unpopulated /etc/ for the first time it is possible to configure the "
"root user\\*(Aqs password to be \"systemd\" like this:"
msgstr ""
"Beachten Sie, dass standardmäßig die Unit-Datei systemd-sysusers\\&.service "
"so eingerichtet ist, dass es die Zugangsdaten »passwd\\&.hashed-"
"password\\&.root«, »passwd\\&.plaintext-password\\&.root«, "
"»passwd\\&.shell\\&.root« und »sysusers\\&.extra« vom Diensteverwalter "
"erbt\\&. Wird daher ein Container mit einem leeren /etc/ das erste Mal "
"aufgerufen, ist es möglich, das Passwort für den Benutzer root wie folgt auf "
"»systemd« zu konfigurieren:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "# systemd-nspawn --image=\\&... --set-credential=passwd\\&.hashed-password\\&.root:\\*(Aq$y$j9T$yAuRJu1o5HioZAGDYPU5d\\&.$F64ni6J2y2nNQve90M/p0ZP0ECP/qqzipNyaY9fjGpC\\*(Aq \\&...\n"
msgstr "# systemd-nspawn --image=… --set-credential=passwd\\&.hashed-password\\&.root:\\*(Aq$y$j9T$yAuRJu1o5HioZAGDYPU5d\\&.$F64ni6J2y2nNQve90M/p0ZP0ECP/qqzipNyaY9fjGpC\\*(Aq …\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note again that the data specified in this credential is consulted only when "
"creating an account for the first time, it may not be used for changing the "
"password or shell of an account that already exists\\&."
msgstr ""
"Beachten Sie nochmals, dass die in diesem Zugangsdatum festgelegten Daten "
"nur herangezogen werden, wenn erstmalig ein Konto erstellt wird, sie können "
"nicht zum Ändern des Passworts oder der Shell eines bereits existierenden "
"Kontos verwandt werden\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Use B<mkpasswd>(1)  for generating UNIX password hashes from the command "
"line\\&."
msgstr ""
"Verwenden Sie B<mkpasswd>(1) zur Erstellung von UNIX-Passwort-Hashes auf der "
"Befehlszeile\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "EXIT-STATUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "On success, 0 is returned, a non-zero failure code otherwise\\&."
msgstr ""
"Bei Erfolg wird 0 zurückgegeben, anderenfalls ein Fehlercode ungleich "
"Null\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<sysusers.d>(5), \\m[blue]B<Users, Groups, UIDs and GIDs on "
"systemd systems>\\m[]\\&\\s-2\\u[2]\\d\\s+2, B<systemd.exec>(5), "
"B<mkpasswd>(1)"
msgstr ""
"B<systemd>(1), B<sysusers.d>(5), \\m[blue]B<Benutzer, Gruppen, UIDs und GIDs "
"auf Systemd-Systemen>\\m[]\\&\\s-2\\u[2]\\d\\s+2, B<systemd.exec>(5), "
"B<mkpasswd>(1)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Discoverable Partitions Specification"
msgstr "Spezifikation für auffindbare Partitionen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\%https://uapi-group.org/specifications/specs/"
"discoverable_partitions_specification"
msgstr ""
"\\%https://uapi-group.org/specifications/specs/"
"discoverable_partitions_specification"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 2."
msgstr " 2."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Users, Groups, UIDs and GIDs on systemd systems"
msgstr "Benutzer, Gruppen, UIDs und GIDs auf Systemd-Systemen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://systemd.io/UIDS-GIDS"
msgstr "\\%https://systemd.io/UIDS-GIDS"

#. type: TH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0
msgid ""
"If invoked with no arguments, it applies all directives from all files found "
"in the directories specified by B<sysusers.d>(5)\\&. When invoked with "
"positional arguments, if option B<--replace=>I<PATH> is specified, arguments "
"specified on the command line are used instead of the configuration file "
"I<PATH>\\&. Otherwise, just the configuration specified by the command line "
"arguments is executed\\&. The string \"-\" may be specified instead of a "
"filename to instruct B<systemd-sysusers> to read the configuration from "
"standard input\\&. If the argument is a relative path, all configuration "
"directories are searched for a matching file and the file found that has the "
"highest priority is executed\\&. If the argument is an absolute path, that "
"file is used directly without searching of the configuration directories\\&."
msgstr ""
"Beim Aufruf ohne Argumente wendet es alle Anweisungen aus allen Dateien in "
"den durch B<sysusers.d>(5) festgelegten Verzeichnissen an\\&. Falls die "
"Option B<--replace=>I<PFAD> angegeben wird und der Aufruf mit "
"positionsbezogenen Argumenten erfolgt, werden die auf der Befehlszeile "
"angegebenen Argumente statt der Konfigurationsdatei I<PFAD> verwandt\\&. "
"Andernfalls wird nur die auf der Befehlszeile angegebene Konfiguration "
"ausgeführt\\&. Die Zeichenkette »-« kann statt eines Dateinamens angegeben "
"werden, um B<systemd-sysusers> anzuweisen, die Konfiguration aus der "
"Standardeingabe zu lesen\\&. Falls das Argument ein relativer Pfad ist, "
"werden alle Konfigurationsverzeichnisse auf passende Dateien durchsucht und "
"die gefundene Datei mit der höchsten Priorität wird ausgeführt\\&. Falls das "
"Agument ein absoluter Pfad ist, wird diese Datei direkt verwandt, ohne in "
"den Konfigurationsdateien zu suchen\\&."

# FIXME I<ImportCredential=> → I<ImportCredential=>/
#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"B<systemd-sysusers> supports the service credentials logic as implemented by "
"I<ImportCredential=>I<LoadCredential=>/I<SetCredential=> (see "
"B<systemd.exec>(1)  for details)\\&. The following credentials are used when "
"passed in:"
msgstr ""
"B<systemd-sysusers> unterstützt die Dienste-Zugangsdatenlogik, wie sie durch "
"I<ImportCredential=>/I<LoadCredential=>/I<SetCredential=> implementiert wird "
"(siehe B<systemd.exec>(1) für Details)\\&. Die folgenden Anmeldedaten werden "
"verwandt, wenn sie hereingereicht werden:"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0
msgid ""
"B<systemd>(1), B<sysusers.d>(5), \\m[blue]B<Users, Groups, UIDs and GIDs on "
"systemd systems>\\m[]\\&\\s-2\\u[2]\\d\\s+2, B<systemd.exec>(1), "
"B<mkpasswd>(1)"
msgstr ""
"B<systemd>(1), B<sysusers.d>(5), \\m[blue]B<Benutzer, Gruppen, UIDs und GIDs "
"auf Systemd-Systemen>\\m[]\\&\\s-2\\u[2]\\d\\s+2, B<systemd.exec>(1), "
"B<mkpasswd>(1)"

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr "systemd 257.1"

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr "systemd 256.7"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr "systemd 255"

#. type: Plain text
#: mageia-cauldron
msgid ""
"B<systemd-sysusers> supports the service credentials logic as implemented by "
"I<ImportCredential=>/I<LoadCredential=>/I<SetCredential=> (see "
"B<systemd.exec>(1)  for details)\\&. The following credentials are used when "
"passed in:"
msgstr ""
"B<systemd-sysusers> unterstützt die Dienste-Zugangsdatenlogik, wie sie durch "
"I<ImportCredential=>/I<LoadCredential=>/I<SetCredential=> implementiert wird "
"(siehe B<systemd.exec>(1) für Details)\\&. Die folgenden Anmeldedaten werden "
"verwandt, wenn sie hereingereicht werden:"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 256.8"
msgstr "systemd 256.8"
