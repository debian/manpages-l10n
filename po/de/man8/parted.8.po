# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2014, 2020, 2024.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2013-2014, 2019-2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.1\n"
"POT-Creation-Date: 2024-09-06 18:22+0200\n"
"PO-Revision-Date: 2024-08-27 18:33+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "PARTED"
msgstr "PARTED"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "2021 September 28"
msgstr "28. September 2021"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "parted"
msgstr "parted"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GNU Parted Manual"
msgstr "GNU-Parted-Handbuch"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "parted - a partition manipulation program"
msgstr "parted - ein Programm zur Bearbeitung von Partitionen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<parted> [options] [device [command [options...]...]]"
msgstr "B<parted> [Optionen] [Gerät [Befehl [Optionen…]…]]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<parted> is a program to manipulate disk partitions.  It supports multiple "
"partition table formats, including MS-DOS and GPT.  It is useful for "
"creating space for new operating systems, reorganising disk usage, and "
"copying data to new hard disks."
msgstr ""
"B<parted> ist ein Programm zum Bearbeiten von Datenträgerpartitionen. Es "
"unterstützt mehrere Formate von Partitionstabellen, einschließlich MS-DOS "
"und GPT. Es ist verwendbar zum Bereitstellen von Platz für neue "
"Betriebssysteme, zum Neuordnen des Plattenplatzes und zum Kopieren von Daten "
"auf neue Festplatten."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page documents B<parted> briefly.  Complete documentation is "
"distributed with the package in GNU Info format."
msgstr ""
"Diese Handbuchseite ist eine Kurzbeschreibung von B<parted>. Die "
"vollständige Dokumentation ist im Paket im GNU-Info-Format enthalten."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h, --help>"
msgstr "B<-h, --help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "displays a help message"
msgstr "Hilfe anzeigen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-l, --list>"
msgstr "B<-l, --list>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "lists partition layout on all block devices"
msgstr "Partitionslayout auf allen Blockgeräten auflisten"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-m, --machine>"
msgstr "B<-m, --machine>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "displays machine parseable output"
msgstr "Maschinenlesbare Ausgaben anzeigen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-j, --json>"
msgstr "B<-j, --json>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "displays JSON output"
msgstr "zeigt die Ausgabe im JSON-Format an."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-s, --script>"
msgstr "B<-s, --script>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "never prompts for user intervention"
msgstr "Nie um Eingriff des Benutzers bitten"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f, --fix>"
msgstr "B<-f, --fix>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "automatically answer \"fix\" to exceptions in script mode"
msgstr "antwortet im Skriptmodus auf Ausnahmen automatisch mit »fix«."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v, --version>"
msgstr "B<-v, --version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "displays the version"
msgstr "Version anzeigen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a >I<alignment-type>B<, --align >I<alignment-type>"
msgstr "B<-a >I<Ausrichtungstyp>B<, --align >I<Ausrichtungstyp>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Set alignment for newly created partitions, valid alignment types are:"
msgstr ""
"Ausrichtung für neu angelegte Partitionen festlegen. Gültige "
"Ausrichtungstypen sind:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "none"
msgstr "none"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Use the minimum alignment allowed by the disk type."
msgstr ""
"Die vom jeweiligen Datenträgertyp erlaubte minimale Ausrichtung verwenden."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "cylinder"
msgstr "cylinder"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Align partitions to cylinders."
msgstr "Partitionen an Zylindern ausrichten."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "minimal"
msgstr "minimal"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Use minimum alignment as given by the disk topology information. This and "
"the opt value will use layout information provided by the disk to align the "
"logical partition table addresses to actual physical blocks on the disks.  "
"The min value is the minimum alignment needed to align the partition "
"properly to physical blocks, which avoids performance degradation."
msgstr ""
"Minimale Ausrichtung, wie durch die topologischen Informationen der Platte "
"angegeben. Dies und der Optimalwert verwenden die von der Platte selbst "
"gemeldeten Layout-Informationen zur Ausrichtung der Adressen der logischen "
"Partitionstabelle an den tatsächlichen physischen Blöcken der Platte. Der "
"Minimalwert ist die minimale Ausrichtung, die für die saubere Ausrichtung "
"der Partition an den physischen Blöcken nötig ist, was Einschränkungen der "
"Performance bewirkt."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "optimal"
msgstr "optimal"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Use optimum alignment as given by the disk topology information. This aligns "
"to a multiple of the physical block size in a way that guarantees optimal "
"performance."
msgstr ""
"Optimale Ausrichtung, wie durch die topologischen Informationen der Platte "
"angegeben. Dies bewirkt eine Ausrichtung an einem Vielfachen der physischen "
"Blockgröße in einer Weise, die optimale Performance gewährleistet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COMMANDS"
msgstr "BEFEHLE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<[device]>"
msgstr "B<[Gerät]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The block device to be used.  When none is given, B<parted> will use the "
"first block device it finds."
msgstr ""
"gibt das zu verwendende Blockgerät an. Wenn nichts angegeben wird, verwendet "
"B<parted> den ersten Block des ersten gefundenen Blockgeräts."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<[command [options]]>"
msgstr "B<[Befehl [Optionen]]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifies the command to be executed.  If no command is given, B<parted> "
"will present a command prompt.  Possible commands are:"
msgstr ""
"gibt den auszuführenden Befehl an. Falls kein Befehl angegeben wird, öffnet "
"B<parted> eine Eingabeaufforderung. Mögliche Befehle sind:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<help >I<[command]>"
msgstr "B<help >I<[Befehl]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print general help, or help on I<command> if specified."
msgstr ""
"zeigt eine allgemeine Hilfe an, oder Hilfe zu I<Befehl>, falls angegeben."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<align-check >I<type>B< >I<partition>"
msgstr "B<align-check >I<Typ>B< >I<Partition>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Check if I<partition> satisfies the alignment constraint of I<type>.  "
"I<type> must be \"minimal\" or \"optimal\"."
msgstr ""
"Überprüfen, ob die I<Partition> die Ausrichtungsbeschränkungen für I<Typ> "
"erfüllt. I<Typ> ist entweder »minimal« oder »optimal«."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<mklabel >I<label-type>"
msgstr "B<mklabel >I<Disklabel-Typ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Create a new disklabel (partition table) of I<label-type>.  I<label-type> "
"should be one of \"aix\", \"amiga\", \"bsd\", \"dvh\", \"gpt\", \"loop\", "
"\"mac\", \"msdos\", \"pc98\", or \"sun\"."
msgstr ""
"erstellt ein neues Disklabel (Partitionstabelle) des Typs I<Disklabel-Typ>. "
"I<Disklabel-Typ> sollte »aix«, »amiga«, »bsd«, »dvh«, »gpt«, »loop«, »mac«, "
"»msdos«, »pc98« oder »sun« sein."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<mkpart [>I<part-type>B< >I<name>B< >I<fs-type>B<] >I<start>B< >I<end>"
msgstr "B<mkpart [>I<Partitionstyp>B< >I<Name>B< >I<Dateisystemtyp>B<] >I<Beginn>B< >I<Ende>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Create a new partition. I<part-type> may be specified only with msdos and "
"dvh partition tables, it should be one of \"primary\", \"logical\", or "
"\"extended\".  I<name> is required for GPT partition tables and I<fs-type> "
"is optional.  I<fs-type> can be one of \"btrfs\", \"ext2\", \"ext3\", "
"\"ext4\", \"fat16\", \"fat32\", \"hfs\", \"hfs+\", \"linux-swap\", \"ntfs\", "
"\"reiserfs\", \"udf\", or \"xfs\"."
msgstr ""
"erzeugt eine neue Partition. Der I<Partitionstyp> darf nur anhand von MSDOS- "
"und DVH-Partitionstabellen angegeben werden, er sollte »primary«, »logical« "
"oder »extended« sein. Der I<Name> ist für GPT-Partitionstabellen "
"erforderlich, der I<Dateisystemtyp> ist optional. Der I<Dateisystemtyp> kann "
"einer von »btrfs«, »ext2«, »ext3«, »ext4«, »fat16«, »fat32«, »hfs«, »hfs+«, "
"»linux-swap«, »ntfs«, »reiserfs«, »udf« oder »xfs« sein."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<name >I<partition>B< >I<name>"
msgstr "B<name >I<Partition>B< >I<Bezeichnung>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Set the name of I<partition> to I<name>. This option works only on Mac, "
"PC98, and GPT disklabels. The name can be placed in double quotes, if "
"necessary.  And depending on the shell may need to also be wrapped in single "
"quotes so that the shell doesn't strip off the double quotes."
msgstr ""
"Den Namen der I<Partition> als I<Name> festlegen. Diese Option ist nur für "
"Mac-, PC-98- und GPT-Disklabels verfügbar. Der Name kann in "
"Anführungszeichen gesetzt werden, falls notwendig. Abhängig von der Shell "
"kann es außerdem erforderlich sein, den Namen in Hochkommata einzuschließen, "
"damit die Shell die Anführungszeichen nicht entfernt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<print >I<print-type>"
msgstr "B<print >I<Ausgabetyp>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Display the partition table.  I<print-type> is optional, and can be one of "
"devices, free, list, or all."
msgstr ""
"zeigt die Partitionstabelle an. Der I<Ausgabetyp> ist optional und kann "
"entweder »devices«, »free«, »list« oder »all« sein."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<quit>"
msgstr "B<quit>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Exit from B<parted>."
msgstr "B<parted> beenden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<rescue >I<start>B< >I<end>"
msgstr "B<rescue >I<Beginn>B< >I<Ende>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Rescue a lost partition that was located somewhere between I<start> and "
"I<end>.  If a partition is found, B<parted> will ask if you want to create "
"an entry for it in the partition table."
msgstr ""
"Eine verlorene Partition wiederherstellen, die irgendwo zwischen I<Beginn> "
"und I<Ende> liegt. Falls eine Partition gefunden wird, fragt B<parted>, ob "
"Sie dafür einen Eintrag in der Partitionstabelle erstellen wollen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<resizepart >I<partition>B< >I<end>"
msgstr "B<resizepart >I<Partition>B< >I<Ende>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Change the I<end> position of I<partition>.  Note that this does not modify "
"any filesystem present in the partition."
msgstr ""
"ändert die I<Ende>-Position einer I<Partition>. Beachten Sie, dass dies "
"keine Auswirkungen auf ein eventuell in der Partition befindliches "
"Dateisystem hat."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<rm >I<partition>"
msgstr "B<rm >I<Partition>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Delete I<partition>."
msgstr "I<Partition> löschen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<select >I<device>"
msgstr "B<select >I<Gerät>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Choose I<device> as the current device to edit. I<device> should usually be "
"a Linux hard disk device, but it can be a partition, software raid device, "
"or an LVM logical volume if necessary."
msgstr ""
"I<Gerät> als das aktuell zu bearbeitende Gerät wählen. I<Gerät> sollte "
"üblicherweise ein Linux-Festplattengerät sein, kann aber auch eine "
"Partition, ein Software-RAID-Gerät oder ein logischer Datenträger (LVM) "
"sein, falls erforderlich."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<set >I<partition>B< >I<flag>B< >I<state>"
msgstr "B<set >I<Partition>B< >I<Flag>B< >I<Status>"

# HK: Flags → Schalter? (siehe Wortliste)
# MB: »Flags« ist bei Partitionierungsprogrammen weit verbreitet. Es geht
# hierbei nicht um Flags im Sinne von Schaltern für die Befehlszeile, sondern
# um die Flags von Partitionen (bootable usw.)
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Change the state of the I<flag> on I<partition> to I<state>.  Supported "
"flags are: \"boot\", \"root\", \"swap\", \"hidden\", \"raid\", \"lvm\", "
"\"lba\", \"legacy_boot\", \"irst\", \"msftres\", \"esp\", "
"\"chromeos_kernel\", \"bls_boot\", \"linux-home\", \"no_automount\", "
"\"bios_grub\", and \"palo\".  I<state> should be either \"on\" or \"off\"."
msgstr ""
"ändert den Status von I<Flag> auf I<Partition> in I<Status>. Unterstützte "
"Flags sind: »boot«, »root«, »swap«, »hidden«, »raid«, »lvm«, »lba«, "
"»legacy_boot«, »irst«, »msftres«, »esp«, »chromeos_kernel«, »bls_boot«, "
"»linux-home«, »no_automount«, »bios_grub« und »palo«. I<Status> sollte "
"entweder »on« oder »off« sein."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<unit >I<unit>"
msgstr "B<unit >I<Einheit>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Set I<unit> as the unit to use when displaying locations and sizes, and for "
"interpreting those given by the user when not suffixed with an explicit "
"unit.  I<unit> can be one of \"s\" (sectors), \"B\" (bytes), \"kB\", \"MB\", "
"\"KiB\", \"MiB\", \"GB\", \"GiB\", \"TB\", \"TiB\", \"%\" (percentage of "
"device size), \"cyl\" (cylinders), \"chs\" (cylinders, heads, sectors), or "
"\"compact\" (megabytes for input, and a human-friendly form for output)."
msgstr ""
"legt I<Einheit> als die Einheit für die Anzeige von Positionen und Größen "
"fest, sowie für die Interpretation der Benutzereingaben, wenn der Benutzer "
"keine bestimmte Einheit angibt. I<Einheit> kann »s« (Sektoren), »B« (Byte), "
"»kB«, »MB«, »KiB«, »MiB«, »GB«, »GiB«, »TB«, »TiB«, »%« (Prozentsatz bezogen "
"auf die Gerätegröße), »cyl« (Zylinder), »chs« (Zylinder, Köpfe, Sektoren) "
"oder »compact« sein (Megabyte für die Eingabe und eine menschenlesbare Form "
"für die Ausgabe)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<toggle >I<partition>B< >I<flag>"
msgstr "B<toggle >I<Partition>B< >I<Flag>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Toggle the state of I<flag> on I<partition>."
msgstr "ändert den Status von I<Flag> der I<Partition>."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<type >I<partition>B< >I<id>B< or >I<uuid>"
msgstr "B<type >I<Partition>B< >I<Kennung>B< oder >I<UUID>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On MS-DOS set the type aka. partition id of I<partition> to I<id>. The I<id> "
"is a value between \"0x01\" and \"0xff\". On GPT the type-uuid of "
"I<partition> to I<uuid>."
msgstr ""
"setzt unter MS-DOS den Typ bzw. die Partitionskennung der I<Partition> auf "
"die angegebene I<Kennung>. Diese I<Kennung> ist ein Wert zwischen »0x01« und "
"»0xff«. Auf GPT-Systemen wird die Typ-UUID der I<Partition> auf die "
"angegebene I<UUID> gesetzt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<disk_set >I<flag>B< >I<state>"
msgstr "B<disk_set >I<Flag>B< >I<Status>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Change a I<flag> on the disk to I<state>. A flag can be either \"on\" or "
"\"off\".  Some or all of these flags will be available, depending on what "
"disk label you are using.  Supported flags are: \"pmbr_boot\" on GPT to "
"enable the boot flag on the GPT's protective MBR partition."
msgstr ""
"ändert ein I<Flag> auf der Platte in den angegebenen I<Status>. Ein Flag "
"kann entweder »on« oder »off« sein. Abhängig von der gewählten "
"Festplattenbezeichnung sind einige oder alle dieser Flags verfügbar. "
"Folgende Flags werden unterstützt: »pmbr_boot« auf GPT zur Aktivierung des "
"Bootfähig-Flags auf der geschützten MBR-Partition von GPT."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<disk_toggle >I<flag>"
msgstr "B<disk_toggle >I<Flag>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Toggle the state of the disk I<flag>."
msgstr "ändert den Status von I<Flag> der Platte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<version>"
msgstr "B<version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Display version information and a copyright message."
msgstr "Versions- und Copyright-Informationen anzeigen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-parted@gnu.orgE<gt>"
msgstr "Melden Sie Fehler (auf Englisch) an E<.MT bug-parted@gnu.org>E<.ME .>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

# FIXME B<info(1)> → B<info>(1)
#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<fdisk>(8), B<mkfs>(8), The I<parted> program is fully documented in the "
"B<info(1)> format I<GNU partitioning software> manual."
msgstr ""
"B<fdisk>(8), B<mkfs>(8), das Programm I<parted> ist im Handbuch I<GNU "
"partitioning software> im B<info>(1)-Format vollständig dokumentiert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page was written by Timshel Knoll E<lt>timshel@debian.orgE<gt>, "
"for the Debian GNU/Linux system (but may be used by others)."
msgstr ""
"Diese Handbuchseite wurde von E<.MT timshel@debian.org>Timshel KnollE<.ME> "
"für das Debian GNU/Linux-System geschrieben (darf aber auch von anderen "
"verwendet werden)."

# HK: Flags → Schalter? (siehe Wortliste)
# MB: »Flags« ist bei Partitionierungsprogrammen weit verbreitet. Es geht
# hierbei nicht um Flags im Sinne von Schaltern für die Befehlszeile, sondern
# um die Flags von Partitionen (bootable usw.)
#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Change the state of the I<flag> on I<partition> to I<state>.  Supported "
"flags are: \"boot\", \"root\", \"swap\", \"hidden\", \"raid\", \"lvm\", "
"\"lba\", \"legacy_boot\", \"irst\", \"msftres\", \"esp\", "
"\"chromeos_kernel\", \"bls_boot\", \"linux-home\", \"bios_grub\", and "
"\"palo\".  I<state> should be either \"on\" or \"off\"."
msgstr ""
"ändert den Status von I<Flag> auf I<Partition> in I<Status>. Unterstützte "
"Flags sind: »boot«, »root«, »swap«, »hidden«, »raid«, »lvm«, »lba«, "
"»legacy_boot«, »irst«, »msftres«, »esp«, »chromeos_kernel«, »bls_boot«, "
"»linux-home«, »bios_grub« und »palo«. I<Status> sollte entweder »on« oder "
"»off« sein."

# FIXME B<info(1)> → B<info>(1)
#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<fdisk>(8), B<mkfs>(8), The I<parted> program is fully documented in the "
"B<info(1)> format I<GNU partitioning software> manual which is distributed "
"with the parted-doc Debian package."
msgstr ""
"B<fdisk>(8), B<mkfs>(8), das Programm I<parted> ist im Handbuch I<GNU "
"partitioning software> im B<info>(1)-Format, das mit dem Parted-Doc-Debian-"
"Paket verteilt wird, vollständig dokumentiert."

#. type: SH
#: fedora-41 fedora-rawhide
#, no-wrap
msgid "UNITS"
msgstr "EINHEITEN"

#. type: Plain text
#: fedora-41 fedora-rawhide
msgid ""
"B<parted> will compute sensible ranges for the locations you specify when "
"using units like \"GB\", \"MB\", etc. Use the sector unit \"s\" or IEC "
"binary units like \"GiB\", \"MiB\", to specify exact locations."
msgstr ""
"B<parted> wird für die von Ihnen angegebenen Orte vernünftige Bereiche "
"berechnen, wenn Sie Einheiten wie »GB«, »MB« usw. verwenden. Verwenden Sie "
"Sektoreinheiten »s« oder IEC-Binäreinheiten wie »GiB«, »MiB«, um genaue Orte "
"festzulegen."

#. type: Plain text
#: fedora-41 fedora-rawhide
msgid ""
"When you specify start or end values using IEC binary units like \"MiB\", "
"\"GiB\", \"TiB\", etc., B<parted> treats those values as exact, and "
"equivalent to the same number specified in bytes (i.e., with the \"B\" "
"suffix), in that it provides no helpful range of sloppiness.  Contrast that "
"with a partition start request of \"4GB\", which may actually resolve to "
"some sector up to 500MB before or after that point.  Thus, when creating a "
"partition in an exact location you should use units of bytes (\"B\"), "
"sectors (\"s\"), or IEC binary units like \"MiB\", \"GiB\", but not \"MB\", "
"\"GB\", etc."
msgstr ""
"Wenn Sie Start- oder Endwerte mittels IEC-Binäreinheiten wie »MiB«, »GiB«, "
"»TiB« usw. festlegen, behandelt B<parted> diese Werte als genau und "
"äquivalent zu der gleichen, in Bytes festgelegten Zahl (d.h. mit der Endung "
"»B«), was bedeutet, dass es keinen hilfreichen Ungenauigkeitsbereich "
"bereitstellt. Im Gegensatz dazu ist die Angabe von »4GB«, die sich "
"tatsächlich auf einen Sektor bis zu 500 MB vor oder nach dem Punkt auflösen "
"könnte. Wenn Sie daher Partitionen an einem genauen Ort erstellen, verwenden "
"Sie Einheiten von Bytes (»B«), Sektoren (»s«) oder IEC-Binäreinheiten wie "
"»MiB«, »GiB«, aber nicht »MB«, »GB« usw."

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--ignore-busy>"
msgstr "B<--ignore-busy>"

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"perform the requested action in script mode although a partition is busy"
msgstr ""
"führt die angeforderte Aktion im Skriptmodus aus, obwohl eine Partition "
"belegt ist."

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--wipesignatures>"
msgstr "B<--wipesignatures>"

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"mkpart wipes the superblock signatures from the disk region where it is "
"about to create the partition"
msgstr ""
"Mkpart entfernt die Superblock-Signaturen von der Plattenregion, an der es "
"die Partition erstellen wird."
