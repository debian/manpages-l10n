# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:00+0100\n"
"PO-Revision-Date: 2024-03-02 13:08+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB-OFPATHNAME"
msgstr "GRUB-OFPATHNAME"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "October 2024"
msgstr "Oktober 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GRUB 2.12"
msgstr "GRUB 2.12"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemverwaltungswerkzeuge"

#. type: SH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "grub-ofpathname - find OpenBOOT path for a device"
msgstr "grub-ofpathname - OpenBOOT-Pfad für ein Gerät finden"

#. type: SH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<grub-ofpathname> I<\\,DEVICE\\/>"
msgstr "B<grub-ofpathname> I<\\,GERÄT\\/>"

#. type: SH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: SH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-ofpathname> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-ofpathname> programs are properly "
"installed at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-ofpathname> wird als ein Texinfo-"
"Handbuch gepflegt. Wenn die Programme B<info>(1) und B<grub-ofpathname> auf "
"Ihrem Rechner ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<info grub-ofpathname>"
msgstr "B<info grub-ofpathname>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "November 2024"
msgstr "November 2024"
