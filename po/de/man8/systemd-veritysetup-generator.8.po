# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2018, 2021, 2022, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-12-22 07:45+0100\n"
"PO-Revision-Date: 2024-12-03 18:58+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYSTEMD-VERITYSETUP-GENERATOR"
msgstr "SYSTEMD-VERITYSETUP-GENERATOR"

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr "systemd 257"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "systemd-veritysetup-generator"
msgstr "systemd-veritysetup-generator"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-veritysetup-generator - Unit generator for verity protected block "
"devices"
msgstr ""
"systemd-veritysetup-generator - Unit-Generator für Verity-geschützte "
"Blockgeräte"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "/usr/lib/systemd/system-generators/systemd-veritysetup-generator"
msgstr "/usr/lib/systemd/system-generators/systemd-veritysetup-generator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd-veritysetup-generator> is a generator that translates kernel "
"command line options configuring verity protected block devices into native "
"systemd units early at boot and when configuration of the system manager is "
"reloaded\\&. This will create B<systemd-veritysetup@.service>(8)  units as "
"necessary\\&."
msgstr ""
"B<Systemd-veritysetup-generator> ist ein Generator, der Kernel-"
"Befehlszeilenoptionen, die während der frühen Systemstartphase oder wenn die "
"Konfiguration des Systemverwaltungsprogramms neu geladen wird, Verity-"
"geschützte Blockgeräte in native Systemd-Units konfiguriert\\&. Dies wird "
"B<systemd-veritysetup@.service>(8)-Units wie notwendig erstellen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"Currently, only two verity devices may be set up with this generator, "
"backing the root and /usr file systems of the OS\\&."
msgstr ""
"Derzeit können nur zwei Verity-Geräte mit diesem Generator eingerichtet "
"werden, die das Wurzel- und /usr-Dateisystem des Betriebssystems "
"hinterlegen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "B<systemd-veritysetup-generator> implements B<systemd.generator>(7)\\&."
msgstr ""
"B<Systemd-veritysetup-generator> implementiert B<systemd.generator>(7)\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "KERNEL COMMAND LINE"
msgstr "KERNEL-BEFEHLSZEILE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd-veritysetup-generator> understands the following kernel command "
"line parameters:"
msgstr ""
"B<Systemd-veritysetup-generator> versteht die folgenden Kernel-"
"Befehlszeilenparameter:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "I<systemd\\&.verity=>, I<rd\\&.systemd\\&.verity=>"
msgstr "I<systemd\\&.verity=>, I<rd\\&.systemd\\&.verity=>"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"Takes a boolean argument\\&. Defaults to \"yes\"\\&. If \"no\", disables the "
"generator entirely\\&.  I<rd\\&.systemd\\&.verity=> is honored only in the "
"initrd while I<systemd\\&.verity=> is honored by both the main system and in "
"the initrd\\&."
msgstr ""
"Erwartet ein logisches Argument\\&. Standardmäßig »yes«\\&. Falls »no«, wird "
"der Generator komplett deaktiviert\\&. I<rd\\&.systemd\\&.verity=> wird nur "
"in der Initrd berücksichtigt, während I<systemd\\&.verity=> sowohl vom "
"Hauptsystem als auch in der Initrd berücksichtigt wird\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "Added in version 233\\&."
msgstr "Hinzugefügt in Version 233\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "I<veritytab=>, I<rd\\&.veritytab=>"
msgstr "I<veritytab=>, I<rd\\&.veritytab=>"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"Takes a boolean argument\\&. Defaults to \"yes\"\\&. If \"no\", causes the "
"generator to ignore any devices configured in /etc/veritytab\\&.  "
"I<rd\\&.veritytab=> is honored only in the initrd while I<veritytab=> is "
"honored by both the main system and in the initrd\\&."
msgstr ""
"Erwartet ein logisches Argument\\&. Standardmäßig »yes«\\&. Falls »no« führt "
"dies dazu, dass der Generator alle in /etc/veritytab konfigurierten Geräte "
"ignoriert\\&. I<rd\\&.veritytab=> wird nur in der Initrd berücksichtigt, "
"während I<veritytab=> sowohl vom Hauptsystem als auch in der Initrd "
"berücksichtigt wird\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "Added in version 248\\&."
msgstr "Hinzugefügt in Version 248\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "I<roothash=>"
msgstr "I<roothash=>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"Takes a root hash value for the root file system\\&. Expects a hash value "
"formatted in hexadecimal characters of the appropriate length (i\\&.e\\&. "
"most likely 256 bit/64 characters, or longer)\\&. If not specified via "
"I<systemd\\&.verity_root_data=> and I<systemd\\&.verity_root_hash=>, the "
"hash and data devices to use are automatically derived from the specified "
"hash value\\&. Specifically, the data partition device is looked for under a "
"GPT partition UUID derived from the first 128-bit of the root hash, the hash "
"partition device is looked for under a GPT partition UUID derived from the "
"last 128-bit of the root hash\\&. Hence it is usually sufficient to specify "
"the root hash to boot from a verity protected root file system, as device "
"paths are automatically determined from it \\(em as long as the partition "
"table is properly set up\\&."
msgstr ""
"Akzeptiert einen Wurzel-Hash-Wert für das Wurzeldateisystem\\&. Erwartet "
"einen in hexadezimalen Zeichen formatierten Hash-Wert der geeigneten Länge "
"(d\\&.h\\&. wahrscheinlich 256 Bit/64 Zeichen oder länger)\\&. Falls nicht "
"mittels I<systemd\\&.verity_root_data=> und I<systemd\\&.verity_root_hash=> "
"angegeben, werden der zu verwendende Hash und das Datengerät automatisch aus "
"dem festgelegten Hash-Wert abgeleitet\\&. Insbesondere wird das Daten-"
"Partitionsgerät unter der GPT-Partitions-UUID, die von den ersten 128 Bit "
"des Wurzel-Hashes abgeleitet wird, nachgeschaut, das Hash-Partitionsgerät "
"wird unter der GPT-Partitions-UUID, die von den letzten 128 Bit des Wurzel-"
"Hashes abgeleitet wird, nachgeschaut\\&. Daher reicht es normalerweise, den "
"Wurzel-Hash anzugeben, um von einem Verity-geschützten Wurzeldateisystem zu "
"starten, da Gerätepfade daraus automatisch bestimmt werden \\(em so lange "
"wie die Partitionstabelle korrekt eingerichtet ist\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "I<systemd\\&.verity_root_data=>, I<systemd\\&.verity_root_hash=>"
msgstr "I<systemd\\&.verity_root_data=>, I<systemd\\&.verity_root_hash=>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"These two settings take block device paths as arguments and may be used to "
"explicitly configure the data partition and hash partition to use for "
"setting up the verity protection for the root file system\\&. If not "
"specified, these paths are automatically derived from the I<roothash=> "
"argument (see above)\\&."
msgstr ""
"Diese zwei Einstellungen akzeptieren Blockgerätepfade als Argumente und "
"können dazu verwandt werden, explizit die Daten- und die Hash-Partition zu "
"konfigurieren, die zur Einrichtung des Verity-Schutzes für das "
"Wurzeldateisystem verwandt werden sollen\\&. Falls nicht angegeben, werden "
"diese Pfade automatisch aus dem Argument I<roothash=> abgeleitet (siehe oben)"
"\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "I<systemd\\&.verity_root_options=>"
msgstr "I<systemd\\&.verity_root_options=>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"Takes a comma-separated list of dm-verity options\\&. Expects the following "
"options B<superblock=>I<BOOLEAN>, B<format=>I<NUMBER>, B<data-block-"
"size=>I<BYTES>, B<hash-block-size=>I<BYTES>, B<data-blocks=>I<BLOCKS>, "
"B<hash-offset=>I<BYTES>, B<salt=>I<HEX>, B<uuid=>I<UUID>, B<ignore-"
"corruption>, B<restart-on-corruption>, B<ignore-zero-blocks>, B<check-at-"
"most-once>, B<panic-on-corruption>, B<hash=>I<HASH>, B<fec-device=>I<PATH>, "
"B<fec-offset=>I<BYTES>, B<fec-roots=>I<NUM> and B<root-hash-"
"signature=>I<PATH>B<|base64:>I<HEX>\\&. See B<veritysetup>(8)  for more "
"details\\&."
msgstr ""
"Akzeptiert eine Kommata-getrennte Liste von dm-verity-Optionen\\&. Erwartet "
"die folgenden Optionen: B<superblock=>I<LOGISCH>, B<format=>I<ZAHL>, B<data-"
"block-size=>I<BYTE>, B<hash-block-size=>I<BYTE>, B<data-blocks=>I<BLÖCKE>, "
"B<hash-offset=>I<BYTE>, B<salt=>I<HEX>, B<uuid=>I<UUID>, B<ignore-"
"corruption>, B<restart-on-corruption>, B<ignore-zero-blocks>, B<check-at-"
"most-once>, B<panic-on-corruption>, B<hash=>I<HASH>, B<fec-device=>I<PFAD>, "
"B<fec-offset=>I<BYTE>, B<fec-roots=>I<ZAHL> und B<root-hash-"
"signature=>I<PFAD>B<|base64:>I<HEX>\\&. Siehe B<veritysetup>(8) für weitere "
"Details\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"I<usrhash=>, I<systemd\\&.verity_usr_data=>, I<systemd\\&.verity_usr_hash=>, "
"I<systemd\\&.verity_usr_options=>"
msgstr ""
"I<usrhash=>, I<systemd\\&.verity_usr_data=>, I<systemd\\&.verity_usr_hash=>, "
"I<systemd\\&.verity_usr_options=>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"Equivalent to their counterparts for the root file system as described "
"above, but apply to the /usr/ file system instead\\&."
msgstr ""
"Äquivalent zu ihrem Gegenstück für das Wurzeldateisystem wie oben "
"beschrieben, aber stattdessen auf das /usr/-Dateisystem angewandt\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "Added in version 250\\&."
msgstr "Hinzugefügt in Version 250\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd>(1), B<systemd-veritysetup@.service>(8), B<veritysetup>(8), "
"B<systemd-fstab-generator>(8)"
msgstr ""
"B<systemd>(1), B<systemd-veritysetup@.service>(8), B<veritysetup>(8), "
"B<systemd-fstab-generator>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/system-generators/systemd-veritysetup-generator"
msgstr "/lib/systemd/system-generators/systemd-veritysetup-generator"

#. type: Plain text
#: debian-bookworm fedora-41 mageia-cauldron
msgid ""
"Takes a boolean argument\\&. Defaults to \"yes\"\\&. If \"no\", disables the "
"generator entirely\\&.  I<rd\\&.systemd\\&.verity=> is honored only by the "
"initrd while I<systemd\\&.verity=> is honored by both the host system and "
"the initrd\\&."
msgstr ""
"Erwartet ein logisches Argument\\&. Standardmäßig »yes«\\&. Falls »no«, wird "
"der Generator komplett deaktiviert\\&. I<rd\\&.systemd\\&.verity=> wird nur "
"von der Initrd berücksichtigt, während I<systemd\\&.verity=> sowohl vom "
"Hauptsystem als auch der Initrd berücksichtigt wird\\&."

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr "systemd 257.1"

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr "systemd 256.7"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr "systemd 255"
