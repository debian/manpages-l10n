# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.1\n"
"POT-Creation-Date: 2024-12-06 18:16+0100\n"
"PO-Revision-Date: 2025-01-09 11:48+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strtod"
msgstr "strtod"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-16"
msgstr "16. Juni 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "strtod, strtof, strtold - convert ASCII string to floating-point number"
msgstr "strtod - konvertiert eine ASCII-Zeichenkette in eine Fließkommazahl"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double strtod(const char *restrict >I<nptr>B<,>\n"
"B<              char **_Nullable restrict >I<endptr>B<);>\n"
"B<float strtof(const char *restrict >I<nptr>B<,>\n"
"B<              char **_Nullable restrict >I<endptr>B<);>\n"
"B<long double strtold(const char *restrict >I<nptr>B<,>\n"
"B<              char **_Nullable restrict >I<endptr>B<);>\n"
msgstr ""
"B<double strtod(const char *restrict >I<nzeig>B<,>\n"
"B<              char **_Nullable restrict >I<endzeig>B<);>\n"
"B<float strtof(const char *restrict >I<nzeig>B<,>\n"
"B<              char **_Nullable restrict >I<endzeig>B<);>\n"
"B<long double strtold(const char *restrict >I<nzeig>B<,>\n"
"B<              char **_Nullable restrict >I<endzeig>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strtof>(), B<strtold>():"
msgstr "B<strtof>(), B<strtold>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strtod>(), B<strtof>(), and B<strtold>()  functions convert the "
"initial portion of the string pointed to by I<nptr> to I<double>, I<float>, "
"and I<long double> representation, respectively."
msgstr ""
"Die Funktionen B<strtod>(), B<strtof>() und B<strtold>() wandeln den ersten "
"Teil der Zeichenkette, auf die I<nzeig> zeigt, in die entsprechende "
"I<double>-, I<float>- und I<long double>-Darstellung um."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The expected form of the (initial portion of the) string is optional leading "
"white space as recognized by B<isspace>(3), an optional plus (\\[aq]+\\[aq]) "
"or minus sign (\\[aq]-\\[aq]) and then either (i) a decimal number, or (ii) "
"a hexadecimal number, or (iii) an infinity, or (iv) a NAN (not-a-number)."
msgstr ""
"Es wird die folgende Form (des Anfangs) der Zeichenkette erwartet: optionale "
"Leerzeichen, wie sie von B<isspace>(3) erkannt werden, ein optionales Plus- "
"»+«) oder Minus-Zeichen (»-«) und dann entweder (i) eine Dezimalzahl oder "
"(ii) eine Hexadezimalzahl oder (iii) ein unendlicher Wert oder (iv) keine "
"Zahl (ein NAN, not-a-number)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A I<decimal number> consists of a nonempty sequence of decimal digits "
"possibly containing a radix character (decimal point, locale-dependent, "
"usually \\[aq].\\[aq]), optionally followed by a decimal exponent.  A "
"decimal exponent consists of an \\[aq]E\\[aq] or \\[aq]e\\[aq], followed by "
"an optional plus or minus sign, followed by a nonempty sequence of decimal "
"digits, and indicates multiplication by a power of 10."
msgstr ""
"Eine I<Dezimalzahl> besteht aus einer nicht leeren Folge von Dezimalziffern, "
"die möglicherweise ein Radix-Zeichen enthält (Dezimalpunkt, von der Locale "
"abhängig, meistens ».«), optional gefolgt von einem dezimalen Exponenten. "
"Ein dezimaler Exponent besteht aus einem »E« oder »e«, gefolgt von einer "
"nicht leeren Folge von Dezimalziffern. Er bedeutet eine Multiplikation mit "
"einer Potenz von 10."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A I<hexadecimal number> consists of a \"0x\" or \"0X\" followed by a "
"nonempty sequence of hexadecimal digits possibly containing a radix "
"character, optionally followed by a binary exponent.  A binary exponent "
"consists of a \\[aq]P\\[aq] or \\[aq]p\\[aq], followed by an optional plus "
"or minus sign, followed by a nonempty sequence of decimal digits, and "
"indicates multiplication by a power of 2.  At least one of radix character "
"and binary exponent must be present."
msgstr ""
"Eine I<Hexadezimalzahl> besteht aus einem »0x« oder »0X« gefolgt von einer "
"nicht leeren Folge von Hexadezimalziffern (die vielleicht ein Radix-Zeichen "
"enthält), der optional ein binärer Exponent folgt. Ein binärer Exponent "
"besteht aus einem »P« oder »p«, gefolgt von einem optionalen Plus- oder "
"Minuszeichen, gefolgt von einer nicht leeren Folge von Dezimalziffern, die "
"für die Multiplikation mit einer Potenz von 2 steht. Mindestens ein Radix-"
"Zeichen und ein binärer Exponent müssen vorhanden sein."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An I<infinity> is either \"INF\" or \"INFINITY\", disregarding case."
msgstr ""
"Ein I<unendlicher Wert> ist ungeachtet der Gross- oder Kleinschreibung "
"entweder  »INF« oder »INFINITY«."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"A I<NAN> is \"NAN\" (disregarding case) optionally followed by a string, "
"I<(n-char-sequence)>, where I<n-char-sequence> specifies in an "
"implementation-dependent way the type of NAN (see VERSIONS)."
msgstr ""
"Ein I<NAN> ist (ungeachtet der Groß- oder Kleinschreibung) ein »NAN«, "
"optional gefolgt von einer Zeichenkette I<(n-char-sequence)>. Die "
"Zeichenfolge beschreibt auf implementationsabhängige Weise den Typ des NaNs "
"(siehe VERSIONEN)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions return the converted value, if any."
msgstr ""
"Diese Funktionen liefern den umgewandelten Wert zurück, wenn er existiert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<endptr> is not NULL, a pointer to the character after the last "
"character used in the conversion is stored in the location referenced by "
"I<endptr>."
msgstr ""
"Wenn I<endzeig> nicht I<NULL> ist, wird an dem durch I<endzeig> bestimmten "
"Ort ein Zeiger auf das erste Zeichen gespeichert, das nicht mehr zur "
"Konvertierung herangezogen wurde."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If no conversion is performed, zero is returned and (unless I<endptr> is "
"null) the value of I<nptr> is stored in the location referenced by I<endptr>."
msgstr ""
"Wenn keine Konvertierung stattgefunden hat, wird 0 zurückgeliefert und "
"(falls I<endzeig> nicht Null ist) der Wert von I<nzeig> wird am Ort "
"gespeichert, auf den I<endzeig> weist."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the correct value would cause overflow, plus or minus B<HUGE_VAL>, "
"B<HUGE_VALF>, or B<HUGE_VALL> is returned (according to the return type and "
"sign of the value), and B<ERANGE> is stored in I<errno>."
msgstr ""
"Falls der korrekte Wert einen Überlauf verursacht, wird plus oder minus "
"B<HUGE_VAL>, B<HUGE_VALF> oder B<HUGE_VALL> zurückgegeben (abhängig vom Typ "
"Vorzeichen des Wertes) und B<ERANGE> wird in I<errno> gespeichert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the correct value would cause underflow, a value with magnitude no larger "
"than B<DBL_MIN>, B<FLT_MIN>, or B<LDBL_MIN> is returned and B<ERANGE> is "
"stored in I<errno>."
msgstr ""
"Verursacht der korrekte Werte eine Bereichsunterschreitung, wird ein Wert "
"zurückgegeben, der nicht größer als B<DBL_MIN>, B<FLT_MIN> oder B<LDBL_MIN> "
"ist und B<ERANGE> wird in I<errno> gespeichert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Overflow or underflow occurred."
msgstr "Überlauf oder Bereichsunterschreitung aufgetreten"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strtod>(),\n"
"B<strtof>(),\n"
"B<strtold>()"
msgstr ""
"B<strtod>(),\n"
"B<strtof>(),\n"
"B<strtold>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Sicher locale"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#.  From glibc 2.8's stdlib/strtod_l.c:
#.      We expect it to be a number which is put in the
#.      mantissa of the number.
#.  It looks as though at least FreeBSD (according to the manual) does
#.  something similar.
#.  C11 says: "An implementation may use the n-char sequence to determine
#. 	extra information to be represented in the NaN's significant."
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the glibc implementation, the I<n-char-sequence> that optionally follows "
"\"NAN\" is interpreted as an integer number (with an optional '0' or '0x' "
"prefix to select base 8 or 16)  that is to be placed in the mantissa "
"component of the returned value."
msgstr ""
"In der Glibc-Implementierung wird die I<n-char-sequence>, der optional ein "
"»NAN« folgt, als Ganzzahl interpretiert (mit einem optionalen Präfix »0« "
"oder »0x«, um entweder 8 oder 16 als Basis zu wählen), die in die Mantissen-"
"Komponente des zurückgegebenen Wertes gesetzt wird."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strtod>()"
msgstr "B<strtod>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C89, POSIX.1-2001."
msgstr "C89, POSIX.1-2001."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strtof>()"
msgstr "B<strtof>()"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strtold>()"
msgstr "B<strtold>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr "WARNUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since 0 can legitimately be returned on both success and failure, the "
"calling program should set I<errno> to 0 before the call, and then determine "
"if an error occurred by checking whether I<errno> has a nonzero value after "
"the call."
msgstr ""
"Da 0 ein legitimer Rückgabewert sowohl bei Erfolg als auch bei Misserfolg "
"zurückgegeben werden kann, sollte das aufrufende Programm I<errno> vor dem "
"Aufruf auf 0 setzen und nach dem Aufruf prüfen, ob I<errno> einen Wert "
"ungleich Null hat."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See the example on the B<strtol>(3)  manual page; the use of the functions "
"described in this manual page is similar."
msgstr ""
"Siehe das Beispiel in der Handbuchseite B<strtol>(3); die Verwendung der in "
"dieser Handbuchseite beschriebenen Funktionen ist ähnlich."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<nan>(3), B<nanf>(3), B<nanl>(3), "
"B<strfromd>(3), B<strtol>(3), B<strtoul>(3)"
msgstr ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<nan>(3), B<nanf>(3), B<nanl>(3), "
"B<strfromd>(3), B<strtol>(3), B<strtoul>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid ""
"B<double strtod(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
"B<float strtof(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
"B<long double strtold(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
msgstr ""
"B<double strtod(const char *restrict >I<nzeig>B<, char **restrict >I<endzeig>B<);>\n"
"B<float strtof(const char *restrict >I<nzeig>B<, char **restrict >I<endzeig>B<);>\n"
"B<long double strtold(const char *restrict >I<nzeig>B<, char **restrict >I<endzeig>B<);>\n"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"A I<NAN> is \"NAN\" (disregarding case) optionally followed by a string, "
"I<(n-char-sequence)>, where I<n-char-sequence> specifies in an "
"implementation-dependent way the type of NAN (see NOTES)."
msgstr ""
"Ein I<NAN> ist (ungeachtet der Groß- oder Kleinschreibung) ein »NAN«, "
"optional gefolgt von einer Zeichenkette I<(n-char-sequence)>. Die "
"Zeichenfolge beschreibt auf implementationsabhängige Weise den Typ des NaNs "
"(siehe ANMERKUNGEN)."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
