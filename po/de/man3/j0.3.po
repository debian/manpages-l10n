# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Ralf Demmer <rdemmer@rdemmer.de>, 1999.
# Helge Kreutzmann <debian@helgefjell.de>, 2012.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2011-2012.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-12-06 18:02+0100\n"
"PO-Revision-Date: 2023-05-01 11:30+0200\n"
"Last-Translator: Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "j0"
msgstr "j0"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. Mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"j0, j0f, j0l, j1, j1f, j1l, jn, jnf, jnl - Bessel functions of the first kind"
msgstr ""
"j0, j0f, j0l, j1, j1f, j1l, jn, jnf, jnl - Bessel-Funktionen der ersten "
"Gattung"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Mathematik-Bibliothek (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double j0(double >I<x>B<);>\n"
"B<double j1(double >I<x>B<);>\n"
"B<double jn(int >I<n>B<, double >I<x>B<);>\n"
msgstr ""
"B<double j0(double >I<x>B<);>\n"
"B<double j1(double >I<x>B<);>\n"
"B<double jn(int >I<n>B<, double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<float j0f(float >I<x>B<);>\n"
"B<float j1f(float >I<x>B<);>\n"
"B<float jnf(int >I<n>B<, float >I<x>B<);>\n"
msgstr ""
"B<float j0f(float >I<x>B<);>\n"
"B<float j1f(float >I<x>B<);>\n"
"B<float jnf(int >I<n>B<, float >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long double j0l(long double >I<x>B<);>\n"
"B<long double j1l(long double >I<x>B<);>\n"
"B<long double jnl(int >I<n>B<, long double >I<x>B<);>\n"
msgstr ""
"B<long double j0l(long double >I<x>B<);>\n"
"B<long double j1l(long double >I<x>B<);>\n"
"B<long double jnl(int >I<n>B<, long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<j0>(), B<j1>(), B<jn>():"
msgstr "B<j0>(), B<j1>(), B<jn>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE\n"
"        || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<j0f>(), B<j0l>(), B<j1f>(), B<j1l>(), B<jnf>(), B<jnl>():"
msgstr "B<j0f>(), B<j0l>(), B<j1f>(), B<j1l>(), B<jnf>(), B<jnl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 600\n"
"        || (_ISOC99_SOURCE && _XOPEN_SOURCE)\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 600\n"
"        || (_ISOC99_SOURCE && _XOPEN_SOURCE)\n"
"        || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# Ist das nicht salopp formuliert? Genau genommen sind es ja Funktionswerte.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<j0>()  and B<j1>()  functions return Bessel functions of I<x> of the "
"first kind of orders 0 and 1, respectively.  The B<jn>()  function returns "
"the Bessel function of I<x> of the first kind of order I<n>."
msgstr ""
"Die Funktionen B<j0>() und B<j1()>() liefern die Bessel-Funktion von I<x> "
"der ersten Gattung der Ordnung 0 beziehungsweise 1 zurück. Die Funktion "
"B<jn>() liefert die Bessel-Funktion von I<x> der ersten Gattung und der "
"Ordnung I<n> zurück."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<j0f>(), B<j1f>(), and B<jnf>(), functions are versions that take and "
"return I<float> values.  The B<j0l>(), B<j1l>(), and B<jnl>()  functions are "
"versions that take and return I<long double> values."
msgstr ""
"Die Funktionen B<j0f>(), B<j1f>() und B<jnf>() sind Versionen, die I<float>-"
"Werte erwarten und zurückliefern. Die Funktionen B<j0l>(), B<j1l>(), and "
"B<jnl>() sind Versionen, die I<long double>-Werte erwarten und zurückliefern."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, these functions return the appropriate Bessel value of the first "
"kind for I<x>."
msgstr ""
"Bei Erfolg geben diese Funktionen den entsprechenden Bessel-Wert erster "
"Gattung für I<x> zurück."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If I<x> is a NaN, a NaN is returned."
msgstr "Falls I<x> keine Zahl (»NaN«) ist, wird »NaN« zurückgegeben."

# msgid "If I<x> is a NaN, a NaN is returned."
# msgstr "Falls I<x> ein NaN ist, wird ein NaN zurückgegeben."
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<x> is too large in magnitude, or the result underflows, a range error "
"occurs, and the return value is 0."
msgstr ""
"Wenn der Betrag von I<x> zu groß ist oder das Ergebnis unterläuft, tritt ein "
"Bereichsfehler ein und der Rückgabewert ist 0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"In B<math_error>(7) erfahren Sie, wie Sie Fehler bei der Ausführung dieser "
"Funktionen erkennen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Die folgenden Fehler können auftreten:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result underflow, or I<x> is too large in magnitude"
msgstr "Bereichsfehler: Ergebnis-Unterlauf oder Betrag von I<x> zu groß"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<errno> is set to B<ERANGE>."
msgstr "I<errno> wird auf B<ERANGE> gesetzt."

#. #-#-#-#-#  archlinux: j0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., j0(1.5e16)
#.  This is intentional.
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6805
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: j0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., j0(1.5e16)
#.  This is intentional.
#.  See http://sources.redhat.com/bugzilla/show_bug.cgi?id=6805
#. type: Plain text
#. #-#-#-#-#  debian-unstable: j0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., j0(1.5e16)
#.  This is intentional.
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6805
#. type: Plain text
#. #-#-#-#-#  fedora-41: j0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., j0(1.5e16)
#.  This is intentional.
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6805
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: j0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., j0(1.5e16)
#.  This is intentional.
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6805
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: j0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., j0(1.5e16)
#.  This is intentional.
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6805
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: j0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., j0(1.5e16)
#.  This is intentional.
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6805
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: j0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., j0(1.5e16)
#.  This is intentional.
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6805
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions do not raise exceptions for B<fetestexcept>(3)."
msgstr ""
"Diese Funktionen lösen keine Ausnahmen (Exceptions) für B<fetestexcept>(3) "
"aus."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<j0>(),\n"
"B<j0f>(),\n"
"B<j0l>()"
msgstr ""
"B<j0>(),\n"
"B<j0f>(),\n"
"B<j0l>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Sicher"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<j1>(),\n"
"B<j1f>(),\n"
"B<j1l>()"
msgstr ""
"B<j1>(),\n"
"B<j1f>(),\n"
"B<j1l>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<jn>(),\n"
"B<jnf>(),\n"
"B<jnl>()"
msgstr ""
"B<jn>(),\n"
"B<jnf>(),\n"
"B<jnl>()"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<j0>()"
msgstr "B<j0>()"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<j1>()"
msgstr "B<j1>()"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<jn>()"
msgstr "B<jn>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Others:"
msgstr "Andere:"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "BSD."
msgstr "BSD."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."
msgstr "SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There are errors of up to 2e-16 in the values returned by B<j0>(), B<j1>(), "
"and B<jn>()  for values of I<x> between -8 and 8."
msgstr ""
"Bei den Rückgabewerten von B<j0>(), B<j1>() und B<jn>() für Werte von I<x> "
"zwischen -8 und 8 treten Fehler von bis zu 2e-16 auf."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<y0>(3)"
msgstr "B<y0>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"The functions returning I<double> conform to SVr4, 4.3BSD, POSIX.1-2001, and "
"POSIX.1-2008.  The others are nonstandard functions that also exist on the "
"BSDs."
msgstr ""
"Die Funktionen mit I<double>-Rückgabewerten sind konform zu SVr4, 4.3BSD, "
"POSIX.1-2001 und POSIX.1-2008. Die anderen Funktionen sind nicht "
"standardisiert und kommen in den BSDs vor."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
