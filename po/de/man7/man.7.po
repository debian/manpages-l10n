# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# René Tschirley <gremlin@cs.tu-berlin.de>, 1996.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
# Helge Kreutzmann <debian@helgefjell.de>, 2019, 2020.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.3\n"
"POT-Creation-Date: 2024-05-01 15:44+0200\n"
"PO-Revision-Date: 2024-03-09 19:18+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "man"
msgstr "man"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm
msgid "man - macros to format man pages"
msgstr "man - Makros für das Formatieren von Handbuchseiten"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm
msgid "B<groff -Tascii -man> I<file> \\&..."
msgstr "B<groff -Tascii -man> I<Datei> …"

#. type: Plain text
#: debian-bookworm
msgid "B<groff -Tps -man> I<file> \\&..."
msgstr "B<groff -Tps -man> I<Datei> …"

#. type: Plain text
#: debian-bookworm
msgid "B<man> [I<section>] I<title>"
msgstr "B<man> [I<Abschnitt>] I<Titel>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm
msgid ""
"This manual page explains the B<groff an.tmac> macro package (often called "
"the B<man> macro package).  This macro package should be used by developers "
"when writing or porting man pages for Linux.  It is fairly compatible with "
"other versions of this macro package, so porting man pages should not be a "
"major problem (exceptions include the NET-2 BSD release, which uses a "
"totally different macro package called mdoc; see B<mdoc>(7))."
msgstr ""
"Diese Handbuchseite beschreibt das B<Groff>-Makropaket B<tmac.an>, das oft "
"als das B<man>-Paket bezeichnet wird. Es sollte von Entwicklern benutzt "
"werden, die Handbuchseiten für Linux schreiben oder portieren. Es ist nahezu "
"vollständig kompatibel zu älteren Versionen dieses Paketes. Daher sollte es "
"relativ problemlos möglich sein, andere Handbuchseiten zu konvertieren. (Zu "
"den Ausnahmen gehört die NET-2 BSD-Veröffentlichung, die das völlig andere "
"Makropaket B<mdoc>(7) benutzt.)"

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that NET-2 BSD mdoc man pages can be used with B<groff> simply by "
"specifying the B<-mdoc> option instead of the B<-man> option.  Using the B<-"
"mandoc> option is, however, recommended, since this will automatically "
"detect which macro package is in use."
msgstr ""
"Beachten Sie, dass Handbuchseiten von NET-2 BSD mit B<Groff> durch die "
"Nutzung der Option B<-mdoc> anstelle der Option B<-man> benutzt werden "
"können. Es wird die Wahl der Option B<-mandoc> empfohlen, da diese "
"automatisch das benutzte Makropaket erkennt."

#. type: Plain text
#: debian-bookworm
msgid ""
"For conventions that should be employed when writing man pages for the Linux "
"I<man-pages> package, see B<man-pages>(7)."
msgstr ""
"Wenn Sie Handbuchseiten für das Linux-Paket »I<man-pages>« schreiben, lesen "
"Sie bitte B<man-pages>(7). Dort finden Sie die Konventionen, an die Sie sich "
"halten sollten."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Title line"
msgstr "Titelzeile"

#. type: Plain text
#: debian-bookworm
msgid ""
"The first command in a man page (after comment lines, that is, lines that "
"start with B<.\\e\">) should be"
msgstr ""
"Der erste Befehl in einer Handbuchseite nach den Kommentarzeilen (die mit B<."
"\\e\"> beginnen) sollte"

#. type: Plain text
#: debian-bookworm
msgid "B<\\&.TH> I<title section date source manual>"
msgstr "B<\\&.TH> I<Titel Abschnitt Datum Quelle Handbuch>"

#. type: Plain text
#: debian-bookworm
msgid ""
"For details of the arguments that should be supplied to the B<TH> command, "
"see B<man-pages>(7)."
msgstr ""
"sein. Für Details zu den Argumenten für den Befehlt B<TH> sei auf B<man-"
"pages>(7) verwiesen."

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that BSD mdoc-formatted pages begin with the B<Dd> command, not the "
"B<TH> command."
msgstr ""
"Beachten Sie, dass mit B<mdoc>(7) formatierte BSD-Seiten nicht mit dem "
"Befehl B<TH> beginnen, sondern mit B<Dd>."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Sections"
msgstr "Abschnitte"

#.  The following doesn't seem to be required (see Debian bug 411303),
#.  If the name contains spaces and appears
#.  on the same line as
#.  .BR \&.SH ,
#.  then place the heading in double quotes.
#. type: Plain text
#: debian-bookworm
msgid "Sections are started with B<\\&.SH> followed by the heading name."
msgstr "Abschnitte beginnen mit B<\\&.SH>, gefolgt von ihrem Titel."

#. type: Plain text
#: debian-bookworm
msgid ""
"The only mandatory heading is NAME, which should be the first section and be "
"followed on the next line by a one-line description of the program:"
msgstr ""
"Der einzige obligatorische Bestandteil ist BEZEICHNUNG. Er sollte der erste "
"Abschnitt werden und in der nächsten Zeile eine einzeilige Beschreibung des "
"Programms folgen:"

#. type: Plain text
#: debian-bookworm
msgid "\\&.SH NAME"
msgstr "\\&.SH BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm
msgid "item \\e- description"
msgstr "Element \\e- Beschreibung"

#. type: Plain text
#: debian-bookworm
msgid ""
"It is extremely important that this format is followed, and that there is a "
"backslash before the single dash which follows the item name.  This syntax "
"is used by the B<mandb>(8)  program to create a database of short "
"descriptions for the B<whatis>(1)  and B<apropos>(1)  commands.  (See "
"B<lexgrog>(1)  for further details on the syntax of the NAME section.)"
msgstr ""
"Es ist überaus wichtig, dass dieses Format eingehalten wird und dass ein "
"Rückwärtsschrägstrich (\\e) vor dem einzelnen Bindestrich nach dem "
"Elementenamen steht. Dieses Format wird vom Programm B<mandb>(8) benutzt, um "
"eine Datenbank mit Kurzbeschreibungen für die Befehle B<whatis>(1) und "
"B<apropos>(1) aufzubauen. (Lesen Sie B<lexgrog>(1) für weitere Details "
"bezüglich der Syntax des Abschnitts NAME.)"

#. type: Plain text
#: debian-bookworm
msgid ""
"For a list of other sections that might appear in a manual page, see B<man-"
"pages>(7)."
msgstr ""
"Eine Liste weiterer möglicher Abschnitte einer Handbuchseite finden Sie in "
"B<man-pages>(7)."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Fonts"
msgstr "Schriften"

#. type: Plain text
#: debian-bookworm
msgid "The commands to select the type face are:"
msgstr "Die Befehle für die Auswahl der Schriftart sind:"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.B>"
msgstr "B<\\&.B>"

#. type: Plain text
#: debian-bookworm
msgid "Bold"
msgstr "Fett"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.BI>"
msgstr "B<\\&.BI>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Bold alternating with italics (especially useful for function specifications)"
msgstr ""
"Fett, abwechselnd mit kursiv (besonders praktisch für Funktions-"
"Deklarationen)"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.BR>"
msgstr "B<\\&.BR>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Bold alternating with Roman (especially useful for referring to other manual "
"pages)"
msgstr ""
"Fett, abwechselnd mit normalem Schrifttyp (besonders praktisch für den "
"Verweis auf andere Handbuchseiten)"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.I>"
msgstr "B<\\&.I>"

#. type: Plain text
#: debian-bookworm
msgid "Italics"
msgstr "Kursiv"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.IB>"
msgstr "B<\\&.IB>"

#. type: Plain text
#: debian-bookworm
msgid "Italics alternating with bold"
msgstr "Kursiv, abwechselnd mit fett"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.IR>"
msgstr "B<\\&.IR>"

#. type: Plain text
#: debian-bookworm
msgid "Italics alternating with Roman"
msgstr "Kursiv, abwechselnd mit normalem Schrifttyp"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.RB>"
msgstr "B<\\&.RB>"

#. type: Plain text
#: debian-bookworm
msgid "Roman alternating with bold"
msgstr "Normaler Schrifttyp, abwechselnd mit fett"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.RI>"
msgstr "B<\\&.RI>"

#. type: Plain text
#: debian-bookworm
msgid "Roman alternating with italics"
msgstr "Normaler Schrifttyp, abwechselnd mit kursiv"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.SB>"
msgstr "B<\\&.SB>"

#. type: Plain text
#: debian-bookworm
msgid "Small alternating with bold"
msgstr "Klein, abwechselnd mit fett"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.SM>"
msgstr "B<\\&.SM>"

#. type: Plain text
#: debian-bookworm
msgid "Small (useful for acronyms)"
msgstr "Klein (praktisch für Akronyme)"

#. type: Plain text
#: debian-bookworm
msgid ""
"Traditionally, each command can have up to six arguments, but the GNU "
"implementation removes this limitation (you might still want to limit "
"yourself to 6 arguments for portability's sake).  Arguments are delimited by "
"spaces.  Double quotes can be used to specify an argument which contains "
"spaces.  For the macros that produce alternating type faces, the arguments "
"will be printed next to each other without intervening spaces, so that the "
"B<\\&.BR> command can be used to specify a word in bold followed by a mark "
"of punctuation in Roman.  If no arguments are given, the command is applied "
"to the following line of text."
msgstr ""
"Traditionell darf ein Befehl bis zu sechs Argumente haben, doch die GNU-"
"Version überschreitet diese Begrenzung. (Sie werden sich aber vielleicht "
"trotzdem auf sechs Argumente beschränken, um portable Handbuchseiten zu "
"schreiben.) Die Argumente werden durch Leerzeichen getrennt. Sollen "
"Argumente Leerzeichen enthalten, müssen die Argumente in doppelte "
"Anführungszeichen eingeschlossen werden. Für die Makros, die wechselnde "
"Schriftarten erstellen, werden hintereinander ohne die dazwischenliegenden "
"Leerzeichen ausgegeben, sodass beispielsweise mit dem Befehl B<\\&.BR> ein "
"Wort im Fettdruck, gefolgt von einem Wort im normalen Schrifttyp dargestellt "
"werden kann. Ohne Argumente wird der Befehl auf die nächste Textzeile "
"angewendet."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Other macros and strings"
msgstr "Weitere Makros und Zeichenketten"

#. type: Plain text
#: debian-bookworm
msgid ""
"Below are other relevant macros and predefined strings.  Unless noted "
"otherwise, all macros cause a break (end the current line of text).  Many of "
"these macros set or use the \"prevailing indent\".  The \"prevailing "
"indent\" value is set by any macro with the parameter I<i> below; macros may "
"omit I<i> in which case the current prevailing indent will be used.  As a "
"result, successive indented paragraphs can use the same indent without "
"respecifying the indent value.  A normal (nonindented) paragraph resets the "
"prevailing indent value to its default value (0.5 inches).  By default, a "
"given indent is measured in ens; try to use ens or ems as units for indents, "
"since these will automatically adjust to font size changes.  The other key "
"macro definitions are:"
msgstr ""
"Im Folgenden werden weitere relevante Makros und vordefinierte Zeichenfolgen "
"aufgelistet. Sofern nicht anders angegeben, beenden alle Makros die aktuelle "
"Textzeile. Viele dieser Makros setzen oder verwenden den aktuellen "
"»vorherrschenden Einzug« (prevailing indent). Der Wert für den Einzug wird "
"für jedes Makro mit dem unten erläuterten Parameter I<i> bestimmt. Makros "
"können I<i> auslassen und so der bisher verwendete Einzug beibehalten "
"werden. Als Ergebnis können die folgenden Absätze den gleichen Einzug "
"verwenden, ohne diesen erneut einzugeben. Ein normaler (nicht eingezogener) "
"Absatz setzt den Einzug wieder auf den Standardwert (0,5 Zoll) zurück. "
"Standardmäßig wird der Einzug in der typografischen Einheit En gemessen. "
"Versuchen Sie, die Einzüge in den Einheiten En oder Em festzulegen, da diese "
"sich automatisch an Änderungen der Schriftgröße anpassen. Weitere wichtige "
"Makros sind wie folgt definiert:"

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Normal paragraphs"
msgstr "Normale Absätze"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.LP>"
msgstr "B<\\&.LP>"

#. type: Plain text
#: debian-bookworm
msgid "Same as B<\\&.PP> (begin a new paragraph)."
msgstr "Das Gleiche wie B<\\&.PP> (einen neuen Absatz anfangen)"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.P>"
msgstr "B<\\&.P>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.PP>"
msgstr "B<\\&.PP>"

#. type: Plain text
#: debian-bookworm
msgid "Begin a new paragraph and reset prevailing indent."
msgstr "Einen neuen Absatz anfangen und den Einzug zurücksetzen"

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Relative margin indent"
msgstr "Relativer Randeinzug"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.RS>I< i>"
msgstr "B<\\&.RS>I< i>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Start relative margin indent: moves the left margin I<i> to the right (if "
"I<i> is omitted, the prevailing indent value is used).  A new prevailing "
"indent is set to 0.5 inches.  As a result, all following paragraph(s) will "
"be indented until the corresponding B<\\&.RE>."
msgstr ""
"Anfang des relativen Randeinzugs: bewegt den linken Rand I<i> nach rechts "
"(wenn I<i> weggelassen wird, wird der aktuelle Wert für den Einzug "
"verwendet). Der Wert für den Einzug wird neu auf 0,5 Zoll festgesetzt. Als "
"Ergebnis werden alle folgenden Absätze bis zum nächsten B<\\&.RE> eingezogen."

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.RE>"
msgstr "B<\\&.RE>"

#. type: Plain text
#: debian-bookworm
msgid ""
"End relative margin indent and restores the previous value of the prevailing "
"indent."
msgstr ""
"Beendet den relativen Randeinzug und stellt den früheren Wert für den "
"Randeinzug wieder her."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Indented paragraph macros"
msgstr "Makros für Absätze mit Einzug"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.HP>I< i>"
msgstr "B<\\&.HP>I< i>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Begin paragraph with a hanging indent (the first line of the paragraph is at "
"the left margin of normal paragraphs, and the rest of the paragraph's lines "
"are indented)."
msgstr ""
"Anfang eines Absatzes mit hängendem Einzug: Die linke Rand der ersten Zeile "
"des Absatzes stimmt mit dem normaler Absätze überein, die restlichen Zeilen "
"des Absatzes sind eingezogen."

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.IP>I< x i>"
msgstr "B<\\&.IP>I< x i>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Indented paragraph with optional hanging tag.  If the tag I<x> is omitted, "
"the entire following paragraph is indented by I<i>.  If the tag I<x> is "
"provided, it is hung at the left margin before the following indented "
"paragraph (this is just like B<\\&.TP> except the tag is included with the "
"command instead of being on the following line).  If the tag is too long, "
"the text after the tag will be moved down to the next line (text will not be "
"lost or garbled).  For bulleted lists, use this macro with \\e(bu (bullet) "
"or \\e(em (em dash)  as the tag, and for numbered lists, use the number or "
"letter followed by a period as the tag; this simplifies translation to other "
"formats."
msgstr ""
"Eingerückter Absatz mit optionaler hängender Beschriftung. Wenn der Hinweis "
"(das Tag) I<x> weggelassen wird, wird der gesamte folgende Absatz um I<i> "
"eingerückt. Wenn das Tag I<x> vorhanden ist, wird die Beschriftung am linken "
"Rand vor dem folgenden eingerückten Absatz aufgehängt. (Dies entspricht "
"B<\\&.TP> mit dem Unterschied, dass die Beschriftung im Befehl enthalten ist "
"und nicht auf der nächsten Zeile steht.) Wenn die Beschriftung zu lang ist, "
"wird der Text nach der Beschriftung auf die nächste Zeile verschoben. (Es "
"geht kein Text verloren, auch wird kein Text verstümmelt.) Für Aufzählungen "
"mit Aufzählungszeichen verwenden Sie dieses Makro mit \\e(bu "
"(Aufzählungszeichen) oder \\e(em (Geviertstrich) als Beschriftung. Für "
"nummerierte Listen verwenden Sie eine Zahl oder einen Buchstaben, denen ein "
"Punkt folgt. Dies vereinfacht die Übersetzung in andere Formate."

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.TP>I< i>"
msgstr "B<\\&.TP>I< i>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Begin paragraph with hanging tag.  The tag is given on the next line, but "
"its results are like those of the B<\\&.IP> command."
msgstr ""
"Beginn eines Absatzes mit einer hängenden Beschriftung. Die Beschriftung "
"wird auf der nächsten Zeile angegeben, aber die Ergebnisse ähneln denen des "
"Befehls B<\\&.IP>."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Hypertext link macros"
msgstr "Makros für Hypertext-Links"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.UR>I< url>"
msgstr "B<\\&.UR>I< URL>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Insert a hypertext link to the URI (URL)  I<url>, with all text up to the "
"following B<\\&.UE> macro as the link text."
msgstr ""
"Fügt einen Hypertext-Link zu der URI (URL) I<URL> ein, wobei sämtlicher Text "
"bis zum folgenden Makro B<\\&.UE> der Link-Text ist."

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.UE>"
msgstr "B<\\&.UE> [I<Nachsatz>]"

#. type: Plain text
#: debian-bookworm
msgid ""
"[I<trailer>] Terminate the link text of the preceding B<\\&.UR> macro, with "
"the optional I<trailer> (if present, usually a closing parenthesis and/or "
"end-of-sentence punctuation) immediately following.  For non-HTML output "
"devices (e.g., B<man -Tutf8>), the link text is followed by the URL in angle "
"brackets; if there is no link text, the URL is printed as its own link text, "
"surrounded by angle brackets.  (Angle brackets may not be available on all "
"output devices.)  For the HTML output device, the link text is hyperlinked "
"to the URL; if there is no link text, the URL is printed as its own link "
"text."
msgstr ""
"Beendet den Link-Text des vorhergehenden Makros B<\\&.UR>, wobei der "
"optionale I<Nachsatz> (falls vorhanden, normalerweise eine schließende "
"Klammer und/oder einen Satzendesatzzeichen) sofort folgt. For Ausgabegeräte "
"ohne HTML (z.B. B<man -Tutf8>) folgt dem Link-Text die URL in spitzen "
"Klammern; falls es keinen Link-Text gibt, wird die URL selbst als Link-Text, "
"eingeschlossen in spitze Klammern, ausgegeben. (Nicht auf allen "
"Ausgabegeräten könnten spitze Klammern verfügbar sein.) Für das HTML-"
"Ausgabegerät wird der Link-Text als Hyperlink auf die URL ausgeführt, falls "
"es keinen Link-Text gibt wird die URL selbst als Link-Text ausgegeben."

#. type: Plain text
#: debian-bookworm
msgid ""
"These macros have been supported since GNU Troff 1.20 (2009-01-05) and "
"Heirloom Doctools Troff since 160217 (2016-02-17)."
msgstr ""
"Diese Makros werden seit GNU-Troff 1.20 (2009-01-05) und Heirloom Doctools "
"Troff seit 160217 (2016-02-17) unterstützt."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Miscellaneous macros"
msgstr "Verschiedene Makros"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.DT>"
msgstr "B<\\&.DT>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Reset tabs to default tab values (every 0.5 inches); does not cause a break."
msgstr ""
"Stellt den Standardwert für Tabulatoren (alle 0,5 Zoll) wieder her; führt "
"nicht zu einem Zeilenumbruch."

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.PD>I< d>"
msgstr "B<\\&.PD>I< d>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Set inter-paragraph vertical distance to d (if omitted, d=0.4v); does not "
"cause a break."
msgstr ""
"Setzt den vertikalen Abstand zwischen Absätzen auf d (ohne Angabe d=0,4v); "
"führt nicht zu einem Zeilenumbruch."

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<\\&.SS>I< t>"
msgstr "B<\\&.SS>I< t>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Subheading I<t> (like B<\\&.SH>, but used for a subsection inside a section)."
msgstr ""
"Unterüberschriften (ähnlich wie B<\\&.SH>, aber für Unterabschnitte "
"innerhalb eines Abschnitts)."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Predefined strings"
msgstr "Vordefinierte Zeichenketten"

#. type: Plain text
#: debian-bookworm
msgid "The B<man> package has the following predefined strings:"
msgstr "Zum B<man>-Paket gehören die folgenden vordefinierten Zeichenketten:"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "\\e*R"
msgstr "\\e*R"

#. type: Plain text
#: debian-bookworm
msgid "Registration Symbol: \\*R"
msgstr "Anmeldungssymbol: \\*R"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "\\e*S"
msgstr "\\e*S"

#. type: Plain text
#: debian-bookworm
msgid "Change to default font size"
msgstr "Wechsel zur Standard-Schriftgröße"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "\\e*(Tm"
msgstr "\\e*(Tm"

#. type: Plain text
#: debian-bookworm
msgid "Trademark Symbol: \\*(Tm"
msgstr "Markenzeichen: \\*(Tm"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "\\e*(lq"
msgstr "\\e*(lq"

#. type: Plain text
#: debian-bookworm
msgid "Left angled double quote: ``"
msgstr "links abgewinkeltes doppeltes Anführungszeichen: ``"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "\\e*(rq"
msgstr "\\e*(rq"

#. type: Plain text
#: debian-bookworm
msgid "Right angled double quote: ''"
msgstr "rechts abgewinkeltes doppeltes Anführungszeichen: ''"

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Safe subset"
msgstr "Sichere Teilmenge"

#. type: Plain text
#: debian-bookworm
msgid ""
"Although technically B<man> is a troff macro package, in reality a large "
"number of other tools process man page files that don't implement all of "
"troff's abilities.  Thus, it's best to avoid some of troff's more exotic "
"abilities where possible to permit these other tools to work correctly.  "
"Avoid using the various troff preprocessors (if you must, go ahead and use "
"B<tbl>(1), but try to use the B<IP> and B<TP> commands instead for two-"
"column tables).  Avoid using computations; most other tools can't process "
"them.  Use simple commands that are easy to translate to other formats.  The "
"following troff macros are believed to be safe (though in many cases they "
"will be ignored by translators): B<\\e\">, B<.>, B<ad>, B<bp>, B<br>, B<ce>, "
"B<de>, B<ds>, B<el>, B<ie>, B<if>, B<fi>, B<ft>, B<hy>, B<ig>, B<in>, B<na>, "
"B<ne>, B<nf>, B<nh>, B<ps>, B<so>, B<sp>, B<ti>, B<tr>."
msgstr ""
"Obwohl technisch gesehen B<man> ein Troff-Makropaket ist, gibt es eine große "
"Zahl von anderen Werkzeugen, die Handbuchseitendateien verarbeiten und nicht "
"alle Troff-Fähigkeiten implementieren. Daher vermeiden Sie am besten den "
"Einsatz einiger eher exotischer Troff-Fähigkeiten soweit wie möglich, damit "
"andere Werkzeuge korrekt arbeiten können. Vermeiden Sie die Verwendung der "
"verschiedenen Troff-Präprozessoren. (Wenn es sein muss, verwenden Sie "
"B<tbl>(1). Versuchen Sie aber, zweispaltige Tabellen mit den Befehlen B<IP> "
"und B<TP> zu realisieren). Vermeiden Sie Berechnungen, die meisten anderen "
"Werkzeuge können sie nicht verarbeiten. Verwenden Sie einfache Befehle, die "
"leicht in andere Formate zu übersetzen sind. Die folgenden Troff-Makros "
"werden als sicher angesehen: B<\\e\">, B<.>, B<ad>, B<bp>, B<br>, B<ce>, "
"B<de>, B<ds>, B<el>, B<ie>, B<if>, B<fi>, B<ft>, B<hy>, B<ig>, B<in>, B<na>, "
"B<ne>, B<nf>, B<nh>, B<ps>, B<so>, B<sp>, B<ti>, B<tr>."

#. type: Plain text
#: debian-bookworm
msgid ""
"You may also use many troff escape sequences (those sequences beginning with "
"\\e).  When you need to include the backslash character as normal text, use "
"\\ee.  Other sequences you may use, where x or xx are any characters and N "
"is any digit, include: B<\\e\\[aq]>, B<\\e\\[ga]>, B<\\e->, B<\\e.>, "
"B<\\e\">, B<\\e%>, B<\\e*x>, B<\\e*(xx>, B<\\e(xx>, B<\\e$N>, B<\\enx>, "
"B<\\en(xx>, B<\\efx>, and B<\\ef(xx>.  Avoid using the escape sequences for "
"drawing graphics."
msgstr ""
"Sie können auch viele Troff-Escape-Sequenzen verwenden (diese Sequenzen "
"beginnen mit \\e). Wenn Sie den umgekehrten Schrägstrich (Backslash) als "
"normalen Text benötigen, verwenden Sie \\ee. Sie können auch die folgenden "
"Sequenzen, in denen x oder xx für einen beliebigen Buchstaben und N für eine "
"beliebige Ziffer stehen, verwenden: B<\\e\\[aq]>, B<\\e\\[ga]>, B<\\e->, "
"B<\\e.>, B<\\e\">, B<\\e%>, B<\\e*x>, B<\\e*(xx>, B<\\e(xx>, B<\\e$N>, "
"B<\\enx>, B<\\en(xx>, B<\\efx> und B<\\ef(xx>. Vermeiden Sie es, mit Escape-"
"Sequenzen Grafiken zu zeichnen."

#. type: Plain text
#: debian-bookworm
msgid ""
"Do not use the optional parameter for B<bp> (break page).  Use only positive "
"values for B<sp> (vertical space).  Don't define a macro (B<de>)  with the "
"same name as a macro in this or the mdoc macro package with a different "
"meaning; it's likely that such redefinitions will be ignored.  Every "
"positive indent (B<in>)  should be paired with a matching negative indent "
"(although you should be using the B<RS> and B<RE> macros instead).  The "
"condition test (B<if,ie>)  should only have \\[aq]t\\[aq] or \\[aq]n\\[aq] "
"as the condition.  Only translations (B<tr>)  that can be ignored should be "
"used.  Font changes (B<ft> and the B<\\ef> escape sequence)  should only "
"have the values 1, 2, 3, 4, R, I, B, P, or CW (the ft command may also have "
"no parameters)."
msgstr ""
"Verwenden Sie nicht den optionalen Parameter für B<bp> (Seitenumbruch). "
"Verwenden Sie nur positive Werte für B<sp> (vertikaler Abstand). Definieren "
"Sie kein Makro (B<de>) mit dem gleichen Namen wie ein Makro in diesem oder "
"dem mdoc-Makropaket mit einer anderen Bedeutung; wahrscheinlich werden "
"solche Neudefinitionen ignoriert. Jeder positive Einzug (B<in>) sollte mit "
"einem passenden negativen Einzug gekoppelt werden (obwohl Sie stattdessen "
"die Makros B<RS> und B<RE> verwenden sollten). Beim Prüfen von Bedingungen "
"(B<if,ie>) sollten Sie sich auf »t« oder »n« beschränken. Nur Übersetzungen "
"(B<tr>), die ignoriert werden können, sollten verwendet werden. Änderungen "
"der Schriftart (B<ft> und die Escape-Sequenz B<\\ef> Escape-Sequenz) sollten "
"nur die Werte 1, 2, 3, 4, R, I, B, P oder CW annehmen. (Der B<ft>-Befehl "
"darf auch keine Parameter haben)."

#. type: Plain text
#: debian-bookworm
msgid ""
"If you use capabilities beyond these, check the results carefully on several "
"tools.  Once you've confirmed that the additional capability is safe, let "
"the maintainer of this document know about the safe command or sequence that "
"should be added to this list."
msgstr ""
"Wenn Sie Fähigkeiten nutzen, die über das Erwähnte herausgehen, überprüfen "
"Sie die Ergebnisse sorgfältig mit mehreren Programmen. Sobald Sie bestätigt "
"haben, dass die zusätzliche Fähigkeit sicher ist, teilen Sie dem Betreuer "
"dieses Dokuments den sicheren Befehl oder die Sequenz mit, damit sie zu "
"dieser Liste hinzugefügt werden kann."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: debian-bookworm
msgid "I</usr/share/groff/>[*/]I<tmac/an.tmac>"
msgstr "I</usr/share/groff/>[*/]I<tmac/an.tmac>"

#. type: Plain text
#: debian-bookworm
msgid "I</usr/man/whatis>"
msgstr "I</usr/man/whatis>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: debian-bookworm
msgid ""
"By all means include full URLs (or URIs) in the text itself; some tools such "
"as B<man2html>(1)  can automatically turn them into hypertext links.  You "
"can also use the B<UR> and B<UE> macros to identify links to related "
"information.  If you include URLs, use the full URL (e.g., E<.UR http://"
"www.kernel.org> E<.UE )> to ensure that tools can automatically find the "
"URLs."
msgstr ""
"Geben Sie auf alle Fälle im Text vollständige URLs (oder URIs) an. Einige "
"Werkzeuge wie B<man2html>(1) können sie automatisch in Hypertext-Links "
"umwandeln. Sie können auch die Makros B<UR> und B<UE> verwenden, um Verweise "
"zu verwandten Informationen zu kennzeichnen. Wenn Sie URLs einschließen, "
"verwenden Sie die vollständige URLs (z. B. E<.UR http://www.kernel.org> "
"E<.UE )>), um sicherzustellen, dass die Werkzeuge die URLs automatisch "
"finden können."

#. type: Plain text
#: debian-bookworm
msgid ""
"Tools processing these files should open the file and examine the first "
"nonwhitespace character.  A period (.) or single quote (\\[aq]) at the "
"beginning of a line indicates a troff-based file (such as man or mdoc).  A "
"left angle bracket (E<lt>) indicates an SGML/XML-based file (such as HTML or "
"Docbook).  Anything else suggests simple ASCII text (e.g., a \"catman\" "
"result)."
msgstr ""
"Werkzeuge, die solche Dateien verarbeiten, sollten die Datei öffnen und das "
"erste Zeichen prüfen, das kein Leerraumzeichen ist. Ein Punkt (.) oder "
"einfaches Anführungszeichen (\\[aq]) am Anfang einer Zeile kennzeichnet eine "
"Troff-Datei (wie man oder mdoc). Eine linke spitze Klammer (E<lt>) zeigt "
"eine SGML/XML-basierte Datei an (z. B. HTML oder DocBook). Alles Andere "
"lässt einfachen ASCII-Text vermuten (z. B. ein Ergebnis von »catman«)."

#. type: Plain text
#: debian-bookworm
msgid ""
"Many man pages begin with B<\\[aq]\\e\"> followed by a space and a list of "
"characters, indicating how the page is to be preprocessed.  For "
"portability's sake to non-troff translators we recommend that you avoid "
"using anything other than B<tbl>(1), and Linux can detect that "
"automatically.  However, you might want to include this information so your "
"man page can be handled by other (less capable) systems.  Here are the "
"definitions of the preprocessors invoked by these characters:"
msgstr ""
"Viele Handbuchseiten beginnen mit B<\\[aq]\\e\">, gefolgt von einem "
"Leerzeichen und einer Liste von Zeichen, welche die Vorverarbeitung der "
"Seite festlegt. Um der Portabilität zu anderen Programmen als Troff zu "
"genügen, wird empfohlen, alles andere als B<tbl>(1) zu vermeiden, Linux kann "
"das automatisch erkennen. Allerdings möchten Sie vielleicht diese "
"Informationen in Ihre Handbuchseite aufnehmen, damit diese von anderen "
"(weniger leistungsfähigen) Systemen verarbeitet werden kann. Mit diesen "
"Zeichen rufen Sie die folgenden Präprozessoren auf:"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<e>"
msgstr "B<e>"

#. type: Plain text
#: debian-bookworm
msgid "eqn(1)"
msgstr "B<eqn>(1)"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<g>"
msgstr "B<g>"

#. type: Plain text
#: debian-bookworm
msgid "grap(1)"
msgstr "B<grap>(1)"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<p>"
msgstr "B<p>"

#. type: Plain text
#: debian-bookworm
msgid "pic(1)"
msgstr "B<pic>(1)"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<r>"
msgstr "B<r>"

#. type: Plain text
#: debian-bookworm
msgid "refer(1)"
msgstr "B<refer>(1)"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<t>"
msgstr "B<t>"

#. type: Plain text
#: debian-bookworm
msgid "tbl(1)"
msgstr "B<tbl>(1)"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<v>"
msgstr "B<v>"

#. type: Plain text
#: debian-bookworm
msgid "vgrind(1)"
msgstr "B<vgrind>(1)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: debian-bookworm
msgid ""
"Most of the macros describe formatting (e.g., font type and spacing) instead "
"of marking semantic content (e.g., this text is a reference to another "
"page), compared to formats like mdoc and DocBook (even HTML has more "
"semantic markings).  This situation makes it harder to vary the B<man> "
"format for different media, to make the formatting consistent for a given "
"media, and to automatically insert cross-references.  By sticking to the "
"safe subset described above, it should be easier to automate transitioning "
"to a different reference page format in the future."
msgstr ""
"Im Vergleich zu Formaten wie mdoc und DocBook beschreibt die Mehrzahl der "
"Makros Formatierungen (z. B. Schriftart und Zeilenabstand) statt semantische "
"Inhalte zu kennzeichnen (z. B.: Dieser Text verweist auf eine andere Seite). "
"Sogar HTML verfügt über mehr semantische Markierungen. Diese Situation macht "
"es schwieriger, das B<man>-Format für verschiedene Medien zu variieren, die "
"Formatierung für ein bestimmtes Medium konsistent zu machen und automatisch "
"Querverweise einzufügen. Mit der Beschränkung auf die oben beschriebene "
"sichere Teilmenge sollte es einfacher sein, den zukünftigen Übergang zu "
"einem anderen Format für Referenzseiten (wie z. B. Manual Pages) zu "
"automatisieren."

#.  .SH AUTHORS
#.  .IP \[em] 3m
#.  James Clark (jjc@jclark.com) wrote the implementation of the macro package.
#.  .IP \[em]
#.  Rickard E. Faith (faith@cs.unc.edu) wrote the initial version of
#.  this manual page.
#.  .IP \[em]
#.  Jens Schweikhardt (schweikh@noc.fdn.de) wrote the Linux Man-Page Mini-HOWTO
#.  (which influenced this manual page).
#.  .IP \[em]
#.  David A. Wheeler (dwheeler@ida.org) heavily modified this
#.  manual page, such as adding detailed information on sections and macros.
#. type: Plain text
#: debian-bookworm
msgid "The Sun macro B<TX> is not implemented."
msgstr "Der Sun-Makro B<TX> ist nicht implementiert."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<apropos>(1), B<groff>(1), B<lexgrog>(1), B<man>(1), B<man2html>(1), "
"B<groff_mdoc>(7), B<whatis>(1), B<groff_man>(7), B<groff_www>(7), B<man-"
"pages>(7), B<mdoc>(7)"
msgstr ""
"B<apropos>(1), B<groff>(1), B<lexgrog>(1), B<man>(1), B<man2html>(1), "
"B<groff_mdoc>(7), B<whatis>(1), B<groff_man>(7), B<groff_www>(7), B<man-"
"pages>(7), B<mdoc>(7)"
