# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Michael Haardt <michael@moria.de>, 1995.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016, 2018.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.17.0\n"
"POT-Creation-Date: 2024-12-06 18:03+0100\n"
"PO-Revision-Date: 2023-02-16 21:16+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "lp"
msgstr "lp"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. Mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "lp - line printer devices"
msgstr "lp - Zeilendrucker (line printer devices)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>linux/lp.hE<gt>>\n"
msgstr "B<#include E<lt>linux/lp.hE<gt>>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "KONFIGURATION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<lp>[0\\[en]2] are character devices for the parallel line printers; they "
"have major number 6 and minor number 0\\[en]2.  The minor numbers correspond "
"to the printer port base addresses 0x03bc, 0x0378, and 0x0278.  Usually they "
"have mode 220 and are owned by user I<root> and group I<lp>.  You can use "
"printer ports either with polling or with interrupts.  Interrupts are "
"recommended when high traffic is expected, for example, for laser printers.  "
"For typical dot matrix printers, polling will usually be enough.  The "
"default is polling."
msgstr ""
"B<lp>[0…2] sind zeichenorientierte Geräte für Drucker an der parallelen "
"Schnittstelle; ihre Hauptnummer (Major Number) ist 6, ihre Nebennummern "
"(Minor Numbers) sind 0…2. Die Nebennummern korrespondieren mit den "
"Basisadressen 0x03bc, 0x0378 und 0x0278 der Druckerports. Normalerweise ist "
"ihr Zugriffsmodus 220, der Besitzer ist I<root> und die Gruppe I<lp>. Die "
"Druckerports können entweder im Polling- oder Interrupt-Betrieb benutzt "
"werden. Falls ein größerer Datendurchsatz erwartet wird, z.B. für "
"Laserdrucker, sollten Interrupts benutzt werden. Für typische Matrixdrucker "
"sollte Polling ausreichen. Der Vorgabewert ist Polling."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following B<ioctl>(2)  calls are supported:"
msgstr "Die folgenden Aufrufe von I<ioctl>(2) werden unterstützt:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPTIME, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPTIME, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets the amount of time that the driver sleeps before rechecking the printer "
"when the printer's buffer appears to be filled to I<arg>.  If you have a "
"fast printer, decrease this number; if you have a slow printer, then "
"increase it.  This is in hundredths of a second, the default 2 being 0.02 "
"seconds.  It influences only the polling driver."
msgstr ""
"Wenn der Druckerpuffer voll ist, schläft der Treiber für I<arg> "
"Hundertstelsekunden, bevor er den Puffer erneut prüft. Für einen schnellen "
"Drucker sollte dieser Wert niedrig, für einen langsamen Drucker dagegen hoch "
"gewählt werden. Die Vorgabe ist 2, also 0,02 Sekunden. Dies beeinflusst nur "
"den abrufenden Treiber (Polling)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPCHAR, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPCHAR, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets the maximum number of busy-wait iterations which the polling driver "
"does while waiting for the printer to get ready for receiving a character to "
"I<arg>.  If printing is too slow, increase this number; if the system gets "
"too slow, decrease this number.  The default is 1000.  It influences only "
"the polling driver."
msgstr ""
"Setzt die maximale Anzahl der Durchgänge, die der abrufende Treiber wartet, "
"bis der Drucker bereit ist, ein Zeichen für I<arg> zu empfangen. Die Zahl "
"sollte vergrößert werden, falls das Drucken zu langsam ist und verkleinert "
"werden, wenn das System zu langsam wird. Der Standardwert ist 1000. Dies "
"beeinflusst nur den abrufenden Treiber (Polling)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPABORT, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPABORT, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<arg> is 0, the printer driver will retry on errors, otherwise it will "
"abort.  The default is 0."
msgstr ""
"Falls I<arg> 0 ist, wird es der Druckertreiber bei Fehlern erneut versuchen, "
"ansonsten aufgeben. Der Standardwert ist 0."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPABORTOPEN, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPABORTOPEN, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<arg> is 0, B<open>(2)  will be aborted on error, otherwise error will "
"be ignored.  The default is to ignore it."
msgstr ""
"Falls I<arg> 0 ist, wird B<open>(2) bei auftretenden Fehlern abgebrochen, "
"ansonsten werden Fehler ignoriert. Standardmäßig werden Fehler ignoriert."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPCAREFUL, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPCAREFUL, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<arg> is 0, then the out-of-paper, offline, and error signals are "
"required to be false on all writes, otherwise they are ignored.  The default "
"is to ignore them."
msgstr ""
"Falls I<arg> 0 ist, müssen die Signale »out-of-paper«, »offline« und »error« "
"bei allen Schreibzugriffen logisch falsch sein, ansonsten werden sie "
"ignoriert. Es ist Standard, sie zu ignorieren."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPWAIT, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPWAIT, int >argB<)>"

#.  FIXME . Actually, since Linux 2.2, the default is 1
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets the number of busy waiting iterations to wait before strobing the "
"printer to accept a just-written character, and the number of iterations to "
"wait before turning the strobe off again, to I<arg>.  The specification says "
"this time should be 0.5 microseconds, but experience has shown the delay "
"caused by the code is already enough.  For that reason, the default value is "
"0.  This is used for both the polling and the interrupt driver."
msgstr ""
"Setzt die Anzahl von »busy-wait«-Iterationen, die ausgeführt werden, bevor "
"der Strobe-Impuls beginnt. (Strobe signalisiert dem Drucker, dass die Daten "
"am Datenport stabil anliegen und ein neues Zeichen enthalten.) Es werden "
"ebenfalls I<arg> Iterationen ausgeführt, bevor der Strobe-Impuls beendet "
"wird. Die Spezifikation gibt eine Impulslänge von 0,5 Mikrosekunden vor. Die "
"Erfahrung hat gezeigt, dass die Verzögerung durch den Code selbst ausreicht. "
"Darum ist der Standardwert 0. Er wird für den interruptgetriebenen wie für "
"den abrufenden Treiber verwendet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPSETIRQ, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPSETIRQ, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This B<ioctl>(2)  requires superuser privileges.  It takes an I<int> "
"containing the new IRQ as argument.  As a side effect, the printer will be "
"reset.  When I<arg> is 0, the polling driver will be used, which is also "
"default."
msgstr ""
"Dieser B<ioctl>(2) benötigt Superuser-Rechte. Das Argument I<arg> ist die "
"Nummer des neuen IRQs. Der Wert 0 schaltet Interrupts ab und Polling ein, "
"was auch Standard ist."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPGETIRQ, int *>argB<)>"
msgstr "B<int ioctl(int >fdB<, LPGETIRQ, int *>argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Stores the currently used IRQ in I<arg>."
msgstr "Speichert den zurzeit genutzten IRQ in I<arg>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPGETSTATUS, int *>argB<)>"
msgstr "B<int ioctl(int >fdB<, LPGETSTATUS, int *>argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Stores the value of the status port in I<arg>.  The bits have the following "
"meaning:"
msgstr ""
"Speichert den Wert des Statusports in I<arg>. Die Bits haben folgende "
"Bedeutung:"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LP_PBUSY"
msgstr "LP_PBUSY"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "inverted busy input, active high"
msgstr "invertierter Busy-Eingang, aktiv hoch"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LP_PACK"
msgstr "LP_PACK"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "unchanged acknowledge input, active low"
msgstr "unveränderter Acknowledge-Eingang, aktiv niedrig"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LP_POUTPA"
msgstr "LP_POUTPA"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "unchanged out-of-paper input, active high"
msgstr "unveränderter »out-of-paper«-Eingang, aktiv hoch"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LP_PSELECD"
msgstr "LP_PSELECD"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "unchanged selected input, active high"
msgstr "unveränderter Selected-Eingang, aktiv hoch"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LP_PERRORP"
msgstr "LP_PERRORP"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "unchanged error input, active low"
msgstr "unveränderter Error-Eingang, aktiv niedrig"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Refer to your printer manual for the meaning of the signals.  Note that "
"undocumented bits may also be set, depending on your printer."
msgstr ""
"Lesen Sie auch Ihr Drucker-Handbuch für die Bedeutung der Signale. Beachten "
"Sie, dass abhängig vom Drucker auch undokumentierte Bits gesetzt werden "
"können."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPRESET)>"
msgstr "B<int ioctl(int >fdB<, LPRESET)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Resets the printer.  No argument is used."
msgstr "Setzt den Drucker zurück. Ohne Argument."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#.  .SH AUTHORS
#.  The printer driver was originally written by Jim Weigand and Linus
#.  Torvalds.
#.  It was further improved by Michael K.\& Johnson.
#.  The interrupt code was written by Nigel Gamble.
#.  Alan Cox modularized it.
#.  LPCAREFUL, LPABORT, LPGETSTATUS were added by Chris Metcalf.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/lp*>"
msgstr "I</dev/lp*>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chmod>(1), B<chown>(1), B<mknod>(1), B<lpcntl>(8), B<tunelp>(8)"
msgstr "B<chmod>(1), B<chown>(1), B<mknod>(1), B<lpcntl>(8), B<tunelp>(8)"

#. type: TH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
