# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2011-2012.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
# Chris Leick <c.leick@vollbio.de>, 2017.
# Erik Pfannenstein <debianignatz@gmx.de>, 2017.
# Helge Kreutzmann <debian@helgefjell.de>, 2012-2017, 2019-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-12-06 18:09+0100\n"
"PO-Revision-Date: 2024-03-23 15:55+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_timerslack_ns"
msgstr "proc_pid_timerslack_ns"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. Mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/timerslack_ns - timer slack in nanoseconds"
msgstr "/proc/pid/timerslack_ns - Timer-Spielraum in Nanosekunden"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</timerslack_ns> (since Linux 4.6)"
msgstr "I</proc/>PIDI</timerslack_ns> (seit Linux 4.6)"

#.  commit da8b44d5a9f8bf26da637b7336508ca534d6b319
#.  commit 5de23d435e88996b1efe0e2cebe242074ce67c9e
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file exposes the process's \"current\" timer slack value, expressed in "
"nanoseconds.  The file is writable, allowing the process's timer slack value "
"to be changed.  Writing 0 to this file resets the \"current\" timer slack to "
"the \"default\" timer slack value.  For further details, see the discussion "
"of B<PR_SET_TIMERSLACK> in B<prctl>(2)."
msgstr ""
"Diese Datei legt den »derzeitigen« Spielraum des Timers ausgedrückt in "
"Nanosekunden offen. Diese Datei ist schreibbar, wodurch das Ändern des "
"Spielraums des Timers möglich ist. Wird 0 in diese Datei geschrieben, wird "
"der Spielraum auf den »Standard«-Spielraum des Timers zurückgesetzt. Weitere "
"Einzelheiten finden Sie in der Besprechung von B<PR_SET_TIMERSLACK> in "
"B<prctl>(2)."

#.  commit 7abbaf94049914f074306d960b0f968ffe52e59f
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Initially, permission to access this file was governed by a ptrace access "
"mode B<PTRACE_MODE_ATTACH_FSCREDS> check (see B<ptrace>(2)).  However, this "
"was subsequently deemed too strict a requirement (and had the side effect "
"that requiring a process to have the B<CAP_SYS_PTRACE> capability would also "
"allow it to view and change any process's memory).  Therefore, since Linux "
"4.9, only the (weaker)  B<CAP_SYS_NICE> capability is required to access "
"this file."
msgstr ""
"Ursprünglich wurde die Zugriffsberechtigung für diese Datei über eine Ptrace-"
"Zugriffsmodusprüfung B<PTRACE_MODE_ATTACH_FSCREDS> geregelt (siehe "
"B<ptrace>(2)). Allerdings wurde dies nachfolgend als zu strenge Anforderung "
"erachtet (und es hatte den Seiteneffekt, dass die Notwendigkeit für einen "
"Prozess, die Capability B<CAP_SYS_PTRACE> zu haben, auch dazu führte, dass "
"er den Speicher jedes Prozesses lesen und verändern konnte). Daher wird seit "
"Linux 4.9 nur die (schwächere) Capability B<CAP_SYS_NICE> für den Zugriff "
"auf diese Datei benötigt."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15. August 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
