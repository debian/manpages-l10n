# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Schulze <joey@infodrom.org>
# Mike Fengler <mike@krt3.krt-soft.de>
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2024-12-06 18:14+0100\n"
"PO-Revision-Date: 2023-01-12 19:57+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "services"
msgstr "services"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. Mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "services - Internet network services list"
msgstr "services - Liste von Internet-Netzwerkdiensten"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<services> is a plain ASCII file providing a mapping between human-friendly "
"textual names for internet services, and their underlying assigned port "
"numbers and protocol types.  Every networking program should look into this "
"file to get the port number (and protocol) for its service.  The C library "
"routines B<getservent>(3), B<getservbyname>(3), B<getservbyport>(3), "
"B<setservent>(3), and B<endservent>(3)  support querying this file from "
"programs."
msgstr ""
"B<services> ist eine einfache ASCII-Datei für die Zuordnung einfach lesbarer "
"Namen von Internet-Diensten zu den zugrundeliegenden zugehörigen Portnummern "
"und Protokolltypen. Jedes Netzwerkprogramm sollte sich seine Portnummer (und "
"das Protokoll) aus dieser Datei holen. Mit den C-Bibliotheksroutinen "
"B<getservent>(3), B<getservbyname>(3), B<getservbyport>(3), B<setservent>(3) "
"und B<endservent>(3) können Sie aus einem Programm heraus diese Datei "
"abfragen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Port numbers are assigned by the IANA (Internet Assigned Numbers Authority), "
"and their current policy is to assign both TCP and UDP protocols when "
"assigning a port number.  Therefore, most entries will have two entries, "
"even for TCP-only services."
msgstr ""
"Portnummern werden von der IANA (Internet Assigned Numbers Authority) "
"vergeben. Ihre derzeitige Politik ist es, jeder Portnummer sowohl das TCP- "
"als auch das UDP-Protokoll zuzuordnen. Daher werden die meisten Einträge "
"zweifach vorhanden sein, selbst bei reinen TCP-Diensten."

# low numbered wirklich übersetzen?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Port numbers below 1024 (so-called \"low numbered\" ports) can be bound to "
"only by root (see B<bind>(2), B<tcp>(7), and B<udp>(7)).  This is so clients "
"connecting to low numbered ports can trust that the service running on the "
"port is the standard implementation, and not a rogue service run by a user "
"of the machine.  Well-known port numbers specified by the IANA are normally "
"located in this root-only space."
msgstr ""
"Portnummern unterhalb 1024 (so genannte »niedrige« Ports) können nur vom "
"Systemverwalter (root) zugewiesen werden (siehe auch B<bind>(2), B<tcp>(7) "
"und B<udp>(7)). Das soll für über niedrige Ports mit dem System verbundene "
"Clients gewährleisten, dass an diesem Port eine Standardimplementierung "
"läuft und keinen bösartigen, vom Benutzer der Maschine gestarteten Dienst. "
"Von der IANA veröffentlichte Portnummern (»well-known port numbers«) liegen "
"normalerweise in diesem nur root zugänglichen Bereich."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The presence of an entry for a service in the B<services> file does not "
"necessarily mean that the service is currently running on the machine.  See "
"B<inetd.conf>(5)  for the configuration of Internet services offered.  Note "
"that not all networking services are started by B<inetd>(8), and so won't "
"appear in B<inetd.conf>(5).  In particular, news (NNTP) and mail (SMTP) "
"servers are often initialized from the system boot scripts."
msgstr ""
"Das Vorhandensein eines Dienste-Eintrags in der B<services>-Datei bedeutet "
"noch nicht, dass dieser Dienst derzeit auch auf der Maschine läuft. Siehe "
"B<inetd.conf>(5) zur Konfiguration der angebotenen Internetdienste. Beachten "
"Sie jedoch, dass nicht alle Netzwerkdienste von B<inetd>(8) gestartet "
"werden, also auch nicht in B<inetd.conf>(5) eingetragen sind. Insbesondere "
"News(NNTP)- und Mail(SMTP)-Server werden häufig von Skripten beim Hochfahren "
"des Systems gestartet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The location of the B<services> file is defined by B<_PATH_SERVICES> in "
"I<E<lt>netdb.hE<gt>>.  This is usually set to I</etc/services>."
msgstr ""
"Den Speicherort der Datei B<services> definiert das Makro B<_PATH_SERVICES> "
"in I</usr/include/netdb.h>. Üblicherweise ist der Pfad auf I</etc/services> "
"gesetzt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Each line describes one service, and is of the form:"
msgstr "Jede Zeile beschreibt einen Dienst und hat folgende Form:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<service-name\\ \\ \\ port>B</>I<protocol\\ \\ \\ >[I<aliases ...>]"
msgstr "I<Dienstname\\ \\ \\ Port>B</>I<Protokoll\\ \\ \\ >[I<Aliase …>]"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "where:"
msgstr "wobei:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<service-name>"
msgstr "I<Dienstname>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the friendly name the service is known by and looked up under.  It is "
"case sensitive.  Often, the client program is named after the I<service-"
"name>."
msgstr ""
"Die leicht lesbare Bezeichnung für den Dienst, unter der er bekannt ist und "
"mit der er gefunden wird. Groß-/Kleinschreibung wird beachtet. Häufig trägt "
"das Client-Programm den gleichen Namen wie I<Dienstname>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<port>"
msgstr "I<Port>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "is the port number (in decimal) to use for this service."
msgstr "ist die für diesen Dienst vorgesehene, dezimale Portnummer."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<protocol>"
msgstr "I<Protokoll>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the type of protocol to be used.  This field should match an entry in the "
"B<protocols>(5)  file.  Typical values include B<tcp> and B<udp>."
msgstr ""
"ist die Art des zu verwendenden Protokolls. Dieses Feld sollte einem Eintrag "
"in der Datei B<protocols>(5) entsprechen. Zu den typischen Werten gehören "
"B<tcp> und B<udp>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<aliases>"
msgstr "I<Aliase>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is an optional space or tab separated list of other names for this service.  "
"Again, the names are case sensitive."
msgstr ""
"ist eine optionale Liste weiterer Namen für diesen Dienst. Auch hier wird "
"zwischen Groß- und Kleinschreibung unterschieden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Either spaces or tabs may be used to separate the fields."
msgstr ""
"Zum Trennen der Felder können Leer- oder Tabulatorzeichen verwendet werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Comments are started by the hash sign (#) and continue until the end of the "
"line.  Blank lines are skipped."
msgstr ""
"Kommentare beginnen mit dem Zeichen # und erstrecken sich bis ans "
"Zeilenende. Leerzeilen werden ignoriert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<service-name> should begin in the first column of the file, since "
"leading spaces are not stripped.  I<service-names> can be any printable "
"characters excluding space and tab.  However, a conservative choice of "
"characters should be used to minimize compatibility problems.  For example, "
"a-z, 0-9, and hyphen (-) would seem a sensible choice."
msgstr ""
"Der I<Dienstname> sollte in der ersten Spalte beginnen, da führende "
"Leerzeichen nicht entfernt werden. I<Dienstname>n können aus allen "
"druckbaren Zeichen außer Leer- und Tabulatorzeichen bestehen. Jedoch sollte "
"die Auswahl konservativ erfolgen, um Probleme bei der Verbindung "
"unterschiedlicher Systeme zu vermeiden. Sinnvoll wäre z.B. die Verwendung "
"von a-z, 0-9 und Bindestrich (-)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Lines not matching this format should not be present in the file.  "
"(Currently, they are silently skipped by B<getservent>(3), "
"B<getservbyname>(3), and B<getservbyport>(3).  However, this behavior should "
"not be relied on.)"
msgstr ""
"Anders aufgebaute Zeilen sollten nicht in der Datei stehen. (Derzeit werden "
"solche von B<getservent>(3), B<getservbyname>(3) und B<getservbyport>(3) "
"stillschweigend übergangen. Darauf sollte man sich aber nicht verlassen.)"

#.  The following is not true as at glibc 2.8 (a line with a comma is
#.  ignored by getservent()); it's not clear if/when it was ever true.
#.    As a backward compatibility feature, the slash (/) between the
#.    .I port
#.    number and
#.    .I protocol
#.    name can in fact be either a slash or a comma (,).
#.    Use of the comma in
#.    modern installations is deprecated.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file might be distributed over a network using a network-wide naming "
"service like Yellow Pages/NIS or BIND/Hesiod."
msgstr ""
"Diese Datei kann über ein Netzwerk verteilt werden, wenn netzwerkweite "
"Namensdienste wie Yellow Pages/NIS oder BIND/Hesiod eingesetzt werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A sample B<services> file might look like this:"
msgstr "Eine Beispiel-B<services> könnte so aussehen:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"netstat         15/tcp\n"
"qotd            17/tcp          quote\n"
"msp             18/tcp          # message send protocol\n"
"msp             18/udp          # message send protocol\n"
"chargen         19/tcp          ttytst source\n"
"chargen         19/udp          ttytst source\n"
"ftp             21/tcp\n"
"# 22 - unassigned\n"
"telnet          23/tcp\n"
msgstr ""
"netstat         15/tcp\n"
"qotd            17/tcp          quote\n"
"msp             18/tcp          # message send protocol\n"
"msp             18/udp          # message send protocol\n"
"chargen         19/tcp          ttytst source\n"
"chargen         19/udp          ttytst source\n"
"ftp             21/tcp\n"
"# 22 - unassigned\n"
"telnet          23/tcp\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/services>"
msgstr "I</etc/services>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The Internet network services list"
msgstr "die Liste von Internet-Netzwerkdiensten"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<E<lt>netdb.hE<gt>>"
msgstr "I<E<lt>netdb.hE<gt>>"

#.  .SH BUGS
#.  It's not clear when/if the following was ever true;
#.  it isn't true for glibc 2.8:
#.     There is a maximum of 35 aliases, due to the way the
#.     .BR getservent (3)
#.     code is written.
#.  It's not clear when/if the following was ever true;
#.  it isn't true for glibc 2.8:
#.     Lines longer than
#.     .B BUFSIZ
#.     (currently 1024) characters will be ignored by
#.     .BR getservent (3),
#.     .BR getservbyname (3),
#.     and
#.     .BR getservbyport (3).
#.     However, this will also cause the next line to be mis-parsed.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Definition of B<_PATH_SERVICES>"
msgstr "Definition von B<_PATH_SERVICES>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<listen>(2), B<endservent>(3), B<getservbyname>(3), B<getservbyport>(3), "
"B<getservent>(3), B<setservent>(3), B<inetd.conf>(5), B<protocols>(5), "
"B<inetd>(8)"
msgstr ""
"B<listen>(2), B<endservent>(3), B<getservbyname>(3), B<getservbyport>(3), "
"B<getservent>(3), B<setservent>(3), B<inetd.conf>(5), B<protocols>(5), "
"B<inetd>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Assigned Numbers RFC, most recently RFC\\ 1700, (AKA STD0002)."
msgstr ""
"Assigned Numbers RFC, der aktuellste: RFC\\ 1700 (auch bekannt als STD0002)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30. Oktober 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
