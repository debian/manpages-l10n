# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2011-2012.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
# Chris Leick <c.leick@vollbio.de>, 2017.
# Erik Pfannenstein <debianignatz@gmx.de>, 2017.
# Helge Kreutzmann <debian@helgefjell.de>, 2012-2017, 2019-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-12-06 18:09+0100\n"
"PO-Revision-Date: 2024-03-23 15:53+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_syscall"
msgstr "proc_pid_syscall"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. Mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/syscall - currently executed system call"
msgstr "/proc/pid/syscall - aktuell ausgeführter Systemaufruf"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</syscall> (since Linux 2.6.27)"
msgstr "I</proc/>PIDI</syscall> (seit Linux 2.6.27)"

#.  commit ebcb67341fee34061430f3367f2e507e52ee051b
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file exposes the system call number and argument registers for the "
"system call currently being executed by the process, followed by the values "
"of the stack pointer and program counter registers.  The values of all six "
"argument registers are exposed, although most system calls use fewer "
"registers."
msgstr ""
"Diese Datei legt die Systemaufrufnummer und -argumentenregister für den "
"derzeit durch den Prozess ausgeführten Systemaufruf offen. Es folgen die "
"Werte des Stack-Zeigers und der Programmzählerregister. Die Werte aller "
"sechs Argumentenregister werden offengelegt, obwohl die meisten "
"Systemaufrufe weniger Register verwenden."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the process is blocked, but not in a system call, then the file displays "
"-1 in place of the system call number, followed by just the values of the "
"stack pointer and program counter.  If process is not blocked, then the file "
"contains just the string \"running\"."
msgstr ""
"Falls der Prozess blockiert aber nicht in einem Systemaufruf ist, dann zeigt "
"die Datei -1 an der Stelle der Systemaufrufnummer an, gefolgt von nur den "
"Werten des Stack-Zeigers und des Programmzählers. Falls der Prozess nicht "
"blockiert ist, dann enthält die Datei nur die Zeichenkette »running«."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file is present only if the kernel was configured with "
"B<CONFIG_HAVE_ARCH_TRACEHOOK>."
msgstr ""
"Diese Datei ist nur vorhanden, falls der Kernel mit "
"B<CONFIG_HAVE_ARCH_TRACEHOOK> konfiguriert wurde."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Permission to access this file is governed by a ptrace access mode "
"B<PTRACE_MODE_ATTACH_FSCREDS> check; see B<ptrace>(2)."
msgstr ""
"Die Rechte, auf diese Datei zuzugreifen, werden von einer Ptrace-"
"Zugriffsmodusprüfung B<PTRACE_MODE_ATTACH_FSCREDS> gesteuert; siehe "
"B<ptrace>(2)."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15. August 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
