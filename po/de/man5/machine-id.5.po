# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2018-2020,2022,2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-12-22 07:30+0100\n"
"PO-Revision-Date: 2024-12-20 19:22+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MACHINE-ID"
msgstr "MACHINE-ID"

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr "systemd 257"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "machine-id"
msgstr "machine-id"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "machine-id - Local machine ID configuration file"
msgstr "machine-id - Lokale Konfigurationsdatei zur Maschinenkennung"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/etc/machine-id"
msgstr "/etc/machine-id"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The /etc/machine-id file contains the unique machine ID of the local system "
"that is set during installation or boot\\&. The machine ID is a single "
"newline-terminated, hexadecimal, 32-character, lowercase ID\\&. When decoded "
"from hexadecimal, this corresponds to a 16-byte/128-bit value\\&. This ID "
"may not be all zeros\\&."
msgstr ""
"Die Datei /etc/machine-id enthält eine eindeutige Maschinenkennung des "
"lokalen Systems, die während der Installation oder des Systemstarts gesetzt "
"wird\\&. Die Maschinenkennung ist eine einzelne, hexadezimale 32-Zeichen-"
"Kennung (in Kleinbuchstaben), die mit einem Zeilenumbruch abgeschlossen "
"wird\\&. Nach der Dekodierung aus dem hexadezimalen entspricht sie einem 16-"
"Byte/128-Bit-Wert\\&. Diese Kennung darf nicht komplett nur aus Nullen "
"bestehen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The machine ID is usually generated from a random source during system "
"installation or first boot and stays constant for all subsequent boots\\&. "
"Optionally, for stateless systems, it is generated during runtime during "
"early boot if necessary\\&."
msgstr ""
"Die Maschinenkennung wird normalerweise während der Systeminstallation oder "
"des ersten Systemstarts aus einer Zufallsquelle erstellt und bleibt für alle "
"nachfolgenden Systemstarts konstant\\&. Für zustandslose Systeme kann sie "
"optional falls notwendig zur Laufzeit während der frühen Systemstartphase "
"erstellt werden\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The machine ID may be set, for example when network booting, with the "
"I<systemd\\&.machine_id=> kernel command line parameter or by passing the "
"option B<--machine-id=> to systemd\\&. An ID specified in this manner has "
"higher priority and will be used instead of the ID stored in /etc/machine-"
"id\\&."
msgstr ""
"Die Maschinenkennung kann beispielsweise beim Systemstart über das Netz mit "
"dem Kernelbefehlszeilenparameter I<systemd\\&.machine_id=> oder durch "
"Übergabe der Option B<--machine-id=> an Systemd gesetzt werden\\&. Eine in "
"dieser Weise festgelegte Maschinenkennung hat eine höhere Priorität und wird "
"statt der in /etc/machine-id gespeicherten Kennung verwandt\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The machine ID does not change based on local or network configuration or "
"when hardware is replaced\\&. Due to this and its greater length, it is a "
"more useful replacement for the B<gethostid>(3)  call that POSIX "
"specifies\\&."
msgstr ""
"Die Maschinenkennung ändert sich nicht, wenn sich die lokale oder "
"Netzkonfiguration ändert oder die Hardware ausgetauscht wird\\&. Daher und "
"aufgrund ihrer größeren Länge ist sie eine nützlichere Ersetzung des von "
"POSIX festgelegten Aufrufs B<gethostid>(3)\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This machine ID adheres to the same format and logic as the D-Bus machine "
"ID\\&."
msgstr ""
"Diese Maschinenkennung folgt dem gleichen Format und der gleichen Logik wie "
"die Maschinenkennung von D-Bus\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This ID uniquely identifies the host\\&. It should be considered "
"\"confidential\", and must not be exposed in untrusted environments, in "
"particular on the network\\&. If a stable unique identifier that is tied to "
"the machine is needed for some application, the machine ID or any part of it "
"must not be used directly\\&. Instead the machine ID should be hashed with a "
"cryptographic, keyed hash function, using a fixed, application-specific "
"key\\&. That way the ID will be properly unique, and derived in a constant "
"way from the machine ID but there will be no way to retrieve the original "
"machine ID from the application-specific one\\&. The "
"B<sd_id128_get_machine_app_specific>(3)  API provides an implementation of "
"such an algorithm\\&."
msgstr ""
"Diese Kennung identifiziert den Rechner eindeutig\\&. Sie sollte "
"»vertraulich« behandelt und nicht in unvertrauenswürdigen Umgebungen, "
"insbesondere im Netz offengelegt werden\\&. Falls für einige Anwendungen "
"eine stabile Kennung, die an eine Maschine gebunden ist, benötigt wird, darf "
"die Maschinenkennung oder Teile davon nicht direkt verwandt werden\\&. "
"Stattdessen sollte die Maschinenkennung mit einer kryptographischen, mit "
"einem Schlüssel versehenen Funktion in einen Hash verwandelt werden, wobei "
"ein fester, anwendungsbezogener Schlüssel verwandt werden sollte\\&. Damit "
"ist die Kennung ausreichend eindeutig und in einer konstanten Art und Weise "
"von der Maschinenkennung abgeleitet, aber es gibt dann keine Möglichkeit, "
"die ursprüngliche Maschinenkennung aus der anwendungsspezifischen "
"abzuleiten\\&. Das API B<sd_id128_get_machine_app_specific>(3) stellt eine "
"Implementierung eines solchen Algorithmus bereit\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "INITIALIZATION"
msgstr "INITIALISIERUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each machine should have a non-empty ID in normal operation\\&. The ID of "
"each machine should be unique\\&. To achieve those objectives, /etc/machine-"
"id can be initialized in a few different ways\\&."
msgstr ""
"Im normalen Betrieb sollte jede Maschine eine nicht leere Kennung haben\\&. "
"Die Kennung von jeder Maschine sollte eindeutig sein\\&. Um diese Ziele zu "
"erreichen, kann /etc/machine-id auf verschiedene Arten initialisiert "
"werden\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For normal operating system installations, where a custom image is created "
"for a specific machine, /etc/machine-id should be populated during "
"installation\\&."
msgstr ""
"Für normale Betriebssysteminstallationen, bei denen ein angepasstes Abbild "
"für eine bestimmte Maschine erstellt wird, sollte /etc/machine-id während "
"der Installation befüllt werden\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd-machine-id-setup>(1)  may be used by installer tools to initialize "
"the machine ID at install time, but /etc/machine-id may also be written "
"using any other means\\&."
msgstr ""
"B<systemd-machine-id-setup>(1) kann von Installationswerkzeugen verwandt "
"werden, um die Maschinenkennung zum Installationszeitpunkt zu "
"initialisieren, aber /etc/machine-id kann auch mit anderen Mitteln "
"geschrieben werden\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For operating system images which are created once and used on multiple "
"machines, for example for containers or in the cloud, /etc/machine-id should "
"be either missing or an empty file in the generic file system image (the "
"difference between the two options is described under \"First Boot "
"Semantics\" below)\\&. An ID will be generated during boot and saved to this "
"file if possible\\&. Having an empty file in place is useful because it "
"allows a temporary file to be bind-mounted over the real file, in case the "
"image is used read-only\\&. Also see \\m[blue]B<Safely Building Images>\\m[]"
"\\&\\s-2\\u[1]\\d\\s+2\\&."
msgstr ""
"Für Betriebssystemabbilder, die einmal erzeugt und dann auf mehreren "
"Maschinen verwandt werden, beispielsweise Container oder in der Cloud, "
"sollte /etc/machine-id entweder fehlen oder in dem generischen Dateisystem "
"leer sein (der Unterschied zwischen diesen zwei Optionen wird weiter unten "
"unter »Semantik beim ersten Systemstart« beschrieben)\\&. Während des "
"Systemstarts wird eine Kennung erstellt und falls möglich in dieser Datei "
"gespeichert\\&. An der Stelle eine leere Datei zu haben ist nützlich, da es "
"erlaubt, eine temporäre Datei mittels einer Bind-Einhängung über die echte "
"Datei zu legen, falls das Abbild nur lesend verwandt wird\\&. Siehe auch "
"\\m[blue]B<Sicheres Bauen von Abbildern>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd-firstboot>(1)  may be used to initialize /etc/machine-id on "
"mounted (but not booted) system images\\&."
msgstr ""
"B<systemd-firstboot>(1) kann zur Initialisierung von /etc/machine-id auf "
"eingehängten (aber nicht gestarteten) Systemabbildern verwandt werden\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"When a machine is booted with B<systemd>(1)  the ID of the machine will be "
"established\\&. If I<systemd\\&.machine_id=> or B<--machine-id=> options "
"(see first section) are specified, this value will be used\\&. Otherwise, "
"the value in /etc/machine-id will be used\\&. If this file is empty or "
"missing, systemd will attempt to use the D-Bus machine ID from /var/lib/dbus/"
"machine-id, the value of the kernel command line option I<container_uuid>, "
"the KVM DMI product_uuid or the devicetree vm,uuid (on KVM systems), the Xen "
"hypervisor uuid, and finally a randomly generated UUID\\&.  "
"I<systemd\\&.machine_id=firmware> can be set to generate the machine ID from "
"the firmware\\&."
msgstr ""
"Wenn eine Maschine mit B<systemd>(1) gestartet wird, wird die Kennung der "
"Maschine etabliert\\&. Falls die Optionen I<systemd\\&.machine_id=> oder B<--"
"machine-id=> (siehe ersten Abschnitt) angegeben werden, wird dieser Wert "
"verwandt\\&. Falls diese Datei leer ist oder fehlt, wird Systemd versuchen, "
"die D-Bus-Maschinenkennung aus /var/lib/dbus/machine-id, den Wert der "
"Kernelbefehlszeilenoption I<container_uuid>, die KVM-DMI-product_uuid oder "
"die Devicetree VM,UUID (auf KVM-Systemen), die Xen-Hypervisor-UUID und "
"schließlich eine zufällig erstellte UUID zu verwenden\\&. Es kann "
"I<systemd\\&.machine_id=firmware> gesetzt werden, um die Maschinenkennung "
"aus der Firmware zu erstellen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"After the machine ID is established, B<systemd>(1)  will attempt to save it "
"to /etc/machine-id\\&. If this fails, it will attempt to bind-mount a "
"temporary file over /etc/machine-id\\&. It is an error if the file system is "
"read-only and does not contain a (possibly empty)  /etc/machine-id file\\&."
msgstr ""
"Nachdem die Maschinenkennung etabliert ist, wird B<systemd>(1) versuchen, "
"sie in /etc/machine-id zu speichern\\&. Falls dies fehlschlägt, wird es "
"versuchen, sie als temporäre Datei mittels Bind-Einhängung über /etc/machine-"
"id zu legen\\&. Es ist ein Fehler, falls das Dateisystem nur lesbar ist und "
"keine (möglicherweise leere) Datei /etc/machine-id enthält\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd-machine-id-commit.service>(8)  will attempt to write the machine "
"ID to the file system if /etc/machine-id or /etc/ are read-only during early "
"boot but become writable later on\\&."
msgstr ""
"B<systemd-machine-id-commit.service>(8) wird versuchen, die Maschinenkennung "
"in das Dateisystem zu schreiben, falls /etc/machine-id oder /etc/ während "
"der frühen Systemstartphase schreibgeschützt sind, aber später schreibbar "
"werden\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FIRST BOOT SEMANTICS"
msgstr "SEMANTIK BEIM ERSTEN SYSTEMSTART"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"/etc/machine-id is used to decide whether a boot is the first one\\&. The "
"rules are as follows:"
msgstr ""
"/etc/machine-id wird für die Entscheidung, ob ein Systemstart der erstmalige "
"ist, verwandt\\&. Die Regeln sind wie folgt:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The kernel command argument I<systemd\\&.condition_first_boot=> may be used "
"to override the autodetection logic, see B<kernel-command-line>(7)\\&."
msgstr ""
"Das Kernelbefehlsargument I<systemd\\&.condition_first_boot=> kann dazu "
"verwandt werden, die automatische Erkennungslogik außer Kraft zu setzen, "
"siehe B<kernel-command-line>(7)\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Otherwise, if /etc/machine-id does not exist, this is a first boot\\&. "
"During early boot, B<systemd> will write \"uninitialized\\en\" to this file "
"and overmount a temporary file which contains the actual machine ID\\&. "
"Later (after first-boot-complete\\&.target has been reached), the real "
"machine ID will be written to disk\\&."
msgstr ""
"Andernfalls, falls /etc/machine-id nicht existiert, ist dies ein erstmaliger "
"Systemstart\\&. Während der frühen Systemstartphase wird B<systemd> "
"»uninitialized\\en« in diese Datei schreiben und eine temporäre Datei "
"darüber einhängen, die die eigentliche Maschinenkennung enthält\\&. Später "
"(nachdem first-boot-complete\\&.target erreicht wurde) wird die echte "
"Maschinenkennung auf Platte geschrieben\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If /etc/machine-id contains the string \"uninitialized\", a boot is also "
"considered the first boot\\&. The same mechanism as above applies\\&."
msgstr ""
"Falls /etc/machine-id die Zeichenkette »uninitialized« enthält, wird ein "
"Systemstart auch als erstmaliger Systemstart betrachtet\\&. Der gleiche "
"Mechanismus wie oben beschrieben erfolgt\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If /etc/machine-id exists and is empty, a boot is I<not> considered the "
"first boot\\&.  B<systemd> will still bind-mount a file containing the "
"actual machine-id over it and later try to commit it to disk (if /etc/ is "
"writable)\\&."
msgstr ""
"Falls /etc/machine-id existiert und leer ist, dann wird ein Systemstart "
"I<nicht> als erstmaliger Systemstart betrachtet\\&. B<systemd> wird "
"weiterhin eine Bind-Einhängung einer Datei, die die eigentliche "
"Maschinenkennung enthält, darüber ausführen und später versuchen, diese auf "
"Platte zu schreiben (falls /etc/ schreibbar ist)\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If /etc/machine-id already contains a valid machine-id, this is not a first "
"boot\\&."
msgstr ""
"Falls /etc/machine-id bereits eine gültige Maschinenkennung enthält, ist "
"dies keine erstmaliger Systemstart\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If according to the above rules a first boot is detected, units with "
"I<ConditionFirstBoot=yes> will be run and B<systemd> will perform additional "
"initialization steps, in particular presetting units\\&."
msgstr ""
"Falls entsprechend der obigen Regeln ein erstmaliger Systemstart erkannt "
"wurde, werden Units mit I<ConditionFirstBoot=yes> ausgeführt und B<systemd> "
"wird zusätzliche Initialisierungsschritte durchlaufen, insbesondere das "
"Voreinstellen von Units\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RELATION TO OSF UUIDS"
msgstr "BEZUG ZU OSF UUIDS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that the machine ID historically is not an OSF UUID as defined by "
"\\m[blue]B<RFC 4122>\\m[]\\&\\s-2\\u[2]\\d\\s+2, nor a Microsoft GUID; "
"however, starting with systemd v30, newly generated machine IDs do qualify "
"as Variant 1 Version 4 UUIDs, as per RFC 4122\\&."
msgstr ""
"Beachten Sie, dass die Maschinenkennung historisch gesehen weder eine OSF-"
"UUID, wie sie durch \\m[blue]B<RFC 4122>\\m[]\\&\\s-2\\u[2]\\d\\s+2 "
"definiert wird, noch eine Microsoft GUID ist; beginnend mit Systemd v30 "
"werden sich allerdings neu erstellte Maschinenkennungen als Variante 1 "
"Version 4 UUIDs (gemäß RFC 4122) eignen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In order to maintain compatibility with existing installations, an "
"application requiring a strictly RFC 4122 compliant UUID should decode the "
"machine ID, and then (non-reversibly) apply the following operations to turn "
"it into a valid RFC 4122 Variant 1 Version 4 UUID\\&. With \"id\" being an "
"unsigned character array:"
msgstr ""
"Zur Wahrung der Kompatibilität mit bestehenden Installationen sollte jede "
"Anwendung, die eine streng RFC-4122-konforme UUID benötigt, die "
"Maschinenkennung dekodieren und dann die nachfolgenden (nicht rückgängig "
"machbare) Aktionen anwenden, um sie in eine gültige RFC-4122-Variante-1-"
"Version-4-UUID zu wandeln (hierbei ist »id« ein vorzeichenfreies "
"Zeichenfeld):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"/* Set UUID version to 4 --- truly random generation */\n"
"id[6] = (id[6] & 0x0F) | 0x40;\n"
"/* Set the UUID variant to DCE */\n"
"id[8] = (id[8] & 0x3F) | 0x80;\n"
msgstr ""
"/* UUID auf Version 4 setzen --- wirklich zufällige Erzeugung */\n"
"id[6] = (id[6] & 0x0F) | 0x40;\n"
"/* Die UUID-Variante auf DCE setzen */\n"
"id[8] = (id[8] & 0x3F) | 0x80;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"(This code is inspired by \"generate_random_uuid()\" of drivers/char/"
"random\\&.c from the Linux kernel sources\\&.)"
msgstr ""
"(Dieser Code ist von »generate_random_uuid()« von drivers/char/random\\&.c "
"aus den Linux-Kernelquellen inspiriert\\&.)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The simple configuration file format of /etc/machine-id originates in the /"
"var/lib/dbus/machine-id file introduced by D-Bus\\&. In fact, this latter "
"file might be a symlink to /etc/machine-id\\&."
msgstr ""
"Das einfache Konfigurationsdateiformat von /etc/machine-id entspringt der "
"durch D-Bus eingeführten Datei /var/lib/dbus/machine-id\\&. Tatsächlich kann "
"letztere Datei ein Symlink auf /etc/machine-id sein\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd-machine-id-setup>(1), B<gethostid>(3), "
"B<hostname>(5), B<machine-info>(5), B<os-release>(5), B<sd-id128>(3), "
"B<sd_id128_get_machine>(3), B<systemd-firstboot>(1)"
msgstr ""
"B<systemd>(1), B<systemd-machine-id-setup>(1), B<gethostid>(3), "
"B<hostname>(5), B<machine-info>(5), B<os-release>(5), B<sd-id128>(3), "
"B<sd_id128_get_machine>(3), B<systemd-firstboot>(1)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Safely Building Images"
msgstr "Sicheres Bauen von Abbildern"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://systemd.io/BUILDING_IMAGES"
msgstr "\\%https://systemd.io/BUILDING_IMAGES"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 2."
msgstr " 2."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "RFC 4122"
msgstr "RFC 4122"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://tools.ietf.org/html/rfc4122"
msgstr "\\%https://tools.ietf.org/html/rfc4122"

#. type: TH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: Plain text
#: debian-bookworm fedora-41 mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"When a machine is booted with B<systemd>(1)  the ID of the machine will be "
"established\\&. If I<systemd\\&.machine_id=> or B<--machine-id=> options "
"(see first section) are specified, this value will be used\\&. Otherwise, "
"the value in /etc/machine-id will be used\\&. If this file is empty or "
"missing, systemd will attempt to use the D-Bus machine ID from /var/lib/dbus/"
"machine-id, the value of the kernel command line option I<container_uuid>, "
"the KVM DMI product_uuid or the devicetree vm,uuid (on KVM systems), the Xen "
"hypervisor uuid, and finally a randomly generated UUID\\&."
msgstr ""
"Wenn eine Maschine mit B<systemd>(1) gestartet wird, wird die Kennung der "
"Maschine etabliert\\&. Falls die Optionen I<systemd\\&.machine_id=> oder B<--"
"machine-id=> (siehe ersten Abschnitt) angegeben werden, wird dieser Wert "
"verwandt\\&. Falls diese Datei leer ist oder fehlt, wird Systemd versuchen, "
"die D-Bus-Maschinenkennung aus /var/lib/dbus/machine-id, den Wert der "
"Kernelbefehlszeilenoption I<container_uuid>, die KVM-DMI-product_uuid oder "
"die Devicetree VM,UUID (auf KVM-Systemen), die Xen-Hypervisor-UUID und "
"schließlich eine zufällig erstellte UUID zu verwenden\\&."

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr "systemd 257.1"

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr "systemd 256.7"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr "systemd 255"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 256.8"
msgstr "systemd 256.8"
