# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.13\n"
"POT-Creation-Date: 2024-09-06 18:21+0200\n"
"PO-Revision-Date: 2022-03-20 16:01+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MSGEXEC"
msgstr "MSGEXEC"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "February 2024"
msgstr "Februar 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU gettext-tools 0.22.5"
msgstr "GNU gettext-tools 0.22.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "msgexec - process translations of message catalog"
msgstr "msgexec - Übersetzungen eines Meldungskatalogs verarbeiten"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<msgexec> [I<\\,OPTION\\/>] I<\\,COMMAND \\/>[I<\\,COMMAND-OPTION\\/>]"
msgstr "B<msgexec> [I<\\,OPTION\\/>] I<\\,BEFEHL \\/>[I<\\,BEFEHLSOPTION\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME translation catalog → message catalog (to be consistent with other gettext man pages)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Applies a command to all translations of a translation catalog.  The COMMAND "
"can be any program that reads a translation from standard input.  It is "
"invoked once for each translation.  Its output becomes msgexec's output.  "
"msgexec's return code is the maximum return code across all invocations."
msgstr ""
"Wendet einen Befehl auf alle Übersetzungen eines Meldungskatalogs an. Der "
"BEFEHL kann jedes Programm sein, das eine Übersetzung aus der "
"Standardeingabe liest. Es wird einmalig für jede Übersetzung aufgerufen. "
"Dessen Ausgabe wird zur Ausgabe von B<msgexec>. Der Rückgabecode von "
"B<msgexec> ist der maximale Rückgabecode über alle Aufrufe hinweg."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A special builtin command called '0' outputs the translation, followed by a "
"null byte.  The output of \"msgexec 0\" is suitable as input for \"xargs "
"B<-0>\"."
msgstr ""
"Ein spezieller eingebauter Befehl namens »0« gibt die Übersetzung gefolgt "
"von einem Nullbyte aus. Der Aufruf von B<msgexec 0> eignet sich als Eingabe "
"für B<xargs -0>."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Command input:"
msgstr "Befehlseingabe:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--newline>"
msgstr "B<--newline>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "add newline at the end of input"
msgstr "fügt einen Zeilenvorschub am Ende der Eingabe hinzu."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Die obligatorischen Argumente für Optionen sind für deren Kurz- und Langform "
"gleich."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Input file location:"
msgstr "Orte der Eingabedateien:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--input>=I<\\,INPUTFILE\\/>"
msgstr "B<-i>, B<--input>=I<\\,EINGABEDATEI\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "input PO file"
msgstr "PO-Eingabedatei"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--directory>=I<\\,DIRECTORY\\/>"
msgstr "B<-D>, B<--directory>=I<\\,VERZEICHNIS\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "add DIRECTORY to list for input files search"
msgstr ""
"fügt das angegebene VERZEICHNIS zur Liste hinzu, in der nach Eingabedateien "
"gesucht wird."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If no input file is given or if it is -, standard input is read."
msgstr ""
"Wenn keine Eingabedatei oder »-« angegeben ist, wird die Standardeingabe "
"gelesen."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Input file syntax:"
msgstr "Syntax der Eingabedatei:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--properties-input>"
msgstr "B<-P>, B<--properties-input>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "input file is in Java .properties syntax"
msgstr "Eingabedatei folgt der I<.properties>-Syntax von Java."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--stringtable-input>"
msgstr "B<--stringtable-input>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "input file is in NeXTstep/GNUstep .strings syntax"
msgstr "Eingabedatei folgt der I<.strings>-Syntax von NeXTstep/GNUstep."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Informative output:"
msgstr "Informative Ausgabe:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "zeigt Hilfeinformationen an und beendet das Programm."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "gibt Versionsinformationen aus und beendet das Programm."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Bruno Haible."
msgstr "Geschrieben von Bruno Haible."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report bugs in the bug tracker at E<lt>https://savannah.gnu.org/projects/"
"gettextE<gt> or by email to E<lt>bug-gettext@gnu.orgE<gt>."
msgstr ""
"Melden Sie Fehler im Fehlererfassungssystem auf E<.UR https://"
"savannah.gnu.org/projects/gettext> E<.UE> oder per E-Mail an E<.MT bug-"
"gettext@gnu.org> E<.ME> (jeweils auf Englisch)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2001-2023 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Copyright \\(co 2001-2023 Free Software Foundation, Inc. Lizenz GPLv3+: "
"E<.UR https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dies ist freie Software: Sie können sie verändern und weitergeben. Es gibt "
"KEINE GARANTIE, soweit gesetzlich zulässig."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The full documentation for B<msgexec> is maintained as a Texinfo manual.  If "
"the B<info> and B<msgexec> programs are properly installed at your site, the "
"command"
msgstr ""
"Die vollständige Dokumentation für B<msgexec> wird als ein Texinfo-Handbuch "
"gepflegt. Wenn die Programme B<info>(1) und B<msgexec> auf Ihrem Rechner "
"ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<info msgexec>"
msgstr "B<info msgexec>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2023"
msgstr "Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU gettext-tools 0.21"
msgstr "GNU gettext-tools 0.21"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2001-2020 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Copyright \\(co 2001-2020 Free Software Foundation, Inc. Lizenz GPLv3+: "
"E<.UR https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "October 2022"
msgstr "Oktober 2022"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU gettext-tools 0.21.1"
msgstr "GNU gettext-tools 0.21.1"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 2001-2022 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Copyright \\(co 2001-2022 Free Software Foundation, Inc. Lizenz GPLv3+: "
"E<.UR https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."
