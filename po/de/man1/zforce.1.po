# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2024-09-06 18:36+0200\n"
"PO-Revision-Date: 2022-04-02 10:56+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ZFORCE"
msgstr "ZFORCE"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "zforce - force a '.gz' extension on all gzip files"
msgstr ""
"zforce - Gzip-Dateien mit der entsprechenden Namenserweiterung ».gz« versehen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<zforce> [ name ...  ]"
msgstr "B<zforce> [ Name …  ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<zforce> command forces a B<.gz> extension on all B<gzip> files so that "
"B<gzip> will not compress them twice.  This can be useful for files with "
"names truncated after a file transfer.  On systems with a 14 char limitation "
"on file names, the original name is truncated to make room for the .gz "
"suffix. For example, 12345678901234 is renamed to 12345678901.gz. A file "
"name such as foo.tgz is left intact."
msgstr ""
"Der Befehl B<zforce> versieht alle B<gzip>-Dateien mit der Erweiterung "
"B<.gz>, so dass B<gzip>(1) diese nicht zweimal komprimiert. Dies ist "
"nützlich für Dateien, deren Namen bei einer Dateiübertragung gekürzt wurden. "
"Auf Systemen, wo die Länge von Dateinamen auf 14 Zeichen begrenzt ist, wird "
"der ursprüngliche Name gekürzt, um Platz für die Erweiterung ».gz« zu "
"gewinnen. Beispielsweise wird 12345678901234 in 12345678901.gz umbenannt. "
"Ein Name wie foo.tgz wird dagegen nicht verändert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

# FIXMe missing markup
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "gzip(1), znew(1), zmore(1), zgrep(1), zdiff(1), gzexe(1)"
msgstr ""
"B<gzip>(1), B<znew>(1), B<zmore>(1), B<zgrep>(1), B<zdiff>(1), B<gzexe>(1)"
