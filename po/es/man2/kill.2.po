# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Vicente Pastor Gómez <vpastorg@santandersupernet.com>, 1998.
# Juan Piernas <piernas@ditec.um.es>, 1998.
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:03+0100\n"
"PO-Revision-Date: 2004-08-06 19:53+0200\n"
"Last-Translator: Miguel Pérez Ibars <mpi79470@alu.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "kill"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 Mayo 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "kill - send signal to a process"
msgstr "kill - enviar una señal a un proceso"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int kill(pid_t >I<pid>B<, int >I<sig>B<);>\n"
msgstr "B<int kill(pid_t >I<pid>B<, int >I<sig>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<kill>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE\n"
msgstr "    _POSIX_C_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<kill>()  system call can be used to send any signal to any process "
"group or process."
msgstr ""
"La llamada B<kill>() se puede usar para enviar cualquier señal a un proceso "
"o grupo de procesos."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "If I<pid> is positive, then signal I<sig> is sent to I<pid>."
msgid ""
"If I<pid> is positive, then signal I<sig> is sent to the process with the ID "
"specified by I<pid>."
msgstr ""
"Si I<pid> es positivo, entonces la señal I<sig> es enviada a I<pid>.  En "
"este caso, se devuelve 0 si hay éxito, o un valor negativo si hay error."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<pid> equals 0, then I<sig> is sent to every process in the process "
#| "group of the current process."
msgid ""
"If I<pid> equals 0, then I<sig> is sent to every process in the process "
"group of the calling process."
msgstr ""
"Si I<pid> es 0, entonces I<sig> se envía a cada proceso en el grupo de "
"procesos del proceso actual."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<pid> equals -1, then I<sig> is sent to every process except for "
#| "process 1 (init), but see below."
msgid ""
"If I<pid> equals -1, then I<sig> is sent to every process for which the "
"calling process has permission to send signals, except for process 1 "
"(I<init>), but see below."
msgstr ""
"Si I<pid> es igual a -1, entonces se envía I<sig> a cada proceso, excepto al "
"proceso 1 (init), vea más abajo."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<pid> is less than -1, then I<sig> is sent to every process in the "
#| "process group I<-pid>."
msgid ""
"If I<pid> is less than -1, then I<sig> is sent to every process in the "
"process group whose ID is I<-pid>."
msgstr ""
"Si I<pid> es menor que -1, entonces se envía I<sig> a cada proceso en el "
"grupo de procesos I<-pid>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<sig> is 0, then no signal is sent, but existence and permission checks "
"are still performed; this can be used to check for the existence of a "
"process ID or process group ID that the caller is permitted to signal."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The process does not have permission to send the signal to any of the "
#| "receiving processes.  For a process to have permission to send a signal "
#| "to process I<pid> it must either have root privileges, or the real or "
#| "effective user ID of the sending process must equal the real or saved set-"
#| "user-ID of the receiving process.  In the case of SIGCONT it suffices "
#| "when the sending and receiving processes belong to the same session."
msgid ""
"For a process to have permission to send a signal, it must either be "
"privileged (under Linux: have the B<CAP_KILL> capability in the user "
"namespace of the target process), or the real or effective user ID of the "
"sending process must equal the real or saved set-user-ID of the target "
"process.  In the case of B<SIGCONT>, it suffices when the sending and "
"receiving processes belong to the same session.  (Historically, the rules "
"were different; see NOTES.)"
msgstr ""
"El proceso no tiene permiso para enviar la señal a alguno de los procesos "
"que la recibirán. Para que un proceso tenga permiso para enviar una señal al "
"proceso I<pid> debe, o bien tener privilegios de root, o bien el ID de "
"usuario real o efectivo del proceso que envía la señal ha de ser igual al "
"set-user-ID real o guardado del proceso que la recibe.  En el caso de "
"SIGCONT es suficiente con que los procesos emisor y receptor pertenezcan a "
"la misma sesión."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success (at least one signal was sent), zero is returned.  On error, -1 "
"is returned, and I<errno> is set to indicate the error."
msgstr ""
"En caso de éxito, se devuelve cero. En caso de error, -1, y se guarda en "
"I<errno> un valor apropiado."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An invalid signal was specified."
msgstr "Se especificó una señal inválida."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The calling process does not have permission to send the signal to any of "
"the target processes."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The pid or process group does not exist.  Note that an existing process "
#| "might be a zombie, a process which already committed termination, but has "
#| "not yet been B<wait()>ed for."
msgid ""
"The target process or process group does not exist.  Note that an existing "
"process might be a zombie, a process that has terminated execution, but has "
"not yet been B<wait>(2)ed for."
msgstr ""
"El pid o grupo de procesos no existe. Nótese que un proceso existente podría "
"ser un zombi, un proceso que ya ha sido terminado, pero que aún no ha sido "
"\"B<wait()>eado\"."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux notes"
msgstr "Notas de Linux"

#.  In the 0.* kernels things chopped and changed quite
#.  a bit - MTK, 24 Jul 02
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Across different kernel versions, Linux has enforced different rules for "
#| "the permissions required for an unprivileged process to send a signal to "
#| "another process.  In kernels 1.0 to 1.2.2, a signal could be sent if the "
#| "effective user ID of the sender matched that of the receiver, or the real "
#| "user ID of the sender matched that of the receiver.  From kernel 1.2.3 "
#| "until 1.3.77, a signal could be sent if the effective user ID of the "
#| "sender matched either the real or effective user ID of the receiver.  The "
#| "current rules, which conform to POSIX 1003.1-2001, were adopted in kernel "
#| "1.3.78."
msgid ""
"Across different kernel versions, Linux has enforced different rules for the "
"permissions required for an unprivileged process to send a signal to another "
"process.  In Linux 1.0 to 1.2.2, a signal could be sent if the effective "
"user ID of the sender matched effective user ID of the target, or the real "
"user ID of the sender matched the real user ID of the target.  From Linux "
"1.2.3 until 1.3.77, a signal could be sent if the effective user ID of the "
"sender matched either the real or effective user ID of the target.  The "
"current rules, which conform to POSIX.1, were adopted in Linux 1.3.78."
msgstr ""
"A lo largo de diferentes versiones del núcleo, Linux ha aplicado diferentes "
"reglas en torno a los permisos requeridos por un proceso no privilegiado "
"para enviar señales a otro proceso.  En las versiones del núcleo 1.0 a la "
"1.2.2, se podía enviar una señal si el identificador de usuario efectivo del "
"remitente coincidía con el del receptor, o el identificador de usuario real "
"del remitente coincidía con el del receptor.  Desde la versión 1.2.3 a la "
"1.3.77, una señal podía ser emitida si el identificador de usuario efectivo "
"del remitente coincidía con el identificador de usuario real o efectivo del "
"receptor.  Las reglas actuales, que son conformes con POSIX 1003.1-2001, "
"fueron adoptadas en la versión 1.3.78 del núcleo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "It is impossible to send a signal to task number one, the init process, "
#| "for which it has not installed a signal handler.  This is done to assure "
#| "the system is not brought down accidentally."
msgid ""
"The only signals that can be sent to process ID 1, the I<init> process, are "
"those for which I<init> has explicitly installed signal handlers.  This is "
"done to assure the system is not brought down accidentally."
msgstr ""
"Es imposible enviar una señal a la tarea número uno, el proceso init, para "
"el que no ha sido instalado un manejador de señales. Esto se hace para "
"asegurarse de que el sistema no se venga abajo accidentalmente."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "POSIX 1003.1-2001 requires that I<kill(-1,sig)> send I<sig> to all "
#| "processes that the current process may send signals to, except possibly "
#| "for some implementation-defined system processes.  Linux allows a process "
#| "to signal itself, but on Linux the call I<kill(-1,sig)> does not signal "
#| "the current process."
msgid ""
"POSIX.1 requires that I<kill(-1,sig)> send I<sig> to all processes that the "
"calling process may send signals to, except possibly for some implementation-"
"defined system processes.  Linux allows a process to signal itself, but on "
"Linux the call I<kill(-1,sig)> does not signal the calling process."
msgstr ""
"POSIX 1003.1-2001 requiere que la llamada I<kill(-1,sig)> envíe I<sig> a "
"todos los procesos a los que el proceso actual puede mandar señales, excepto "
"posiblemente a algunos procesos del sistema definidos por la "
"implementación.  Linux permite a un proceso enviarse una señal a sí mismo, "
"pero en Linux la llamada I<kill(-1,sig)> no envía ninguna señal al proceso "
"actual."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1 requires that if a process sends a signal to itself, and the sending "
"thread does not have the signal blocked, and no other thread has it "
"unblocked or is waiting for it in B<sigwait>(3), at least one unblocked "
"signal must be delivered to the sending thread before the B<kill>()  returns."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In Linux 2.6 up to and including Linux 2.6.7, there was a bug that meant "
"that when sending signals to a process group, B<kill>()  failed with the "
"error B<EPERM> if the caller did not have permission to send the signal to "
"I<any> (rather than I<all>) of the members of the process group.  "
"Notwithstanding this error return, the signal was still delivered to all of "
"the processes for which the caller had permission to signal."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<kill>(1), B<_exit>(2), B<pidfd_send_signal>(2), B<signal>(2), B<tkill>(2), "
"B<exit>(3), B<killpg>(3), B<sigqueue>(3), B<capabilities>(7), "
"B<credentials>(7), B<signal>(7)"
msgstr ""
"B<kill>(1), B<_exit>(2), B<pidfd_send_signal>(2), B<signal>(2), B<tkill>(2), "
"B<exit>(3), B<killpg>(3), B<sigqueue>(3), B<capabilities>(7), "
"B<credentials>(7), B<signal>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 Diciembre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
