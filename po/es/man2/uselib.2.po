# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:20+0100\n"
"PO-Revision-Date: 1998-02-09 19:53+0200\n"
"Last-Translator: Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "uselib"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 Mayo 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "uselib - select shared library"
msgid "uselib - load shared library"
msgstr "uselib - selecciona biblioteca compartida"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int uselib(const char *>I<library>B<);>"
msgid "B<[[deprecated]] int uselib(const char *>I<library>B<);>\n"
msgstr "B<int uselib(const char *>I<biblioteca>B<);>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The system call B<uselib>()  serves to load a shared library to be used by "
"the calling process.  It is given a pathname.  The address where to load is "
"found in the library itself.  The library can have any recognized binary "
"format."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"En caso de éxito se devuelve cero. En caso de error se devuelve -1, y "
"I<errno> se configura para indicar el error."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In addition to all of the error codes returned by B<open>(2)  and "
"B<mmap>(2), the following may also be returned:"
msgstr ""
"Además de todos los códigos de error devueltos por B<open>(2)  y B<mmap>(2), "
"también pueden devolverse los siguientes:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The library specified by I<library> does not have read or execute "
"permission, or the caller does not have search permission for one of the "
"directories in the path prefix.  (See also B<path_resolution>(7).)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"Se ha alcanzado el límite máximo de archivos abiertos para el conjunto del "
"sistema."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOEXEC>"
msgstr "B<ENOEXEC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The file specified by I<library> is not executable, or does not have the "
#| "correct magic numbers."
msgid ""
"The file specified by I<library> is not an executable of a known type; for "
"example, it does not have the correct magic numbers."
msgstr ""
"El fichero especificado por I<biblioteca> no es ejecutable, o no posee los "
"números mágicos correctos."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This obsolete system call is not supported by glibc.  No declaration is "
"provided in glibc headers, but, through a quirk of history, glibc before "
"glibc 2.23 did export an ABI for this system call.  Therefore, in order to "
"employ this system call, it was sufficient to manually declare the interface "
"in your code; alternatively, you could invoke the system call using "
"B<syscall>(2)."
msgstr ""

#. #-#-#-#-#  archlinux: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .PP
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .PP
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .PP
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  fedora-41: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In ancient libc versions (before glibc 2.0), B<uselib>()  was used to load "
"the shared libraries with names found in an array of names in the binary."
msgstr ""

#.  commit 69369a7003735d0d8ef22097e27a55a8bad9557a
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since Linux 3.15, this system call is available only when the kernel is "
"configured with the B<CONFIG_USELIB> option."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<ar>(1), B<gcc>(1), B<ld>(1), B<ldd>(1), B<mmap>(2), B<open>(2), "
"B<dlopen>(3), B<capabilities>(7), B<ld.so>(8)"
msgstr ""
"B<ar>(1), B<gcc>(1), B<ld>(1), B<ldd>(1), B<mmap>(2), B<open>(2), "
"B<dlopen>(3), B<capabilities>(7), B<ld.so>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-07"
msgstr "7 Enero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<uselib>()  is Linux-specific, and should not be used in programs intended "
"to be portable."
msgstr ""
"B<uselib>() es específica de Linux, y no debería emplearse en programas que "
"se pretendan transportables."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
