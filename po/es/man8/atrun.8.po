# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Román Ramírez <rramirez@encomix.es>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-09-06 18:09+0200\n"
"PO-Revision-Date: 2021-06-09 17:17+0200\n"
"Last-Translator: Román Ramírez <rramirez@encomix.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.1\n"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ATRUN"
msgstr "ATRUN"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Nov 1996"
msgstr "Noviembre 1996"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "local"
msgstr "local"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "atrun - run jobs queued for later execution"
msgstr "atrun - ejecuta órdenes encoladas para una ejecución posterior"

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<atrun> [B<-l> I<load_avg>] [B<-d>]"
msgstr "B<atrun> [B<-l> I<carga_media>] [B<-d>]"

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid ""
#| "B<atrun> runs jobs queued by B<at(1)>.  It is a shell script containing "
#| "invoking B<${exec_prefix}/sbin/atd> with the I<-s> option, and is "
#| "provided for backward compatibility with older installations."
msgid ""
"B<atrun> runs jobs queued by B<at(1)>.  It is a shell script invoking B</usr/"
"bin/atd> with the I<-s> option, and is provided for backward compatibility "
"with older installations."
msgstr ""
"B<atrun> ejecuta trabajos encolados por B<at>(1).  Es un guión shell que "
"contiene una llamada B<${exec_prefix}/sbin/atd> con la opción I<-s>, y se "
"proporciona por compatibilidad con instalaciones antiguas."

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<at>(1), B<atd>(8)."
msgstr "B<at>(1), B<atd>(8)."

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "At was mostly written by Thomas Koenig."
msgstr "At fue escrito en su mayor parte por Thomas Koenig."

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<atrun> runs jobs queued by B<at(1)>.  It is a shell script containing "
#| "invoking B<${exec_prefix}/sbin/atd> with the I<-s> option, and is "
#| "provided for backward compatibility with older installations."
msgid ""
"B<atrun> runs jobs queued by B<at(1)>.  It is a shell script invoking B</usr/"
"sbin/atd> with the I<-s> option, and is provided for backward compatibility "
"with older installations."
msgstr ""
"B<atrun> ejecuta trabajos encolados por B<at>(1).  Es un guión shell que "
"contiene una llamada B<${exec_prefix}/sbin/atd> con la opción I<-s>, y se "
"proporciona por compatibilidad con instalaciones antiguas."
