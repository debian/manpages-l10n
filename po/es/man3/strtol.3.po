# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Carlos Gomez Romero <cgomez@databasedm.es>, 1998.
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2005.
# Marcos Fouces <marcos@debian.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:16+0100\n"
"PO-Revision-Date: 2023-03-11 00:35+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strtol"
msgstr "strtoll"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-16"
msgstr "16 Junio 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "strtol, strtoll, strtoq - convert a string to a long integer"
msgstr ""
"strtol, strtoll, strtoq - convierten una cadena en un entero de tipo long"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long strtol(const char *restrict >I<nptr>B<,>\n"
"B<            char **_Nullable restrict >I<endptr>B<, int >I<base>B<);>\n"
"B<long long strtoll(const char *restrict >I<nptr>B<,>\n"
"B<            char **_Nullable restrict >I<endptr>B<, int >I<base>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strtoll>():"
msgstr "B<strtoll>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strtol>()  function converts the initial part of the string in I<nptr> "
"to a long integer value according to the given I<base>, which must be "
"between 2 and 36 inclusive, or be the special value 0."
msgstr ""
"La función B<strtol>() convierte la parte inicial de la cadena de entrada "
"I<nptr> en un valor entero de tipo long de acuerdo a la I<base> dada, que "
"debe estar entre 2 y 36 ambos incluidos o ser el valor especial 0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The string must begin with an arbitrary amount of white space (as "
#| "determined by B<isspace>(3))  followed by a single optional `+' or `-' "
#| "sign.  If I<base> is zero or 16, the string may then include a `0x' "
#| "prefix, and the number will be read in base 16; otherwise, a zero I<base> "
#| "is taken as 10 (decimal) unless the next character is `0', in which case "
#| "it is taken as 8 (octal)."
msgid ""
"The string may begin with an arbitrary amount of white space (as determined "
"by B<isspace>(3))  followed by a single optional \\[aq]+\\[aq] or \\[aq]-"
"\\[aq] sign.  If I<base> is zero or 16, the string may then include a \"0x\" "
"or \"0X\" prefix, and the number will be read in base 16; otherwise, a zero "
"I<base> is taken as 10 (decimal) unless the next character is \\[aq]0\\[aq], "
"in which case it is taken as 8 (octal)."
msgstr ""
"La cadena debe comenzar con una cantidad arbitraria de espacios en blanco, "
"(tal y como los define la función B<isspace>(3))  seguida por un único y "
"opcional signo `+' o `-'.  Si la I<base> is 0 o 16, la cadena puede incluir "
"el prefijo `0x', y el número será interpretado en base 16, en caso contrario "
"la I<base> cero se toma como base 10 (decimal), a menos que el carácter "
"siguiente sea `0', en cuyo caso se toma como base 8 (octal)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The remainder of the string is converted to a long int value in the "
#| "obvious manner, stopping at the first character which is not a valid "
#| "digit in the given base.  (In bases above 10, the letter `A' in either "
#| "upper or lower case represents 10, `B' represents 11, and so forth, with "
#| "`Z' representing 35.)"
msgid ""
"The remainder of the string is converted to a I<long> value in the obvious "
"manner, stopping at the first character which is not a valid digit in the "
"given base.  (In bases above 10, the letter \\[aq]A\\[aq] in either "
"uppercase or lowercase represents 10, \\[aq]B\\[aq] represents 11, and so "
"forth, with \\[aq]Z\\[aq] representing 35.)"
msgstr ""
"El resto de la cadena se convierte en un entero de tipo long de una forma "
"evidente, parándose la conversión en el primer carácter que no es un dígito "
"válido en la base dada.  (En bases superiores a 10, la letra `A' en "
"mayúsculas o minúsculas representa el 10, `B' representa el 11, y así "
"sucesivamente, con la `Z' representando el 35.)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<endptr> is not NULL, B<strtol>()  stores the address of the first "
#| "invalid character in I<*endptr>.  If there were no digits at all, "
#| "B<strtol>()  stores the original value of I<nptr> in I<*endptr> (and "
#| "returns 0).  In particular, if I<*nptr> is not \\(aq\\e0\\(aq but "
#| "I<**endptr> is \\(aq\\e0\\(aq on return, the entire string is valid."
msgid ""
"If I<endptr> is not NULL, and the I<base> is supported, B<strtol>()  stores "
"the address of the first invalid character in I<*endptr>.  If there were no "
"digits at all, B<strtol>()  stores the original value of I<nptr> in "
"I<*endptr> (and returns 0).  In particular, if I<*nptr> is not "
"\\[aq]\\[rs]0\\[aq] but I<**endptr> is \\[aq]\\[rs]0\\[aq] on return, the "
"entire string is valid."
msgstr ""
"Si I<endptr> no es NULL, B<strtol>() almacena la dirección del primer "
"carácter no válido en I<*endptr>.  Si no hubiera dígitos en toda la cadena "
"B<strtol>() almacena el valor original de I<nptr> en I<*endptr>.  (y "
"devuelve 0).  En particular, si I<*nptr> es distinto de `\\e0' pero "
"I<**endptr> es `\\e0' a la vuelta, la cadena entera es válida."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strtoll>()  function works just like the B<strtol>()  function but "
"returns a I<long long> integer value."
msgstr ""
"La función B<strtoll>() hace el mismo trabajo que la función B<strtol>() "
"pero devuelve un valor entero de tipo I<long long>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strtol>()  function returns the result of the conversion, unless the "
"value would underflow or overflow.  If an underflow occurs, B<strtol>()  "
"returns B<LONG_MIN>.  If an overflow occurs, B<strtol>()  returns "
"B<LONG_MAX>.  In both cases, I<errno> is set to B<ERANGE>.  Precisely the "
"same holds for B<strtoll>()  (with B<LLONG_MIN> and B<LLONG_MAX> instead of "
"B<LONG_MIN> and B<LONG_MAX>)."
msgstr ""
"La función B<strtol>() devuelve el resultado de la conversión, a menos que "
"el valor se desbordara por arriba o por abajo.  Si ocurriera un "
"desbordamiento inferior, B<strtol>() devuelve B<LONG_MIN>.  Si ocurriera un "
"desbordamiento superior, B<strtol>() devuelve B<LONG_MAX>.  En ambos casos, "
"I<errno> se establece a B<ERANGE>.  Precisamente lo mismo se aplica a "
"B<strtoll>() (con B<LLONG_MIN> y B<LLONG_MAX> en lugar de B<LONG_MIN> y "
"B<LONG_MAX>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "These functions do not set I<errno>."
msgid "This function does not modify I<errno> on success."
msgstr "Estas funciones no definen I<errno>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "(not in C99)  The given I<base> contains an unsupported value."
msgstr "(no está en C99)  La I<base> dada contiene un valor no soportado."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The resulting value was out of range."
msgstr "El valor resultante está fuera de rango."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The implementation may also set I<errno> to B<EINVAL> in case no conversion "
"was performed (no digits seen, and 0 returned)."
msgstr ""
"La implementación puede poner también I<errno> a B<EINVAL> en caso de que no "
"se realice ninguna conversión (no se encuentren dígitos, y se devuelva 0)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strtol>(),\n"
"B<strtoll>(),\n"
"B<strtoq>()"
msgstr ""
"B<strtol>(),\n"
"B<strtoll>(),\n"
"B<strtoq>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "Configuración regional de multi-hilo seguro"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"According to POSIX.1, in locales other than \"C\" and \"POSIX\", these "
"functions may accept other, implementation-defined numeric strings."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "BSD also has"
msgstr "BSD tiene también la función"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<quad_t strtoq(const char *>I<nptr>B<, char **>I<endptr>B<, int >I<base>B<);>\n"
msgstr "B<quad_t strtoq(const char *>I<nptr>B<, char **>I<endptr>B<, int >I<base>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"with completely analogous definition.  Depending on the wordsize of the "
"current architecture, this may be equivalent to B<strtoll>()  or to "
"B<strtol>()."
msgstr ""
"con una definición completamente análoga.  Dependiendo del tamaño de palabra "
"de la arquitectura actual, ésta puede ser equivalente a B<strtoll>() o a "
"B<strtol>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strtoll>():"
msgid "B<strtol>()"
msgstr "B<strtoll>():"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C89, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, C89, SVr4, 4.3BSD."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strtoll>():"
msgid "B<strtoll>()"
msgstr "B<strtoll>():"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since B<strtol>()  can legitimately return 0, B<LONG_MAX>, or B<LONG_MIN> "
"(B<LLONG_MAX> or B<LLONG_MIN> for B<strtoll>())  on both success and "
"failure, the calling program should set I<errno> to 0 before the call, and "
"then determine if an error occurred by checking whether I<errno == ERANGE> "
"after the call."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the I<base> needs to be tested, it should be tested in a call where the "
"string is known to succeed.  Otherwise, it's impossible to portably "
"differentiate the errors."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"errno = 0;\n"
"strtol(\"0\", NULL, base);\n"
"if (errno == EINVAL)\n"
"    goto unsupported_base;\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program shown below demonstrates the use of B<strtol>().  The first "
"command-line argument specifies a string from which B<strtol>()  should "
"parse a number.  The second (optional) argument specifies the base to be "
"used for the conversion.  (This argument is converted to numeric form using "
"B<atoi>(3), a function that performs no error checking and has a simpler "
"interface than B<strtol>().)  Some examples of the results produced by this "
"program are the following:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out 123>\n"
"strtol() returned 123\n"
"$B< ./a.out \\[aq]    123\\[aq]>\n"
"strtol() returned 123\n"
"$B< ./a.out 123abc>\n"
"strtol() returned 123\n"
"Further characters after number: \"abc\"\n"
"$B< ./a.out 123abc 55>\n"
"strtol: Invalid argument\n"
"$B< ./a.out \\[aq]\\[aq]>\n"
"No digits were found\n"
"$B< ./a.out 4000000000>\n"
"strtol: Numerical result out of range\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Código fuente"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int base;\n"
"    char *endptr, *str;\n"
"    long val;\n"
"\\&\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s str [base]\\[rs]n\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 0;\n"
"\\&\n"
"    errno = 0;    /* To distinguish success/failure after call */\n"
"    strtol(\"0\", NULL, base);\n"
"    if (errno == EINVAL) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    errno = 0;    /* To distinguish success/failure after call */\n"
"    val = strtol(str, &endptr, base);\n"
"\\&\n"
"    /* Check for various possible errors. */\n"
"\\&\n"
"    if (errno == ERANGE) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (endptr == str) {\n"
"        fprintf(stderr, \"No digits were found\\[rs]n\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* If we got here, strtol() successfully parsed a number. */\n"
"\\&\n"
"    printf(\"strtol() returned %ld\\[rs]n\", val);\n"
"\\&\n"
"    if (*endptr != \\[aq]\\[rs]0\\[aq])        /* Not necessarily an error... */\n"
"        printf(\"Further characters after number: \\[rs]\"%s\\[rs]\"\\[rs]n\", endptr);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<strtod>(3), B<strtoimax>(3), "
"B<strtoul>(3)"
msgstr ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<strtod>(3), B<strtoimax>(3), "
"B<strtoul>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid ""
"B<long strtol(const char *restrict >I<nptr>B<,>\n"
"B<            char **restrict >I<endptr>B<, int >I<base>B<);>\n"
"B<long long strtoll(const char *restrict >I<nptr>B<,>\n"
"B<            char **restrict >I<endptr>B<, int >I<base>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "If I<endptr> is not NULL, B<strtol>()  stores the address of the first "
#| "invalid character in I<*endptr>.  If there were no digits at all, "
#| "B<strtol>()  stores the original value of I<nptr> in I<*endptr> (and "
#| "returns 0).  In particular, if I<*nptr> is not \\(aq\\e0\\(aq but "
#| "I<**endptr> is \\(aq\\e0\\(aq on return, the entire string is valid."
msgid ""
"If I<endptr> is not NULL, B<strtol>()  stores the address of the first "
"invalid character in I<*endptr>.  If there were no digits at all, "
"B<strtol>()  stores the original value of I<nptr> in I<*endptr> (and returns "
"0).  In particular, if I<*nptr> is not \\[aq]\\e0\\[aq] but I<**endptr> is "
"\\[aq]\\e0\\[aq] on return, the entire string is valid."
msgstr ""
"Si I<endptr> no es NULL, B<strtol>() almacena la dirección del primer "
"carácter no válido en I<*endptr>.  Si no hubiera dígitos en toda la cadena "
"B<strtol>() almacena el valor original de I<nptr> en I<*endptr>.  (y "
"devuelve 0).  En particular, si I<*nptr> es distinto de `\\e0' pero "
"I<**endptr> es `\\e0' a la vuelta, la cadena entera es válida."

#. type: Plain text
#: debian-bookworm
msgid "B<strtol>(): POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "B<strtol>(): POSIX.1-2001, POSIX.1-2008, C99 SVr4, 4.3BSD."

#. type: Plain text
#: debian-bookworm
msgid "B<strtoll>(): POSIX.1-2001, POSIX.1-2008, C99."
msgstr "B<strtoll>(): POSIX.1-2001, POSIX.1-2008, C99."

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: debian-bookworm
msgid ""
"Since B<strtol>()  can legitimately return 0, B<LONG_MAX>, or B<LONG_MIN> "
"(B<LLONG_MAX> or B<LLONG_MIN> for B<strtoll>())  on both success and "
"failure, the calling program should set I<errno> to 0 before the call, and "
"then determine if an error occurred by checking whether I<errno> has a "
"nonzero value after the call."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int base;\n"
"    char *endptr, *str;\n"
"    long val;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int base;\n"
"    char *endptr, *str;\n"
"    long val;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s str [base]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 0;\n"
msgstr ""
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 0;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    errno = 0;    /* To distinguish success/failure after call */\n"
"    val = strtol(str, &endptr, base);\n"
msgstr ""
"    errno = 0;    /* Para dstinguir éxito/error después de invocar */\n"
"    val = strtol(str, &endptr, base);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* Check for various possible errors. */\n"
msgstr "    /* Prueba varios posibles errores. */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (errno != 0) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (errno != 0) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (endptr == str) {\n"
"        fprintf(stderr, \"No digits were found\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* If we got here, strtol() successfully parsed a number. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    printf(\"strtol() returned %ld\\en\", val);\n"
msgstr "    printf(\"strtol() returned %ld\\en\", val);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (*endptr != \\[aq]\\e0\\[aq])        /* Not necessarily an error... */\n"
"        printf(\"Further characters after number: \\e\"%s\\e\"\\en\", endptr);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-12-19"
msgstr "19 Diciembre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "If I<endptr> is not NULL, B<strtol>()  stores the address of the first "
#| "invalid character in I<*endptr>.  If there were no digits at all, "
#| "B<strtol>()  stores the original value of I<nptr> in I<*endptr> (and "
#| "returns 0).  In particular, if I<*nptr> is not \\(aq\\e0\\(aq but "
#| "I<**endptr> is \\(aq\\e0\\(aq on return, the entire string is valid."
msgid ""
"If I<endptr> is not NULL, and the I<base> is supported, B<strtol>()  stores "
"the address of the first invalid character in I<*endptr>.  If there were no "
"digits at all, B<strtol>()  stores the original value of I<nptr> in "
"I<*endptr> (and returns 0).  In particular, if I<*nptr> is not "
"\\[aq]\\e0\\[aq] but I<**endptr> is \\[aq]\\e0\\[aq] on return, the entire "
"string is valid."
msgstr ""
"Si I<endptr> no es NULL, B<strtol>() almacena la dirección del primer "
"carácter no válido en I<*endptr>.  Si no hubiera dígitos en toda la cadena "
"B<strtol>() almacena el valor original de I<nptr> en I<*endptr>.  (y "
"devuelve 0).  En particular, si I<*nptr> es distinto de `\\e0' pero "
"I<**endptr> es `\\e0' a la vuelta, la cadena entera es válida."

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int base;\n"
"    char *endptr, *str;\n"
"    long val;\n"
"\\&\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s str [base]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 0;\n"
"\\&\n"
"    errno = 0;    /* To distinguish success/failure after call */\n"
"    strtol(\"0\", NULL, base);\n"
"    if (errno == EINVAL) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    errno = 0;    /* To distinguish success/failure after call */\n"
"    val = strtol(str, &endptr, base);\n"
"\\&\n"
"    /* Check for various possible errors. */\n"
"\\&\n"
"    if (errno == ERANGE) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (endptr == str) {\n"
"        fprintf(stderr, \"No digits were found\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* If we got here, strtol() successfully parsed a number. */\n"
"\\&\n"
"    printf(\"strtol() returned %ld\\en\", val);\n"
"\\&\n"
"    if (*endptr != \\[aq]\\e0\\[aq])        /* Not necessarily an error... */\n"
"        printf(\"Further characters after number: \\e\"%s\\e\"\\en\", endptr);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
