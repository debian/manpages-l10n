# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Pedro Pablo Fábrega <pfabrega@arrakis.es>, 2000.
# Juan Piernas <piernas@ditec.um.es>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:21+0100\n"
"PO-Revision-Date: 2020-11-24 18:09+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<wcwidth>()"
msgid "wcwidth"
msgstr "B<wcwidth>()"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 Junio 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "wcwidth - determine columns needed for a wide character"
msgstr "wcwidth - determina las columnas necesarias para un carácter ancho"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _XOPEN_SOURCE>       /* See feature_test_macros(7) */\n"
"B<#include E<lt>wchar.hE<gt>>\n"
msgstr ""
"B<#define _XOPEN_SOURCE>       /* Vea feature_test_macros(7) */\n"
"B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int wcwidth(wchar_t >I<c>B<);>\n"
msgstr "B<int wcwidth(wchar_t >I<c>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<wcwidth>()  function returns the number of columns needed to "
#| "represent the wide character I<c>.  If I<c> is a printable wide "
#| "character, the value is at least 0.  If I<c> is null wide character "
#| "(L\\(aq\\e0\\(aq), the value is 0.  Otherwise, -1 is returned."
msgid ""
"The B<wcwidth>()  function returns the number of columns needed to represent "
"the wide character I<c>.  If I<c> is a printable wide character, the value "
"is at least 0.  If I<c> is null wide character (L\\[aq]\\[rs]0\\[aq]), the "
"value is 0.  Otherwise, -1 is returned."
msgstr ""
"La función B<wcwidth> devuelve el número de columnas necesarias para "
"representar el carácter ancho I<c>.  Si I<c> es una carácter ancho "
"imprimible, el valor es al menos 0. Si I<c> es L\\(aq\\e0\\(aq), el valor es "
"cero.  En otro caso devuelve -1."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<wcwidth>()  function returns the number of column positions for I<c>."
msgstr ""
"La función B<wcwidth>() devuelve el número de posiciones de columna para "
"I<c>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<wcwidth>()"
msgstr "B<wcwidth>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "Configuración regional de multi-hilo seguro"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "Note that glibc before 2.2.5 used the prototype"
msgid "Note that before glibc 2.2.5, glibc used the prototype"
msgstr "Observe que la versión de glibc anterior a la 2.2.5 usaba el prototipo"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int wcwidth(wint_t >I<c>B<);>\n"
msgstr "B<int wcwidth(wint_t >I<c>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The behavior of B<wcwidth>()  depends on the B<LC_CTYPE> category of the "
"current locale."
msgstr ""
"El comportamiento de B<wcwidth>() depende de la categoría B<LC_CTYPE> de la "
"localización actual."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<iswprint>(3), B<wcswidth>(3)"
msgstr "B<iswprint>(3), B<wcswidth>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "The B<wcwidth>()  function returns the number of columns needed to "
#| "represent the wide character I<c>.  If I<c> is a printable wide "
#| "character, the value is at least 0.  If I<c> is null wide character "
#| "(L\\(aq\\e0\\(aq), the value is 0.  Otherwise, -1 is returned."
msgid ""
"The B<wcwidth>()  function returns the number of columns needed to represent "
"the wide character I<c>.  If I<c> is a printable wide character, the value "
"is at least 0.  If I<c> is null wide character (L\\[aq]\\e0\\[aq]), the "
"value is 0.  Otherwise, -1 is returned."
msgstr ""
"La función B<wcwidth> devuelve el número de columnas necesarias para "
"representar el carácter ancho I<c>.  Si I<c> es una carácter ancho "
"imprimible, el valor es al menos 0. Si I<c> es L\\(aq\\e0\\(aq), el valor es "
"cero.  En otro caso devuelve -1."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
