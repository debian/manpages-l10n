# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2021, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.2\n"
"POT-Creation-Date: 2024-12-06 18:01+0100\n"
"PO-Revision-Date: 2024-06-03 10:53+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "intro"
msgstr "intro"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. maj 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "intro - introduction to library functions"
msgstr "intro - introduktion til biblioteksfunktioner"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Section 3 of the manual describes all library functions excluding the "
"library functions (system call wrappers)  described in Section 2, which "
"implement system calls."
msgstr ""
"Afsnit 3 af manualen beskriver alle biblioteksfunktioner eksklusive "
"biblioteksfunktionerne (systemkaldomslag) beskrivet i afsnit 2, der "
"implementerer systemkald."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Many of the functions described in the section are part of the Standard C "
"Library (I<libc>).  Some functions are part of other libraries (e.g., the "
"math library, I<libm>, or the real-time library, I<librt>)  in which case "
"the manual page will indicate the linker option needed to link against the "
"required library (e.g., I<-lm> and I<-lrt>, respectively, for the "
"aforementioned libraries)."
msgstr ""
"Mange af funktionerne beskrevet i afsnittet er en del af Standard C-"
"biblioteket (I<libc>). Nogle funktioner er en del af andre biblioteker "
"(f.eks. math-biblioteket, I<libm>, eller realtidsbiblioteket, I<librt> i så "
"fald vil manualsiden indikere henvisningstilvalget krævet for at henvise mod "
"det krævede bibliotek (f.eks. I<-lm> og I<-lrt>, respektivt for de nævnte "
"biblioteker."

#
#.  There
#.  are various function groups which can be identified by a letter which
#.  is appended to the chapter number:
#.  .IP (3C)
#.  These functions,
#.  the functions from chapter 2 and from chapter 3S are
#.  contained in the C standard library libc,
#.  which will be used by
#.  .BR cc (1)
#.  by default.
#.  .IP (3S)
#.  These functions are parts of the
#.  .BR stdio (3)
#.  library.  They are contained in the standard C library libc.
#.  .IP (3M)
#.  These functions are contained in the arithmetic library libm.  They are
#.  used by the
#.  .BR f77 (1)
#.  FORTRAN compiler by default,
#.  but not by the
#.  .BR cc (1)
#.  C compiler,
#.  which needs the option \fI\-lm\fP.
#.  .IP (3F)
#.  These functions are part of the FORTRAN library libF77.  There are no
#.  special compiler flags needed to use these functions.
#.  .IP (3X)
#.  Various special libraries.  The manual pages documenting their functions
#.  specify the library names.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In some cases, the programmer must define a feature test macro in order to "
"obtain the declaration of a function from the header file specified in the "
"man page SYNOPSIS section.  (Where required, these I<feature test macros> "
"must be defined before including I<any> header files.)  In such cases, the "
"required macro is described in the man page.  For further information on "
"feature test macros, see B<feature_test_macros>(7)."
msgstr ""
"I nogle tilfælde skal programmøren definere en funktionstekstmakro for at "
"opnå erklæringen for en funktion fra teksthovedfilen anført i "
"manualsideafsnittet SYNOPSIS. (Hvor krævet, skal disse "
"I<funktionstekstmakroer> være defineret før inkludering I<eventuelle> "
"teksthovedfiler). I sådanne tilfælde er den krævede makro beskrevet i "
"manualsiden. For yderligere information om funktionstestmakroer, se "
"B<feature_test_macros>(7)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Subsections"
msgstr "Underafsnit"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Section 3 of this manual is organized into subsections that reflect the "
"complex structure of the standard C library and its many implementations:"
msgstr ""
"Afsnit 3 i denne manual er organisseret i underafsnit, der reflekterer den "
"komplekse struktur hos C-standardbiblioteket og dets mange implementeringer:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "3const"
msgstr "3const"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "3head"
msgstr "3head"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "3type"
msgstr "3type"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This difficult history frequently makes it a poor example to follow in "
"design, implementation, and presentation."
msgstr ""
"Denne svære historik gør det ofte til et dårligt eksempel at følge i design, "
"implementering og præsentation."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Ideally, a library for the C language is designed such that each header file "
"presents the interface to a coherent software module.  It provides a small "
"number of function declarations and exposes only data types and constants "
"that are required for use of those functions.  Together, these are termed an "
"API or I<application program interface>.  Types and constants to be shared "
"among multiple APIs should be placed in header files that declare no "
"functions.  This organization permits a C library module to be documented "
"concisely with one header file per manual page.  Such an approach improves "
"the readability and accessibility of library documentation, and thereby the "
"usability of the software."
msgstr ""
"Ideelt er et bibliotek for C-sproget designet så at hver teksthovedfil "
"præsenterer grænsefladen til et samlet programmodul. Det tilbyder et lille "
"antal funktionsdeklarationer og viser kun datatyper og konstanter, der er "
"krævet for brug af disse funktioner. Typer og konstanter til deling mellem "
"flere API'er bør placeres i teksthovedfiler, der erklærer ingen funktioner. "
"Denne organisering gør det muligt for et C-biblioteksmodul at blive "
"dokumenteret præcist med en teksthovedfil per manualside. Sådan en "
"fremgangsmåde forbedrer læsevenligheden og tilgængeligheden for "
"biblioteksdokumentation og dermed brugbarheden af programmet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Certain terms and abbreviations are used to indicate UNIX variants and "
"standards to which calls in this section conform.  See B<standards>(7)."
msgstr ""
"Bestemte termer og forkortelser bruges til at indikere UNIX-varianter og "
"standarder som kald i dette afsnit er i overensstemmelse med. Se "
"B<standards>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTER"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Authors and copyright conditions"
msgstr "Forfattere og ophavsretsbetingelser"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Look at the header of the manual page source for the author(s) and copyright "
"conditions.  Note that these can be different from page to page!"
msgstr ""
"Kig i teksthovedet for manualsidens kilde for forfatter(e) og "
"ophavsretsbetingelser. Bemærk at disse kan være forskellige fra side til "
"side!"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7), "
"B<system_data_types>(7)"
msgstr ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7), "
"B<system_data_types>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. oktober 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (ej udgivet)"
