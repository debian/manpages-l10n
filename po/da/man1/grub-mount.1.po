# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-10-04 17:51+0200\n"
"PO-Revision-Date: 2022-06-18 15:07+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-MOUNT"
msgstr "GRUB-MOUNT"

#. type: TH
#: archlinux
#, no-wrap
msgid "September 2024"
msgstr "september 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12-3"
msgstr "GRUB 2:2.12-3"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-mount - export GRUB filesystem with FUSE"
msgstr "grub-mount - eksporter GRUB-filsystem med FUSE"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"B<grub-mount> [I<\\,OPTION\\/>...] I<\\,IMAGE1 \\/>[I<\\,IMAGE2 \\/>...] "
"I<\\,MOUNTPOINT\\/>"
msgstr ""
"B<grub-mount> [I<\\,TILVALG\\/>...] I<\\,AFTRYK1 \\/>[I<\\,AFTRYK2 \\/>...] "
"I<\\,MONTERINGSPUNKT\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Debug tool for filesystem driver."
msgstr "Fejlsøgningsværktøj til filsystemdriver."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-C>, B<--crypto>"
msgstr "B<-C>, B<--crypto>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Mount crypto devices."
msgstr "Monter kryptoenheder."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--debug>=I<\\,STRING\\/>"
msgstr "B<-d>, B<--debug>=I<\\,STRENG\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Set debug environment variable."
msgstr "Angiv miljøvariabel til fejlsøgning."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-K>, B<--zfs-key>=I<\\,FILE\\/>|prompt"
msgstr "B<-K>, B<--zfs-key>=I<\\,FIL\\/>|prompt"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Load zfs crypto key."
msgstr "Indlæs zfs-kryptonøgle."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--root>=I<\\,DEVICE_NAME\\/>"
msgstr "B<-r>, B<--root>=I<\\,ENHEDSNAVN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Set root device."
msgstr "Sæt rodenhed."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "udskriv uddybende meddelelser."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "vis denne hjælpeliste"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "vis en kort besked om brug af programmet"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "udskriv programversion"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriske eller valgfri argumenter til lange tilvalg er også "
"obligatoriske henholdsvis valgfri til de tilsvarende korte."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-mount> is maintained as a Texinfo manual.  "
"If the B<info> and B<grub-mount> programs are properly installed at your "
"site, the command"
msgstr ""
"De volledige documentatie voor B<grub-mount> wordt bijgehouden als een "
"Texinfo-handleiding. Als de programmas B<info> en B<grub-mount> correct op "
"uw systeem zijn geïnstalleerd, geeft u de opdracht"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-mount>"
msgstr "B<info grub-mount>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "give dig adgang til den fulde manual."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2024"
msgstr "februar 2024"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.12-1~bpo12+1"
msgstr "GRUB 2.12-1~bpo12+1"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "July 2024"
msgstr "juli 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-5"
msgstr "GRUB 2.12-5"
