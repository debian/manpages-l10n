# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.14.0\n"
"POT-Creation-Date: 2023-06-27 20:02+0200\n"
"PO-Revision-Date: 2022-05-24 19:53+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "xsetpointer"
msgstr "xsetpointer"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "xsetpointer 1.0.1"
msgstr "xsetpointer 1.0.1"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "X Version 11"
msgstr "X Version 11"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "xsetpointer - set an X Input device as the main pointer"
msgstr "xsetpointer - angiv en X-indtastningsenhed som hovedpeger"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<xsetpointer -l> | I<device-name>"
msgstr "B<xsetpointer -l> | I<enhedsnavn>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Xsetpointer sets an XInput device as the main pointer.  When called with the "
"-l flag it lists the available devices.  When called with the -c/+c flag, it "
"toggles the sending of core input events, for servers which implement a "
"virtual core pointer; -c disables core events, and +c enables."
msgstr ""
"Xsetpointer angiver en XInput-enhed som hovedpegeren. Når kaldt med flaget "
"-l vises de tilgængelige enheder. Når kaldt med flaget -c/+c, så aktiveres "
"afsendelse af grundlæggende inddatahændelser, for servere der implementerer "
"en virtuel grundpeger; -c deaktiverer grundlæggende hændelser og +c "
"aktiverer."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "FORFATTER"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Frederic Lepied"
msgstr "Frederic Lepied"
