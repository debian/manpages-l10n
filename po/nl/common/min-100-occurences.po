# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2025.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2024-12-06 18:21+0100\n"
"PO-Revision-Date: 2025-01-08 07:48+0100\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.12.0\n"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTEN"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#: archlinux mageia-cauldron opensuse-leap-16-0 debian-bullseye
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "BESCHIKBAARHEID"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribuut"

#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "Augustus 2023"

#: mageia-cauldron archlinux fedora-41
#, no-wrap
#| msgid "August 2021"
msgid "August 2024"
msgstr "Augustus 2024"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed fedora-41 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed fedora-41 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed debian-bookworm
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed opensuse-leap-16-0
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#: debian-bookworm archlinux fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#: mageia-cauldron opensuse-leap-16-0 archlinux debian-unstable fedora-41
#: fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#: debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed archlinux
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#: archlinux mageia-cauldron opensuse-leap-16-0 debian-bullseye
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#: debian-bookworm fedora-rawhide
msgid "Display help text and exit."
msgstr "een hulptekst tonen en stoppen."

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#: mageia-cauldron archlinux opensuse-leap-16-0
#, no-wrap
msgid "ENVIRONMENT"
msgstr "OMGEVING"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#: debian-bookworm debian-unstable mageia-cauldron archlinux fedora-41
#: fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "VOORBEELDEN"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "EIND WAARDE"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#: archlinux mageia-cauldron opensuse-leap-16-0 debian-bullseye
#, no-wrap
msgid "FILES"
msgstr "BESTANDEN"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr "Feature Test Macro´s eisen in  glibc (zie B<feature_test_macros>(7)):"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Voor een uitleg van de termen in deze sectie, zie B<attributes>(7)."

#: debian-bookworm fedora-rawhide
msgid "For bug reports, use the issue tracker at"
msgstr "Gebruik om bugs te rapporteren de issue tracker op"

#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#: mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#: debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed archlinux
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed archlinux
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Online hulp bij GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHIEDENIS"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "Januari 2024"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niet vrijgegeven)"

#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed debian-bookworm
msgid "Linux."
msgstr "Linux."

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#: archlinux mageia-cauldron opensuse-leap-16-0 debian-bullseye
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#: fedora-rawhide debian-unstable opensuse-tumbleweed
#, no-wrap
#| msgid "September 2022"
msgid "November 2024"
msgstr "November 2024"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#: archlinux mageia-cauldron opensuse-leap-16-0 debian-bullseye
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIES"

#: debian-unstable fedora-41 mageia-cauldron opensuse-leap-16-0 fedora-rawhide
#, no-wrap
#| msgid "October 2023"
msgid "October 2024"
msgstr "Oktober 2024"

#: debian-bookworm archlinux debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed debian-bookworm mageia-cauldron
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#: debian-bookworm fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed archlinux debian-unstable
msgid "Print version and exit."
msgstr "Toon versie en stop."

#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed fedora-41 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTEREN VAN BUGS"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#: archlinux mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed archlinux
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Meld alle vertaalfouten op E<lt>https://translationproject.org/team/nl."
"htmlE<gt>"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#: archlinux mageia-cauldron opensuse-leap-16-0 debian-bullseye
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed archlinux
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#: archlinux mageia-cauldron opensuse-leap-16-0 debian-bullseye
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#: fedora-41 opensuse-tumbleweed archlinux debian-unstable fedora-rawhide
#: mageia-cauldron
#, no-wrap
#| msgid "September 2022"
msgid "September 2024"
msgstr "September 2024"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dit is vrije software: u mag het vrijelijk wijzigen en verder verspreiden. "
"Deze software kent GEEN GARANTIE, voor zover de wet dit toestaat."

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread veiligheid"

#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed fedora-41 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "User Commands"
msgstr "Opdrachten voor gebruikers"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIES"

#: archlinux debian-bookworm debian-unstable opensuse-tumbleweed fedora-41
#: fedora-rawhide mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "Value"
msgstr "Waarde"

#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed archlinux
msgid "display this help and exit"
msgstr "toon de helptekst en stop"

#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed archlinux
msgid "output version information and exit"
msgstr "toon programmaversie en stop"

#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed fedora-41 fedora-rawhide opensuse-leap-16-0
msgid "should give you access to the complete manual."
msgstr "toegang tot de volledige handleiding."

#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr "systemd 255"

#: fedora-41
#, no-wrap
#| msgid "systemd 252"
msgid "systemd 256.7"
msgstr "systemd 256.7"

#: archlinux opensuse-tumbleweed
#, no-wrap
#| msgid "systemd 252"
msgid "systemd 256.8"
msgstr "systemd 256.8"

#: debian-unstable fedora-rawhide
#, no-wrap
#| msgid "systemd 252"
msgid "systemd 257~rc3"
msgstr "systemd 257-rc3"

#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"
