# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexey, 2016.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014-2017.
# kogamatranslator49 <r.podarov@yandex.ru>, 2015.
# Darima Kogan <silverdk99@gmail.com>, 2014.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-12-06 18:09+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_task"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/task/, /proc/tid/, /proc/thread-self/ - thread information"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</task/> (since Linux 2.6.0)"
msgstr ""

#.  Precisely: Linux 2.6.0-test6
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is a directory that contains one subdirectory for each thread in the "
"process.  The name of each subdirectory is the numerical thread ID (I<tid>)  "
"of the thread (see B<gettid>(2))."
msgstr ""
"В этом каталоге содержатся подкаталоги, под одному на нить процесса. Имя "
"подкаталога задаётся числом, которое является ID (I<tid>) нити (смотрите "
"B<gettid>(2))."

#.  in particular: "children" :/
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Within each of these subdirectories, there is a set of files with the same "
"names and contents as under the I</proc/>pid directories.  For attributes "
"that are shared by all threads, the contents for each of the files under the "
"I<task/>tid subdirectories will be the same as in the corresponding file in "
"the parent I</proc/>pid directory (e.g., in a multithreaded process, all of "
"the I<task/>tidI</cwd> files will have the same value as the I</proc/>pidI</"
"cwd> file in the parent directory, since all of the threads in a process "
"share a working directory).  For attributes that are distinct for each "
"thread, the corresponding files under I<task/>tid may have different values "
"(e.g., various fields in each of the I<task/>tidI</status> files may be "
"different for each thread), or they might not exist in I</proc/>pid at all."
msgstr ""

#.  The following was still true as at kernel 2.6.13
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In a multithreaded process, the contents of the I</proc/>pidI</task> "
"directory are not available if the main thread has already terminated "
"(typically by calling B<pthread_exit>(3))."
msgstr ""
"В многонитевых процессах, содержимое каталога I</proc/>pidI</task> "
"недоступно, если главная нить уже завершила работу (обычно при помощи вызова "
"B<pthread_exit>(3))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>tidI</>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There is a numerical subdirectory for each running thread that is not a "
"thread group leader (i.e., a thread whose thread ID is not the same as its "
"process ID); the subdirectory is named by the thread ID.  Each one of these "
"subdirectories contains files and subdirectories exposing information about "
"the thread with the thread ID I<tid>.  The contents of these directories are "
"the same as the corresponding I</proc/>pidI</task/>tid directories."
msgstr ""
"Подкаталог с числовым названием для каждой выполняющейся нити, которая не "
"является лидером группы нитей (т. е., нить, чей ID нити не совпадает с ID её "
"процесса); подкаталог называется по ID нити. В подкаталоге содержатся файлы "
"и подкаталоги с информацией о нити с ID нити I<tid>. Содержимое этих "
"каталогов совпадает с соответствующими каталогами I</proc/>pidI</task/>tid."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I</proc/>tid subdirectories are I<not> visible when iterating through I</"
"proc> with B<getdents>(2)  (and thus are I<not> visible when one uses "
"B<ls>(1)  to view the contents of I</proc>).  However, the pathnames of "
"these directories are visible to (i.e., usable as arguments in)  system "
"calls that operate on pathnames."
msgstr ""
"Подкаталоги I</proc/>tid I<невидимы> при обходе I</proc> с помощью "
"B<getdents>(2) (и поэтому I<невидимы> программам, подобным B<ls>(1), "
"отображающим содержимое I</proc>). Однако пути к этим каталогам видимы (т. "
"е., их можно использовать в качестве параметров) системным вызовам, "
"работающими с этими путями."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/thread-self/> (since Linux 3.17)"
msgstr ""

#.  commit 0097875bd41528922fb3bb5f348c53f17e00e2fd
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory refers to the thread accessing the I</proc> filesystem, and "
"is identical to the I</proc/self/task/>tid directory named by the process "
"thread ID (I<tid>)  of the same thread."
msgstr ""
"Этот каталог ссылает на нить, обращающуюся к файловой системе I</proc>, и он "
"идентичен каталогу I</proc/self/task/>tid, где в имени ID нити процесса "
"(I<tid>) — эта же нить."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15 августа 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
