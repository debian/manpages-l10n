# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Aleksandr Felda <isk8da@gmail.com>, 2024.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-12-22 07:27+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Aleksandr Felda <isk8da@gmail.com>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "IPTABLES-APPLY"
msgstr "IPTABLES-APPLY"

#. type: TH
#: archlinux
#, no-wrap
msgid "iptables 1.8.10"
msgstr "iptables 1.8.10"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-16-0
msgid "iptables-apply - a safer way to update iptables remotely"
msgstr ""
"iptables-apply - более безопасный способ удаленного обновления iptables"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<iptables-apply> [-B<hV>] [B<-t> I<timeout>] [B<-w> I<savefile>] "
"{[I<rulesfile]|-c [runcmd]}>"
msgstr ""
"B<iptables-apply> [-B<hV>] [B<-t> I<timeout>] [B<-w> I<savefile>] "
"{[I<rulesfile]|-c [runcmd]}>"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"iptables-apply will try to apply a new rulesfile (as output by iptables-"
"save, read by iptables-restore) or run a command to configure iptables and "
"then prompt the user whether the changes are okay. If the new iptables rules "
"cut the existing connection, the user will not be able to answer "
"affirmatively. In this case, the script rolls back to the previous working "
"iptables rules after the timeout expires."
msgstr ""
"iptables-apply попытается применить новый файл правил (как вывод iptables-"
"save, прочитанный iptables-restore) или выполнить команду для настройки "
"iptables, а затем запросить  пользователя о допустимости выполнить "
"изменения. Если новые правила iptables сбросят существующее соединение, то "
"пользователь не сможет ответить утвердительно. В этом случае сценарий "
"выполнит возврат к предыдущим рабочим правилам iptables по истечении тайм-"
"аута."

#. type: Plain text
#: archlinux
msgid ""
"Successfully applied rules can also be written to savefile and later used to "
"roll back to this state. This can be used to implement a store last good "
"configuration mechanism when experimenting with an iptables setup script: "
"iptables-apply -w /etc/iptables/iptables.rules -c /etc/iptables/iptables.run"
msgstr ""
"Успешно примененные правила также могут быть записаны в файл сохранения и "
"позже использованы для возврата к этому состоянию. Это может быть "
"использовано для реализации механизма сохранения последней хорошей "
"конфигурации при экспериментах со сценарием установки iptables: iptables-"
"apply -w /etc/iptables/iptables.rules -c /etc/iptables/iptables.run"

#. type: Plain text
#: archlinux
msgid ""
"When called as ip6tables-apply, the script will use ip6tables-save/-restore "
"and IPv6 default values instead. Default value for rulesfile is '/etc/"
"iptables/iptables.rules'."
msgstr ""
"При вызове как ip6tables-apply скрипт будет использовать вместо этого "
"ip6tables-save/-restore и значения по умолчанию IPv6. Значение по умолчанию "
"для rulesfile равно '/etc/iptables/iptables.rules'."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "ОПЦИИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-t> I<seconds>, B<--timeout> I<seconds>"
msgstr "B<-t> I<seconds>, B<--timeout> I<seconds>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Sets the timeout in seconds after which the script will roll back to the "
"previous ruleset (default: 10)."
msgstr ""
"Определяет время ожидания в секундах, по истечении которого сценарий "
"выполнит возврат к предыдущему набору правил (по умолчанию: 10)."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-w> I<savefile>, B<--write> I<savefile>"
msgstr "B<-w> I<savefile>, B<--write> I<savefile>"

#. type: Plain text
#: archlinux
msgid ""
"Specify the savefile where successfully applied rules will be written to "
"(default if empty string is given: /etc/iptables/iptables.rules)."
msgstr ""
"Определяет файл сохранения, в который будут записаны успешно примененные "
"правила (по умолчанию, если указана пустая строка: /etc/iptables/iptables."
"rules)."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c> I<runcmd>, B<--command> I<runcmd>"
msgstr "B<-c> I<runcmd>, B<--command> I<runcmd>"

#. type: Plain text
#: archlinux
msgid ""
"Run command runcmd to configure iptables instead of applying a rulesfile "
"(default: /etc/iptables/iptables.run)."
msgstr ""
"Запускает команду runcmd для настройки iptables вместо применения файла "
"правил (по умолчанию: /etc/iptables/iptables.run)."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Display usage information."
msgstr "Выводит на дисплей (терминал) справочные сведения по использованию."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Display version information."
msgstr "Вывести на дисплей сведения о версии."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<iptables-restore>(8), B<iptables-save>(8), B<iptables>(8)."
msgstr "B<iptables-restore>(8), B<iptables-save>(8), B<iptables>(8)."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "LEGALESE"
msgstr "ЮРИДИЧЕСКОЕ ПРАВО"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Original iptables-apply - Copyright 2006 Martin F. Krafft "
"E<lt>madduck@madduck.netE<gt>.  Version 1.1 - Copyright 2010 GW "
"E<lt>gw.2010@tnode.com or http://gw.tnode.com/E<gt>."
msgstr ""
"Первоисточник iptables-apply - Авторское право 2006 Martin F. Krafft "
"E<lt>madduck@madduck.netE<gt>.  Версия 1.1 - Авторское право 2010 GW "
"E<lt>gw.2010@tnode.com or http://gw.tnode.com/E<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This manual page was written by Martin F. Krafft E<lt>madduck@madduck."
"netE<gt> and extended by GW E<lt>gw.2010@tnode.com or http://gw.tnode.com/"
"E<gt>."
msgstr ""
"Эта страница руководства была разработана Мартином Ф. Краффтом "
"E<lt>madduck@madduck.netE<gt> и доработана в GW E<lt>gw.2010@tnode.com или "
"http://gw.tnode.com/E<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Permission is granted to copy, distribute and/or modify this document under "
"the terms of the Artistic License 2.0."
msgstr ""
"Разрешается копировать, распространять и/или изменять данный документ в "
"соответствии с условиями Артистической лицензии 2.0."

#. type: TH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "iptables 1.8.9"
msgstr "iptables 1.8.9"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Successfully applied rules can also be written to savefile and later used to "
"roll back to this state. This can be used to implement a store last good "
"configuration mechanism when experimenting with an iptables setup script: "
"iptables-apply -w /etc/network/iptables.up.rules -c /etc/network/iptables.up."
"run"
msgstr ""
"Успешно примененные правила также могут быть записаны в файл сохранения и "
"позже использованы для возврата к этому состоянию. Это может быть "
"использовано для реализации механизма сохранения последней хорошей "
"конфигурации при экспериментах со сценарием установки iptables: iptables-"
"apply -w /etc/network/iptables.up.rules -c /etc/network/iptables.up.run"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"When called as ip6tables-apply, the script will use ip6tables-save/-restore "
"and IPv6 default values instead. Default value for rulesfile is '/etc/"
"network/iptables.up.rules'."
msgstr ""
"При вызове как ip6tables-apply сценарий будет использовать ip6tables-save/-"
"restore и значения по умолчанию IPv6. Значение по умолчанию для файла правил "
"- '/etc/network/iptables.up.rules'."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Specify the savefile where successfully applied rules will be written to "
"(default if empty string is given: /etc/network/iptables.up.rules)."
msgstr ""
"Определяет файл, в который будут сохранены успешно примененные правила (по "
"умолчанию, если указана пустая строка, то это файл: /etc/network/iptables.up."
"rules)."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Run command runcmd to configure iptables instead of applying a rulesfile "
"(default: /etc/network/iptables.up.run)."
msgstr ""
"Выполняет команду runcmd для настройки iptables вместо применения файла с "
"правилами (файл по умолчанию: /etc/network/iptables.up.run)."

#. type: TH
#: debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "iptables 1.8.11"
msgstr "iptables 1.8.11"

#. type: Plain text
#: debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "iptables-apply \\(em a safer way to update iptables remotely"
msgstr ""
"iptables-apply \\(em более безопасный способ удаленного обновления iptables"
