# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 17:59+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getlogin>()"
msgid "getlogin"
msgstr "B<getlogin>()"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "getlogin, getlogin_r, cuserid - get username"
msgstr "getlogin, getlogin_r, cuserid - возвращает имя пользователя"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int getlogin_r(char *>I<buf>B<, size_t >I<bufsize>B<);>"
msgid ""
"B<char *getlogin(void);>\n"
"B<int getlogin_r(char >I<buf>B<[.>I<bufsize>B<], size_t >I<bufsize>B<);>\n"
msgstr "B<int getlogin_r(char *>I<buf>B<, size_t >I<bufsize>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *cuserid(char *>I<string>B<);>"
msgid "B<char *cuserid(char *>I<string>B<);>\n"
msgstr "B<char *cuserid(char *>I<string>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. #-#-#-#-#  archlinux: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-41: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  mageia-cauldron: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-16-0: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getlogin_r>()"
msgid "B<getlogin_r>():"
msgstr "B<getlogin_r>()"

#.  Deprecated: _REENTRANT ||
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "_POSIX_C_SOURCE E<gt>= 200809L"
msgid "    _POSIX_C_SOURCE E<gt>= 199506L\n"
msgstr "_POSIX_C_SOURCE E<gt>= 200809L"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<cuserid>():"
msgstr "B<cuserid>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    Since glibc 2.24:\n"
#| "        (_XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
#| "        || _GNU_SOURCE\n"
#| "    Up to and including glibc 2.23:\n"
#| "        _XOPEN_SOURCE\n"
msgid ""
"    Since glibc 2.24:\n"
"        (_XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"            || _GNU_SOURCE\n"
"    Up to and including glibc 2.23:\n"
"        _XOPEN_SOURCE\n"
msgstr ""
"    Начиная с glibc 2.24:\n"
"        (_XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"        || _GNU_SOURCE\n"
"    До glibc 2.23 включительно:\n"
"        _XOPEN_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getlogin>()  returns a pointer to a string containing the name of the user "
"logged in on the controlling terminal of the process, or a null pointer if "
"this information cannot be determined.  The string is statically allocated "
"and might be overwritten on subsequent calls to this function or to "
"B<cuserid>()."
msgstr ""
"Функция B<getlogin>() возвращает указатель на строку, содержащую имя "
"пользователя, вошедшего в систему c терминала, который является управляющим "
"для процесса, или указатель null, если эта информация не может быть "
"получена. Строка выделяется статически и может быть перезаписана при "
"последующих вызовах этой функции или B<cuserid>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getlogin_r>()  returns this same username in the array I<buf> of size "
"I<bufsize>."
msgstr ""
"Функция B<getlogin_r>() возвращает то же имя пользователя, но в массиве "
"I<buf> размером I<bufsize>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<cuserid>()  returns a pointer to a string containing a username associated "
"with the effective user ID of the process.  If I<string> is not a null "
"pointer, it should be an array that can hold at least B<L_cuserid> "
"characters; the string is returned in this array.  Otherwise, a pointer to a "
"string in a static area is returned.  This string is statically allocated "
"and might be overwritten on subsequent calls to this function or to "
"B<getlogin>()."
msgstr ""
"Функция B<cuserid>() возвращает указатель на строку, содержащую имя "
"пользователя, связанное с идентификатором эффективного пользователя "
"процесса. Если I<string> не равно указателю null, то значение должно быть "
"массивом, который способен вместить как минимум B<L_cuserid> символов; "
"строка возвращается в этом массиве. В противном случае возвращается "
"указатель на строку в фиксированной области. Эта строка выделена статически "
"и может быть перезаписана при последующих вызовах этой функции или функции "
"B<getlogin>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The macro B<L_cuserid> is an integer constant that indicates how long an "
"array you might need to store a username.  B<L_cuserid> is declared in "
"I<E<lt>stdio.hE<gt>>."
msgstr ""
"Макрос B<L_cuserid> является целочисленной константой, показывающей длину "
"массива, который может понадобиться для хранения имени пользователя. "
"B<L_cuserid> описан в I<E<lt>stdio.hE<gt>>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions let your program identify positively the user who is running "
"(B<cuserid>())  or the user who logged in this session (B<getlogin>()).  "
"(These can differ when set-user-ID programs are involved.)"
msgstr ""
"Эти функции позволяют программе точно определить работающего пользователя "
"(B<cuserid>()) или пользователя этого сеанса (B<getlogin>()) (значения могут "
"различаться, если у программы установлен бит set-user-ID)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For most purposes, it is more useful to use the environment variable "
"B<LOGNAME> to find out who the user is.  This is more flexible precisely "
"because the user can set B<LOGNAME> arbitrarily."
msgstr ""
"В большинстве случаев для определения пользователя полезнее использовать "
"переменную окружения B<LOGNAME>, потому что пользователь может установить "
"B<LOGNAME> каким угодно."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<getlogin>()  returns a pointer to the username when successful, and "
#| "NULL on failure, with I<errno> set to indicate the cause of the error.  "
#| "B<getlogin_r>()  returns 0 when successful, and nonzero on failure."
msgid ""
"B<getlogin>()  returns a pointer to the username when successful, and NULL "
"on failure, with I<errno> set to indicate the error.  B<getlogin_r>()  "
"returns 0 when successful, and nonzero on failure."
msgstr ""
"При успешном выполнении функция B<getlogin>() возвращает указатель на имя "
"пользователя и NULL при ошибке, устанавливая в I<errno> значение ошибки. При "
"успешном выполнении B<getlogin_r>() возвращается 0 и не ноль при ошибке."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX specifies:"
msgstr "В POSIX определены:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""
"Было достигнуто ограничение по количеству открытых файловых дескрипторов на "
"процесс."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr "Достигнуто максимальное количество открытых файлов в системе."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENXIO>"
msgstr "B<ENXIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The calling process has no controlling terminal."
msgstr "У вызывающего процесса нет управляющего терминала."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "(getlogin_r)  The length of the username, including the terminating null "
#| "byte (\\(aq\\e0\\(aq), is larger than I<bufsize>."
msgid ""
"(getlogin_r)  The length of the username, including the terminating null "
"byte (B<\\[aq]\\[rs]0\\[aq]>), is larger than I<bufsize>."
msgstr ""
"(getlogin_r) Длина имени пользователя, включая завершающий байт null "
"(\\(aq\\e0\\(aq), больше чем I<bufsize>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux/glibc also has:"
msgstr "В Linux/glibc также есть:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "There was no corresponding entry in the utmp-file."
msgstr "Нет соответствующей записи в файле utmp."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Insufficient memory to allocate passwd structure."
msgstr "Недостаточно памяти для выделения под структуру passwd."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTTY>"
msgstr "B<ENOTTY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard input didn't refer to a terminal.  (See BUGS.)"
msgstr "Стандартный ввод не ссылается на терминал (смотрите ДЕФЕКТЫ)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/passwd>"
msgstr "I</etc/passwd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "password database file"
msgstr "файл, содержащий базу паролей"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</var/run/utmp>"
msgstr "I</var/run/utmp>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "(traditionally I</etc/utmp>; some libc versions used I</var/adm/utmp>)"
msgstr ""
"(обычно, I</etc/utmp>; в некоторых версиях libc используется I</var/adm/"
"utmp>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. #-#-#-#-#  archlinux: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-41: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-16-0: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<getlogin>()"
msgstr "B<getlogin>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Unsafe race:getlogin race:utent\n"
msgid ""
"MT-Unsafe race:getlogin race:utent\n"
"sig:ALRM timer locale"
msgstr "MT-Unsafe race:getlogin race:utent\n"

#. #-#-#-#-#  archlinux: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-41: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  mageia-cauldron: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-16-0: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<getlogin_r>()"
msgstr "B<getlogin_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Unsafe race:utent sig:ALRM timer\n"
msgid ""
"MT-Unsafe race:utent sig:ALRM timer\n"
"locale"
msgstr "MT-Unsafe race:utent sig:ALRM timer\n"

#. #-#-#-#-#  archlinux: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-41: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-16-0: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<cuserid>()"
msgstr "B<cuserid>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:cuserid/!string locale"
msgstr "MT-Unsafe race:cuserid/!string locale"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the above table, I<utent> in I<race:utent> signifies that if any of the "
"functions B<setutent>(3), B<getutent>(3), or B<endutent>(3)  are used in "
"parallel in different threads of a program, then data races could occur.  "
"B<getlogin>()  and B<getlogin_r>()  call those functions, so we use race:"
"utent to remind users."
msgstr ""
"В приведённой выше таблице I<utent> в I<race:utent> означает, что если любая "
"из функций B<setutent>(3), B<getutent>(3) или B<endutent>(3) используется "
"одновременно в нескольких нитях программы, то может возникнуть "
"состязательность по данным. Эти функции вызываются из B<getlogin>() и "
"B<getlogin_r>() поэтому мы используем race:utent для напоминания."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"OpenBSD has B<getlogin>()  and B<setlogin>(), and a username associated with "
"a session, even if it has no controlling terminal."
msgstr ""
"В OpenBSD имеются B<getlogin>() и B<setlogin>(), а имя пользователя "
"связывается с сеансом даже если не имеется управляющего терминала."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Отсутствуют."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001."
msgid "POSIX.1-2001.  OpenBSD."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"System V, POSIX.1-1988.  Removed in POSIX.1-1990.  SUSv2.  Removed in "
"POSIX.1-2001."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"System V has a B<cuserid>()  function which uses the real user ID rather "
"than the effective user ID."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Unfortunately, it is often rather easy to fool B<getlogin>().  Sometimes it "
"does not work at all, because some program messed up the utmp file.  Often, "
"it gives only the first 8 characters of the login name.  The user currently "
"logged in on the controlling terminal of our program need not be the user "
"who started it.  Avoid B<getlogin>()  for security-related purposes."
msgstr ""
"К сожалению, зачастую довольно просто «обмануть» B<getlogin>(). Иногда она "
"вообще не работает из-за того, что какая-то программа испортила содержимое "
"файла utmp. Часто функция возвращает только первые 8 символов имени. "
"Пользователь, вошедший в систему с терминала, который является управляющим "
"для нашей программы, необязательно будет пользователем, запустившим "
"программу. Избегайте использования B<getlogin>() из соображений безопасности."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that glibc does not follow the POSIX specification and uses I<stdin> "
"instead of I</dev/tty>.  A bug.  (Other recent systems, like SunOS 5.8 and "
"HP-UX 11.11 and FreeBSD 4.8 all return the login name also when I<stdin> is "
"redirected.)"
msgstr ""
"Заметим, что glibc не следует спецификации POSIX и использует I<stdin> "
"вместо I</dev/tty>. Дефект ( в других современных системах, например SunOS "
"5.8, HP-UX 11.11 и FreeBSD 4.8, также возвращают имя пользователя если было "
"перенаправление I<stdin>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Nobody knows precisely what B<cuserid>()  does; avoid it in portable "
"programs.  Or avoid it altogether: use I<getpwuid(geteuid())> instead, if "
"that is what you meant.  B<Do not use> B<cuserid>()."
msgstr ""
"Никто точно не знает что делает B<cuserid>(), поэтому не используйте её в "
"переносимых программах. Вместо неё используйте I<getpwuid(geteuid())>, если "
"это необходимо. B<Не используйте> B<cuserid>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<logname>(1), B<geteuid>(2), B<getuid>(2), B<utmp>(5)"
msgstr "B<logname>(1), B<geteuid>(2), B<getuid>(2), B<utmp>(5)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "(getlogin_r)  The length of the username, including the terminating null "
#| "byte (\\(aq\\e0\\(aq), is larger than I<bufsize>."
msgid ""
"(getlogin_r)  The length of the username, including the terminating null "
"byte (\\[aq]\\e0\\[aq]), is larger than I<bufsize>."
msgstr ""
"(getlogin_r) Длина имени пользователя, включая завершающий байт null "
"(\\(aq\\e0\\(aq), больше чем I<bufsize>."

#. type: Plain text
#: debian-bookworm
msgid "B<getlogin>()  and B<getlogin_r>(): POSIX.1-2001, POSIX.1-2008."
msgstr "B<getlogin>() и B<getlogin_r>(): POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
msgid ""
"System V has a B<cuserid>()  function which uses the real user ID rather "
"than the effective user ID.  The B<cuserid>()  function was included in the "
"1988 version of POSIX, but removed from the 1990 version.  It was present in "
"SUSv2, but removed in POSIX.1-2001."
msgstr ""
"В System V имеется функция B<cuserid>(), использующая идентификатор "
"реального пользователя вместо идентификатора эффективного пользователя. "
"Функция B<cuserid>() была включена в версию POSIX 1988 года, но удалена из "
"версии 1990 года. Она имеется в SUSv2, но удалена из POSIX.1-2001."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
