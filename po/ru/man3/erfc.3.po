# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 17:57+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "erfc"
msgstr "erfc"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "erfc, erfcf, erfcl - complementary error function"
msgstr "erfc, erfcf, erfcl - дополнительная функция ошибки"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Математическая библиотека (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double erfc(double >I<x>B<);>\n"
"B<float erfcf(float >I<x>B<);>\n"
"B<long double erfcl(long double >I<x>B<);>\n"
msgstr ""
"B<double erfc(double >I<x>B<);>\n"
"B<float erfcf(float >I<x>B<);>\n"
"B<long double erfcl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<erfc>():"
msgstr "B<erfc>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L || _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L || _XOPEN_SOURCE\n"
"        || /* Начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<erfcf>(), B<erfcl>():"
msgstr "B<erfcf>(), B<erfcl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions return the complementary error function of I<x>, that is, "
"1.0 - erf(x)."
msgstr ""
"Эти функции возвращают дополнительную функцию ошибки от I<x>, то есть 1.0 - "
"erf(x)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, these functions return the complementary error function of I<x>, "
"a value in the range [0,2]."
msgstr ""
"При успешном выполнении эти функции возвращают дополнительную функцию ошибки "
"от I<x>, значение в диапазоне [0,2]."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If I<x> is a NaN, a NaN is returned."
msgstr "Если I<x> имеет значение NaN, будет возвращено NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If I<x> is +0 or -0, 1 is returned."
msgstr "Если I<x> равен +0 или -0, возвращается -1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If I<x> is positive infinity, +0 is returned."
msgstr "Если I<x> стремится к плюс бесконечности, то будет возвращено +0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If I<x> is negative infinity, +2 is returned."
msgstr "Если I<x> равно минус бесконечности, возвращается +2."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the function result underflows and produces an unrepresentable value, the "
"return value is 0.0."
msgstr ""
"Если результат функции исчерпал степень, что грозит созданием "
"непредставимого числа, то возвращается значение 0.0."

#.  e.g., erfc(27) on x86-32
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the function result underflows but produces a representable (i.e., "
"subnormal) value, that value is returned, and a range error occurs."
msgstr ""
"Если в результате функции исчерпана степень, но значение представимо (т.е. "
"субнормально), то возвращается это значение и возникает ошибка диапазона."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Смотрите B<math_error>(7), чтобы определить, какие ошибки могут возникать "
"при вызове этих функций."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Могут возникать следующие ошибки:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result underflow (result is subnormal)"
msgstr "Ошибка диапазона: результат исчерпал степень(результат субнормален)"

#.  .I errno
#.  is set to
#.  .BR ERANGE .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An underflow floating-point exception (B<FE_UNDERFLOW>)  is raised."
msgstr ""
"Возникает исключение исчерпания степени чисел с плавающей запятой "
"(B<FE_UNDERFLOW>)."

#. #-#-#-#-#  archlinux: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see http://sources.redhat.com/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  debian-unstable: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  fedora-41: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions do not set I<errno>."
msgstr "Эти функции не изменяют I<errno>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<erfc>(),\n"
"B<erfcf>(),\n"
"B<erfcl>()"
msgstr ""
"B<erfc>(),\n"
"B<erfcf>(),\n"
"B<erfcl>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The variant returning I<double> also conforms to SVr4, 4.3BSD."
msgstr ""
"Вариант, возвращающий значение типа I<double>, также соответствует SVr4, "
"4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<erfc>(), B<erfcf>(), and B<erfcl>()  functions are provided to avoid "
"the loss accuracy that would occur for the calculation 1-erf(x) for large "
"values of I<x> (for which the value of erf(x) approaches 1)."
msgstr ""
"Функции B<erfc>(), B<erfcf>() и B<erfcl>() созданы с целью избежания потери "
"точности, которая возникает при вычислении 1-erf(x) для больших значений "
"I<x> (для которых значение erf(x) стремится к 1)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<cerf>(3), B<erf>(3), B<exp>(3)"
msgstr "B<cerf>(3), B<erf>(3), B<exp>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
