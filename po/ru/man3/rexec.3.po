# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2016.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Lockal <lockalsash@gmail.com>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Баринов Владимир, 2016.
# Иван Павлов <pavia00@gmail.com>, 2017,2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:11+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "rexec_af()"
msgid "rexec"
msgstr "rexec_af()"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "rexec, rexec_af - return stream to a remote command"
msgstr "rexec, rexec_af - возвращает поток удалённой команде"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>netdb.hE<gt>>\n"
msgstr "B<#include E<lt>netdb.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int rexec(char **>I<ahost>B<, int >I<inport>B<, const char *>I<user>B<, >\n"
#| "B<          const char *>I<passwd>B<, const char *>I<cmd>B<, int *>I<fd2p>B<);>\n"
msgid ""
"B<[[deprecated]]>\n"
"B<int rexec(char **restrict >I<ahost>B<, int >I<inport>B<,>\n"
"B<          const char *restrict >I<user>B<, const char *restrict >I<passwd>B<,>\n"
"B<          const char *restrict >I<cmd>B<, int *restrict >I<fd2p>B<);>\n"
msgstr ""
"B<int rexec(char **>I<ahost>B<, int >I<inport>B<, const char *>I<user>B<, >\n"
"B<          const char *>I<passwd>B<, const char *>I<cmd>B<, int *>I<fd2p>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int rexec_af(char **>I<ahost>B<, int >I<inport>B<, const char *>I<user>B<, >\n"
#| "B<             const char *>I<passwd>B<, const char *>I<cmd>B<, int *>I<fd2p>B<,>\n"
#| "B<             sa_family_t >I<af>B<);>\n"
msgid ""
"B<[[deprecated]]>\n"
"B<int rexec_af(char **restrict >I<ahost>B<, int >I<inport>B<,>\n"
"B<          const char *restrict >I<user>B<, const char *restrict >I<passwd>B<,>\n"
"B<          const char *restrict >I<cmd>B<, int *restrict >I<fd2p>B<,>\n"
"B<          sa_family_t >I<af>B<);>\n"
msgstr ""
"B<int rexec_af(char **>I<ahost>B<, int >I<inport>B<, const char *>I<user>B<, >\n"
"B<             const char *>I<passwd>B<, const char *>I<cmd>B<, int *>I<fd2p>B<,>\n"
"B<             sa_family_t >I<af>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rexec>(), B<rexec_af>():"
msgstr "B<rexec>(), B<rexec_af>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    In glibc up to and including 2.19:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    в glibc до версии 2.19 включительно:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This interface is obsoleted by B<rcmd>(3)."
msgstr "Этот интерфейс устарел, используйте B<rcmd>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<rexec>()  function looks up the host I<*ahost> using "
"B<gethostbyname>(3), returning -1 if the host does not exist.  Otherwise, "
"I<*ahost> is set to the standard name of the host.  If a username and "
"password are both specified, then these are used to authenticate to the "
"foreign host; otherwise the environment and then the I<.netrc> file in "
"user's home directory are searched for appropriate information.  If all this "
"fails, the user is prompted for the information."
msgstr ""
"Функция B<rexec>() ищет узел I<*ahost>, используя B<gethostbyname>(3), и "
"возвращает -1, если узел не существует. В противном случае, I<*ahost> "
"присваивается стандартное имя узла. Если указаны имя пользователя и пароль, "
"то они используются для аутентификации на другом узле; иначе происходит "
"поиск соответствующей информации в окружении и затем в файле I<.netrc>, "
"находящемся в домашнем каталоге пользователя. Если всё это заканчивается с "
"ошибкой, пользователь извещается об этом."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The port I<inport> specifies which well-known DARPA Internet port to use for "
"the connection; the call I<getservbyname(\"exec\", \"tcp\")> (see "
"B<getservent>(3))  will return a pointer to a structure that contains the "
"necessary port.  The protocol for connection is described in detail in "
"B<rexecd>(8)."
msgstr ""
"В I<inport> указывается, какой хорошо известный (well-known) порт DARPA "
"Internet нужно использовать для подключения; вызов I<getservbyname(\"exec\", "
"\"tcp\")> (смотрите B<getservent>(3)) вернёт указатель на структуру, которая "
"содержит необходимый порт. Протокол подключения подробно описан в "
"B<rexecd>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the connection succeeds, a socket in the Internet domain of type "
"B<SOCK_STREAM> is returned to the caller, and given to the remote command as "
"I<stdin> and I<stdout>.  If I<fd2p> is nonzero, then an auxiliary channel to "
"a control process will be setup, and a file descriptor for it will be placed "
"in I<*fd2p>.  The control process will return diagnostic output from the "
"command (unit 2) on this channel, and will also accept bytes on this channel "
"as being UNIX signal numbers, to be forwarded to the process group of the "
"command.  The diagnostic information returned does not include remote "
"authorization failure, as the secondary connection is set up after "
"authorization has been verified.  If I<fd2p> is 0, then the I<stderr> (unit "
"2 of the remote command) will be made the same as the I<stdout> and no "
"provision is made for sending arbitrary signals to the remote process, "
"although you may be able to get its attention by using out-of-band data."
msgstr ""
"Если соединение успешно установлено, то вызывающему возвращается сокет в "
"Интернет-домене типа B<SOCK_STREAM>, который для удалённой команды считается "
"I<stdin> и I<stdout>. Если I<fd2p> не равно нулю, то устанавливается "
"вспомогательный канал до управляющего процесса, а его файловый дескриптор "
"будет помещён в I<*fd2p>. Управляющий процесс возвращает диагностический "
"вывод из команды (устройства 2) в этот канал, а также принимает байты из "
"этого канала, считая их номерами сигналов UNIX, для их пересылки группе "
"процессов команды. Возвращаемая диагностическая информация не содержит "
"ошибки удалённой авторизации, так как второе подключение устанавливается "
"после прохождения авторизации. Если I<fd2p> равно 0, то I<stderr> "
"(устройство 2 удалённой команды) будет работать аналогично I<stdout> и для "
"отправки произвольных сигналов в удалённый процесс ничего не будет сделано, "
"хотя вы сможете привлечь к себе внимание, используя внеполосную передачу "
"данных."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "rexec_af()"
msgstr "rexec_af()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<rexec>()  function works over IPv4 (B<AF_INET>).  By contrast, the "
"B<rexec_af>()  function provides an extra argument, I<af>, that allows the "
"caller to select the protocol.  This argument can be specified as "
"B<AF_INET>, B<AF_INET6>, or B<AF_UNSPEC> (to allow the implementation to "
"select the protocol)."
msgstr ""
"Функция B<rexec>() работает по протоколу IPv4 (B<AF_INET>). В отличие от "
"неё, B<rexec_af>() предоставляет ещё один аргумент, I<af>, который позволяет "
"вызывающему выбрать протокол. В этом аргументе можно указать значения "
"B<AF_INET>, B<AF_INET6> или B<AF_UNSPEC> (чтобы позволить реализации "
"выбирать протокол)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<rexec>(),\n"
"B<rexec_af>()"
msgstr ""
"B<rexec>(),\n"
"B<rexec_af>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe"
msgstr "MT-Unsafe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Отсутствуют."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<execve>(2)"
msgid "B<rexec>()"
msgstr "B<execve>(2)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "4.2BSD, BSD, Solaris."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "rexec_af()"
msgid "B<rexec_af>()"
msgstr "rexec_af()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.2."
msgstr "glibc 2.2."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<rexec>()  function sends the unencrypted password across the network."
msgstr "Функция B<rexec>() посылает пароль через сеть в нешифрованном виде."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The underlying service is considered a big security hole and therefore not "
"enabled on many sites; see B<rexecd>(8)  for explanations."
msgstr ""
"Считается, что лежащая в основе служба является большой дырой в "
"безопасности, и поэтому не включена на большинстве сайтов; смотрите "
"объяснение в B<rexecd>(8)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rcmd>(3), B<rexecd>(8)"
msgstr "B<rcmd>(3), B<rexecd>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "The B<rexec_af>()  function was added to glibc in version 2.2."
msgid "The B<rexec_af>()  function was added in glibc 2.2."
msgstr "Функция B<rexec_af>() впервые появилась в glibc 2.2."

#. type: Plain text
#: debian-bookworm
msgid ""
"These functions are not in POSIX.1.  The B<rexec>()  function first appeared "
"in 4.2BSD, and is present on the BSDs, Solaris, and many other systems.  The "
"B<rexec_af>()  function is more recent, and less widespread."
msgstr ""
"Данные функции отсутствуют в POSIX.1. Функция B<rexec>() впервые появилась в "
"4.2BSD, и есть в разных BSD, Solaris и других системах. Функция "
"B<rexec_af>() более новая и распространена гораздо меньше."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
