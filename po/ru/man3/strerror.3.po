# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:15+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "strerror_r()"
msgid "strerror"
msgstr "strerror_r()"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "strerror, strerror_r, strerror_l - return string describing error number"
msgid ""
"strerror, strerrorname_np, strerrordesc_np, strerror_r, strerror_l - return "
"string describing error number"
msgstr ""
"strerror, strerror_r, strerror_l - возвращают строку описания номера ошибки"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *strerror(int >I<errnum>B<);>\n"
"B<const char *strerrorname_np(int >I<errnum>B<);>\n"
"B<const char *strerrordesc_np(int >I<errnum>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int strerror_r(int >I<errnum>B<, char *>I<buf>B<, size_t >I<buflen>B<);>\n"
#| "            /* XSI-compliant */\n"
msgid ""
"B<int strerror_r(int >I<errnum>B<, char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<);>\n"
"               /* XSI-compliant */\n"
msgstr ""
"B<int strerror_r(int >I<errnum>B<, char *>I<buf>B<, size_t >I<buflen>B<);>\n"
"            /* XSI-совместимо */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<char *strerror_r(int >I<errnum>B<, char *>I<buf>B<, size_t >I<buflen>B<);>\n"
#| "            /* GNU-specific */\n"
msgid ""
"B<char *strerror_r(int >I<errnum>B<, char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<);>\n"
"               /* GNU-specific */\n"
msgstr ""
"B<char *strerror_r(int >I<errnum>B<, char *>I<buf>B<, size_t >I<buflen>B<);>\n"
"            /* есть только в GNU */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strerror_l(int >I<errnum>B<, locale_t >I<locale>B<);>\n"
msgstr "B<char *strerror_l(int >I<errnum>B<, locale_t >I<locale>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "B<psignal>(3), B<strerror>(3)"
msgid "B<strerrorname_np>(), B<strerrordesc_np>():"
msgstr "B<psignal>(3), B<strerror>(3)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strerror_r>():"
msgstr "B<strerror_r>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    The XSI-compliant version is provided if:\n"
"        (_POSIX_C_SOURCE E<gt>= 200112L) && ! _GNU_SOURCE\n"
"    Otherwise, the GNU-specific version is provided.\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strerror>()  function returns a pointer to a string that describes "
#| "the error code passed in the argument I<errnum>, possibly using the "
#| "B<LC_MESSAGES> part of the current locale to select the appropriate "
#| "language.  (For example, if I<errnum> is B<EINVAL>, the returned "
#| "description will be \"Invalid argument\".)  This string must not be "
#| "modified by the application, but may be modified by a subsequent call to "
#| "B<strerror>()  or B<strerror_l>().  No other library function, including "
#| "B<perror>(3), will modify this string."
msgid ""
"The B<strerror>()  function returns a pointer to a string that describes the "
"error code passed in the argument I<errnum>, possibly using the "
"B<LC_MESSAGES> part of the current locale to select the appropriate "
"language.  (For example, if I<errnum> is B<EINVAL>, the returned description "
"will be \"Invalid argument\".)  This string must not be modified by the "
"application, and the returned pointer will be invalidated on a subsequent "
"call to B<strerror>()  or B<strerror_l>(), or if the thread that obtained "
"the string exits.  No other library function, including B<perror>(3), will "
"modify this string."
msgstr ""
"Функция B<strerror>() возвращает указатель на строку, которая описывает код "
"ошибки, переданный в аргументе I<errnum>, возможно, с помощью части "
"B<LC_MESSAGES> текущей локали для выбора соответствующего языка (например, "
"если I<errnum> равно B<EINVAL>, то возвращается описание «Invalid "
"argument»). Эту строку нельзя изменять в приложении, она может измениться "
"при последующих вызовах B<strerror>() или B<strerror_l>(). Другие "
"библиотечные функции, включая B<perror>(3), не изменяют эту строку."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Like B<strerror>(), the B<strerrordesc_np>()  function returns a pointer to "
"a string that describes the error code passed in the argument I<errnum>, "
"with the difference that the returned string is not translated according to "
"the current locale."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strerrorname_np>()  function returns a pointer to a string containing "
"the name of the error code passed in the argument I<errnum>.  For example, "
"given B<EPERM> as an argument, this function returns a pointer to the string "
"\"EPERM\".  Given B<0> as an argument, this function returns a pointer to "
"the string \"0\"."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strerror_r()"
msgstr "strerror_r()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strerror_r>()  function is similar to B<strerror>(), but is thread "
#| "safe.  This function is available in two versions: an XSI-compliant "
#| "version specified in POSIX.1-2001 (available since glibc 2.3.4, but not "
#| "POSIX-compliant until glibc 2.13), and a GNU-specific version (available "
#| "since glibc 2.0).  The XSI-compliant version is provided with the feature "
#| "test macros settings shown in the SYNOPSIS; otherwise the GNU-specific "
#| "version is provided.  If no feature test macros are explicitly defined, "
#| "then (since glibc 2.4)  B<_POSIX_C_SOURCE> is defined by default with the "
#| "value 200112L, so that the XSI-compliant version of B<strerror_r>()  is "
#| "provided by default."
msgid ""
"B<strerror_r>()  is like B<strerror>(), but might use the supplied buffer "
"I<buf> instead of allocating one internally.  This function is available in "
"two versions: an XSI-compliant version specified in POSIX.1-2001 (available "
"since glibc 2.3.4, but not POSIX-compliant until glibc 2.13), and a GNU-"
"specific version (available since glibc 2.0).  The XSI-compliant version is "
"provided with the feature test macros settings shown in the SYNOPSIS; "
"otherwise the GNU-specific version is provided.  If no feature test macros "
"are explicitly defined, then (since glibc 2.4)  B<_POSIX_C_SOURCE> is "
"defined by default with the value 200112L, so that the XSI-compliant version "
"of B<strerror_r>()  is provided by default."
msgstr ""
"Функция B<strerror_r>() подобна B<strerror>(), но её можно безопасно "
"использовать в нитях. Она доступна в двух версиях: версия, совместимая с "
"XSI, определена в POSIX.1-2001 (доступна в glibc начиная с 2.3.4, но не "
"совместима с POSIX до glibc 2.13) и версия, совместимая с GNU (доступна, "
"начиная с glibc 2.0). Версия, совместимая с XSI, предоставляется при наличии "
"набора макросов тестирования свойств, показанных в ОБЗОРЕ; в противном "
"случае предоставляется версия GNU. Если макросы тестирования свойств не "
"указаны явным образом, то (начиная с glibc 2.4) по умолчанию определяется "
"B<_POSIX_C_SOURCE> со значением 200112L, то есть версия B<strerror_r>() XSI "
"предоставляется по умолчанию."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The XSI-compliant B<strerror_r>()  is preferred for portable applications.  "
"It returns the error string in the user-supplied buffer I<buf> of length "
"I<buflen>."
msgstr ""
"Совместимая с XSI версия B<strerror_r>() более предпочтительна для "
"переносимых приложений. Она возвращает строку ошибки в предоставляемом "
"пользователем буфере I<buf> длиной I<buflen>."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The GNU-specific B<strerror_r>()  returns a pointer to a string "
#| "containing the error message.  This may be either a pointer to a string "
#| "that the function stores in I<buf>, or a pointer to some (immutable) "
#| "static string (in which case I<buf> is unused).  If the function stores a "
#| "string in I<buf>, then at most I<buflen> bytes are stored (the string may "
#| "be truncated if I<buflen> is too small and I<errnum> is unknown).  The "
#| "string always includes a terminating null byte (\\(aq\\e0\\(aq)."
msgid ""
"The GNU-specific B<strerror_r>()  returns a pointer to a string containing "
"the error message.  This may be either a pointer to a string that the "
"function stores in I<buf>, or a pointer to some (immutable) static string "
"(in which case I<buf> is unused).  If the function stores a string in "
"I<buf>, then at most I<buflen> bytes are stored (the string may be truncated "
"if I<buflen> is too small and I<errnum> is unknown).  The string always "
"includes a terminating null byte (\\[aq]\\[rs]0\\[aq])."
msgstr ""
"Специальная GNU-версия B<strerror_r>() возвращает строку, содержащую "
"сообщение об ошибке. Это может быть или указатель на строку, которую функция "
"записывает в I<buf>, или указатель на некую (неизменную) статическую строку "
"(в этом случае I<buf> не используется). Если функция сохраняет строку в "
"I<buf>, то сохраняется не более I<buflen> байт (строка может быть обрезана, "
"если значение I<buflen> слишком мало и I<errnum> неизвестно). В строке "
"всегда содержится конечный байт null (\\(aq\\e0\\(aq)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strerror_l()"
msgstr "strerror_l()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<strerror_l>()  is like B<strerror>(), but maps I<errnum> to a locale-"
"dependent error message in the locale specified by I<locale>.  The behavior "
"of B<strerror_l>()  is undefined if I<locale> is the special locale object "
"B<LC_GLOBAL_LOCALE> or is not a valid locale object handle."
msgstr ""
"Функция B<strerror_l>() подобна B<strerror>(), но отражает I<errnum> в "
"локале-зависимое сообщение об ошибке, в зависимости от локали, заданной в "
"I<locale>. Поведение B<strerror_l>() не определено, если значение I<locale> "
"равно специальному объекту локали B<LC_GLOBAL_LOCALE> или некорректному "
"описателю объекта локали."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strerror>(), B<strerror_l>(), and the GNU-specific B<strerror_r>()  "
"functions return the appropriate error description string, or an \"Unknown "
"error nnn\" message if the error number is unknown."
msgstr ""
"Функции B<strerror>(), B<strerror_l>() и GNU-версия B<strerror_r>() "
"возвращают соответствующую строку описания ошибки или сообщение «Unknown "
"error nnn», если номер ошибки неизвестен."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strerror>(), B<strerror_l>(), and the GNU-specific B<strerror_r>()  "
#| "functions return the appropriate error description string, or an "
#| "\"Unknown error nnn\" message if the error number is unknown."
msgid ""
"On success, B<strerrorname_np>()  and B<strerrordesc_np>()  return the "
"appropriate error description string.  If I<errnum> is an invalid error "
"number, these functions return NULL."
msgstr ""
"Функции B<strerror>(), B<strerror_l>() и GNU-версия B<strerror_r>() "
"возвращают соответствующую строку описания ошибки или сообщение «Unknown "
"error nnn», если номер ошибки неизвестен."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The XSI-compliant B<strerror_r>()  function returns 0 on success.  On "
#| "error, a (positive) error number is returned (since glibc 2.13), or -1 is "
#| "returned and I<errno> is set to indicate the error (glibc versions before "
#| "2.13)."
msgid ""
"The XSI-compliant B<strerror_r>()  function returns 0 on success.  On error, "
"a (positive) error number is returned (since glibc 2.13), or -1 is returned "
"and I<errno> is set to indicate the error (before glibc 2.13)."
msgstr ""
"При успешном выполнении совместимая с XSI функция B<strerror_r>() возвращает "
"0. При ошибке возвращается (положительный) номер ошибки (начиная с glibc "
"2.13), или -1 с изменением I<errno> на соответствующий код ошибки (версии "
"glibc до 2.13)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 and POSIX.1-2008 require that a successful call to "
"B<strerror>()  or B<strerror_l>()  shall leave I<errno> unchanged, and note "
"that, since no function return value is reserved to indicate an error, an "
"application that wishes to check for errors should initialize I<errno> to "
"zero before the call, and then check I<errno> after the call."
msgstr ""
"В POSIX.1-2001 и POSIX.1-2008 требуется, чтобы успешный вызов B<strerror>() "
"или B<strerror_l>() не изменял I<errno>, и отмечается, что так как для "
"указания на ошибку не зарезервировано возвращаемого значения, в приложениях "
"для проверки ошибки нужно инициализировать I<errno> нулём до вызова и "
"проверять I<errno> после вызова."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The value of I<errnum> is not a valid error number."
msgstr "Значение I<errnum> не является допустимым номером ошибки."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Insufficient storage was supplied to contain the error description string."
msgstr "Предоставлено недостаточно места для сохранения описания ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. #-#-#-#-#  archlinux: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-41: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-16-0: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strerror>()"
msgstr "B<strerror>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<strcmp>(),\n"
#| "B<strncmp>()"
msgid ""
"B<strerrorname_np>(),\n"
"B<strerrordesc_np>()"
msgstr ""
"B<strcmp>(),\n"
"B<strncmp>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strerror_r>(),\n"
msgid ""
"B<strerror_r>(),\n"
"B<strerror_l>()"
msgstr "B<strerror_r>(),\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Before glibc 2.32, B<strerror>()  is not MT-Safe."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strerror_r>():"
msgid "B<strerror_r>()"
msgstr "B<strerror_r>():"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strerror_l>()"
msgstr "B<strerror_l>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strerror_l>()"
msgid "B<strerrorname_np>()"
msgstr "B<strerror_l>()"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strerror_l>()"
msgid "B<strerrordesc_np>()"
msgstr "B<strerror_l>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#.  e.g., Solaris 8, HP-UX 11
#.  e.g., FreeBSD 5.4, Tru64 5.1B
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 permits B<strerror>()  to set I<errno> if the call encounters "
"an error, but does not specify what value should be returned as the function "
"result in the event of an error.  On some systems, B<strerror>()  returns "
"NULL if the error number is unknown.  On other systems, B<strerror>()  "
"returns a string something like \"Error nnn occurred\" and sets I<errno> to "
"B<EINVAL> if the error number is unknown.  C99 and POSIX.1-2008 require the "
"return value to be non-NULL."
msgstr ""
"В POSIX.1-2001 разрешено B<strerror>() изменять I<errno>, если при вызове "
"возникла ошибка, но не указано какое значение нужно возвращать в качестве "
"результата функции. В некоторых системах B<strerror>() возвращает NULL, если "
"номер ошибки неизвестен. В других системах B<strerror>() возвращает строку "
"вроде «Error nnn occurred» и записывает в I<errno> значение B<EINVAL>, если "
"номер ошибки неизвестен. В C99 и POSIX.1-2008 требуется, чтобы возвращаемое "
"значение не было равно NULL."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C89."
msgstr "POSIX.1-2001, C89."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.6.  POSIX.1-2008."
msgstr "glibc 2.6.  POSIX.1-2008."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.32."
msgstr "glibc 2.32."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<strerrorname_np>()  and B<strerrordesc_np>()  are thread-safe and async-"
"signal-safe."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<err>(3), B<errno>(3), B<error>(3), B<perror>(3), B<strsignal>(3), "
#| "B<locale>(7)"
msgid ""
"B<err>(3), B<errno>(3), B<error>(3), B<perror>(3), B<strsignal>(3), "
"B<locale>(7), B<signal-safety>(7)"
msgstr ""
"B<err>(3), B<errno>(3), B<error>(3), B<perror>(3), B<strsignal>(3), "
"B<locale>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<strerror>()  function returns a pointer to a string that describes the "
"error code passed in the argument I<errnum>, possibly using the "
"B<LC_MESSAGES> part of the current locale to select the appropriate "
"language.  (For example, if I<errnum> is B<EINVAL>, the returned description "
"will be \"Invalid argument\".)  This string must not be modified by the "
"application, but may be modified by a subsequent call to B<strerror>()  or "
"B<strerror_l>().  No other library function, including B<perror>(3), will "
"modify this string."
msgstr ""
"Функция B<strerror>() возвращает указатель на строку, которая описывает код "
"ошибки, переданный в аргументе I<errnum>, возможно, с помощью части "
"B<LC_MESSAGES> текущей локали для выбора соответствующего языка (например, "
"если I<errnum> равно B<EINVAL>, то возвращается описание «Invalid "
"argument»). Эту строку нельзя изменять в приложении, она может измениться "
"при последующих вызовах B<strerror>() или B<strerror_l>(). Другие "
"библиотечные функции, включая B<perror>(3), не изменяют эту строку."

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<strerrorname_np>()  function returns a pointer to a string containing "
"the name of the error code passed in the argument I<errnum>.  For example, "
"given B<EPERM> as an argument, this function returns a pointer to the string "
"\"EPERM\"."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<strerror_r>()  function is similar to B<strerror>(), but is thread "
"safe.  This function is available in two versions: an XSI-compliant version "
"specified in POSIX.1-2001 (available since glibc 2.3.4, but not POSIX-"
"compliant until glibc 2.13), and a GNU-specific version (available since "
"glibc 2.0).  The XSI-compliant version is provided with the feature test "
"macros settings shown in the SYNOPSIS; otherwise the GNU-specific version is "
"provided.  If no feature test macros are explicitly defined, then (since "
"glibc 2.4)  B<_POSIX_C_SOURCE> is defined by default with the value 200112L, "
"so that the XSI-compliant version of B<strerror_r>()  is provided by default."
msgstr ""
"Функция B<strerror_r>() подобна B<strerror>(), но её можно безопасно "
"использовать в нитях. Она доступна в двух версиях: версия, совместимая с "
"XSI, определена в POSIX.1-2001 (доступна в glibc начиная с 2.3.4, но не "
"совместима с POSIX до glibc 2.13) и версия, совместимая с GNU (доступна, "
"начиная с glibc 2.0). Версия, совместимая с XSI, предоставляется при наличии "
"набора макросов тестирования свойств, показанных в ОБЗОРЕ; в противном "
"случае предоставляется версия GNU. Если макросы тестирования свойств не "
"указаны явным образом, то (начиная с glibc 2.4) по умолчанию определяется "
"B<_POSIX_C_SOURCE> со значением 200112L, то есть версия B<strerror_r>() XSI "
"предоставляется по умолчанию."

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "The GNU-specific B<strerror_r>()  returns a pointer to a string "
#| "containing the error message.  This may be either a pointer to a string "
#| "that the function stores in I<buf>, or a pointer to some (immutable) "
#| "static string (in which case I<buf> is unused).  If the function stores a "
#| "string in I<buf>, then at most I<buflen> bytes are stored (the string may "
#| "be truncated if I<buflen> is too small and I<errnum> is unknown).  The "
#| "string always includes a terminating null byte (\\(aq\\e0\\(aq)."
msgid ""
"The GNU-specific B<strerror_r>()  returns a pointer to a string containing "
"the error message.  This may be either a pointer to a string that the "
"function stores in I<buf>, or a pointer to some (immutable) static string "
"(in which case I<buf> is unused).  If the function stores a string in "
"I<buf>, then at most I<buflen> bytes are stored (the string may be truncated "
"if I<buflen> is too small and I<errnum> is unknown).  The string always "
"includes a terminating null byte (\\[aq]\\e0\\[aq])."
msgstr ""
"Специальная GNU-версия B<strerror_r>() возвращает строку, содержащую "
"сообщение об ошибке. Это может быть или указатель на строку, которую функция "
"записывает в I<buf>, или указатель на некую (неизменную) статическую строку "
"(в этом случае I<buf> не используется). Если функция сохраняет строку в "
"I<buf>, то сохраняется не более I<buflen> байт (строка может быть обрезана, "
"если значение I<buflen> слишком мало и I<errnum> неизвестно). В строке "
"всегда содержится конечный байт null (\\(aq\\e0\\(aq)."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
msgid "The B<strerror_l>()  function first appeared in glibc 2.6."
msgstr "Функция B<strerror_l>() впервые появилась в glibc 2.6."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "The B<strerror_l>()  function first appeared in glibc 2.6."
msgid ""
"The B<strerrorname_np>()  and B<strerrordesc_np>()  functions first appeared "
"in glibc 2.32."
msgstr "Функция B<strerror_l>() впервые появилась в glibc 2.6."

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "MT-Unsafe race:strerror"
msgstr "MT-Unsafe race:strerror"

#.  FIXME . for later review when Issue 8 is one day released...
#.  A future POSIX.1 may remove strerror_r()
#.  http://austingroupbugs.net/tag_view_page.php?tag_id=8
#.  http://austingroupbugs.net/view.php?id=508
#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<strerror>()  is specified by POSIX.1-2001, POSIX.1-2008, C89, and C99.  "
#| "B<strerror_r>()  is specified by POSIX.1-2001 and POSIX.1-2008."
msgid ""
"B<strerror>()  is specified by POSIX.1-2001, POSIX.1-2008, and C99.  "
"B<strerror_r>()  is specified by POSIX.1-2001 and POSIX.1-2008."
msgstr ""
"Функция B<strerror>() определена в POSIX.1-2001, POSIX.1-2008, C89 и C99. "
"Функция B<strerror_r>() определена в POSIX.1-2001 и POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
msgid "B<strerror_l>()  is specified in POSIX.1-2008."
msgstr "Функция B<strerror_l>() описана в POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "The GNU-specific B<strerror_r>()  function is a nonstandard extension."
msgid ""
"The GNU-specific functions B<strerror_r>(), B<strerrorname_np>(), and "
"B<strerrordesc_np>()  are nonstandard extensions."
msgstr "Функция GNU B<strerror_r>() является нестандартизованным расширением."

#. type: Plain text
#: debian-bookworm
msgid ""
"The GNU C Library uses a buffer of 1024 characters for B<strerror>().  This "
"buffer size therefore should be sufficient to avoid an B<ERANGE> error when "
"calling B<strerror_r>()."
msgstr ""
"В библиотеке GNU C для B<strerror>() используется буфер в 1024 символов. "
"Размер буфера должен быть достаточным для исключения возникновения ошибки "
"B<ERANGE> при вызове B<strerror_r>()."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<err>(3), B<errno>(3), B<error>(3), B<perror>(3), B<strsignal>(3), "
"B<locale>(7)"
msgstr ""
"B<err>(3), B<errno>(3), B<error>(3), B<perror>(3), B<strsignal>(3), "
"B<locale>(7)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
