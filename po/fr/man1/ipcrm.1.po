# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2021.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"POT-Creation-Date: 2024-12-22 07:27+0100\n"
"PO-Revision-Date: 2022-08-20 17:23+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "IPCRM"
msgstr "IPCRM"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm
msgid "ipcrm - remove certain IPC resources"
msgstr "ipcrm - Supprimer certaines ressources IPC"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm
msgid "B<ipcrm> [options]"
msgstr "B<ipcrm> [I<options>]"

#. type: Plain text
#: debian-bookworm
msgid "B<ipcrm> [B<shm>|B<msg>|B<sem>] I<ID> ..."
msgstr "B<ipcrm> [B<shm>|B<msg>|B<sem>] I<ID> ..."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<ipcrm> removes System V inter-process communication (IPC) objects and "
"associated data structures from the system. In order to delete such objects, "
"you must be superuser, or the creator or owner of the object."
msgstr ""
"B<ipcrm> supprime des objets de communication entre processus (IPC) System V "
"et les structures de données associées définies sur le système. Afin de les "
"détruire, vous devez être le superutilisateur, le créateur ou le "
"propriétaire des objets."

#. type: Plain text
#: debian-bookworm
msgid ""
"System V IPC objects are of three types: shared memory, message queues, and "
"semaphores. Deletion of a message queue or semaphore object is immediate "
"(regardless of whether any process still holds an IPC identifier for the "
"object). A shared memory object is only removed after all currently attached "
"processes have detached (B<shmdt>(2)) the object from their virtual address "
"space."
msgstr ""
"Les objets IPC System V sont de trois types\\ : mémoire partagée, file de "
"messages et sémaphores. La suppression d'une file de messages ou d'un "
"ensemble de sémaphores est immédiate (même s'il y a des processus qui "
"disposent d'un identificateur sur l'objet). Un segment de mémoire partagée "
"n'est supprimé qu'à la condition que tous les processus aient détaché "
"(B<shmdt>(2)) l'objet de leur espace d'adressage virtuel."

#. type: Plain text
#: debian-bookworm
msgid ""
"Two syntax styles are supported. The old Linux historical syntax specifies a "
"three-letter keyword indicating which class of object is to be deleted, "
"followed by one or more IPC identifiers for objects of this type."
msgstr ""
"Deux syntaxes sont prises en charge. La syntaxe historique sous Linux "
"utilise un mot clef de trois lettres indiquant la classe de l'objet à "
"supprimer, suivi d'un ou de plusieurs identificateurs d'objets IPC de ce "
"type."

#. type: Plain text
#: debian-bookworm
msgid ""
"The SUS-compliant syntax allows the specification of zero or more objects of "
"all three types in a single command line, with objects specified either by "
"key or by identifier (see below). Both keys and identifiers may be specified "
"in decimal, hexadecimal (specified with an initial \\(aq0x\\(aq or "
"\\(aq0X\\(aq), or octal (specified with an initial \\(aq0\\(aq)."
msgstr ""
"La syntaxe compatible SUS permet l'utilisation d'aucun ou de plusieurs "
"objets des trois types en une seule ligne de commande, objets désignés par "
"leur clef ou leur identificateur (voir ci-dessous). Les clefs et les "
"identificateurs peuvent être indiqués en décimal, hexadécimal (commençant "
"par «\\ 0x\\ » ou «\\ 0X\\ »), ou octal (commençant par un «\\ 0\\ »)."

#. type: Plain text
#: debian-bookworm
msgid ""
"The details of the removes are described in B<shmctl>(2), B<msgctl>(2), and "
"B<semctl>(2). The identifiers and keys can be found by using B<ipcs>(1)."
msgstr ""
"Des précisions sur les suppressions sont décrites dans B<shmctl>(2), "
"B<msgctl>(2) et B<semctl>(2). Les identificateurs et les clefs peuvent être "
"trouvés avec B<ipcs>(1)."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--all> [B<shm>] [B<msg>] [B<sem>]"
msgstr "B<-a>, B<--all> [B<shm>] [B<msg>] [B<sem>]"

#. type: Plain text
#: debian-bookworm
msgid ""
"Remove all resources. When an option argument is provided, the removal is "
"performed only for the specified resource types."
msgstr ""
"Supprimer toutes les ressources. Quand un argument d’option est fourni, la "
"suppression ne concerne que les types de ressource indiqués."

#. type: Plain text
#: debian-bookworm
msgid ""
"I<Warning!> Do not use B<-a> if you are unsure how the software using the "
"resources might react to missing objects. Some programs create these "
"resources at startup and may not have any code to deal with an unexpected "
"disappearance."
msgstr ""
"B<Attention>, n’utilisez pas B<-a> en cas de doute sur la façon dont le "
"logiciel utilisant les ressources réagirait aux objets manquants. Certains "
"programmes créent ces ressources au démarrage et pourraient ne pas avoir de "
"code pour gérer une disparition inattendue."

#. type: Plain text
#: debian-bookworm
msgid "B<-M>, B<--shmem-key> I<shmkey>"
msgstr "B<-M>, B<--shmem-key> I<clef_shm>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Remove the shared memory segment created with I<shmkey> after the last "
"detach is performed."
msgstr ""
"Supprimer le segment de mémoire partagée créé avec I<clef_shm> après son "
"dernier détachement."

#. type: Plain text
#: debian-bookworm
msgid "B<-m>, B<--shmem-id> I<shmid>"
msgstr "B<-m>, B<--shmem-id> I<id_shm>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Remove the shared memory segment identified by I<shmid> after the last "
"detach is performed."
msgstr ""
"Supprimer le segment de mémoire partagée identifié par I<id_shm> après son "
"dernier détachement."

#. type: Plain text
#: debian-bookworm
msgid "B<-Q>, B<--queue-key> I<msgkey>"
msgstr "B<-Q>, B<--queue-key> I<clef_msg>"

#. type: Plain text
#: debian-bookworm
msgid "Remove the message queue created with I<msgkey>."
msgstr "Supprimer la file de messages créée avec I<clef_msg>."

#. type: Plain text
#: debian-bookworm
msgid "B<-q>, B<--queue-id> I<msgid>"
msgstr "B<-q>, B<--queue-id> I<id_msg>"

#. type: Plain text
#: debian-bookworm
msgid "Remove the message queue identified by I<msgid>."
msgstr "Supprimer la file de messages identifiée par I<id_msg>."

#. type: Plain text
#: debian-bookworm
msgid "B<-S>, B<--semaphore-key> I<semkey>"
msgstr "B<-S>, B<--semaphore-key> I<clef_sem>"

#. type: Plain text
#: debian-bookworm
msgid "Remove the semaphore created with I<semkey>."
msgstr "Supprimer le sémaphore créé avec I<clef_sem>."

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--semaphore-id> I<semid>"
msgstr "B<-s>, B<--semaphore-id> I<id_sem>"

#. type: Plain text
#: debian-bookworm
msgid "Remove the semaphore identified by I<semid>."
msgstr "Supprimer le sémaphore identifié par I<id_sem>."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Afficher la version puis quitter."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: debian-bookworm
msgid ""
"In its first Linux implementation, B<ipcrm> used the deprecated syntax shown "
"in the second line of the B<SYNOPSIS>. Functionality present in other *nix "
"implementations of B<ipcrm> has since been added, namely the ability to "
"delete resources by key (not just identifier), and to respect the same "
"command-line syntax. For backward compatibility the previous syntax is still "
"supported."
msgstr ""
"Dans sa première implémentation sous Linux, B<ipcrm> utilisait la syntaxe, "
"déconseillée présentée dans la deuxième ligne du B<SYNOPSIS>. Les "
"fonctionnalités présentes dans d'autres implémentations *nix d’B<ipcrm> ont "
"été ajoutées depuis, notamment la possibilité de supprimer une ressource par "
"sa clef (et pas uniquement par son identificateur) tout en respectant la "
"même syntaxe en ligne de commande. Pour assurer la rétrocompatibilité, la "
"syntaxe précédente est toujours acceptée."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<ipcmk>(1), B<ipcs>(1), B<msgctl>(2), B<msgget>(2), B<semctl>(2), "
"B<semget>(2), B<shmctl>(2), B<shmdt>(2), B<shmget>(2), B<ftok>(3), "
"B<sysvipc>(7)"
msgstr ""
"B<ipcmk>(1), B<ipcs>(1), B<msgctl>(2), B<msgget>(2), B<semctl>(2), "
"B<semget>(2), B<shmctl>(2), B<shmdt>(2), B<shmget>(2), B<ftok>(3), "
"B<sysvipc>(7)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pour envoyer un rapport de bogue, utilisez le système de gestion des "
"problèmes à l'adresse"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<ipcrm> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"La commande B<ipcrm> fait partie du paquet util-linux téléchargeable sur"
