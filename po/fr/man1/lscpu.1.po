# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2022
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"POT-Creation-Date: 2024-12-22 07:29+0100\n"
"PO-Revision-Date: 2022-08-27 12:29+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LSCPU"
msgstr "LSCPU"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-08-04"
msgstr "4 août 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm
msgid "lscpu - display information about the CPU architecture"
msgstr "lscpu - Afficher des informations sur l'architecture du processeur"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm
msgid "B<lscpu> [options]"
msgstr "B<lscpu> [I<options>]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<lscpu> gathers CPU architecture information from I<sysfs>, I</proc/"
"cpuinfo> and any applicable architecture-specific libraries (e.g. B<librtas> "
"on Powerpc). The command output can be optimized for parsing or for easy "
"readability by humans. The information includes, for example, the number of "
"CPUs, threads, cores, sockets, and Non-Uniform Memory Access (NUMA) nodes. "
"There is also information about the CPU caches and cache sharing, family, "
"model, bogoMIPS, byte order, and stepping."
msgstr ""
"B<lscpu> collecte des renseignements sur l'architecture du processeur à "
"partir de I<sysfs>, I</proc/cpuinfo> et de n'importe quelle bibliothèque "
"spécifique à une architecture (comme B<librtas> sur Powerpc). La sortie de "
"la commande peut être optimisée pour l’analyse ou pour faciliter la lecture. "
"Par exemple, le nombre de processeurs, de processus légers, de cœurs, de "
"sockets et de nœuds NUMA font partie des renseignements. Des renseignements "
"sont aussi fournis sur les caches et les partages de cache, la famille, le "
"modèle, le BogoMips, le boutisme et la révision."

#. type: Plain text
#: debian-bookworm
msgid ""
"The default output formatting on terminal is subject to change and maybe "
"optimized for better readability. The output for non-terminals (e.g., pipes) "
"is never affected by this optimization and it is always in \"Field: "
"data\\(rsn\" format. Use for example \"B<lscpu | less>\" to see the default "
"output without optimizations."
msgstr ""
"Le format d'affichage par défaut sur un terminal est sujet à des "
"modifications et peut être optimisé pour une meilleure lisibilité. La sortie "
"hors d’un terminal (par exemple, les redirections) n'est jamais touchée par "
"cette optimisation et reste au format « Field: data\\(rsn ». Utiliser par "
"exemple « B<lscpu | less> » pour voir la sortie par défaut sans optimisation."

#. type: Plain text
#: debian-bookworm
msgid ""
"In virtualized environments, the CPU architecture information displayed "
"reflects the configuration of the guest operating system which is typically "
"different from the physical (host) system. On architectures that support "
"retrieving physical topology information, B<lscpu> also displays the number "
"of physical sockets, chips, cores in the host system."
msgstr ""
"Dans les environnements virtualisés, les informations affichées sur "
"l'architecture du processeur reflètent la configuration du système "
"d'exploitation invité qui diffère en général du système physique (l'hôte). "
"Sur les architectures qui récupèrent les informations de topologie physique, "
"B<lscpu> affiche aussi le nombre de sockets, de puces et de cœurs physiques "
"du système hôte."

#. type: Plain text
#: debian-bookworm
msgid ""
"Options that result in an output table have a I<list> argument. Use this "
"argument to customize the command output. Specify a comma-separated list of "
"column labels to limit the output table to only the specified columns, "
"arranged in the specified order. See B<COLUMNS> for a list of valid column "
"labels. The column labels are not case sensitive."
msgstr ""
"Les options ayant pour résultat un tableau en sortie ont un argument "
"I<liste>. Utilisez cet argument pour personnaliser la sortie de la commande. "
"Indiquez une liste d’étiquettes de colonne séparées par des virgules pour "
"limiter le tableau en sortie à ces colonnes dans l’ordre indiqué. Consultez "
"B<COLONNES> pour une liste des étiquettes de colonne possibles. Les "
"étiquettes de colonne ne sont pas sensibles à la casse."

#. type: Plain text
#: debian-bookworm
msgid ""
"Not all columns are supported on all architectures. If an unsupported column "
"is specified, B<lscpu> prints the column but does not provide any data for "
"it."
msgstr ""
"Toutes les colonnes ne sont pas prises en charge sur toutes les "
"architectures. Si une colonne non prise en charge est indiquée, B<lscpu> "
"affiche la colonne, mais ne fournit pas de données pour cette colonne."

#. type: Plain text
#: debian-bookworm
msgid ""
"The cache sizes are reported as summary from all CPUs. The versions before "
"v2.34 reported per-core sizes, but this output was confusing due to "
"complicated CPUs topology and the way how caches are shared between CPUs. "
"For more details about caches see B<--cache>. Since version v2.37 B<lscpu> "
"follows cache IDs as provided by Linux kernel and it does not always start "
"from zero."
msgstr ""
"Les tailles du cache sont indiquées sous forme d'un résumé de tous les "
"processeurs. Les versions inférieures à la version 2.34 affichaient des "
"tailles par cœur, mais cette sortie était perturbante du fait de la "
"topologie compliquée des processeurs et de la manière dont les caches sont "
"partagés entre eux. Pour plus de détails sur les caches, voir B<--cache>. "
"Depuis la version  v2.37, B<lspcu> suit les identifiants comme le noyau "
"Linux les fournit ils ne commencent pas toujours à zéro."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Include lines for online and offline CPUs in the output (default for B<-e>). "
"This option may only be specified together with option B<-e> or B<-p>."
msgstr ""
"Inclure les lignes pour les processeurs en ligne et hors ligne dans la "
"sortie (par défaut pour B<-e>). Cette option ne peut être indiquée qu’avec "
"les options B<-e> ou B<-p>."

#. type: Plain text
#: debian-bookworm
msgid "B<-B>, B<--bytes>"
msgstr "B<-B>, B<--bytes>"

#. type: Plain text
#: debian-bookworm
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr ""
"Afficher la taille (colonne SIZE) en octets plutôt qu'en format lisible."

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""
"Par défaut l'unité dans laquelle les tailles sont exprimées est l'octet et "
"les préfixes d'unité sont des puissances de 2^10 (1024). Les abréviations "
"des symboles sont tronqués pour obtenir une meilleur lisibilité, en "
"n'affichant que la première lettre, par exemple : « 1 Kio » et « 1 Mio » "
"sont affichés « 1 K » et « 1 M » en omettant délibérément l'indication "
"« io » qui fait partie de ces abréviations."

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--online>"
msgstr "B<-b>, B<--online>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the output to online CPUs (default for B<-p>). This option may only be "
"specified together with option B<-e> or B<-p>."
msgstr ""
"Limiter la sortie aux processeurs en ligne (par défaut pour B<-p>). Cette "
"option ne peut être indiquée qu’avec les options B<-e> ou B<-p>."

#. type: Plain text
#: debian-bookworm
msgid "B<-C>, B<--caches>[=I<list>]"
msgstr "B<-C>, B<--caches>[=I<liste>]"

#. type: Plain text
#: debian-bookworm
msgid ""
"Display details about CPU caches. For details about available information "
"see B<--help> output."
msgstr ""
"Afficher les détails des caches du processeur. Pour plus de détails sur les "
"informations disponibles, voir l'affichage de B<--help>."

#. type: Plain text
#: debian-bookworm
msgid ""
"If the I<list> argument is omitted, all columns for which data is available "
"are included in the command output."
msgstr ""
"Si l’argument I<liste> est omis, toutes les colonnes ayant des données "
"disponibles sont incluses dans la sortie de la commande."

#. type: Plain text
#: debian-bookworm
msgid ""
"When specifying the I<list> argument, the string of option, equal sign (=), "
"and I<list> must not contain any blanks or other whitespace. Examples: B<-"
"C=NAME,ONE-SIZE> or B<--caches=NAME,ONE-SIZE>."
msgstr ""
"Quand l’argument I<liste> est indiqué, la chaîne option, signe égal (=) et "
"I<liste> ne doit pas contenir d’espace. Par exemple : B<-C=NOM,UNE-TAILLE> "
"ou B<--caches=NOM,UNE-TAILLE>."

#. type: Plain text
#: debian-bookworm
msgid ""
"The default list of columns may be extended if list is specified in the "
"format +list (e.g., B<lscpu -C=+ALLOC-POLICY>)."
msgstr ""
"La liste de colonnes par défaut peut-être étendue si une liste est spécifiée "
"au format « +liste » (par exemple, B<lscpu -C=+ALLOC-POLICY>)."

#. type: Plain text
#: debian-bookworm
msgid "B<-c>, B<--offline>"
msgstr "B<-c>, B<--offline>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the output to offline CPUs. This option may only be specified together "
"with option B<-e> or B<-p>."
msgstr ""
"Limiter la sortie aux processeurs hors ligne. Cette option ne peut être "
"indiquée qu’avec les options B<-e> ou B<-p>."

#. type: Plain text
#: debian-bookworm
msgid "B<-e>, B<--extended>[=I<list>]"
msgstr "B<-e>, B<--extended>[B<=>I<liste>]"

#. type: Plain text
#: debian-bookworm
msgid "Display the CPU information in human-readable format."
msgstr "Afficher les renseignements sur le processeur au format lisible."

#. type: Plain text
#: debian-bookworm
msgid ""
"If the I<list> argument is omitted, the default columns are included in the "
"command output.  The default output is subject to change."
msgstr ""
"Si l’argument I<liste> est omis, toutes les colonnes par défaut sont "
"incluses dans la sortie de la commande. La sortie par défaut est sujette à "
"des modifications."

#. type: Plain text
#: debian-bookworm
msgid ""
"When specifying the I<list> argument, the string of option, equal sign (=), "
"and I<list> must not contain any blanks or other whitespace. Examples: "
"\\(aqB<-e=cpu,node>\\(aq or \\(aqB<--extended=cpu,node>\\(aq."
msgstr ""
"Quand l’argument I<liste> est indiqué, la chaîne option, signe égal (=) et "
"I<liste> ne doit pas contenir d’espace. Par exemple : « B<-e=cpu,node> » ou "
"« B<--extended=cpu,node> »."

#. type: Plain text
#: debian-bookworm
msgid ""
"The default list of columns may be extended if list is specified in the "
"format +list (e.g., lscpu -e=+MHZ)."
msgstr ""
"La liste de colonnes par défaut peut-être étendue si une liste est spécifiée "
"au format « +liste » (par exemple, lscpu -e=+MHZ)."

#. type: Plain text
#: debian-bookworm
msgid "B<-J>, B<--json>"
msgstr "B<-J>, B<--json>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Use JSON output format for the default summary or extended output (see B<--"
"extended>)."
msgstr ""
"Utiliser le format d'affichage JSON pour le résumé par défaut ou l'affichage "
"étendu (voir B<--extended>)."

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--parse>[=I<list>]"
msgstr "B<-p>, B<--parse>[B<=>I<liste>]"

#. type: Plain text
#: debian-bookworm
msgid "Optimize the command output for easy parsing."
msgstr "Optimiser la sortie de la commande pour faciliter l’analyse."

#. type: Plain text
#: debian-bookworm
msgid ""
"If the I<list> argument is omitted, the command output is compatible with "
"earlier versions of B<lscpu>. In this compatible format, two commas are used "
"to separate CPU cache columns. If no CPU caches are identified the cache "
"column is omitted. If the I<list> argument is used, cache columns are "
"separated with a colon (:)."
msgstr ""
"En absence d'argument I<liste>, la sortie de la commande est compatible avec "
"les versions précédentes de B<lscpu>. Dans ce format compatible, deux "
"virgules séparent les colonnes de cache de processeur. Si aucun cache de "
"processeur n'est identifié, la colonne est omise. Si l'argument I<liste> est "
"utilisé, les colonnes de cache sont séparées par des deux-points (:)."

#. type: Plain text
#: debian-bookworm
msgid ""
"When specifying the I<list> argument, the string of option, equal sign (=), "
"and I<list> must not contain any blanks or other whitespace. Examples: "
"\\(aqB<-p=cpu,node>\\(aq or \\(aqB<--parse=cpu,node>\\(aq."
msgstr ""
"Quand l’argument I<liste> est indiqué, la chaîne option, signe égal (=) et "
"I<liste> ne doit pas contenir d’espace. Par exemple : « B<-p=cpu,node> » ou "
"« B<--parse=cpu,node> »."

#. type: Plain text
#: debian-bookworm
msgid ""
"The default list of columns may be extended if list is specified in the "
"format +list (e.g., lscpu -p=+MHZ)."
msgstr ""
"La liste de colonnes par défaut peut-être étendue si une liste est spécifiée "
"au format « +liste » (par exemple, lscpu -p=+MHZ)."

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--sysroot> I<directory>"
msgstr "B<-s>, B<--sysroot> I<répertoire>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Gather CPU data for a Linux instance other than the instance from which the "
"B<lscpu> command is issued. The specified I<directory> is the system root of "
"the Linux instance to be inspected."
msgstr ""
"Collecter les données de processeur pour une autre instance Linux que celle "
"utilisée pour la commande B<lscpu>. Le I<répertoire> indiqué est la racine "
"du système de l’instance Linux à inspecter."

#. type: Plain text
#: debian-bookworm
msgid "B<-x>, B<--hex>"
msgstr "B<-x>, B<--hex>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Use hexadecimal masks for CPU sets (for example \"ff\"). The default is to "
"print the sets in list format (for example 0,1). Note that before version "
"2.30 the mask has been printed with 0x prefix."
msgstr ""
"Utiliser des masques hexadécimaux pour les ensembles de processeurs (par "
"exemple « ff »). Par défaut, l'affichage est au format liste (par exemple "
"0,1). Remarquez qu'avant la version 2.30, le masque s'affichait avec le "
"préfixe 0x."

#. type: Plain text
#: debian-bookworm
msgid "B<-y>, B<--physical>"
msgstr "B<-y>, B<--physical>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Display physical IDs for all columns with topology elements (core, socket, "
"etc.). Other than logical IDs, which are assigned by B<lscpu>, physical IDs "
"are platform-specific values that are provided by the kernel. Physical IDs "
"are not necessarily unique and they might not be arranged sequentially. If "
"the kernel could not retrieve a physical ID for an element B<lscpu> prints "
"the dash (-) character."
msgstr ""
"Afficher les identifiants physiques de toutes les colonnes ayant des "
"éléments de topologie (cœur, sockets, etc.). À part les identifiants "
"logiques, affectés par B<lscpu>, les valeurs des identifiants physiques sont "
"spécifiques aux valeurs de chaque plateforme fournies par le noyau. Les "
"identifiants physiques ne sont pas nécessairement uniques et ils pourraient "
"ne pas être organisés séquentiellement. Si le noyau n'a pas pu récupérer "
"l'identifiant physique d'un élément, B<lscpu> affiche le caractère tiret (-)."

#. type: Plain text
#: debian-bookworm
msgid "The CPU logical numbers are not affected by this option."
msgstr ""
"Les numéros logiques d’un processeur ne sont pas touchés par cette option."

#. type: Plain text
#: debian-bookworm
msgid "B<--output-all>"
msgstr "B<--output-all>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Output all available columns. This option must be combined with either B<--"
"extended>, B<--parse> or B<--caches>."
msgstr ""
"Afficher les colonnes disponibles. Cette option peut être associée soit à "
"B<--extended>, soit à B<--parse>, soit à B<--caches>."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: debian-bookworm
msgid ""
"The basic overview of CPU family, model, etc. is always based on the first "
"CPU only."
msgstr ""
"La vue d'ensemble de base de la famille, du modèle, etc., de processeur "
"n’est toujours relative qu’au premier processeur."

#. type: Plain text
#: debian-bookworm
msgid "Sometimes in Xen Dom0 the kernel reports wrong data."
msgstr "Quelques fois sous Xen Dom0, les résultats du noyau sont incorrects."

#. type: Plain text
#: debian-bookworm
msgid "On virtual hardware the number of cores per socket, etc. can be wrong."
msgstr ""
"Sur le matériel virtuel, le nombre de cœurs par socket, etc., peut être faux."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm
msgid "B<chcpu>(8)"
msgstr "B<chcpu>(8)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pour envoyer un rapport de bogue, utilisez le système de gestion des "
"problèmes à l'adresse"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<lscpu> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"La commande B<lscpu> fait partie du paquet util-linux qui peut être "
"téléchargé de"
