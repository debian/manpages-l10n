# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-08-02 17:18+0200\n"
"PO-Revision-Date: 2023-11-14 16:37+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-EMU"
msgstr "GRUB-EMU"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2024"
msgstr "Février 2024"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.12-1~bpo12+1"
msgstr "GRUB 2.12-1~bpo12+1"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "grub-emu - GRUB emulator"
msgstr "grub-emu – Émulateur de GRUB"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub-emu> [I<\\,OPTION\\/>...]"
msgstr "B<grub-emu> [I<\\,OPTION\\/>...]"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "GRUB emulator."
msgstr "Émulateur de GRUB."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,RÉP\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use GRUB files in the directory DIR [default=/boot/grub]"
msgstr ""
"Utiliser les fichiers GRUB dans le répertoire I<RÉP> [/boot/grub par défaut]."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-H>, B<--hold>[=I<\\,SECS\\/>]"
msgstr "B<-H>, B<--hold>[=I<\\,SECONDES\\/>]"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "wait until a debugger will attach"
msgstr "Attendre qu'un débogueur soit attaché."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--memdisk>=I<\\,FILE\\/>"
msgstr "B<--memdisk>=I<\\,FICHIER\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as memdisk"
msgstr "Utiliser FICHIER comme disque RAM."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr "B<-m>, B<--device-map>=I<\\,FICHIER\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as the device map [default=/boot/grub/device.map]"
msgstr ""
"Utiliser FICHIER comme mappage de périphérique [/boot/grub/device.map par "
"défaut]."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--root>=I<\\,DEVICE_NAME\\/>"
msgstr "B<-r>, B<--root>=I<\\,NOM_PÉRIPHÉRIQUE\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Set root device."
msgstr "Établir un périphérique racine."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "Afficher des messages détaillés."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-X>, B<--kexec>"
msgstr "B<-X>, B<--kexec>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"use kexec to boot Linux kernels via systemctl (pass twice to enable "
"dangerous fallback to non-systemctl)."
msgstr ""
"utiliser kexec pour démarrer les noyaux Linux au moyen de systemctl (passer "
"deux fois pour activer un recours dangereux n'utilisant pas systemctl)."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "give this help list"
msgstr "Afficher l’aide-mémoire."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "Afficher un court message pour l’utilisation."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print program version"
msgstr "Afficher la version du programme."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Les paramètres obligatoires ou facultatifs pour les options de forme longue "
"le sont aussi pour les options correspondantes de forme courte."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Signaler toute erreur à E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If you are trying to install GRUB, then you should use B<grub-install>(8)  "
"rather than this program."
msgstr ""
"Si vous essayer d'installer GRUB, vous devriez utiliser B<grub-install>(8) "
"plutôt que ce programme."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-emu> is maintained as a Texinfo manual.  "
"If the B<info> and B<grub-emu> programs are properly installed at your site, "
"the command"
msgstr ""
"La documentation complète de B<grub-emu> est disponible dans un manuel "
"Texinfo. Si les programmes B<info> et B<grub-emu> sont correctement "
"installés, la commande"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<info grub-emu>"
msgstr "B<info grub-emu>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "devrait vous donner accès au manuel complet."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "July 2024"
msgstr "Juillet 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-5"
msgstr "GRUB 2.12-5"
