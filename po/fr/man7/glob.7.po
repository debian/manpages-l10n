# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2024-12-22 07:25+0100\n"
"PO-Revision-Date: 2023-05-05 09:39+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: vim\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "glob"
msgstr "glob"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 juin 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "glob - globbing pathnames"
msgstr "glob - Développement des noms de fichiers"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Long ago, in UNIX\\ V6, there was a program I</etc/glob> that would expand "
"wildcard patterns.  Soon afterward this became a shell built-in."
msgstr ""
"Il y a bien longtemps, dans UNIX V6, existait un programme nommé I</etc/"
"glob> qui permettait de développer les motifs génériques dans les noms de "
"fichiers. Ce programme devint bientôt une routine interne du shell."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These days there is also a library routine B<glob>(3)  that will perform "
"this function for a user program."
msgstr ""
"De nos jours, on trouve également une routine de bibliothèque nommée "
"B<glob>(3) qui effectue le même travail en étant invoquée par un programme "
"utilisateur."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The rules are as follows (POSIX.2, 3.13)."
msgstr "Les règles de développement sont les suivantes (POSIX.2, 3.13)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Wildcard matching"
msgstr "Motifs génériques"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A string is a wildcard pattern if it contains one of the characters \\[aq]?"
"\\[aq], \\[aq]*\\[aq], or \\[aq][\\[aq].  Globbing is the operation that "
"expands a wildcard pattern into the list of pathnames matching the pattern.  "
"Matching is defined by:"
msgstr ""
"Une chaîne est un motif générique si elle contient un ou plusieurs "
"caractères parmi «\\ ?\\ », «\\ *\\ » et «\\ [\\ ». Le développement "
"(I<globbing>) est l'opération qui transforme un motif générique en une liste "
"de noms de fichiers correspondant à ce motif. La correspondance est définie "
"ainsi\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A \\[aq]?\\[aq] (not between brackets) matches any single character."
msgstr ""
"Un «\\ ?\\ » (non inclus dans des crochets) correspond à n'importe quel "
"caractère."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A \\[aq]*\\[aq] (not between brackets) matches any string, including the "
"empty string."
msgstr ""
"Un «\\ *\\ » (non inclus dans des crochets) correspond à n'importe quelle "
"chaîne, y compris la chaîne vide."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<Character classes>"
msgstr "B<Classes de caractères>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An expression \"I<[...]>\" where the first character after the leading \\[aq]"
"[\\[aq] is not an \\[aq]!\\[aq] matches a single character, namely any of "
"the characters enclosed by the brackets.  The string enclosed by the "
"brackets cannot be empty; therefore \\[aq]]\\[aq] can be allowed between the "
"brackets, provided that it is the first character.  (Thus, \"I<[][!]>\" "
"matches the three characters \\[aq][\\[aq], \\[aq]]\\[aq], and \\[aq]!"
"\\[aq].)"
msgstr ""
"Une expression du type «\\ I<[...]>\\ », dans laquelle le premier caractère "
"après le «\\ [\\ » n'est pas un «\\ !\\ » est mise en correspondance avec un "
"seul des caractères contenus entre les crochets. L'ensemble des caractères "
"cités ne peut pas être vide, ainsi, le crochet fermant «\\ ]\\ » peut être "
"présent dans l'ensemble, à la condition qu'il soit en première place. Par "
"conséquent, la chaîne «\\ I<[][!]>\\ » peut être mise en correspondance avec "
"l'un des trois caractères «\\ [\\ », «\\ ]\\ » et «\\ !\\ »."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<Ranges>"
msgstr "B<Intervalles>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There is one special convention: two characters separated by \\[aq]-\\[aq] "
"denote a range.  (Thus, \"I<[A-Fa-f0-9]>\" is equivalent to "
"\"I<[ABCDEFabcdef0123456789]>\".)  One may include \\[aq]-\\[aq] in its "
"literal meaning by making it the first or last character between the "
"brackets.  (Thus, \"I<[]-]>\" matches just the two characters \\[aq]]\\[aq] "
"and \\[aq]-\\[aq], and \"I<[--0]>\" matches the three characters \\[aq]-"
"\\[aq], \\[aq].\\[aq], and \\[aq]0\\[aq], since \\[aq]/\\[aq] cannot be "
"matched.)"
msgstr ""
"Il existe une convention particulière, suivant laquelle deux caractères "
"séparés par un tiret «\\ -\\ » indiquent un intervalle. Ainsi, «\\ I<[A-Fa-"
"f0-9]>\\ » équivaut à «\\ I<[ABCDEFabcdef0123456789]>\\ ». Pour inclure un "
"tiret au sens littéral, il suffit de l'utiliser en premier ou en dernier "
"entre les crochets. Ainsi, «\\ I<[]-]>\\ » correspond uniquement aux "
"caractères «\\ ]\\ » et «\\ -\\ », et «\\ I<[--0]>\\ » correspond aux trois "
"caractères «\\ -\\ », «\\ .\\ » et «\\ 0\\ », puisqu'il ne peut y avoir de "
"correspondance avec « / »."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<Complementation>"
msgstr "B<Négation>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An expression \"I<[!...]>\" matches a single character, namely any character "
"that is not matched by the expression obtained by removing the first \\[aq]!"
"\\[aq] from it.  (Thus, \"I<[!]a-]>\" matches any single character except "
"\\[aq]]\\[aq], \\[aq]a\\[aq], and \\[aq]-\\[aq].)"
msgstr ""
"Une expression «\\ I<[!...]>\\ » correspond à n'importe quel caractère qui "
"ne puisse pas être mis en correspondance avec la chaîne obtenue en "
"supprimant le «\\ !\\ » initial (ainsi, «\\ I<[!]a-]>\\ » correspond à tout "
"caractère sauf «\\ ]\\ », «\\ a\\ » et «\\ -\\ »)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "One can remove the special meaning of \\[aq]?\\[aq], \\[aq]*\\[aq], and "
#| "\\[aq][\\[aq] by preceding them by a backslash, or, in case this is part "
#| "of a shell command line, enclosing them in quotes.  Between brackets "
#| "these characters stand for themselves.  Thus, \"I<[[?*\\e]>\" matches the "
#| "four characters \\[aq][\\[aq], \\[aq]?\\[aq], \\[aq]*\\[aq], and "
#| "\\[aq]\\e\\[aq]."
msgid ""
"One can remove the special meaning of \\[aq]?\\[aq], \\[aq]*\\[aq], and "
"\\[aq][\\[aq] by preceding them by a backslash, or, in case this is part of "
"a shell command line, enclosing them in quotes.  Between brackets these "
"characters stand for themselves.  Thus, \"I<[[?*\\[rs]]>\" matches the four "
"characters \\[aq][\\[aq], \\[aq]?\\[aq], \\[aq]*\\[aq], and "
"\\[aq]\\[rs]\\[aq]."
msgstr ""
"On peut désactiver le comportement spécial des caractères «\\ ?\\ », «\\ *\\ "
"» et «\\ [\\ » en les faisant précéder par une barre oblique inverse «\\ "
"\\e\\ », ou, dans le cas d'une ligne de commande shell, en les encadrant par "
"des guillemets. Entre crochets, ces caractères ne prennent que leur "
"signification littérale. Ainsi, «\\ I<[[?*\\e]>\\ » correspond aux quatre "
"caractères «\\ [\\ », «\\ ?\\ », «\\ *\\ » et «\\ \\e\\ »."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Pathnames"
msgstr "Chemins d'accès"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Globbing is applied on each of the components of a pathname separately.  A "
"\\[aq]/\\[aq] in a pathname cannot be matched by a \\[aq]?\\[aq] or "
"\\[aq]*\\[aq] wildcard, or by a range like \"I<[.-0]>\".  A range containing "
"an explicit \\[aq]/\\[aq] character is syntactically incorrect.  (POSIX "
"requires that syntactically incorrect patterns are left unchanged.)"
msgstr ""
"Le développement est appliqué à chaque composant du chemin d'accès "
"séparément. Un «\\ /\\ » dans un chemin ne peut pas être mis en "
"correspondance avec un «\\ ?\\ » ou «\\ *\\ », ni par un intervalle tel que "
"«\\ I<[.-0]>\\ ». Un intervalle ne peut pas contenir explicitement un "
"caractère «\\ /\\ ». Cela déclencherait une erreur de syntaxe. (POSIX "
"réclame qu'un motif soit laissé inchangé s'il est syntaxiquement incorrect.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If a filename starts with a \\[aq].\\[aq], this character must be matched "
"explicitly.  (Thus, I<rm\\ *> will not remove .profile, and I<tar\\ c\\ *> "
"will not archive all your files; I<tar\\ c\\ .> is better.)"
msgstr ""
"Si un nom de fichier commence par un «\\ .\\ », ce caractère doit être mis "
"en correspondance explicitement (ainsi, I<rm *> ne supprimera pas .profile, "
"et I<tar c *> n'archivera pas tous les fichiers\\ ; I<tar c .>serait "
"préférable)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Empty lists"
msgstr "Listes vides"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The nice and simple rule given above: \"expand a wildcard pattern into the "
"list of matching pathnames\" was the original UNIX definition.  It allowed "
"one to have patterns that expand into an empty list, as in"
msgstr ""
"La définition simple et élégante fournie plus haut «\\ transformer un motif "
"générique en une liste de noms de fichiers correspondants\\ » est la "
"définition UNIX originale. Elle autorisait la présence de motif se "
"développant en listes vides, comme"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    xv -wait 0 *.gif *.jpg\n"
msgstr "    xv -wait 0 *.gif *.jpg\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"where perhaps no *.gif files are present (and this is not an error).  "
"However, POSIX requires that a wildcard pattern is left unchanged when it is "
"syntactically incorrect, or the list of matching pathnames is empty.  With "
"I<bash> one can force the classical behavior using this command:"
msgstr ""
"alors qu'il n'y a peut-être aucun fichier *.gif présent (et on ne peut pas "
"considérer cela comme une erreur). Toutefois, POSIX réclame qu'un motif soit "
"laissé inchangé s'il est syntaxiquement incorrect, ou si la liste des noms "
"de fichiers correspondants est vide. On peut forcer B<bash> à adopter le "
"comportement classique en utilisant cette commande :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "shopt -s nullglob\n"
msgstr "shopt -s nullglob\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "(Similar problems occur elsewhere.  For example, where old scripts have"
msgstr ""
"(Des problèmes similaires se produisent ailleurs. Par exemple, de vieux "
"scripts avec"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "rm \\`find . -name \"*\\[ti]\"\\`\n"
msgstr "rm \\`find . -name \"*\\[ti]\"\\`\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "new scripts require"
msgstr "devraient être remplacés par des nouveaux scripts contenant"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "rm -f nosuchfile \\`find . -name \"*\\[ti]\"\\`\n"
msgstr "rm -f fichier-non-existant \\`find . -name \"*\\[ti]\"\\`\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "to avoid error messages from I<rm> called with an empty argument list.)"
msgstr ""
"pour éviter les messages d'erreurs si B<rm> est invoqué avec une liste vide "
"d'arguments)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Regular expressions"
msgstr "Expressions rationnelles"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that wildcard patterns are not regular expressions, although they are a "
"bit similar.  First of all, they match filenames, rather than text, and "
"secondly, the conventions are not the same: for example, in a regular "
"expression \\[aq]*\\[aq] means zero or more copies of the preceding thing."
msgstr ""
"Notez que les motifs génériques ne sont pas des expressions rationnelles "
"bien qu'ils leur ressemblent. Tout d'abord, ils correspondent à des noms de "
"fichiers, et pas à du texte. De plus, les conventions ne sont pas "
"identiques. Par exemple, dans une expression rationnelle, «\\ *\\ » signifie "
"zéro ou plusieurs copies de l'élément précédent."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Now that regular expressions have bracket expressions where the negation is "
"indicated by a \\[aq]\\[ha]\\[aq], POSIX has declared the effect of a "
"wildcard pattern \"I<[\\[ha]...]>\" to be undefined."
msgstr ""
"Maintenant que les expressions rationnelles disposent de composants entre "
"crochets où la négation est indiquée par un «\\ \\(ha\\ », POSIX a précisé "
"que le motif générique «\\ I<[\\[ha]...]>\\ » a un effet indéfini."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Character classes and internationalization"
msgstr "Internationalisation et classes de caractères"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Of course ranges were originally meant to be ASCII ranges, so that \"I<[\\ -"
"%]>\" stands for \"I<[\\ !\"#$%]>\" and \"I<[a-z]>\" stands for \"any "
"lowercase letter\".  Some UNIX implementations generalized this so that a "
"range X-Y stands for the set of characters with code between the codes for X "
"and for Y.  However, this requires the user to know the character coding in "
"use on the local system, and moreover, is not convenient if the collating "
"sequence for the local alphabet differs from the ordering of the character "
"codes.  Therefore, POSIX extended the bracket notation greatly, both for "
"wildcard patterns and for regular expressions.  In the above we saw three "
"types of items that can occur in a bracket expression: namely (i) the "
"negation, (ii) explicit single characters, and (iii) ranges.  POSIX "
"specifies ranges in an internationally more useful way and adds three more "
"types:"
msgstr ""
"Bien entendu, les intervalles ont été créés à l'origine en tant "
"qu'intervalle ASCII où «\\ I<[\\ -%]>\\ » signifie «\\ I<[\\ !\"#$%]>\\ » et "
"«\\ I<[a-z]>\\ » correspond à «\\ toute lettre minuscule\\ ». Certaines "
"implémentations UNIX ont généralisé ceci de manière à ce que l'intervalle X-"
"Y corresponde à l'ensemble des caractères dont les codes se trouvent entre "
"ceux de X et de Y. Néanmoins, ceci signifie que l'utilisateur doit connaître "
"le codage utilisé sur le système local. De plus, ceci ne fonctionne pas si "
"l'ordre de l'alphabet local n'est pas celui adopté pour le codage des "
"caractères. POSIX a alors étendu grandement la notation des expressions "
"entre crochets autant pour les motifs génériques que pour les expressions "
"rationnelles. Nous avons vu précédemment 3 types d'éléments pouvant se "
"trouver entre crochets. Plus précisément (i) une négation, (ii) des "
"caractères explicites, et (iii) des intervalles. POSIX spécifie les "
"intervalles d'une manière plus utile au niveau international, et ajoute "
"trois nouveaux types\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"(iii) Ranges X-Y comprise all characters that fall between X and Y "
"(inclusive) in the current collating sequence as defined by the "
"B<LC_COLLATE> category in the current locale."
msgstr ""
"(iii) Les intervalles X-Y comprenant tous les caractères se trouvant entre X "
"et Y (inclus) dans l'ordre de l'alphabet décrit par la catégorie "
"B<LC_COLLATE> de la localisation en cours."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "(iv) Named character classes, like"
msgstr "(iv) Des classes de caractères comme"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"[:alnum:]  [:alpha:]  [:blank:]  [:cntrl:]\n"
"[:digit:]  [:graph:]  [:lower:]  [:print:]\n"
"[:punct:]  [:space:]  [:upper:]  [:xdigit:]\n"
msgstr ""
"[:alnum:]  [:alpha:]  [:blank:]  [:cntrl:]\n"
"[:digit:]  [:graph:]  [:lower:]  [:print:]\n"
"[:punct:]  [:space:]  [:upper:]  [:xdigit:]\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"so that one can say \"I<[[:lower:]]>\" instead of \"I<[a-z]>\", and have "
"things work in Denmark, too, where there are three letters past "
"\\[aq]z\\[aq] in the alphabet.  These character classes are defined by the "
"B<LC_CTYPE> category in the current locale."
msgstr ""
"Ainsi, on peut écrire «\\ I<[[:lower:]]>\\ » à la place de «\\ I<[a-z]>\\ », "
"et que le comportement soit aussi valable au Danemark, où il existe 3\\ "
"lettres postérieures au «\\ z\\ ». Ces classes de caractères sont définies "
"par la catégorie B<LC_CTYPE> des paramètres régionaux actuels."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"(v) Collating symbols, like \"I<[.ch.]>\" or \"I<[.a-acute.]>\", where the "
"string between \"I<[.>\" and \"I<.]>\" is a collating element defined for "
"the current locale.  Note that this may be a multicharacter element."
msgstr ""
"(v) Les symboles groupant plusieurs lettres comme «\\ I<[.ch.]>\\ » ou «\\ "
"I<[.a-acute.]>\\ », dans lesquels les chaînes entre «\\ I<[.>\\ » et «\\ "
"I<.]>\\ » sont un élément de classement (I<collating element>) défini dans "
"la localisation en cours. Il peut également s'agir de multicaractères."

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"(vi) Equivalence class expressions, like \"I<[=a=]>\", where the string "
"between \"I<[=>\" and \"I<=]>\" is any collating element from its "
"equivalence class, as defined for the current locale.  For example, "
"\"I<[[=a=]]>\" might be equivalent to \"I<[a\\('a\\(`a\\(:a\\(^a]>\", that "
"is, to \"I<[a[.a-acute.][.a-grave.][.a-umlaut.][.a-circumflex.]]>\"."
msgstr ""
"(vi) Des classes d'équivalence comme «\\ I<[=a=]>\\ », où la chaîne entre "
"«\\ I<[=>\\ » et «\\ I<=]>\\ » est un élément de classement définissant une "
"classe d'équivalence dans la localisation en cours. Par exemple, «\\ "
"I<[[=a=]]>\\ » peut être équivalent à « I<[a\\('a\\(`a\\(:a\\(^a]> », c'est-"
"à-dire «\\ I<[a[.a-acute.][.a-grave.][.a-umlaut.][.a-circumflex.]]>\\ »."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<sh>(1), B<fnmatch>(3), B<glob>(3), B<locale>(7), B<regex>(7)"
msgstr "B<sh>(1), B<fnmatch>(3), B<glob>(3), B<locale>(7), B<regex>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"There is one special convention: two characters separated by \\[aq]-\\[aq] "
"denote a range.  (Thus, \"I<[A-Fa-f0-9]>\" is equivalent to "
"\"I<[ABCDEFabcdef0123456789]>\".)  One may include \\[aq]-\\[aq] in its "
"literal meaning by making it the first or last character between the "
"brackets.  (Thus, \"I<[]-]>\" matches just the two characters \\[aq]]\\[aq] "
"and \\[aq]-\\[aq], and \"I<[--0]>\" matches the three characters \\[aq]-"
"\\[aq], \\[aq].\\[aq], \\[aq]0\\[aq], since \\[aq]/\\[aq] cannot be matched.)"
msgstr ""
"Il existe une convention particulière, suivant laquelle deux caractères "
"séparés par un tiret «\\ -\\ » indiquent un intervalle. Ainsi, «\\ I<[A-Fa-"
"f0-9]>\\ » équivaut à «\\ I<[ABCDEFabcdef0123456789]>\\ ». Pour inclure un "
"tiret au sens littéral, il suffit de l'utiliser en premier ou en dernier "
"entre les crochets. Ainsi, «\\ I<[]-]>\\ » correspond uniquement aux "
"caractères «\\ ]\\ » et «\\ -\\ », et «\\ I<[--0]>\\ » correspond aux trois "
"caractères «\\ -\\ », «\\ .\\ » et «\\ 0\\ », puisqu'il ne peut y avoir de "
"correspondance avec « / »."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"One can remove the special meaning of \\[aq]?\\[aq], \\[aq]*\\[aq], and "
"\\[aq][\\[aq] by preceding them by a backslash, or, in case this is part of "
"a shell command line, enclosing them in quotes.  Between brackets these "
"characters stand for themselves.  Thus, \"I<[[?*\\e]>\" matches the four "
"characters \\[aq][\\[aq], \\[aq]?\\[aq], \\[aq]*\\[aq], and \\[aq]\\e\\[aq]."
msgstr ""
"On peut désactiver le comportement spécial des caractères «\\ ?\\ », «\\ *\\ "
"» et «\\ [\\ » en les faisant précéder par une barre oblique inverse «\\ "
"\\e\\ », ou, dans le cas d'une ligne de commande shell, en les encadrant par "
"des guillemets. Entre crochets, ces caractères ne prennent que leur "
"signification littérale. Ainsi, «\\ I<[[?*\\e]>\\ » correspond aux quatre "
"caractères «\\ [\\ », «\\ ?\\ », «\\ *\\ » et «\\ \\e\\ »."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"(vi) Equivalence class expressions, like \"I<[=a=]>\", where the string "
"between \"I<[=>\" and \"I<=]>\" is any collating element from its "
"equivalence class, as defined for the current locale.  For example, "
"\"I<[[=a=]]>\" might be equivalent to \"I<[a\\['a]\\[`a]\\[:a]\\[^a]]>\", "
"that is, to \"I<[a[.a-acute.][.a-grave.][.a-umlaut.][.a-circumflex.]]>\"."
msgstr ""
"(vi) Des classes d'équivalence comme «\\ I<[=a=]>\\ », où la chaîne entre "
"«\\ I<[=>\\ » et «\\ I<=]>\\ » est un élément de classement définissant une "
"classe d'équivalence dans la localisation en cours. Par exemple, «\\ "
"I<[[=a=]]>\\ » peut être équivalent à « I<[a\\('a\\(`a\\(:a\\(^a]> », c'est-"
"à-dire «\\ I<[a[.a-acute.][.a-grave.][.a-umlaut.][.a-circumflex.]]>\\ »."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
