# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gérard Delafond <gerard@delafond.org>
# Christophe Blaess <ccb@club-internet.fr>, 1999, 2001, 2003, 2005, 2006.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003.
# Denis Barbier <barbier@debian.org>, 2004.
# Nicolas François <nicolas.francois@centraliens.net>, 2007, 2009.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2007, 2008, 2009.
# Thomas Blein <tblein@tblein.eu>, 2011, 2014.
# David Prévot <david@tilapin.org>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: glibc\n"
"POT-Creation-Date: 2024-12-06 18:10+0100\n"
"PO-Revision-Date: 2024-05-08 09:08+0200\n"
"Last-Translator: Thomas Blein <tblein@tblein.eu>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_once"
msgstr "pthread_once"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "pthread_once - once-only initialization"
msgstr "pthread_once - Exécution unique"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<#include E<lt>pthread.hE<gt>>"
msgstr "B<#include E<lt>pthread.hE<gt>>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pthread_once_t >I<once_control>B< = PTHREAD_ONCE_INIT;>"
msgstr "B<pthread_once_t >I<once_control>B< = PTHREAD_ONCE_INIT;>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<int pthread_once(pthread_once_t *>I<once_control>B<, void "
"(*>I<init_routine>B<) (void));>"
msgstr ""
"B<int pthread_once(pthread_once_t *>I<once_control>B<, void "
"(*>I<init_routine>B<) (void));>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The purpose of B<pthread_once> is to ensure that a piece of initialization "
"code is executed at most once.  The I<once_control> argument points to a "
"static or extern variable statically initialized to B<PTHREAD_ONCE_INIT>."
msgstr ""
"L'objectif de B<pthread_once>() est de s'assurer qu'une séquence "
"d'initialisation n'est exécutée qu'une seule fois. Le paramètre "
"I<once_control> pointe sur une variable statique ou externe statiquement "
"initialisée à B<PTHREAD_ONCE_INIT>."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The first time B<pthread_once> is called with a given I<once_control> "
"argument, it calls I<init_routine> with no argument and changes the value of "
"the I<once_control> variable to record that initialization has been "
"performed.  Subsequent calls to B<pthread_once> with the same "
"B<once_control> argument do nothing."
msgstr ""
"La première fois que B<pthread_once>() est appelée avec un argument "
"I<once_control> donné, elle appelle I<init_routine> sans aucun argument et "
"modifie la valeur de la variable I<once_control> pour enregistrer le fait "
"que l'initialisation a été réalisée. Les appels suivants B<pthread_once>() "
"avec le même argument I<once_control> ne feront rien."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pthread_once> always returns 0."
msgstr "B<pthread_once>() renvoie toujours 0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Aucun."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "PTHREAD_ONCE"
msgstr "PTHREAD_ONCE"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LinuxThreads"
msgstr "LinuxThreads"

#. type: Plain text
#: debian-bookworm
msgid ""
"The purpose of B<pthread_once> is to ensure that a piece of initialization "
"code is executed at most once. The I<once_control> argument points to a "
"static or extern variable statically initialized to B<PTHREAD_ONCE_INIT>."
msgstr ""
"L'objectif de B<pthread_once>() est de s'assurer qu'une séquence "
"d'initialisation n'est exécutée qu'une seule fois. Le paramètre "
"I<once_control> pointe sur une variable statique ou externe statiquement "
"initialisée à B<PTHREAD_ONCE_INIT>."

#. type: Plain text
#: debian-bookworm
msgid ""
"The first time B<pthread_once> is called with a given I<once_control> "
"argument, it calls I<init_routine> with no argument and changes the value of "
"the I<once_control> variable to record that initialization has been "
"performed. Subsequent calls to B<pthread_once> with the same B<once_control> "
"argument do nothing."
msgstr ""
"La première fois que B<pthread_once>() est appelée avec un argument "
"I<once_control> donné, elle appelle I<init_routine> sans aucun argument et "
"modifie la valeur de la variable I<once_control> pour enregistrer le fait "
"que l'initialisation a été réalisée. Les appels suivants B<pthread_once>() "
"avec le même argument I<once_control> ne feront rien."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: debian-bookworm
msgid "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"
msgstr "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
