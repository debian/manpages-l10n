# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2012, 2014.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2024-12-22 07:42+0100\n"
"PO-Revision-Date: 2023-03-31 00:27+0100\n"
"Last-Translator: Grégoire Scano <gregoire.scano@malloc.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strnlen"
msgstr "strnlen"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 juin 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "strnlen - determine the length of a fixed-size string"
msgstr "strnlen - Déterminer la longueur d'une chaîne de longueur déterminée"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<size_t strnlen(const char >I<s>B<[.>I<maxlen>B<], size_t >I<maxlen>B<);>\n"
msgstr "B<size_t strnlen(const char >I<s>B<[.>I<maxlen>B<], size_t >I<maxlen>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strnlen>():"
msgstr "B<strnlen>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.10 :\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Avant la glibc 2.10 :\n"
"        _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strnlen>()  function returns the number of bytes in the string "
#| "pointed to by I<s>, excluding the terminating null byte "
#| "(\\[aq]\\e0\\[aq]), but at most I<maxlen>.  In doing this, B<strnlen>()  "
#| "looks only at the first I<maxlen> characters in the string pointed to by "
#| "I<s> and never beyond I<s[maxlen-1]>."
msgid ""
"The B<strnlen>()  function returns the number of bytes in the string pointed "
"to by I<s>, excluding the terminating null byte (\\[aq]\\[rs]0\\[aq]), but "
"at most I<maxlen>.  In doing this, B<strnlen>()  looks only at the first "
"I<maxlen> characters in the string pointed to by I<s> and never beyond "
"I<s[maxlen-1]>."
msgstr ""
"La fonction B<strnlen>() renvoie le nombre d'octets de la chaîne pointée par "
"I<s>, octet NULL final («\\ \\e0\\ ») non compris, mais en examinant au "
"maximum I<maxlen> octets. Ce faisant, B<strnlen>() n'examine que les "
"I<maxlen> premiers octets de la chaîne pointée par I<s> et n'atteint jamais "
"I<s[maxlen-1]>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strnlen>()  function returns I<strlen(s)>, if that is less than "
#| "I<maxlen>, or I<maxlen> if there is no null terminating "
#| "(\\[aq]\\e0\\[aq]) among the first I<maxlen> characters pointed to by "
#| "I<s>."
msgid ""
"The B<strnlen>()  function returns I<strlen(s)>, if that is less than "
"I<maxlen>, or I<maxlen> if there is no null terminating "
"(\\[aq]\\[rs]0\\[aq]) among the first I<maxlen> characters pointed to by "
"I<s>."
msgstr ""
"La fonction B<strnlen>() renvoie I<strlen(s)> si la longueur est inférieure "
"à I<maxlen>, ou I<maxlen> s'il n'y a pas d'octet NULL final (« \\e0 ») parmi "
"les I<maxlen> premiers octets de la chaîne pointée par I<s>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strnlen>()"
msgstr "B<strnlen>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strlen>(3)"
msgstr "B<strlen>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The B<strnlen>()  function returns the number of bytes in the string pointed "
"to by I<s>, excluding the terminating null byte (\\[aq]\\e0\\[aq]), but at "
"most I<maxlen>.  In doing this, B<strnlen>()  looks only at the first "
"I<maxlen> characters in the string pointed to by I<s> and never beyond "
"I<s[maxlen-1]>."
msgstr ""
"La fonction B<strnlen>() renvoie le nombre d'octets de la chaîne pointée par "
"I<s>, octet NULL final («\\ \\e0\\ ») non compris, mais en examinant au "
"maximum I<maxlen> octets. Ce faisant, B<strnlen>() n'examine que les "
"I<maxlen> premiers octets de la chaîne pointée par I<s> et n'atteint jamais "
"I<s[maxlen-1]>."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The B<strnlen>()  function returns I<strlen(s)>, if that is less than "
"I<maxlen>, or I<maxlen> if there is no null terminating (\\[aq]\\e0\\[aq]) "
"among the first I<maxlen> characters pointed to by I<s>."
msgstr ""
"La fonction B<strnlen>() renvoie I<strlen(s)> si la longueur est inférieure "
"à I<maxlen>, ou I<maxlen> s'il n'y a pas d'octet NULL final (« \\e0 ») parmi "
"les I<maxlen> premiers octets de la chaîne pointée par I<s>."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
