# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-12-06 17:56+0100\n"
"PO-Revision-Date: 2024-11-05 15:35+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "copy_file_range"
msgstr "copy_file_range"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 juin 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "copy_file_range - Copy a range of data from one file to another"
msgstr ""
"copy_file_range - Copier une plage de données d'un fichier vers un autre"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>\n"
"B<#define _FILE_OFFSET_BITS 64>\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>\n"
"B<#define _FILE_OFFSET_BITS 64>\n"
"B<#include E<lt>unistd.hE<gt>>\n"

# no-wrap
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<ssize_t copy_file_range(int >I<fd_in>B<, off_t *_Nullable >I<off_in>B<,>\n"
"B<                        int >I<fd_out>B<, off_t *_Nullable >I<off_out>B<,>\n"
"B<                        size_t >I<len>B<, unsigned int >I<flags>B<);>\n"
msgstr ""
"B<ssize_t copy_file_range(int >I<fd_in>B<, off_t *_Nullable >I<off_in>B<,>\n"
"B<                        int >I<fd_out>B<, off_t *_Nullable >I<off_out>B<,>\n"
"B<                        size_t >I<len>B<, unsigned int >I<flags>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<copy_file_range>()  system call performs an in-kernel copy between two "
"file descriptors without the additional cost of transferring data from the "
"kernel to user space and then back into the kernel.  It copies up to I<len> "
"bytes of data from the source file descriptor I<fd_in> to the target file "
"descriptor I<fd_out>, overwriting any data that exists within the requested "
"range of the target file."
msgstr ""
"L'appel système B<copy_file_range>() effectue une copie interne au noyau "
"entre deux descripteurs de fichier sans devoir en plus transférer des "
"données du noyau à l'espace utilisateur puis revenir au noyau. Jusqu'à "
"I<len> octets de données sont transférés du descripteur de fichier I<fd_in> "
"au descripteur de fichier I<fd_out>, écrasant toute donnée se trouvant dans "
"la plage du fichier cible sollicité."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following semantics apply for I<off_in>, and similar statements apply to "
"I<off_out>:"
msgstr ""
"La sémantique suivante s'applique à I<off_in> et des déclarations identiques "
"s'appliquent à I<off_out> :"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<off_in> is NULL, then bytes are read from I<fd_in> starting from the "
"file offset, and the file offset is adjusted by the number of bytes copied."
msgstr ""
"Si I<off_in> est NULL, les octets sont lus dans I<fd_in> à partir de la "
"position du fichier, laquelle est ajustée par le nombre d'octets copiés."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<off_in> is not NULL, then I<off_in> must point to a buffer that "
"specifies the starting offset where bytes from I<fd_in> will be read.  The "
"file offset of I<fd_in> is not changed, but I<off_in> is adjusted "
"appropriately."
msgstr ""
"Si I<off_in> n'est pas NULL, I<off_in> doit pointer vers un tampon qui "
"indique le point de départ à partir duquel les octets de I<fd_in> seront "
"lus. La position du fichier de I<fd_in> n'est pas modifiée mais I<off_in> "
"est ajusté correctement."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<fd_in> and I<fd_out> can refer to the same file.  If they refer to the "
"same file, then the source and target ranges are not allowed to overlap."
msgstr ""
"I<fd_in> et I<fd_out> peuvent se rapporter au même fichier. Dans ce cas, les "
"plages de la source et de la cible ne sont pas autorisées à se chevaucher."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<flags> argument is provided to allow for future extensions and "
"currently must be set to 0."
msgstr ""
"L'argument I<flags> est fourni pour de futures extensions et doit être "
"positionné actuellement sur B<0>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Upon successful completion, B<copy_file_range>()  will return the number of "
"bytes copied between files.  This could be less than the length originally "
"requested.  If the file offset of I<fd_in> is at or past the end of file, no "
"bytes are copied, and B<copy_file_range>()  returns zero."
msgstr ""
"En cas de succès, B<copy_file_range>() renverra le nombre d'octets copiés "
"entre les fichiers. Il pourrait être inférieur à la taille demandée au "
"départ. Si la position du fichier de I<fd_in> est à la fin du fichier ou au-"
"delà, aucun octet n'est copié et B<copy_file_range>() renvoie zéro."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On error, B<copy_file_range>()  returns -1 and I<errno> is set to indicate "
"the error."
msgstr ""
"En cas d'erreur, B<copy_file_range>() renvoie B<-1> et I<errno> est "
"configuré pour indiquer l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "One or more file descriptors are not valid."
msgstr "Un ou plusieurs descripteurs de fichier ne sont pas valables."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<fd_in> is not open for reading; or I<fd_out> is not open for writing."
msgstr ""
"I<fd_in> n'est pas ouvert en lecture ou I<fd_out> n'est pas ouvert en "
"écriture."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<O_APPEND> flag is set for the open file description (see B<open>(2))  "
"referred to by the file descriptor I<fd_out>."
msgstr ""
"L'attribut B<O_APPEND> est configuré pour une description d'un fichier "
"ouvert (voir B<open>(2)) auquel renvoie le descripteur de fichier I<fd_out>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFBIG>"
msgstr "B<EFBIG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An attempt was made to write at a position past the maximum file offset the "
"kernel supports."
msgstr ""
"Tentative d'écriture sur une position dépassant la position maximale du "
"fichier gérée par le noyau."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An attempt was made to write a range that exceeds the allowed maximum file "
"size.  The maximum file size differs between filesystem implementations and "
"can be different from the maximum allowed file offset."
msgstr ""
"Tentative d'écriture d'une plage dépassant la taille maximale d'un fichier "
"permise. La taille maximale d'un fichier varie selon les implémentations de "
"système de fichiers et peut être différente de la position du fichier "
"maximale autorisée."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An attempt was made to write beyond the process's file size resource limit.  "
"This may also result in the process receiving a B<SIGXFSZ> signal."
msgstr ""
"Tentative d'écriture au-delà de la limite de ressource de la taille du "
"fichier du processus. Cela peut aussi avoir pour conséquence la réception, "
"par le processus, d'un signal I<SIGXFSZ>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The I<flags> argument is not 0."
msgstr "Le paramètre I<flags> ne vaut pas B<0>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<fd_in> and I<fd_out> refer to the same file and the source and target "
"ranges overlap."
msgstr ""
"I<fd_in> et I<fd_out> se rapportent au même fichier et les plages de la "
"source et de la cible se chevauchent."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Either I<fd_in> or I<fd_out> is not a regular file."
msgstr "I<fd_in> ou I<fd_out> n'est pas un fichier normal."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A low-level I/O error occurred while copying."
msgstr "Une erreur E/S de bas niveau s'est produite lors de la copie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EISDIR>"
msgstr "B<EISDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Either I<fd_in> or I<fd_out> refers to a directory."
msgstr "I<fd_in> ou I<fd_out> se rapporte à un répertoire."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Out of memory."
msgstr "Plus assez de mémoire."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There is not enough space on the target filesystem to complete the copy."
msgstr ""
"Il n'y a pas assez d'espace sur le système de fichiers cible pour terminer "
"la copie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP> (since Linux 5.19)"
msgstr "B<EOPNOTSUPP> (depuis Linux 5.19)"

#.  commit 868f9f2f8e004bfe0d3935b1976f625b2924893b
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The filesystem does not support this operation."
msgstr "Le système de fichiers ne prend pas en charge cette opération."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EOVERFLOW>"
msgstr "B<EOVERFLOW>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The requested source or destination range is too large to represent in the "
"specified data types."
msgstr ""
"La plage source ou de destination demandée est trop grande pour être "
"représentée dans les types de données indiqués."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<fd_out> refers to an immutable file."
msgstr "I<fd_out> se rapporte à un fichier immuable."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ETXTBSY>"
msgstr "B<ETXTBSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Either I<fd_in> or I<fd_out> refers to an active swap file."
msgstr "I<fd_in> ou I<fd_out> se rapporte à un fichier d'échange actif."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EXDEV> (before Linux 5.3)"
msgstr "B<EXDEV> (depuis Linux 5.3)"

#.  commit 5dae222a5ff0c269730393018a5539cc970a4726
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The files referred to by I<fd_in> and I<fd_out> are not on the same "
"filesystem."
msgstr ""
"Les fichiers auxquels se rapportent I<fd_in> et I<fd_out> ne sont pas sur le "
"même système de fichiers."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EXDEV> (since Linux 5.19)"
msgstr "B<EXDEV> (depuis Linux 5.19)"

#.  commit 868f9f2f8e004bfe0d3935b1976f625b2924893b
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The files referred to by I<fd_in> and I<fd_out> are not on the same "
"filesystem, and the source and target filesystems are not of the same type, "
"or do not support cross-filesystem copy."
msgstr ""
"Les fichiers auxquels se rapportent I<fd_in> et I<fd_out> ne sont pas sur le "
"même système de fichiers et les systèmes de fichiers source et cible ne sont "
"pas du même type ou ne prennent pas en charge la copie entre systèmes de "
"fichiers."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A major rework of the kernel implementation occurred in Linux 5.3.  Areas of "
"the API that weren't clearly defined were clarified and the API bounds are "
"much more strictly checked than on earlier kernels."
msgstr ""
"L'implémentation du noyau a été profondément retravaillée dans Linux 5.3. "
"Les zones de l'API qui n'étaient pas clairement définies ont été clarifiées "
"et les limites de l'API sont vérifiées beaucoup plus strictement que sur les "
"noyaux précédents. Les applications devraient cibler le comportement et les "
"exigences des noyaux 5.3."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since Linux 5.19, cross-filesystem copies can be achieved when both "
"filesystems are of the same type, and that filesystem implements support for "
"it.  See BUGS for behavior prior to Linux 5.19."
msgstr ""
"Depuis Linux 5.19, les copies entre systèmes de fichiers peuvent se faire "
"quand les deux systèmes de fichiers sont du même type et si le système de "
"fichiers le prend en charge. Voir BOGUES pour le comportement avant la 5.19."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Applications should target the behaviour and requirements of Linux 5.19, "
"that was also backported to earlier stable kernels."
msgstr ""
"Les applications devraient cibler le comportement et les exigences de "
"Linux 5.3 qui ont aussi été rétroportés dans les noyaux stable plus récents."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux, GNU."
msgstr "Linux, GNU."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#.  https://sourceware.org/git/?p=glibc.git;a=commit;f=posix/unistd.h;h=bad7a0c81f501fbbcc79af9eaa4b8254441c4a1f
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Linux 4.5, but glibc 2.27 provides a user-space emulation when it is not "
"available."
msgstr ""
"Linux 4.5, mais la glibc 2.27 offre une émulation dans l'espace utilisateur "
"s'il n'est pas disponible."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<fd_in> is a sparse file, then B<copy_file_range>()  may expand any "
"holes existing in the requested range.  Users may benefit from calling "
"B<copy_file_range>()  in a loop, and using the B<lseek>(2)  B<SEEK_DATA> and "
"B<SEEK_HOLE> operations to find the locations of data segments."
msgstr ""
"Si I<fd_in> est un fichier éparpillé, il se peut que B<copy_file_range>() "
"agrandisse les trous existant dans la plage demandée. Les utilisateurs "
"peuvent bénéficier d'un appel à B<copy_file_range>() dans une boucle et "
"utiliser les opérations B<SEEK_DATA> et B<SEEK_HOLE> de B<lseek>(2) pour "
"chercher des emplacements de segments de données."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<copy_file_range>()  gives filesystems an opportunity to implement \"copy "
"acceleration\" techniques, such as the use of reflinks (i.e., two or more "
"inodes that share pointers to the same copy-on-write disk blocks)  or server-"
"side-copy (in the case of NFS)."
msgstr ""
"B<copy_file_range>() donne aux systèmes de fichiers la possibilité "
"d'implémenter des techniques de « copie accélérée » telles que l'utilisation "
"de reflink (c'est-à-dire deux ou plusieurs i-nœuds partageant des pointeurs "
"avec les mêmes blocs de disque copy-on-write) ou server-side-copy (dans le "
"cas de NFS)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<_FILE_OFFSET_BITS> should be defined to be 64 in code that uses non-null "
"I<off_in> or I<off_out> or that takes the address of B<copy_file_range>, if "
"the code is intended to be portable to traditional 32-bit x86 and ARM "
"platforms where B<off_t>'s width defaults to 32 bits."
msgstr ""
"B<_FILE_OFFSET_BITS> devrait être défini pour être 64 dans le code qui "
"utilise I<off_in> ou I<off_out> non NULL ou qui obtient l'adresse de "
"B<copy_file_range>, si le code est destiné à être portable sur les "
"plateformes x86 32 bits et ARM traditionnelles où la taille par défaut de "
"B<off_t> est de 32 bits."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In Linux 5.3 to Linux 5.18, cross-filesystem copies were implemented by the "
"kernel, if the operation was not supported by individual filesystems.  "
"However, on some virtual filesystems, the call failed to copy, while still "
"reporting success."
msgstr ""
"De Linux 5.3 à Linux 5.18, les copies entre système de fichiers étaient "
"implémentées par le noyau si l'opération n'était pas gérée par les systèmes "
"de fichiers eux-mêmes. Cependant, sur certains systèmes de fichiers "
"virtuels, le code n'arrivait pas à faire la copie mais la présentait comme "
"réussie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#define _FILE_OFFSET_BITS 64\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd_in, fd_out;\n"
"    off_t        len, ret;\n"
"    struct stat  stat;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>sourceE<gt> E<lt>destinationE<gt>\\[rs]n\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    fd_in = open(argv[1], O_RDONLY);\n"
"    if (fd_in == -1) {\n"
"        perror(\"open (argv[1])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (fstat(fd_in, &stat) == -1) {\n"
"        perror(\"fstat\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    len = stat.st_size;\n"
"\\&\n"
"    fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);\n"
"    if (fd_out == -1) {\n"
"        perror(\"open (argv[2])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    do {\n"
"        ret = copy_file_range(fd_in, NULL, fd_out, NULL, len, 0);\n"
"        if (ret == -1) {\n"
"            perror(\"copy_file_range\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"\\&\n"
"        len -= ret;\n"
"    } while (len E<gt> 0 && ret E<gt> 0);\n"
"\\&\n"
"    close(fd_in);\n"
"    close(fd_out);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#define _FILE_OFFSET_BITS 64\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd_in, fd_out;\n"
"    off_t        len, ret;\n"
"    struct stat  stat;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Utilisation : %s E<lt>sourceE<gt> E<lt>destinationE<gt>\\[rs]n\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    fd_in = open(argv[1], O_RDONLY);\n"
"    if (fd_in == -1) {\n"
"        perror(\"open (argv[1])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (fstat(fd_in, &stat) == -1) {\n"
"        perror(\"fstat\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    len = stat.st_size;\n"
"\\&\n"
"    fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);\n"
"    if (fd_out == -1) {\n"
"        perror(\"open (argv[2])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    do {\n"
"        ret = copy_file_range(fd_in, NULL, fd_out, NULL, len, 0);\n"
"        if (ret == -1) {\n"
"            perror(\"copy_file_range\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"\\&\n"
"        len -= ret;\n"
"    } while (len E<gt> 0 && ret E<gt> 0);\n"
"\\&\n"
"    close(fd_in);\n"
"    close(fd_out);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<lseek>(2), B<sendfile>(2), B<splice>(2)"
msgstr "B<lseek>(2), B<sendfile>(2), B<splice>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"B<ssize_t copy_file_range(int >I<fd_in>B<, off64_t *_Nullable >I<off_in>B<,>\n"
"B<                        int >I<fd_out>B<, off64_t *_Nullable >I<off_out>B<,>\n"
"B<                        size_t >I<len>B<, unsigned int >I<flags>B<);>\n"
msgstr ""
"B<ssize_t copy_file_range(int >I<fd_in>B<, off64_t *_Nullable >I<off_in>B<,>\n"
"B<                        int >I<fd_out>B<, off64_t *_Nullable >I<off_out>B<,>\n"
"B<                        size_t >I<len>B<, unsigned int >I<flags>B<);>\n"

#.  https://sourceware.org/git/?p=glibc.git;a=commit;f=posix/unistd.h;h=bad7a0c81f501fbbcc79af9eaa4b8254441c4a1f
#. type: Plain text
#: debian-bookworm
msgid ""
"The B<copy_file_range>()  system call first appeared in Linux 4.5, but glibc "
"2.27 provides a user-space emulation when it is not available."
msgstr ""
"L'appel système B<copy_file_range>() est apparu pour la première fois dans "
"Linux 4.5, mais la glibc 2.27 offre une émulation dans l'espace utilisateur "
"s'il n'est pas disponible."

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<copy_file_range>()  system call is a nonstandard Linux and GNU "
"extension."
msgstr ""
"L'appel système B<copy_file_range>() est une extension GNU et un non "
"standard de Linux."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd_in, fd_out;\n"
"    off64_t      len, ret;\n"
"    struct stat  stat;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd_in, fd_out;\n"
"    off64_t      len, ret;\n"
"    struct stat  stat;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>sourceE<gt> E<lt>destinationE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Utilisation : %s E<lt>sourceE<gt> E<lt>destinationE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    fd_in = open(argv[1], O_RDONLY);\n"
"    if (fd_in == -1) {\n"
"        perror(\"open (argv[1])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    fd_in = open(argv[1], O_RDONLY);\n"
"    if (fd_in == -1) {\n"
"        perror(\"open (argv[1])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (fstat(fd_in, &stat) == -1) {\n"
"        perror(\"fstat\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (fstat(fd_in, &stat) == -1) {\n"
"        perror(\"fstat\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    len = stat.st_size;\n"
msgstr "    len = stat.st_size;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);\n"
"    if (fd_out == -1) {\n"
"        perror(\"open (argv[2])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);\n"
"    if (fd_out == -1) {\n"
"        perror(\"open (argv[2])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    do {\n"
"        ret = copy_file_range(fd_in, NULL, fd_out, NULL, len, 0);\n"
"        if (ret == -1) {\n"
"            perror(\"copy_file_range\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
msgstr ""
"    do {\n"
"        ret = copy_file_range(fd_in, NULL, fd_out, NULL, len, 0);\n"
"        if (ret == -1) {\n"
"            perror(\"copy_file_range\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"        len -= ret;\n"
"    } while (len E<gt> 0 && ret E<gt> 0);\n"
msgstr ""
"        len -= ret;\n"
"    } while (len E<gt> 0 && ret E<gt> 0);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    close(fd_in);\n"
"    close(fd_out);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    close(fd_in);\n"
"    close(fd_out);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#define _FILE_OFFSET_BITS 64\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd_in, fd_out;\n"
"    off_t        len, ret;\n"
"    struct stat  stat;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>sourceE<gt> E<lt>destinationE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    fd_in = open(argv[1], O_RDONLY);\n"
"    if (fd_in == -1) {\n"
"        perror(\"open (argv[1])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (fstat(fd_in, &stat) == -1) {\n"
"        perror(\"fstat\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    len = stat.st_size;\n"
"\\&\n"
"    fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);\n"
"    if (fd_out == -1) {\n"
"        perror(\"open (argv[2])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    do {\n"
"        ret = copy_file_range(fd_in, NULL, fd_out, NULL, len, 0);\n"
"        if (ret == -1) {\n"
"            perror(\"copy_file_range\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"\\&\n"
"        len -= ret;\n"
"    } while (len E<gt> 0 && ret E<gt> 0);\n"
"\\&\n"
"    close(fd_in);\n"
"    close(fd_out);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#define _FILE_OFFSET_BITS 64\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd_in, fd_out;\n"
"    off_t        len, ret;\n"
"    struct stat  stat;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Utilisation : %s E<lt>sourceE<gt> E<lt>destinationE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    fd_in = open(argv[1], O_RDONLY);\n"
"    if (fd_in == -1) {\n"
"        perror(\"open (argv[1])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (fstat(fd_in, &stat) == -1) {\n"
"        perror(\"fstat\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    len = stat.st_size;\n"
"\\&\n"
"    fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);\n"
"    if (fd_out == -1) {\n"
"        perror(\"open (argv[2])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    do {\n"
"        ret = copy_file_range(fd_in, NULL, fd_out, NULL, len, 0);\n"
"        if (ret == -1) {\n"
"            perror(\"copy_file_range\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"\\&\n"
"        len -= ret;\n"
"    } while (len E<gt> 0 && ret E<gt> 0);\n"
"\\&\n"
"    close(fd_in);\n"
"    close(fd_out);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
