# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 18:03+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LESSECHO"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "Version 668: 06 Oct 2024"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "lessecho - expand metacharacters"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<lessecho> I<[-ox] [-cx] [-pn] [-dn] [-mx] [-nn] [-ex] [-a] file ...>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<lessecho> is a program that simply echos its arguments on standard "
"output.  But any metacharacter in the output is preceded by an \"escape\" "
"character, which by default is a backslash.  B<lessecho> is invoked "
"internally by B<less>, and is not intended to be used directly by humans."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A summary of options is included below."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-e>I<x>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifies \"I<x>\", rather than backslash, to be the escape char for "
"metachars.  If I<x> is \"-\", no escape char is used and arguments "
"containing metachars are surrounded by quotes instead."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>I<x>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifies \"I<x>\", rather than double-quote, to be the open quote "
"character, which is used if the -e- option is specified."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>I<x>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Specifies \"I<x>\" to be the close quote character."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>I<n>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Specifies \"I<n>\" to be the open quote character, as an integer."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>I<n>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Specifies \"I<n>\" to be the close quote character, as an integer."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>I<x>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifies \"I<x>\" to be a metachar.  By default, no characters are "
"considered metachars."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>I<n>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Specifies \"I<n>\" to be a metachar, as an integer."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>I<n>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Specifies \"I<n>\" to be the escape char for metachars, as an integer."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifies that all arguments are to be quoted.  The default is that only "
"arguments containing metacharacters are quoted."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<less>(1)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page was written by Thomas Schoepf E<lt>schoepf@debian.orgE<gt>, "
"for the Debian GNU/Linux system (but may be used by others)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Report bugs at https://github.com/gwsw/less/issues."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Version 590: 03 Jun 2021"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<lessecho> is a program that simply echos its arguments on standard "
"output.  But any metacharacter in the output is preceded by an \"escape\" "
"character, which by default is a backslash."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-ex>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specifies \"x\", rather than backslash, to be the escape char for "
"metachars.  If x is \"-\", no escape char is used and arguments containing "
"metachars are surrounded by quotes instead."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-ox>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specifies \"x\", rather than double-quote, to be the open quote character, "
"which is used if the -e- option is specified."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-cx>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specifies \"x\" to be the close quote character."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-pn>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specifies \"n\" to be the open quote character, as an integer."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-dn>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specifies \"n\" to be the close quote character, as an integer."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-mx>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specifies \"x\" to be a metachar.  By default, no characters are considered "
"metachars."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-nn>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specifies \"n\" to be a metachar, as an integer."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-fn>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specifies \"n\" to be the escape char for metachars, as an integer."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "Version 643: 20 Jul 2023"
msgstr ""

#. type: Plain text
#: debian-unstable opensuse-leap-16-0
msgid ""
"B<lessecho> is a program that simply echos its arguments on standard "
"output.  But any metacharacter in the output is preceded by an \"escape\" "
"character, which by default is a backslash."
msgstr ""

#. type: TH
#: fedora-41 opensuse-tumbleweed
#, no-wrap
msgid "Version 661: 29 Jun 2024"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Version 668: 18 Oct 2024"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "Version 633: 03 May 2023"
msgstr ""
