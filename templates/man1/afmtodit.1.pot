# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:17+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: SY
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "afmtodit"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "28 August 2024"
msgstr ""

#. type: TH
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "groff 1.23.0"
msgstr ""

#. type: SH
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"afmtodit - adapt Adobe Font Metrics files for I<groff> PostScript and PDF "
"output"
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Synopsis"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"[B<-ckmnsx>] [B<-a\\ >I<slant>] [B<-d\\ >I<device-description-file>] [B<-e\\ "
">I<encoding-file>] [B<-f\\ >I<internal-name>] [B<-i\\ >I<italic-correction-"
"factor>] [B<-o\\ >I<output-file>] [B<-w\\ >I<space-width>] I<afm-file> I<map-"
"file> I<font-description-file>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--help>"
msgstr ""

#. #-#-#-#-#  archlinux: afmtodit.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: afmtodit.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: afmtodit.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: afmtodit.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: afmtodit.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--version>"
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<\\%afmtodit> adapts an Adobe Font Metric file, I<afm-file>, for use with "
"the B<ps> and B<pdf> output devices of E<.MR \\%troff 1 .>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<map-file> associates a I<groff> ordinary or special character name with a "
"PostScript glyph name."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Output is written in E<.MR groff_font 5> format to I<font-description-file,> "
"a file named for the intended I<groff> font name (but see the B<-o> option)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<map-file> should contain a sequence of lines of the form"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<ps-glyph groff-char>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"where I<ps-glyph> is the PostScript glyph name and I<groff-char> is a "
"I<groff> ordinary (if of unit length)  or special (if longer)  character "
"identifier."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The same I<ps-glyph> can occur multiple times in the file; each I<groff-"
"char> must occur at most once."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Lines starting with \\[lq]#\\[rq] and blank lines are ignored."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the file isn't found in the current directory, it is sought in the "
"I<devps/generate> subdirectory of the default font directory."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If a PostScript glyph is not mentioned in I<map-file>, and a I<groff> "
"character name can't be deduced using the Adobe Glyph List (AGL, built into "
"I<afmtodit>), then I<\\%afmtodit> puts the PostScript glyph into the "
"I<groff> font description file as an unnamed glyph which can only be "
"accessed by the \\[lq]\\eN\\[rq] escape sequence in a I<roff> document."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In particular, this is true for glyph variants named in the form \\"
"[lq]I<foo>.I<bar>\\[rq]; all glyph names containing one or more periods are "
"mapped to unnamed entities."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Unless B<-e> is specified, the encoding defined in the AFM file (i.e., "
"entries with non-negative codes)  is used."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Refer to section \\[lq]Using Symbols\\[rq] in I<Groff: The GNU "
"Implementation of troff>, the I<groff> Texinfo manual, or E<.MR groff_char "
"7 ,> which describe how I<groff> character identifiers are constructed."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Glyphs not encoded in the AFM file (i.e., entries indexed as \\[lq]-1\\"
"[rq])  are still available in I<groff>; they get glyph index values greater "
"than 255 (or greater than the biggest code used in the AFM file in the "
"unlikely case that it is greater than 255)  in the I<groff> font description "
"file."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Unencoded glyph indices don't have a specific order; it is best to access "
"them only via special character identifiers."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the font file proper (not just its metrics)  is available, listing it in "
"the files I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%font/\\:\\%devps/"
"\\:\\%download> and I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%font/"
"\\:\\%devpdf/\\:\\%download> enables it to be embedded in the output "
"produced by E<.MR grops 1> and E<.MR gropdf 1 ,> respectively."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the B<-i> option is used, I<\\%afmtodit> automatically generates an "
"italic correction, a left italic correction, and a subscript correction for "
"each glyph (the significance of these is explained in E<.MR groff_font 5 );> "
"they can be specified for individual glyphs by adding to the I<afm-file> "
"lines of the form:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"italicCorrectionI<\\ ps-glyph\\ n>\n"
"leftItalicCorrectionI<\\ ps-glyph\\ n>\n"
"subscriptCorrectionI<\\ ps-glyph\\ n>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"where I<ps-glyph> is the PostScript glyph name, and I<n> is the desired "
"value of the corresponding parameter in thousandths of an em."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Such parameters are normally needed only for italic (or oblique)  fonts."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<-s> option should be given if the font is \\[lq]special\\[rq], meaning "
"that I<groff> should search it whenever a glyph is not found in the current "
"font."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In that case, I<font-description-file> should be listed as an argument to "
"the B<fonts> directive in the output device's I<DESC> file; if it is not "
"special, there is no need to do so, since E<.MR \\%troff 1> will "
"automatically mount it when it is first used."
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Options"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<--help> displays a usage message, while B<-v> and B<\\%--version> show "
"version information; all exit afterward."
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a\\ >I<slant>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Use I<slant> as the slant (\\[lq]angle\\[rq]) parameter in the font "
"description file; this is used by I<groff> in the positioning of accents."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"By default I<\\%afmtodit> uses the negative of the B<\\%ItalicAngle> "
"specified in the AFM file; with true italic fonts it is sometimes desirable "
"to use a slant that is less than this."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you find that an italic font places accents over base glyphs too far to "
"the right, use B<-a> to give it a smaller slant."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Include comments in the font description file identifying the PostScript "
"font."
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d\\ >I<device-description-file>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The device description file is I<desc-file> rather than the default I<DESC>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If not found in the current directory, the I<devps> subdirectory of the "
"default font directory is searched (this is true for both the default device "
"description file and a file given with option B<-d>)."
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-e\\ >I<encoding-file>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The PostScript font should be reencoded to use the encoding described in "
"I<enc-file>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "The format of I<enc-file> is described in E<.MR grops 1 .>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If not found in the current directory, the I<devps> subdirectory of the "
"default font directory is searched."
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f\\ >I<internal-name>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "The internal name of the I<groff> font is set to I<name>."
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i\\ >I<italic-correction-factor>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Generate an italic correction for each glyph so that its width plus its "
"italic correction is equal to I<italic-correction-factor> thousandths of an "
"em plus the amount by which the right edge of the glyph's bounding box is to "
"the right of its origin."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If this would result in a negative italic correction, use a zero italic "
"correction instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Also generate a subscript correction equal to the product of the tangent of "
"the slant of the font and four fifths of the x-height of the font."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If this would result in a subscript correction greater than the italic "
"correction, use a subscript correction equal to the italic correction "
"instead."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Also generate a left italic correction for each glyph equal to I<italic-"
"correction-factor> thousandths of an em plus the amount by which the left "
"edge of the glyph's bounding box is to the left of its origin."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The left italic correction may be negative unless option B<-m> is given."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "This option is normally needed only with italic (or oblique)  fonts."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The font description files distributed with I<groff> were created using an "
"option of B<-i50> for italic fonts."
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-o\\ >I<output-file>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Write to I<output-file> instead of I<font-description-file.>"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-k>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Omit any kerning data from the I<groff> font; use only for monospaced "
"(constant-width) fonts."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-m>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Prevent negative left italic correction values."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Font description files for roman styles distributed with I<groff> were "
"created with \\[lq]B<-i0\\ -m>\\[rq] to improve spacing with E<.MR \\%eqn "
"1 .>"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-n>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Don't output a B<ligatures> command for this font; use with monospaced "
"(constant-width) fonts."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-s>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Add the B<special> directive to the font description file."
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-w\\ >I<space-width>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Use I<space-width> as the with of inter-word spaces."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-x>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Don't use the built-in Adobe Glyph List."
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Files"
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%font/\\:\\%devps/\\:DESC>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "describes the B<ps> output device."
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%font/\\:\\%devps/>F"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "describes the font known as\\ I<F> on device B<ps>."
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%font/\\:\\%devps/\\:\\%download>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"lists fonts available for embedding within the PostScript document (or "
"download to the device)."
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%font/\\:\\%devps/\\:\\%generate/\\:\\%dingbats.map>"
msgstr ""

#. type: TQ
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%font/\\:\\%devps/\\:\\%generate/\\:\\%dingbats-reversed.map>"
msgstr ""

#. type: TQ
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%font/\\:\\%devps/\\:\\%generate/\\:\\%slanted-symbol.map>"
msgstr ""

#. type: TQ
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%font/\\:\\%devps/\\:\\%generate/\\:\\%symbol.map>"
msgstr ""

#. type: TQ
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%font/\\:\\%devps/\\:\\%generate/\\:\\%text.map>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"map names in the Adobe Glyph List to I<groff> special character identifiers "
"for Zapf Dingbats (B<ZD>), reversed Zapf Dingbats (B<ZDR>), slanted symbol "
"(B<SS>), symbol (B<S>), and text fonts, respectively."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These I<map-file>s are used to produce the font description files provided "
"with I<groff> for the I<\\%grops> output driver."
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Diagnostics"
msgstr ""

#. type: TP
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AGL name \\[aq]I<x>\\[aq] already mapped to groff name \\[aq]I<y>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\[aq]; ignoring AGL name \\[aq]uniI<XXXX>\\[aq] You can disregard these if "
"they're in the form shown, where the ignored AGL name contains four "
"hexadecimal digits I<XXXX>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Adobe Glyph List (AGL) has its own names for glyphs; they are often "
"different from I<groff>'s special character names."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<\\%afmtodit> is constructing a mapping from I<groff> special character "
"names to AGL names; this can be a one-to-one or many-to-one mapping, but one-"
"to-many will not work, so I<\\%afmtodit> discards the excess mappings."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For example, if I<x> is B<*D>, I<y> is B<\\%Delta>, and I<z> is B<uni0394>, "
"I<\\%afmtodit> is telling you that the I<groff> font description that it is "
"writing cannot map the I<groff> special character B<\\[rs][*D]> to AGL "
"glyphs B<\\%Delta> and B<uni0394> at the same time."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you get a message like this but are unhappy with which mapping is "
"ignored, a remedy is to craft an alternative I<map-file> and re-run I<\\"
"%afmtodit> using it."
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "See also"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I<Groff: The GNU Implementation of troff>, by Trent A.\\& Fisher and Werner "
"Lemberg, is the primary I<groff> manual."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Section \\[lq]Using Symbols\\[rq] may be of particular note."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"You can browse it interactively with \\[lq]info \\[aq](groff)Using \\"
"%Symbols\\[aq]\\[rq]."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "E<.MR groff 1 ,> E<.MR gropdf 1 ,> E<.MR grops 1 ,> E<.MR groff_font 5>"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "AFMTODIT"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "7 March 2023"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "groff 1.22.4"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "afmtodit - create font files for use with groff -Tps and -Tpdf"
msgstr ""

#.  ====================================================================
#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "-ckmnsx"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "-a"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "n"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "-d"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "desc-file"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "-e"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "enc-file"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "-f"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "internal-name"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "-i"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "-o"
msgstr ""

#. type: OP
#: debian-bookworm
#, no-wrap
msgid "output-file"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<afm-file> I<map-file> I<font>"
msgstr ""

#.  ====================================================================
#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<afmtodit> creates a font file for use with I<groff>, I<grops>, and "
"I<gropdf>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<afmtodit> is written in Perl; you must have Perl version 5.004 or newer "
"installed in order to run I<afmtodit>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<afm-file> is the AFM (Adobe Font Metric) file for the font."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<map-file> is a file that says which I<groff> character names map onto each "
"PostScript character name; this file should contain a sequence of lines of "
"the form"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<ps-char groff-char>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"where I<ps-char> is the PostScript name of the character and I<groff-char> "
"is the groff name of the character (as used in the I<groff> font file)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The same I<ps-char> can occur multiple times in the file; each I<groff-char> "
"must occur at most once."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Lines starting with \\(oq#\\(cq and blank lines are ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If the file isn't found in the current directory, it is searched for in the "
"I<devps/generate> subdirectory of the default font directory."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If a PostScript character is not mentioned in I<map-file>, and a generic "
"I<groff> glyph name can't be deduced using the Adobe Glyph List (AGL, built "
"into I<afmtodit>), then I<afmtodit> puts the PostScript character into the "
"I<groff> font file as an unnamed character which can only be accessed by the "
"\\(oq\\eN\\(cq escape sequence in a I<roff> document."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"In particular, this is true for glyph variants named in the form \\"
"(lqfoo.bar\\(rq; all glyph names containing one or more periods are mapped "
"to unnamed entities."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If option B<-e> is not specified, the encoding defined in the AFM file "
"(i.e., entries with non-negative character codes) is used."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Refer to section \\(lqUsing Symbols\\(rq in I<Groff: The GNU Implementation "
"of troff>, the I<groff> Texinfo manual, which describes how I<groff> glyph "
"names are constructed."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Characters not encoded in the AFM file (i.e., entries which have \\(oq-1\\"
"(cq as the character code) are still available in I<groff>; they get glyph "
"index values greater than 255 (or greater than the biggest character code "
"used in the AFM file in the unlikely case that it is greater than 255) in "
"the I<groff> font file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Glyph indices of unencoded characters don't have a specific order; it is "
"best to access them with glyph names only."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<groff> font file will be output to a file called I<font>, unless the "
"B<-o> option is used."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If there is a downloadable font file for the font, it may be listed in the "
"file I</usr/\\:share/\\:groff/\\:1.22.4/\\:font/\\:devps/\\:download>; see "
"I<grops>(1)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If the B<-i> option is used, I<afmtodit> will automatically generate an "
"italic correction, a left italic correction and a subscript correction for "
"each character (the significance of these parameters is explained in "
"I<groff_font>(5)); these parameters may be specified for individual "
"characters by adding to the I<afm-file> lines of the form:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"B<italicCorrection>I<\\ ps-char\\ n>\n"
"B<leftItalicCorrection>I<\\ ps-char\\ n>\n"
"B<subscriptCorrection>I<\\ ps-char\\ n>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"where I<ps-char> is the PostScript name of the character, and I<n> is the "
"desired value of the corresponding parameter in thousandths of an em."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"These parameters are normally needed only for italic (or oblique) fonts."
msgstr ""

#.  ====================================================================
#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Whitespace is permitted between a command-line option and its argument."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-a>I<n>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Use I<n> as the slant parameter in the font file; this is used by I<groff> "
"in the positioning of accents."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"By default I<afmtodit> uses the negative of the B<ItalicAngle> specified in "
"the AFM file; with true italic fonts it is sometimes desirable to use a "
"slant that is less than this."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If you find that characters from an italic font have accents placed too far "
"to the right over them, then use the B<-a> option to give the font a smaller "
"slant."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Include comments in the font file in order to identify the PostScript font."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-d>I<desc-file>"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-e>I<enc-file>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The format of I<enc-file> is described in I<grops>(1)."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-f>I<name>"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-i>I<n>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Generate an italic correction for each character so that the character's "
"width plus the character's italic correction is equal to I<n> thousandths of "
"an em plus the amount by which the right edge of the character's bounding "
"box is to the right of the character's origin."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Also generate a left italic correction for each character equal to I<n> "
"thousandths of an em plus the amount by which the left edge of the "
"character's bounding box is to the left of the character's origin."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This option is normally needed only with italic (or oblique) fonts."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The font files distributed with I<groff> were created using an option of B<-"
"i50> for italic fonts."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-o>I<output-file>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The output file is I<output-file> instead of I<font>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Roman font files distributed with I<groff> were created with B<-i0\\ -m> to "
"improve spacing with I<eqn>(1)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The font is special."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The effect of this option is to add the B<special> command to the font file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#.  ====================================================================
#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILES"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "I</usr/\\:share/\\:groff/\\:1.22.4/\\:font/\\:devps/\\:DESC>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Device description file."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "I</usr/\\:share/\\:groff/\\:1.22.4/\\:font/\\:devps/\\:>F"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Font description file for font I<F>."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "I</usr/\\:share/\\:groff/\\:1.22.4/\\:font/\\:devps/\\:download>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List of downloadable fonts."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "I</usr/\\:share/\\:groff/\\:1.22.4/\\:font/\\:devps/\\:text.enc>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Encoding used for text fonts."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "I</usr/\\:share/\\:groff/\\:1.22.4/\\:font/\\:devps/\\:generate/\\:textmap>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Standard mapping."
msgstr ""

#.  ====================================================================
#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Section \\(lqUsing Symbols\\(rq may be of particular note."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"You can browse it interactively with \\(lqinfo \\(aq(groff)Using Symbols\\"
"(aq\\(rq."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<groff>(1), I<gropdf>(1), I<grops>(1), I<groff_font>(5), I<perl>(1)"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "5 December 2024"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "13 September 2024"
msgstr ""
