# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:41+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "HEXDUMP"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"hexdump - display file contents in hexadecimal, decimal, octal, or ascii"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<hexdump> I<options file> ..."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<hd> I<options file> ..."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<hexdump> utility is a filter which displays the specified files, or "
"standard input if no files are specified, in a user-specified format."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Below, the I<length> and I<offset> arguments may be followed by the "
"multiplicative suffixes KiB (=1024), MiB (=1024*1024), and so on for GiB, "
"TiB, PiB, EiB, ZiB and YiB (the \"iB\" is optional, e.g., \"K\" has the same "
"meaning as \"KiB\"), or the suffixes KB (=1000), MB (=1000*1000), and so on "
"for GB, TB, PB, EB, ZB and YB."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--one-byte-octal>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<One-byte octal display>. Display the input offset in hexadecimal, followed "
"by sixteen space-separated, three-column, zero-filled bytes of input data, "
"in octal, per line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-c>, B<--one-byte-char>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<One-byte character display>. Display the input offset in hexadecimal, "
"followed by sixteen space-separated, three-column, space-filled characters "
"of input data per line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-C>, B<--canonical>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<Canonical hex+ASCII display>. Display the input offset in hexadecimal, "
"followed by sixteen space-separated, two-column, hexadecimal bytes, followed "
"by the same sixteen bytes in B<%_p> format enclosed in B<|> characters. "
"Invoking the program as B<hd> implies this option."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-d>, B<--two-bytes-decimal>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<Two-byte decimal display>. Display the input offset in hexadecimal, "
"followed by eight space-separated, five-column, zero-filled, two-byte units "
"of input data, in unsigned decimal, per line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-e>, B<--format> I<format_string>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specify a format string to be used for displaying data."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-f>, B<--format-file> I<file>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify a file that contains one or more newline-separated format strings. "
"Empty lines and lines whose first non-blank character is a hash mark (#) are "
"ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-L>, B<--color>[=I<when>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Accept color units for the output. The optional argument I<when> can be "
"B<auto>, B<never> or B<always>. If the I<when> argument is omitted, it "
"defaults to B<auto>. The colors can be disabled; for the current built-in "
"default see the B<--help> output. See also the B<Colors> subsection and the "
"B<COLORS> section below."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--length> I<length>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Interpret only I<length> bytes of input."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--two-bytes-octal>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<Two-byte octal display>. Display the input offset in hexadecimal, followed "
"by eight space-separated, six-column, zero-filled, two-byte quantities of "
"input data, in octal, per line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--skip> I<offset>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Skip I<offset> bytes from the beginning of the input."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-v>, B<--no-squeezing>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<-v> option causes B<hexdump> to display all input data. Without the B<-"
"v> option, any number of groups of output lines which would be identical to "
"the immediately preceding group of output lines (except for the input "
"offsets), are replaced with a line comprised of a single asterisk."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-x>, B<--two-bytes-hex>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<Two-byte hexadecimal display>. Display the input offset in hexadecimal, "
"followed by eight space-separated, four-column, zero-filled, two-byte "
"quantities of input data, in hexadecimal, per line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"For each input file, B<hexdump> sequentially copies the input to standard "
"output, transforming the data according to the format strings specified by "
"the B<-e> and B<-f> options, in the order that they were specified."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FORMATS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"A format string contains any number of format units, separated by "
"whitespace. A format unit contains up to three items: an iteration count, a "
"byte count, and a format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The iteration count is an optional positive integer, which defaults to one. "
"Each format is applied iteration count times."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The byte count is an optional positive integer. If specified it defines the "
"number of bytes to be interpreted by each iteration of the format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If an iteration count and/or a byte count is specified, a single slash must "
"be placed after the iteration count and/or before the byte count to "
"disambiguate them. Any whitespace before or after the slash is ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The format is required and must be surrounded by double quote (\" \") marks. "
"It is interpreted as a fprintf-style format string (see B<fprintf>(3)), with "
"the following exceptions:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "1."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "An asterisk (*) may not be used as a field width or precision."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "2."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"A byte count or field precision I<is> required for each B<s> conversion "
"character (unlike the B<fprintf>(3) default which prints the entire string "
"if the precision is unspecified)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "3."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The conversion characters B<h>, B<l>, B<n>, B<p>, and B<q> are not supported."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "4."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The single character escape sequences described in the C standard are "
"supported:"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid ".sp\n"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "NULL"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "\\(rs0"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "E<lt>alert characterE<gt>"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "\\(rsa"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "E<lt>backspaceE<gt>"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "\\(rsb"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "E<lt>form-feedE<gt>"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "\\(rsf"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "E<lt>newlineE<gt>"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "\\(rsn"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "E<lt>carriage returnE<gt>"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "\\(rsr"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "E<lt>tabE<gt>"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "\\(rst"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "E<lt>vertical tabE<gt>"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "\\(rsv"
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Conversion strings"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<hexdump> utility also supports the following additional conversion "
"strings."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<_a[dox]>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display the input offset, cumulative across input files, of the next byte to "
"be displayed. The appended characters B<d>, B<o>, and B<x> specify the "
"display base as decimal, octal or hexadecimal respectively."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<_A[dox]>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Almost identical to the B<_a> conversion string except that it is only "
"performed once, when all of the input data has been processed."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<_c>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Output characters in the default character set. Non-printing characters are "
"displayed in three-character, zero-padded octal, except for those "
"representable by standard escape notation (see above), which are displayed "
"as two-character strings."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<_p>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Output characters in the default character set. Non-printing characters are "
"displayed as a single \\(aqB<.>\\(aq."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<_u>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Output US ASCII characters, with the exception that control characters are "
"displayed using the following, lower-case, names. Characters greater than "
"0xff, hexadecimal, are displayed as hexadecimal strings."
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "000 nul"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "001 soh"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "002 stx"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "003 etx"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "004 eot"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "005 enq"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "006 ack"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "007 bel"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "008 bs"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "009 ht"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "00A lf"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "00B vt"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "00C ff"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "00D cr"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "00E so"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "00F si"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "010 dle"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "011 dc1"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "012 dc2"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "013 dc3"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "014 dc4"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "015 nak"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "016 syn"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "017 etb"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "018 can"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "019 em"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "01A sub"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "01B esc"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "01C fs"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "01D gs"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "01E rs"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "01F us"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "0FF del"
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Colors"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When put at the end of a format specifier, B<hexdump> highlights the "
"respective string with the color specified. Conditions, if present, are "
"evaluated prior to highlighting."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<_L[color_unit_1,color_unit_2,...,color_unit_n]>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The full syntax of a color unit is as follows:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<[!]COLOR[:VALUE][@OFFSET_START[-END]]>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<!>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Negate the condition. Please note that it only makes sense to negate a unit "
"if both a value/string and an offset are specified. In that case the "
"respective output string will be highlighted if and only if the value/string "
"does not match the one at the offset."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<COLOR>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "One of the 8 basic shell colors."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<VALUE>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"A value to be matched specified in hexadecimal, or octal base, or as a "
"string. Please note that the usual C escape sequences are not interpreted by "
"B<hexdump> inside the color_units."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<OFFSET>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"An offset or an offset range at which to check for a match. Please note that "
"lone OFFSET_START uses the same value as END offset."
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Counters"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default and supported byte counts for the conversion characters are as "
"follows:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<%_c>, B<%_p>, B<%_u>, B<%c>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "One byte counts only."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<%d>, B<%i>, B<%o>, B<%u>, B<%X>, B<%x>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Four byte default, one, two and four byte counts supported."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<%E>, B<%e>, B<%f>, B<%G>, B<%g>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Eight byte default, four byte counts supported."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The amount of data interpreted by each format string is the sum of the data "
"required by each format unit, which is the iteration count times the byte "
"count, or the iteration count times the number of bytes required by the "
"format if the byte count is not specified."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The input is manipulated in I<blocks>, where a block is defined as the "
"largest amount of data specified by any format string. Format strings "
"interpreting less than an input block\\(cqs worth of data, whose last format "
"unit both interprets some number of bytes and does not have a specified "
"iteration count, have the iteration count incremented until the entire input "
"block has been processed or there is not enough data remaining in the block "
"to satisfy the format string."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If, either as a result of user specification or B<hexdump> modifying the "
"iteration count as described above, an iteration count is greater than one, "
"no trailing whitespace characters are output during the last iteration."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"It is an error to specify a byte count as well as multiple conversion "
"characters or strings unless all but one of the conversion characters or "
"strings is B<_a> or B<_A>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If, as a result of the specification of the B<-n> option or end-of-file "
"being reached, input data only partially satisfies a format string, the "
"input block is zero-padded sufficiently to display all available data (i.e., "
"any format units overlapping the end of data will display some number of the "
"zero bytes)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Further output by such format strings is replaced by an equivalent number of "
"spaces. An equivalent number of spaces is defined as the number of spaces "
"output by an B<s> conversion character with the same field width and "
"precision as the original conversion character or conversion string but with "
"any \\(aqB<+>\\(aq, \\(aq \\(aq, \\(aqB<#>\\(aq conversion flag characters "
"removed, and referencing a NULL string."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If no format strings are specified, the default display is very similar to "
"the B<-x> output format (the B<-x> option causes more space to be used "
"between format units than in the default output)."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<hexdump> exits 0 on success and E<gt> 0 if an error occurred."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<hexdump> utility is expected to be IEEE Std 1003.2 (\"POSIX.2\") "
"compatible."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display the input in perusal format:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"   \"%06.6_ao \"  12/1 \"%3_u \"\n"
"   \"\\(rst\" \"%_p \"\n"
"   \"\\(rsn\"\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Implement the B<-x> option:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"   \"%07.7_Ax\\(rsn\"\n"
"   \"%07.7_ax  \" 8/2 \"%04x \" \"\\(rsn\"\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"MBR Boot Signature example: Highlight the addresses cyan and the bytes at "
"offsets 510 and 511 green if their value is 0xAA55, red otherwise."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"   \"%07.7_Ax_L[cyan]\\(rsn\"\n"
"   \"%07.7_ax_L[cyan]  \" 8/2 \"   %04x_L[green:0xAA55@510-511,!red:0xAA55@510-511] \" \"\\(rsn\"\n"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "COLORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The output colorization is implemented by B<terminal-colors.d>(5) "
"functionality.  Implicit coloring can be disabled by an empty file"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I</etc/terminal-colors.d/hexdump.disable>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "for the B<hexdump> command or for all tools by"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I</etc/terminal-colors.d/disable>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The user-specific I<$XDG_CONFIG_HOME/terminal-colors.d> or I<$HOME/.config/"
"terminal-colors.d> overrides the global setting."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that the output colorization may be enabled by default, and in this "
"case I<terminal-colors.d> directories do not have to exist yet."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<hexdump> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
