# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-09-06 18:36+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Yuvsplittoppm User Manual"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "26 August 93"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "yuvsplittoppm - convert separate Y, U, and V files into a PPM image"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<yuvsplittoppm >"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<basename> I<width> I<height> [B<-ccir601>]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<yuvsplittoppm> reads three files, containing the YUV components, as "
"input.  These files are I<basename>.Y, I<basename>.U, and I<basename>.V.  "
"Produces a PPM image on Standard Output."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since the YUV files are raw files, the dimensions I<width> and I<height> "
"must be specified on the command line."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"In addition to the options common to all programs based on libnetpbm\n"
"(most notably B<-quiet>, see \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&), B<yuvsplittoppm> recognizes the following\n"
"command line option:\n"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-ccir601>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Assumes that the YUV triplets are scaled into the smaller range of the CCIR "
"601 (MPEG) standard. Else, the JFIF (JPEG) standard is assumed."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "B<ppmtoyuvsplit>(1)  \\&, B<yuvtoppm>(1)  \\&, B<ppm>(1)  \\&"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Marcel Wijkstra E<lt>I<wijkstra@fwi.uva.nl>E<gt>, based on B<ppmtoyuvsplit>."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/yuvsplittoppm.html>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<ppmtoyuvsplit>(1)  \\&, B<yuvtoppm>(1)  \\&, B<ppm>(5)  \\&"
msgstr ""
