# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 19:51+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "RENAME"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "rename - rename files"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<rename> [options] I<expression replacement file>..."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<rename> will rename the specified files by replacing the first occurrence "
"of I<expression> in their name by I<replacement>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--symlink>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do not rename a symlink but its target."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Show which files were renamed, if any."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--no-act>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do not make any changes; add B<--verbose> to see what would be made."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Replace all occurrences of I<expression> rather than only the first one."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-l>, B<--last>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Replace the last occurrence of I<expression> rather than the first one."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--no-overwrite>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Do not overwrite existing files. When B<--symlink> is active, do not "
"overwrite symlinks pointing to existing targets."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-i>, B<--interactive>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Ask before overwriting existing files."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "WARNING"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The renaming has no safeguards by default or without any one of the options "
"B<--no-overwrite>, B<--interactive> or B<--no-act>. If the user has "
"permission to rewrite file names, the command will perform the action "
"without any questions. For example, the result can be quite drastic when the "
"command is run as root in the I</lib> directory. Always make a backup before "
"running the command, unless you truly know what you are doing."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "INTERACTIVE MODE"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"As most standard utilities rename can be used with a terminal device (tty in "
"short) in canonical mode, where the line is buffered by the tty and you "
"press ENTER to validate the user input. If you put your tty in cbreak mode "
"however, rename requires only a single key press to answer the prompt. To "
"set cbreak mode, run for example:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "sh -c \\(aqstty -icanon min 1; \"$0\" \"$@\"; stty icanon\\(aq rename -i from to files\n"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<0>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "all requested rename operations were successful"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<1>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "all rename operations failed"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<2>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "some rename operations failed"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<4>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "nothing was renamed"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<64>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "unanticipated error occurred"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Given the files I<foo1>, ..., I<foo9>, I<foo10>, ..., I<foo278>, the commands"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"rename foo foo00 foo?\n"
"rename foo foo0 foo??\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"will turn them into I<foo001>, ..., I<foo009>, I<foo010>, ..., I<foo278>. And"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "rename .htm .html *.htm\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"will fix the extension of your html files. Provide an empty string for "
"shortening:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "rename \\(aq_with_long_name\\(aq \\(aq\\(aq file_with_long_name.*\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "will remove the substring in the filenames."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<mv>(1)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<rename> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
