# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 18:21+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "YUM-BUILDDEP"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Jan 22, 2023"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "4.3.1"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "dnf-plugins-core"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "yum-builddep - redirecting to DNF builddep Plugin"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Install whatever is needed to build the given .src.rpm, .nosrc.rpm or .spec "
"file."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<WARNING:>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Build dependencies in a package (i.e. src.rpm) might be different than you "
"would expect because they were evaluated according macros set on the package "
"build host."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<dnf builddep E<lt>packageE<gt>...>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "ARGUMENTS"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<E<lt>packageE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Either path to .src.rpm, .nosrc.rpm or .spec file or package available in a "
"repository."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"All general DNF options are accepted, see I<Options> in B<dnf(8)> for "
"details."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--help-cmd>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Show this help."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-D E<lt>macro exprE<gt>, --define E<lt>macro exprE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Define the RPM macro named I<macro> to the value I<expr> when parsing spec "
"files. Does not apply for source rpm files."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--spec>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Treat arguments as .spec files."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--srpm>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Treat arguments as source rpm files."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--skip-unavailable>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Skip build dependencies not available in repositories. All available build "
"dependencies will be installed."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Note that I<builddep> command does not honor the I<–skip-broken> option, so "
"there is no way to skip uninstallable packages (e.g. with broken "
"dependencies)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<dnf builddep foobar.spec>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Install the needed build requirements, defined in the foobar.spec file."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<dnf builddep --spec foobar.spec.in>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Install the needed build requirements, defined in the spec file when "
"filename ends with something different than B<\\&.spec>\\&."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<dnf builddep foobar-1.0-1.src.rpm>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Install the needed build requirements, defined in the foobar-1.0-1.src.rpm "
"file."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<dnf builddep foobar-1.0-1>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Look up foobar-1.0-1 in enabled repositories and install build requirements "
"for its source rpm."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<dnf builddep -D \\(aqscl python27\\(aq python-foobar.spec>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Install the needed build requirements for the python27 SCL version of python-"
"foobar."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "See AUTHORS in your Core DNF Plugins distribution"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-bookworm
msgid "2023, Red Hat, Licensed under GPLv2+"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "Aug 22, 2024"
msgstr ""

#. type: TH
#: debian-unstable fedora-41
#, no-wrap
msgid "4.9.0"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide
msgid "2024, Red Hat, Licensed under GPLv2+"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "Aug 15, 2024"
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide
msgid ""
"Note that I<builddep> command does not honor the I<--skip-broken> option, so "
"there is no way to skip uninstallable packages (e.g. with broken "
"dependencies)."
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "Nov 12, 2024"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "4.10.0"
msgstr ""
