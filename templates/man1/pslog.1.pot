# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-08-02 17:27+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "PSLOG"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "2020-09-09"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "Linux\""
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "Linux User's Manual"
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "pslog - report current logs path of a process"
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "B<pslog> I<pid> \\&...\""
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "B<pslog -V>"
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "The B<pslog> command reports the current working logs of a process."
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: opensuse-leap-16-0
#, no-wrap
msgid "B<-V>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "Display version information."
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "B<pgrep>(1), B<ps>(1), B<pwdx>(1)."
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Vito Mule\\(cq E<.MT mulevito@gmail.com> E<.ME> wrote B<pslog> in 2015. "
"Please send bug reports to E<.MT mulevito@gmail.com> E<.ME .>"
msgstr ""
