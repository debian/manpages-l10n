# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Dd
#: debian-bookworm debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "April 25, 2003"
msgstr ""

#. type: Dt
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "WTF 6"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "E<.Nm wtf>"
msgstr ""

#. type: Nd
#: debian-bookworm debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "translates acronyms for you"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-tumbleweed
msgid ""
"E<.Nm> E<.Op Fl f Ar dbfile> E<.Op Fl t Ar type> E<.Op Ar is> E<.Ar acronym "
"Ar ...>"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-tumbleweed
msgid ""
"The E<.Nm> utility displays the expansion of the acronyms specified on the "
"command line.  If the acronym is unknown, E<.Nm> will check to see if the "
"acronym is known by the E<.Xr whatis 1> command."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-tumbleweed
msgid ""
"If E<.Dq is> is specified on the command line, it will be ignored, allowing "
"the fairly natural E<.Dq wtf is WTF> usage."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-tumbleweed
msgid "The following options are available:"
msgstr ""

#. type: It
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Fl f Ar dbfile"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-tumbleweed
msgid ""
"Overrides the default acronym database, bypassing the value of the E<.Ev "
"ACRONYMDB> variable."
msgstr ""

#. type: It
#: debian-bookworm debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Fl t Ar type"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Specifies the acronym's type.  Simply put, it makes the program use the "
"acronyms database named E<.Pa /usr/share/games/bsdgames/acronyms.type>, "
"where E<.Ar type> is given by the argument."
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: It
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Ev ACRONYMDB"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-tumbleweed
msgid ""
"The default acronym database may be overridden by setting the environment "
"variable E<.Ev ACRONYMDB> to the name of a file in the proper format "
"(acronym[tab]meaning)."
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr ""

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Pa /usr/share/games/bsdgames/acronyms"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "default acronym database."
msgstr ""

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Pa /usr/share/games/bsdgames/acronyms.comp"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-tumbleweed
msgid "computer-related acronym database."
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-tumbleweed
msgid "E<.Xr whatis 1>"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-tumbleweed
msgid "E<.Nm> first appeared in E<.Nx 1.5>."
msgstr ""

#. type: Dd
#: fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "April 22, 2015"
msgstr ""

#. type: Nd
#: fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "look up terms"
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid "E<.Nm> E<.Op Fl f Ar dbfile> E<.Op Fl o> E<.Op Ar is> E<.Ar term ...>"
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"The E<.Nm> utility looks up the meaning of one or more E<.Ar term> operands "
"specified on the command line."
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"E<.Ar term> will first be searched for as an acronym in the acronym "
"databases, which are expected to be in the format E<.Dq "
"acronym[tab]meaning>.  If no match has been found, E<.Nm> will check to see "
"if the term is known by E<.Xr whatis 1>, E<.Xr pkg_info 1>, or, when called "
"from within a pkgsrc package directory, pkgsrc's internal help facility, "
"E<.Dq make help topic=XXX>."
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"The optional E<.Ar is> operand will be ignored, allowing the fairly natural "
"E<.Dq wtf is WTF> usage."
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid "The following option is available:"
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"Overrides the default list of acronym databases, bypassing the value of the "
"E<.Ev ACRONYMDB> variable.  Unlike this variable the E<.Fl f> option only "
"accepts one file name as an argument, but it may be given multiple times to "
"specify more than one file to use."
msgstr ""

#. type: It
#: fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Fl o"
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"Include acronyms that could be considered offensive to some.  Please consult "
"E<.Xr fortune 6> for more information about the E<.Fl o> flag."
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"The default list of acronym databases may be overridden by setting the "
"environment variable E<.Ev ACRONYMDB> to the name of one or more space-"
"separated file names of acronym databases."
msgstr ""

#. type: It
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Pa /usr/share/misc/acronyms"
msgstr ""

#. type: It
#: fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Pa /usr/share/misc/acronyms-o"
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid "default offensive acronym database."
msgstr ""

#. type: It
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Pa /usr/share/misc/acronyms.comp"
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid "default computer-related acronym database."
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid "E<.Xr make 1>, E<.Xr pkg_info 1>, E<.Xr whatis 1>, E<.Xr fortune 6>"
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"E<.Nm> first appeared in E<.Nx 1.5>.  Initially it only translated acronyms; "
"functionality to look up the meaning of terms in other sources was added "
"later."
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"Specifies the acronym's type.  Simply put, it makes the program use the "
"acronyms database named E<.Pa /usr/share/misc/acronyms.type>, where E<.Ar "
"type> is given by the argument."
msgstr ""
