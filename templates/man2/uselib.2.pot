# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 18:20+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "uselib"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "uselib - load shared library"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] int uselib(const char *>I<library>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The system call B<uselib>()  serves to load a shared library to be used by "
"the calling process.  It is given a pathname.  The address where to load is "
"found in the library itself.  The library can have any recognized binary "
"format."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In addition to all of the error codes returned by B<open>(2)  and "
"B<mmap>(2), the following may also be returned:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The library specified by I<library> does not have read or execute "
"permission, or the caller does not have search permission for one of the "
"directories in the path prefix.  (See also B<path_resolution>(7).)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOEXEC>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The file specified by I<library> is not an executable of a known type; for "
"example, it does not have the correct magic numbers."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This obsolete system call is not supported by glibc.  No declaration is "
"provided in glibc headers, but, through a quirk of history, glibc before "
"glibc 2.23 did export an ABI for this system call.  Therefore, in order to "
"employ this system call, it was sufficient to manually declare the interface "
"in your code; alternatively, you could invoke the system call using "
"B<syscall>(2)."
msgstr ""

#. #-#-#-#-#  archlinux: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .PP
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .PP
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .PP
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  fedora-41: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: uselib.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .P
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .P
#.  glibc2 does not use this call.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In ancient libc versions (before glibc 2.0), B<uselib>()  was used to load "
"the shared libraries with names found in an array of names in the binary."
msgstr ""

#.  commit 69369a7003735d0d8ef22097e27a55a8bad9557a
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since Linux 3.15, this system call is available only when the kernel is "
"configured with the B<CONFIG_USELIB> option."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<ar>(1), B<gcc>(1), B<ld>(1), B<ldd>(1), B<mmap>(2), B<open>(2), "
"B<dlopen>(3), B<capabilities>(7), B<ld.so>(8)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-07"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<uselib>()  is Linux-specific, and should not be used in programs intended "
"to be portable."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
