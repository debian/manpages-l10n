# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 18:07+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "openat2"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "openat2 - open and possibly create a file (extended)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>>          /* Definition of B<O_*> and B<S_*> constants */\n"
"B<#include E<lt>linux/openat2.hE<gt>>  /* Definition of B<RESOLVE_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>    /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long syscall(SYS_openat2, int >I<dirfd>B<, const char *>I<pathname>B<,>\n"
"B<             struct open_how *>I<how>B<, size_t >I<size>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<openat2>(), necessitating the use "
"of B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<openat2>()  system call is an extension of B<openat>(2)  and provides "
"a superset of its functionality."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<openat2>()  system call opens the file specified by I<pathname>.  If "
"the specified file does not exist, it may optionally (if B<O_CREAT> is "
"specified in I<how.flags>)  be created."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"As with B<openat>(2), if I<pathname> is a relative pathname, then it is "
"interpreted relative to the directory referred to by the file descriptor "
"I<dirfd> (or the current working directory of the calling process, if "
"I<dirfd> is the special value B<AT_FDCWD>).  If I<pathname> is an absolute "
"pathname, then I<dirfd> is ignored (unless I<how.resolve> contains "
"B<RESOLVE_IN_ROOT>, in which case I<pathname> is resolved relative to "
"I<dirfd>)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Rather than taking a single I<flags> argument, an extensible structure "
"(I<how>) is passed to allow for future extensions.  The I<size> argument "
"must be specified as I<sizeof(struct open_how)>."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "The open_how structure"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<how> argument specifies how I<pathname> should be opened, and acts as "
"a superset of the I<flags> and I<mode> arguments to B<openat>(2).  This "
"argument is a pointer to an I<open_how> structure, described in "
"B<open_how>(2type)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Any future extensions to B<openat2>()  will be implemented as new fields "
"appended to the I<open_how> structure, with a zero value in a new field "
"resulting in the kernel behaving as though that extension field was not "
"present.  Therefore, the caller I<must> zero-fill this structure on "
"initialization.  (See the \"Extensibility\" section of the B<NOTES> for more "
"detail on why this is necessary.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The fields of the I<open_how> structure are as follows:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<flags>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This field specifies the file creation and file status flags to use when "
"opening the file.  All of the B<O_*> flags defined for B<openat>(2)  are "
"valid B<openat2>()  flag values."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Whereas B<openat>(2)  ignores unknown bits in its I<flags> argument, "
"B<openat2>()  returns an error if unknown or conflicting flags are specified "
"in I<how.flags>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<mode>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This field specifies the mode for the new file, with identical semantics to "
"the I<mode> argument of B<openat>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Whereas B<openat>(2)  ignores bits other than those in the range I<07777> in "
"its I<mode> argument, B<openat2>()  returns an error if I<how.mode> contains "
"bits other than I<07777>.  Similarly, an error is returned if B<openat2>()  "
"is called with a nonzero I<how.mode> and I<how.flags> does not contain "
"B<O_CREAT> or B<O_TMPFILE>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<resolve>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is a bit-mask of flags that modify the way in which B<all> components "
"of I<pathname> will be resolved.  (See B<path_resolution>(7)  for background "
"information.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The primary use case for these flags is to allow trusted programs to "
"restrict how untrusted paths (or paths inside untrusted directories) are "
"resolved.  The full list of I<resolve> flags is as follows:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RESOLVE_BENEATH>"
msgstr ""

#.  commit adb21d2b526f7f196b2f3fdca97d80ba05dd14a0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Do not permit the path resolution to succeed if any component of the "
"resolution is not a descendant of the directory indicated by I<dirfd>.  This "
"causes absolute symbolic links (and absolute values of I<pathname>)  to be "
"rejected."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Currently, this flag also disables magic-link resolution (see below).  "
"However, this may change in the future.  Therefore, to ensure that magic "
"links are not resolved, the caller should explicitly specify "
"B<RESOLVE_NO_MAGICLINKS>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RESOLVE_IN_ROOT>"
msgstr ""

#.  commit 8db52c7e7ee1bd861b6096fcafc0fe7d0f24a994
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Treat the directory referred to by I<dirfd> as the root directory while "
"resolving I<pathname>.  Absolute symbolic links are interpreted relative to "
"I<dirfd>.  If a prefix component of I<pathname> equates to I<dirfd>, then an "
"immediately following I<..\\&> component likewise equates to I<dirfd> (just "
"as I</..\\&> is traditionally equivalent to I</>).  If I<pathname> is an "
"absolute path, it is also interpreted relative to I<dirfd>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The effect of this flag is as though the calling process had used "
"B<chroot>(2)  to (temporarily) modify its root directory (to the directory "
"referred to by I<dirfd>).  However, unlike B<chroot>(2)  (which changes the "
"filesystem root permanently for a process), B<RESOLVE_IN_ROOT> allows a "
"program to efficiently restrict path resolution on a per-open basis."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Currently, this flag also disables magic-link resolution.  However, this may "
"change in the future.  Therefore, to ensure that magic links are not "
"resolved, the caller should explicitly specify B<RESOLVE_NO_MAGICLINKS>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RESOLVE_NO_MAGICLINKS>"
msgstr ""

#.  commit 278121417a72d87fb29dd8c48801f80821e8f75a
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Disallow all magic-link resolution during path resolution."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Magic links are symbolic link-like objects that are most notably found in "
"B<proc>(5); examples include I</proc/>pidI</exe> and I</proc/>pidI</fd/*>.  "
"(See B<symlink>(7)  for more details.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Unknowingly opening magic links can be risky for some applications.  "
"Examples of such risks include the following:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the process opening a pathname is a controlling process that currently "
"has no controlling terminal (see B<credentials>(7)), then opening a magic "
"link inside I</proc/>pidI</fd> that happens to refer to a terminal would "
"cause the process to acquire a controlling terminal."
msgstr ""

#.  From https://lwn.net/Articles/796868/:
#.      The presence of this flag will prevent a path lookup operation
#.      from traversing through one of these magic links, thus blocking
#.      (for example) attempts to escape from a container via a /proc
#.      entry for an open file descriptor.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In a containerized environment, a magic link inside I</proc> may refer to an "
"object outside the container, and thus may provide a means to escape from "
"the container."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Because of such risks, an application may prefer to disable magic link "
"resolution using the B<RESOLVE_NO_MAGICLINKS> flag."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the trailing component (i.e., basename) of I<pathname> is a magic link, "
"I<how.resolve> contains B<RESOLVE_NO_MAGICLINKS>, and I<how.flags> contains "
"both B<O_PATH> and B<O_NOFOLLOW>, then an B<O_PATH> file descriptor "
"referencing the magic link will be returned."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RESOLVE_NO_SYMLINKS>"
msgstr ""

#.  commit 278121417a72d87fb29dd8c48801f80821e8f75a
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Disallow resolution of symbolic links during path resolution.  This option "
"implies B<RESOLVE_NO_MAGICLINKS>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the trailing component (i.e., basename) of I<pathname> is a symbolic "
"link, I<how.resolve> contains B<RESOLVE_NO_SYMLINKS>, and I<how.flags> "
"contains both B<O_PATH> and B<O_NOFOLLOW>, then an B<O_PATH> file descriptor "
"referencing the symbolic link will be returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that the effect of the B<RESOLVE_NO_SYMLINKS> flag, which affects the "
"treatment of symbolic links in all of the components of I<pathname>, differs "
"from the effect of the B<O_NOFOLLOW> file creation flag (in I<how.flags>), "
"which affects the handling of symbolic links only in the final component of "
"I<pathname>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Applications that employ the B<RESOLVE_NO_SYMLINKS> flag are encouraged to "
"make its use configurable (unless it is used for a specific security "
"purpose), as symbolic links are very widely used by end-users.  Setting this "
"flag indiscriminately\\[em]i.e., for purposes not specifically related to "
"security\\[em]for all uses of B<openat2>()  may result in spurious errors on "
"previously functional systems.  This may occur if, for example, a system "
"pathname that is used by an application is modified (e.g., in a new "
"distribution release)  so that a pathname component (now) contains a "
"symbolic link."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RESOLVE_NO_XDEV>"
msgstr ""

#.  commit 72ba29297e1439efaa54d9125b866ae9d15df339
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Disallow traversal of mount points during path resolution (including all "
"bind mounts).  Consequently, I<pathname> must either be on the same mount as "
"the directory referred to by I<dirfd>, or on the same mount as the current "
"working directory if I<dirfd> is specified as B<AT_FDCWD>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Applications that employ the B<RESOLVE_NO_XDEV> flag are encouraged to make "
"its use configurable (unless it is used for a specific security purpose), as "
"bind mounts are widely used by end-users.  Setting this flag "
"indiscriminately\\[em]i.e., for purposes not specifically related to "
"security\\[em]for all uses of B<openat2>()  may result in spurious errors on "
"previously functional systems.  This may occur if, for example, a system "
"pathname that is used by an application is modified (e.g., in a new "
"distribution release)  so that a pathname component (now) contains a bind "
"mount."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RESOLVE_CACHED>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Make the open operation fail unless all path components are already present "
"in the kernel's lookup cache.  If any kind of revalidation or I/O is needed "
"to satisfy the lookup, B<openat2>()  fails with the error B<EAGAIN>.  This "
"is useful in providing a fast-path open that can be performed without "
"resorting to thread offload, or other mechanisms that an application might "
"use to offload slower operations."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If any bits other than those listed above are set in I<how.resolve>, an "
"error is returned."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, a new file descriptor is returned.  On error, -1 is returned, "
"and I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The set of errors returned by B<openat2>()  includes all of the errors "
"returned by B<openat>(2), as well as the following additional errors:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<E2BIG>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An extension that this kernel does not support was specified in I<how>.  "
"(See the \"Extensibility\" section of B<NOTES> for more detail on how "
"extensions are handled.)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<how.resolve> contains either B<RESOLVE_IN_ROOT> or B<RESOLVE_BENEATH>, and "
"the kernel could not ensure that a \"..\" component didn't escape (due to a "
"race condition or potential attack).  The caller may choose to retry the "
"B<openat2>()  call."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<RESOLVE_CACHED> was set, and the open operation cannot be performed using "
"only cached information.  The caller should retry without B<RESOLVE_CACHED> "
"set in I<how.resolve>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An unknown flag or invalid value was specified in I<how>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<mode> is nonzero, but I<how.flags> does not contain B<O_CREAT> or "
"B<O_TMPFILE>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<size> was smaller than any known version of I<struct open_how>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<how.resolve> contains B<RESOLVE_NO_SYMLINKS>, and one of the path "
"components was a symbolic link (or magic link)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<how.resolve> contains B<RESOLVE_NO_MAGICLINKS>, and one of the path "
"components was a magic link."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EXDEV>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<how.resolve> contains either B<RESOLVE_IN_ROOT> or B<RESOLVE_BENEATH>, and "
"an escape from the root during path resolution was detected."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<how.resolve> contains B<RESOLVE_NO_XDEV>, and a path component crosses a "
"mount point."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#.  commit fddb5d430ad9fa91b49b1d34d0202ffe2fa0e179
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux 5.6."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The semantics of B<RESOLVE_BENEATH> were modeled after FreeBSD's "
"B<O_BENEATH>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Extensibility"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In order to allow for future extensibility, B<openat2>()  requires the user-"
"space application to specify the size of the I<open_how> structure that it "
"is passing.  By providing this information, it is possible for B<openat2>()  "
"to provide both forwards- and backwards-compatibility, with I<size> acting "
"as an implicit version number.  (Because new extension fields will always be "
"appended, the structure size will always increase.)  This extensibility "
"design is very similar to other system calls such as B<sched_setattr>(2), "
"B<perf_event_open>(2), and B<clone3>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If we let I<usize> be the size of the structure as specified by the user-"
"space application, and I<ksize> be the size of the structure which the "
"kernel supports, then there are three cases to consider:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<ksize> equals I<usize>, then there is no version mismatch and I<how> "
"can be used verbatim."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<ksize> is larger than I<usize>, then there are some extension fields "
"that the kernel supports which the user-space application is unaware of.  "
"Because a zero value in any added extension field signifies a no-op, the "
"kernel treats all of the extension fields not provided by the user-space "
"application as having zero values.  This provides backwards-compatibility."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<ksize> is smaller than I<usize>, then there are some extension fields "
"which the user-space application is aware of but which the kernel does not "
"support.  Because any extension field must have its zero values signify a no-"
"op, the kernel can safely ignore the unsupported extension fields if they "
"are all-zero.  If any unsupported extension fields are nonzero, then -1 is "
"returned and I<errno> is set to B<E2BIG>.  This provides forwards-"
"compatibility."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Because the definition of I<struct open_how> may change in the future (with "
"new fields being added when system headers are updated), user-space "
"applications should zero-fill I<struct open_how> to ensure that recompiling "
"the program with new headers will not result in spurious errors at run "
"time.  The simplest way is to use a designated initializer:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct open_how how = { .flags = O_RDWR,\n"
"                        .resolve = RESOLVE_IN_ROOT };\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or explicitly using B<memset>(3)  or similar:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct open_how how;\n"
"memset(&how, 0, sizeof(how));\n"
"how.flags = O_RDWR;\n"
"how.resolve = RESOLVE_IN_ROOT;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A user-space application that wishes to determine which extensions the "
"running kernel supports can do so by conducting a binary search on I<size> "
"with a structure which has every byte nonzero (to find the largest value "
"which doesn't produce an error of B<E2BIG>)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<openat>(2), B<open_how>(2type), B<path_resolution>(7), B<symlink>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Make the open operation fail unless all path components are already present "
"in the kernel's lookup cache.  If any kind of revalidation or I/O is needed "
"to satisfy the lookup, B<openat2>()  fails with the error B<EAGAIN .> This "
"is useful in providing a fast-path open that can be performed without "
"resorting to thread offload, or other mechanisms that an application might "
"use to offload slower operations."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"B<RESOLVE_CACHED> was set, and the open operation cannot be performed using "
"only cached information.  The caller should retry without B<RESOLVE_CACHED> "
"set in I<how.resolve .>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#.  commit fddb5d430ad9fa91b49b1d34d0202ffe2fa0e179
#. type: Plain text
#: debian-bookworm
msgid "B<openat2>()  first appeared in Linux 5.6."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This system call is Linux-specific."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Because the definition of I<struct open_how> may change in the future (with "
"new fields being added when system headers are updated), user-space "
"applications should zero-fill I<struct open_how> to ensure that recompiling "
"the program with new headers will not result in spurious errors at runtime.  "
"The simplest way is to use a designated initializer:"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
