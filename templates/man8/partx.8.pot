# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:33+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "PARTX"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"partx - tell the kernel about the presence and numbering of on-disk "
"partitions"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<partx> [B<-a>|B<-d>|B<-P>|B<-r>|B<-s>|B<-u>] [B<-t> I<type>] [B<-n> "
"I<M>:_N_] [-] I<disk>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<partx> [B<-a>|B<-d>|B<-P>|B<-r>|B<-s>|B<-u>] [B<-t> I<type>] I<partition> "
"[I<disk>]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Given a device or disk-image, B<partx> tries to parse the partition table "
"and list its contents. It can also tell the kernel to add or remove "
"partitions from its bookkeeping."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<disk> argument is optional when a I<partition> argument is provided. "
"To force scanning a partition as if it were a whole disk (for example to "
"list nested subpartitions), use the argument \"-\" (hyphen-minus). For "
"example:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "partx --show - /dev/sda3"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This will see sda3 as a whole-disk rather than as a partition."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<partx is not an fdisk program> - adding and removing partitions does not "
"change the disk, it just tells the kernel about the presence and numbering "
"of on-disk partitions."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--add>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Add the specified partitions, or read the disk and add all partitions."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-d>, B<--delete>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Delete the specified partitions or all partitions. It is not error to remove "
"non-existing partitions, so this option is possible to use together with "
"large B<--nr> ranges without care about the current partitions set on the "
"device."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-g>, B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do not print a header line with B<--show> or B<--raw>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-l>, B<--list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"List the partitions. Note that all numbers are in 512-byte sectors. This "
"output format is DEPRECATED in favour of B<--show>. Do not use it in newly "
"written scripts."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--nr> I<M>B<:>I<N>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify the range of partitions. For backward compatibility also the format "
"I<M>B<->I<N> is supported. The range may contain negative numbers, for "
"example B<--nr -1:-1> means the last partition, and B<--nr -2:-1> means the "
"last two partitions. Supported range specifications are:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<M>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specifies just one partition (e.g. B<--nr 3>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<M>B<:>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specifies the lower limit only (e.g. B<--nr 2:>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<:>I<N>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specifies the upper limit only (e.g. B<--nr :4>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<M>B<:>I<N>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specifies the lower and upper limits (e.g. B<--nr 2:4>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Define the output columns to use for B<--show>, B<--pairs> and B<--raw> "
"output. If no output arrangement is specified, then a default set is used. "
"Use B<--help> to get I<list> of all supported columns. This option cannot be "
"combined with the B<--add>, B<--delete>, B<--update> or B<--list> options."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--output-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Output all available columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-P>, B<--pairs>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List the partitions using the KEY=\"value\" format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List the partitions using the raw output format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--show>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"List the partitions. The output columns can be selected and rearranged with "
"the B<--output> option. All numbers (except SIZE) are in 512-byte sectors."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-t>, B<--type> I<type>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Specify the partition table type."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--list-types>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List supported partition types and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-u>, B<--update>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Update the specified partitions."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-S>, B<--sector-size> I<size>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Overwrite default sector size."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Verbose mode."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "LIBBLKID_DEBUG=all"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "enables libblkid debug output."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"partx --show /dev/sdb3, partx --show --nr 3 /dev/sdb, partx --show /dev/"
"sdb3 /dev/sdb"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "All three commands list partition 3 of I</dev/sdb>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "partx --show - /dev/sdb3"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Lists all subpartitions on I</dev/sdb3> (the device is used as whole-disk)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "partx -o START -g --nr 5 /dev/sdb"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Prints the start sector of partition 5 on I</dev/sdb> without header."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "partx -o SECTORS,SIZE /dev/sda5 /dev/sda"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Lists the length in sectors and human-readable size of partition 5 on I</dev/"
"sda>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "partx --add --nr 3:5 /dev/sdd"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Adds all available partitions from 3 to 5 (inclusive) on I</dev/sdd>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "partx -d --nr :-1 /dev/sdd"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Removes the last partition on I</dev/sdd>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The original version was written by"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<addpart>(8), B<delpart>(8), B<fdisk>(8), B<parted>(8), B<partprobe>(8)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<partx> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
