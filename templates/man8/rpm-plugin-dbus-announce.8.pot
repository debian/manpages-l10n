# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:38+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "RPM-DBUS-ANNOUNCE"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "03 Jun 2020"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "rpm-plugin-dbus-announce - DBus plugin for the RPM Package Manager"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Description"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The plugin writes basic information about rpm transactions to the system "
"dbus - like packages installed or removed.  Other programs can subscribe to "
"the signals to be notified of the packages on the system change."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DBus Signals"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Sends \\f[B]StartTransaction\\f[R] and \\f[B]EndTransaction\\f[R] messages "
"from the \\f[B]/org/rpm/Transaction\\f[R] object with the "
"\\f[B]org.rpm.Transaction\\f[R] interface.\\fR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The signal passes the DB cookie as a string and the transaction id as an "
"unsigned 32 bit integer."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Configuration"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"There are currently no options for this plugin in particular.  See \\f[B]rpm-"
"plugins\\f[R](8) on how to control plugins in general.\\fR"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "\\f[I]dbus-monitor\\f[R](1) \\f[I]rpm-plugins\\f[R](8)\\fR"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid "\\f[B]dbus-monitor\\f[R](1), \\f[B]rpm-plugins\\f[R](8)\\fR"
msgstr ""
