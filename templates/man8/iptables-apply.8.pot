# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "IPTABLES-APPLY"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "iptables 1.8.10"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-16-0
msgid "iptables-apply - a safer way to update iptables remotely"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<iptables-apply> [-B<hV>] [B<-t> I<timeout>] [B<-w> I<savefile>] "
"{[I<rulesfile]|-c [runcmd]}>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"iptables-apply will try to apply a new rulesfile (as output by iptables-"
"save, read by iptables-restore) or run a command to configure iptables and "
"then prompt the user whether the changes are okay. If the new iptables rules "
"cut the existing connection, the user will not be able to answer "
"affirmatively. In this case, the script rolls back to the previous working "
"iptables rules after the timeout expires."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Successfully applied rules can also be written to savefile and later used to "
"roll back to this state. This can be used to implement a store last good "
"configuration mechanism when experimenting with an iptables setup script: "
"iptables-apply -w /etc/iptables/iptables.rules -c /etc/iptables/iptables.run"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"When called as ip6tables-apply, the script will use ip6tables-save/-restore "
"and IPv6 default values instead. Default value for rulesfile is '/etc/"
"iptables/iptables.rules'."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-t> I<seconds>, B<--timeout> I<seconds>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Sets the timeout in seconds after which the script will roll back to the "
"previous ruleset (default: 10)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-w> I<savefile>, B<--write> I<savefile>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify the savefile where successfully applied rules will be written to "
"(default if empty string is given: /etc/iptables/iptables.rules)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c> I<runcmd>, B<--command> I<runcmd>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Run command runcmd to configure iptables instead of applying a rulesfile "
"(default: /etc/iptables/iptables.run)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Display usage information."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Display version information."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<iptables-restore>(8), B<iptables-save>(8), B<iptables>(8)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "LEGALESE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Original iptables-apply - Copyright 2006 Martin F. Krafft "
"E<lt>madduck@madduck.netE<gt>.  Version 1.1 - Copyright 2010 GW "
"E<lt>gw.2010@tnode.com or http://gw.tnode.com/E<gt>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This manual page was written by Martin F. Krafft "
"E<lt>madduck@madduck.netE<gt> and extended by GW E<lt>gw.2010@tnode.com or "
"http://gw.tnode.com/E<gt>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Permission is granted to copy, distribute and/or modify this document under "
"the terms of the Artistic License 2.0."
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "iptables 1.8.9"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Successfully applied rules can also be written to savefile and later used to "
"roll back to this state. This can be used to implement a store last good "
"configuration mechanism when experimenting with an iptables setup script: "
"iptables-apply -w /etc/network/iptables.up.rules -c /etc/network/"
"iptables.up.run"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"When called as ip6tables-apply, the script will use ip6tables-save/-restore "
"and IPv6 default values instead. Default value for rulesfile is '/etc/"
"network/iptables.up.rules'."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Specify the savefile where successfully applied rules will be written to "
"(default if empty string is given: /etc/network/iptables.up.rules)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Run command runcmd to configure iptables instead of applying a rulesfile "
"(default: /etc/network/iptables.up.run)."
msgstr ""

#. type: TH
#: debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "iptables 1.8.11"
msgstr ""

#. type: Plain text
#: debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "iptables-apply \\(em a safer way to update iptables remotely"
msgstr ""
