# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:37+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "CHMEM"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "chmem - configure memory"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<chmem> [B<-h] [>-V*] [B<-v>] [B<-e>|B<-d>] [I<SIZE>|I<RANGE> B<-b> "
"I<BLOCKRANGE>] [B<-z> I<ZONE>]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The chmem command sets a particular size or range of memory online or "
"offline."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify I<SIZE> as E<lt>sizeE<gt>[m|M|g|G]. With m or M, E<lt>sizeE<gt> "
"specifies the memory size in MiB (1024 x 1024 bytes). With g or G, "
"E<lt>sizeE<gt> specifies the memory size in GiB (1024 x 1024 x 1024 bytes). "
"The default unit is MiB."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify I<RANGE> in the form 0xE<lt>startE<gt>-0xE<lt>endE<gt> as shown in "
"the output of the B<lsmem>(1) command. E<lt>startE<gt> is the hexadecimal "
"address of the first byte and E<lt>endE<gt> is the hexadecimal address of "
"the last byte in the memory range."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify I<BLOCKRANGE> in the form E<lt>firstE<gt>-E<lt>lastE<gt> or "
"E<lt>blockE<gt> as shown in the output of the B<lsmem>(1) command. "
"E<lt>firstE<gt> is the number of the first memory block and E<lt>lastE<gt> "
"is the number of the last memory block in the memory range. Alternatively a "
"single block can be specified. I<BLOCKRANGE> requires the B<--blocks> option."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify I<ZONE> as the name of a memory zone, as shown in the output of the "
"B<lsmem -o +ZONES> command. The output shows one or more valid memory zones "
"for each memory range. If multiple zones are shown, then the memory range "
"currently belongs to the first zone. By default, B<chmem> will set memory "
"online to the zone Movable, if this is among the valid zones. This default "
"can be changed by specifying the B<--zone> option with another valid zone. "
"For memory ballooning, it is recommended to select the zone Movable for "
"memory online and offline, if possible. Memory in this zone is much more "
"likely to be able to be offlined again, but it cannot be used for arbitrary "
"kernel allocations, only for migratable pages (e.g., anonymous and page "
"cache pages). Use the B<--help> option to see all available zones."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<SIZE> and I<RANGE> must be aligned to the Linux memory block size, as "
"shown in the output of the B<lsmem>(1) command."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Setting memory online can fail for various reasons. On virtualized systems "
"it can fail if the hypervisor does not have enough memory left, for example "
"because memory was overcommitted. Setting memory offline can fail if Linux "
"cannot free the memory. If only part of the requested memory can be set "
"online or offline, a message tells you how much memory was set online or "
"offline instead of the requested amount."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When setting memory online B<chmem> starts with the lowest memory block "
"numbers. When setting memory offline B<chmem> starts with the highest memory "
"block numbers."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--blocks>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Use a I<BLOCKRANGE> parameter instead of I<RANGE> or I<SIZE> for the B<--"
"enable> and B<--disable> options."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-d>, B<--disable>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Set the specified I<RANGE>, I<SIZE>, or I<BLOCKRANGE> of memory offline."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-e>, B<--enable>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Set the specified I<RANGE>, I<SIZE>, or I<BLOCKRANGE> of memory online."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-z>, B<--zone>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Select the memory I<ZONE> where to set the specified I<RANGE>, I<SIZE>, or "
"I<BLOCKRANGE> of memory online or offline. By default, memory will be set "
"online to the zone Movable, if possible."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Verbose mode. Causes B<chmem> to print debugging messages about it\\(cqs "
"progress."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<chmem> has the following exit status values:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<0>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "success"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<1>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "failure"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<64>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "partial success"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<chmem --enable 1024>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This command requests 1024 MiB of memory to be set online."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<chmem -e 2g>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This command requests 2 GiB of memory to be set online."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<chmem --disable 0x00000000e4000000-0x00000000f3ffffff>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"This command requests the memory range starting with 0x00000000e4000000 and "
"ending with 0x00000000f3ffffff to be set offline."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<chmem -b -d 10>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This command requests the memory block number 10 to be set offline."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<lsmem>(1)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<chmem> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
