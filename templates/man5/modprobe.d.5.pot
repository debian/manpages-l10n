# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:31+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "MODPROBE.D"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-08-26"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "kmod"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "modprobe.d"
msgstr ""

#. #-#-#-#-#  archlinux: modprobe.d.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: modprobe.d.5.pot (PACKAGE VERSION)  #-#-#-#-#
#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#. #-#-#-#-#  debian-unstable: modprobe.d.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-41: modprobe.d.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-rawhide: modprobe.d.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: modprobe.d.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-leap-16-0: modprobe.d.5.pot (PACKAGE VERSION)  #-#-#-#-#
#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: modprobe.d.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "modprobe.\\&d - Configuration directory for modprobe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "/etc/modprobe.\\&d/*.\\&conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "/run/modprobe.\\&d/*.\\&conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "/usr/local/lib/modprobe.\\&d/*.\\&conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "/usr/lib/modprobe.\\&d/*.\\&conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "/lib/modprobe.\\&d/*.\\&conf"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Because the B<modprobe> command can add or remove more than one module, due "
"to modules having dependencies, we need a method of specifying what options "
"are to be used with those modules.\\& One can also use them to create "
"convenient aliases: alternate names for a module, or they can override the "
"normal B<modprobe> behavior altogether for those with special requirements "
"(such as inserting more than one module).\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Note that module and alias names (like other module names) can have - or _ "
"in them: both are interchangeable throughout all the module commands as "
"underscore conversion happens automatically.\\&"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION FORMAT"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The configuration files contain one command per line, with blank lines and "
"lines starting with '\\&#'\\& ignored (useful for adding comments).\\& A "
"'\\&\\e'\\& at the end of a line causes it to continue on the next line, "
"which makes the files a bit neater.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "See the COMMANDS section below for more.\\&"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Configuration files are read from directories in listed in SYNOPSYS in that "
"order of precedence.\\& Once a file of a given filename is loaded, any file "
"of the same name in subsequent directories is ignored.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"All configuration files are sorted in lexicographic order, regardless of the "
"directory they reside in.\\& Configuration files can either be completely "
"replaced (by having a new configuration file with the same name in a "
"directory of higher priority) or partially replaced (by having a "
"configuration file that is ordered later).\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"NOTE: The configuration directories may be altered via the MODPROBE_OPTIONS "
"environment variable.\\& See the ENVIRONMENT section in B<modprobe>(8).\\&"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COMMANDS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "alias I<wildcard> I<modulename>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This allows you to give alternate names for a module.\\& For example: "
"\"alias my-mod really_long_modulename\" means you can use \"modprobe my-"
"mod\" instead of \"modprobe really_long_modulename\".\\& You can also use "
"shell-style wildcards, so \"alias my-mod* really_long_modulename\" means "
"that \"modprobe my-mod-something\" has the same effect.\\& You can'\\&t have "
"aliases to other aliases (that way lies madness), but aliases can have "
"options, which will be added to any other options.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Note that modules can also contain their own aliases, which you can see "
"using B<modinfo>.\\& These aliases are used as a last resort (ie.\\& if "
"there is no real module, B<install>, B<remove>, or B<alias> command in the "
"configuration).\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "blacklist I<modulename>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Modules can contain their own aliases: usually these are aliases describing "
"the devices they support, such as \"pci:123.\\&.\\&.\\&\".\\& These "
"\"internal\" aliases can be overridden by normal \"alias\" keywords, but "
"there are cases where two or more modules both support the same devices, or "
"a module invalidly claims to support a device that it does not: the "
"B<blacklist> keyword indicates that all of that particular module'\\&s "
"internal aliases are to be ignored.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "install I<modulename> I<command.\\&.\\&.\\&>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This command instructs B<modprobe> to run your command instead of inserting "
"the module in the kernel as normal.\\& The command can be any shell command: "
"this allows you to do any kind of complex processing you might wish.\\& For "
"example, if the module \"fred\" works better with the module \"barney\" "
"already installed (but it doesn'\\&t depend on it, so B<modprobe> won'\\&t "
"automatically load it), you could say \"install fred /sbin/modprobe barney; /"
"sbin/modprobe --ignore-install fred\", which would do what you wanted.\\& "
"Note the B<--ignore-install>, which stops the second B<modprobe> from "
"running the same B<install> command again.\\& See also B<remove> below.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The long term future of this command as a solution to the problem of "
"providing additional module dependencies is not assured and it is intended "
"to replace this command with a warning about its eventual removal or "
"deprecation at some point in a future release.\\& Its use complicates the "
"automated determination of module dependencies by distribution utilities, "
"such as mkinitrd (because these now need to somehow interpret what the "
"B<install> commands might be doing.\\& In a perfect world, modules would "
"provide all dependency information without the use of this command and work "
"is underway to implement soft dependency support within the Linux kernel.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If you use the string \"$CMDLINE_OPTS\" in the command, it will be replaced "
"by any options specified on the modprobe command line.\\& This can be useful "
"because users expect \"modprobe fred opt=1\" to pass the \"opt=1\" arg to "
"the module, even if there'\\&s an install command in the configuration file."
"\\& So our above example becomes \"install fred /sbin/modprobe barney; /sbin/"
"modprobe --ignore-install fred $CMDLINE_OPTS\""
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "options I<modulename> I<option.\\&.\\&.\\&>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This command allows you to add options to the module I<modulename> (which "
"might be an alias) every time it is inserted into the kernel: whether "
"directly (using B<modprobe> I<modulename>) or because the module being "
"inserted depends on this module.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"All options are added together: they can come from an B<option> for the "
"module itself, for an alias, and on the command line.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "remove I<modulename> I<command.\\&.\\&.\\&>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This is similar to the B<install> command above, except it is invoked when "
"\"modprobe -r\" is run.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"softdep I<modulename> pre: I<modules.\\&.\\&.\\&> post: I<modules.\\&.\\&."
"\\&>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The B<softdep> command allows you to specify soft, or optional, module "
"dependencies.\\& I<modulename> can be used without these optional modules "
"installed, but usually with some features missing.\\& For example, a driver "
"for a storage HBA might require another module be loaded in order to use "
"management features.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"pre-deps and post-deps modules are lists of names and/or aliases of other "
"modules that modprobe will attempt to install (or remove) in order before "
"and after the main module given in the I<modulename> argument.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Example: Assume \"softdep c pre: a b post: d e\" is provided in the "
"configuration.\\& Running \"modprobe c\" is now equivalent to \"modprobe a b "
"c d e\" without the softdep.\\& Flags such as --use-blacklist are applied to "
"all the specified modules, while module parameters only apply to module c.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Note: if there are B<install> or B<remove> commands with the same "
"I<modulename> argument, B<softdep> takes precedence.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "weakdep I<modulename> I<modules.\\&.\\&.\\&>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The B<weakdep> command allows you to specify weak module dependencies.\\& "
"Those are similar to pre softdep, with the difference that userspace "
"doesn'\\&t attempt to load that dependency before the specified module.\\& "
"Instead the kernel may request one or multiple of them during module probe, "
"depending on the hardware it'\\&s binding to.\\& The purpose of weak module "
"is to allow a driver to specify that a certain dependency may be needed, so "
"it should be present in the filesystem (e.\\&g.\\& in initramfs)  when that "
"module is probed.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Example: Assume \"weakdep c a b\".\\& A program creating an initramfs knows "
"it should add a, b, and c to the filesystem since a and b may be required/"
"desired at runtime.\\& When c is loaded and is being probed, it may issue "
"calls to request_module() causing a or b to also be loaded.\\&"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COMPATIBILITY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"A future version of kmod will come with a strong warning to avoid use of the "
"B<install> as explained above.\\& This will happen once support for soft "
"dependencies in the kernel is complete.\\& That support will complement the "
"existing softdep support within this utility by providing such dependencies "
"directly within the modules.\\&"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This manual page originally Copyright 2004, Rusty Russell, IBM Corporation."
"\\&"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<modprobe>(8), B<modules.\\&dep>(5)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Numerous contributions have come from the linux-modules mailing list "
"E<lt>linux-modules@vger.\\&kernel.\\&orgE<gt> and Github.\\& If you have a "
"clone of kmod.\\&git itself, the output of B<git-shortlog>(1) and B<git-"
"blame>(1) can show you the authors for specific parts of the project.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<Lucas De Marchi> E<lt>lucas.\\&de.\\&marchi@gmail.\\&comE<gt> is the "
"current maintainer of the project.\\&"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "MODPROBE\\&.D"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "12/10/2022"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "modprobe.d - Configuration directory for modprobe"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "/lib/modprobe\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "/usr/local/lib/modprobe\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "/run/modprobe\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "/etc/modprobe\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Because the B<modprobe> command can add or remove more than one module, due "
"to modules having dependencies, we need a method of specifying what options "
"are to be used with those modules\\&. All files underneath the /etc/"
"modprobe\\&.d directory which end with the \\&.conf extension specify those "
"options as required\\&. They can also be used to create convenient aliases: "
"alternate names for a module, or they can override the normal B<modprobe> "
"behavior altogether for those with special requirements (such as inserting "
"more than one module)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Note that module and alias names (like other module names) can have - or _ "
"in them: both are interchangeable throughout all the module commands as "
"underscore conversion happens automatically\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"The format of files under modprobe\\&.d is simple: one command per line, "
"with blank lines and lines starting with \\*(Aq#\\*(Aq ignored (useful for "
"adding comments)\\&. A \\*(Aq\\e\\*(Aq at the end of a line causes it to "
"continue on the next line, which makes the file a bit neater\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"This allows you to give alternate names for a module\\&. For example: "
"\"alias my-mod really_long_modulename\" means you can use \"modprobe my-"
"mod\" instead of \"modprobe really_long_modulename\"\\&. You can also use "
"shell-style wildcards, so \"alias my-mod* really_long_modulename\" means "
"that \"modprobe my-mod-something\" has the same effect\\&. You can\\*(Aqt "
"have aliases to other aliases (that way lies madness), but aliases can have "
"options, which will be added to any other options\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Note that modules can also contain their own aliases, which you can see "
"using B<modinfo>\\&. These aliases are used as a last resort (ie\\&. if "
"there is no real module, B<install>, B<remove>, or B<alias> command in the "
"configuration)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Modules can contain their own aliases: usually these are aliases describing "
"the devices they support, such as \"pci:123\\&.\\&.\\&.\"\\&. These "
"\"internal\" aliases can be overridden by normal \"alias\" keywords, but "
"there are cases where two or more modules both support the same devices, or "
"a module invalidly claims to support a device that it does not: the "
"B<blacklist> keyword indicates that all of that particular module\\*(Aqs "
"internal aliases are to be ignored\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "install I<modulename> I<command\\&.\\&.\\&.>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"This command instructs B<modprobe> to run your command instead of inserting "
"the module in the kernel as normal\\&. The command can be any shell command: "
"this allows you to do any kind of complex processing you might wish\\&. For "
"example, if the module \"fred\" works better with the module \"barney\" "
"already installed (but it doesn\\*(Aqt depend on it, so B<modprobe> "
"won\\*(Aqt automatically load it), you could say \"install fred /sbin/"
"modprobe barney; /sbin/modprobe --ignore-install fred\", which would do what "
"you wanted\\&. Note the B<--ignore-install>, which stops the second "
"B<modprobe> from running the same B<install> command again\\&. See also "
"B<remove> below\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"The long term future of this command as a solution to the problem of "
"providing additional module dependencies is not assured and it is intended "
"to replace this command with a warning about its eventual removal or "
"deprecation at some point in a future release\\&. Its use complicates the "
"automated determination of module dependencies by distribution utilities, "
"such as mkinitrd (because these now need to somehow interpret what the "
"B<install> commands might be doing\\&. In a perfect world, modules would "
"provide all dependency information without the use of this command and work "
"is underway to implement soft dependency support within the Linux kernel\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"If you use the string \"$CMDLINE_OPTS\" in the command, it will be replaced "
"by any options specified on the modprobe command line\\&. This can be useful "
"because users expect \"modprobe fred opt=1\" to pass the \"opt=1\" arg to "
"the module, even if there\\*(Aqs an install command in the configuration "
"file\\&. So our above example becomes \"install fred /sbin/modprobe barney; /"
"sbin/modprobe --ignore-install fred $CMDLINE_OPTS\""
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "options I<modulename> I<option\\&.\\&.\\&.>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"This command allows you to add options to the module I<modulename> (which "
"might be an alias) every time it is inserted into the kernel: whether "
"directly (using B<modprobe > I<modulename>) or because the module being "
"inserted depends on this module\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"All options are added together: they can come from an B<option> for the "
"module itself, for an alias, and on the command line\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "remove I<modulename> I<command\\&.\\&.\\&.>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"This is similar to the B<install> command above, except it is invoked when "
"\"modprobe -r\" is run\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"softdep I<modulename> pre: I<modules\\&.\\&.\\&.> post: I<modules\\&.\\&."
"\\&.>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"The B<softdep> command allows you to specify soft, or optional, module "
"dependencies\\&.  I<modulename> can be used without these optional modules "
"installed, but usually with some features missing\\&. For example, a driver "
"for a storage HBA might require another module be loaded in order to use "
"management features\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"pre-deps and post-deps modules are lists of names and/or aliases of other "
"modules that modprobe will attempt to install (or remove) in order before "
"and after the main module given in the I<modulename> argument\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Example: Assume \"softdep c pre: a b post: d e\" is provided in the "
"configuration\\&. Running \"modprobe c\" is now equivalent to \"modprobe a b "
"c d e\" without the softdep\\&. Flags such as --use-blacklist are applied to "
"all the specified modules, while module parameters only apply to module c\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Note: if there are B<install> or B<remove> commands with the same "
"I<modulename> argument, B<softdep> takes precedence\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"A future version of kmod will come with a strong warning to avoid use of the "
"B<install> as explained above\\&. This will happen once support for soft "
"dependencies in the kernel is complete\\&. That support will complement the "
"existing softdep support within this utility by providing such dependencies "
"directly within the modules\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"This manual page originally Copyright 2004, Rusty Russell, IBM "
"Corporation\\&. Maintained by Jon Masters and others\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<modprobe>(8), B<modules.dep>(5)"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<Jon Masters> E<lt>\\&jcm@jonmasters\\&.org\\&E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "Developer"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<Robby Workman> E<lt>\\&rworkman@slackware\\&.com\\&E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<Lucas De Marchi> E<lt>\\&lucas\\&.de\\&.marchi@gmail\\&.com\\&E<gt>"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2024-08-20"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"Configuration files are read from directories in listed in SYNOPSIS in that "
"order of precedence.\\& Once a file of a given filename is loaded, any file "
"of the same name in subsequent directories is ignored.\\&"
msgstr ""

#. type: TH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2024-08-13"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "03/05/2024"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "/usr/lib/modprobe\\&.d/*\\&.conf"
msgstr ""
