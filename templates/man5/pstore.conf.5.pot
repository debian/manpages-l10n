# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:35+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "PSTORE\\&.CONF"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "pstore.conf"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "pstore.conf, pstore.conf.d - PStore configuration file"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "/etc/systemd/pstore\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "/run/systemd/pstore\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "/usr/local/lib/systemd/pstore\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "/usr/lib/systemd/pstore\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "/etc/systemd/pstore\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "/run/systemd/pstore\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "/usr/local/lib/systemd/pstore\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "/usr/lib/systemd/pstore\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"This file configures the behavior of B<systemd-pstore>(8), a tool for "
"archiving the contents of the persistent storage filesystem, "
"\\m[blue]B<pstore>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. The main "
"configuration file is loaded from one of the listed directories in order of "
"priority, only the first file found is used: /etc/systemd/, /run/systemd/, /"
"usr/local/lib/systemd/ \\&\\s-2\\u[2]\\d\\s+2, /usr/lib/systemd/\\&. The "
"vendor version of the file contains commented out entries showing the "
"defaults as a guide to the administrator\\&. Local overrides can also be "
"created by creating drop-ins, as described below\\&. The main configuration "
"file can also be edited for this purpose (or a copy in /etc/ if it\\*(Aqs "
"shipped under /usr/), however using drop-ins for local configuration is "
"recommended over modifications to the main configuration file\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid ""
"In addition to the main configuration file, drop-in configuration snippets "
"are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/systemd/"
"*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins have "
"higher precedence and override the main configuration file\\&. Files in the "
"*\\&.conf\\&.d/ configuration subdirectories are sorted by their filename in "
"lexicographic order, regardless of in which of the subdirectories they "
"reside\\&. When multiple files specify the same option, for options which "
"accept just a single value, the entry in the file sorted last takes "
"precedence, and for options which accept a list of values, entries are "
"collected as they occur in the sorted files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering\\&. This also defines a concept of drop-in "
"priorities to allow OS vendors to ship drop-ins within a specific range "
"lower than the range used by users\\&. This should lower the risk of package "
"drop-ins overriding accidentally drop-ins defined by users\\&. It is "
"recommended to use the range 10-40 for drop-ins in /usr/ and the range 60-90 "
"for drop-ins in /etc/ and /run/, to make sure that local and transient drop-"
"ins take priority over drop-ins shipped by the OS vendor\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"To disable a configuration file supplied by the vendor, the recommended way "
"is to place a symlink to /dev/null in the configuration directory in /etc/, "
"with the same filename as the vendor configuration file\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "All options are configured in the [PStore] section:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "I<Storage=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"Controls where to archive (i\\&.e\\&. copy) files from the pstore "
"filesystem\\&. One of \"none\", \"external\", and \"journal\"\\&. When "
"\"none\", the tool exits without processing files in the pstore "
"filesystem\\&. When \"external\" (the default), files are archived into /var/"
"lib/systemd/pstore/, and logged into the journal\\&. When \"journal\", "
"pstore file contents are logged only in the journal\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "Added in version 243\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "I<Unlink=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"Controls whether or not files are removed from pstore after processing\\&. "
"Takes a boolean value\\&. When true, a pstore file is removed from the "
"pstore once it has been archived (either to disk or into the journal)\\&. "
"When false, processing of pstore files occurs normally, but the files remain "
"in the pstore\\&. The default is true in order to maintain the pstore in a "
"nearly empty state, so that the pstore has storage available for the next "
"kernel error event\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"The defaults for all values are listed as comments in the template /etc/"
"systemd/pstore\\&.conf file that is installed by default\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "B<systemd-pstore.service>(8)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "pstore"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"\\%https://docs.kernel.org/admin-guide/abi-testing.html#abi-sys-fs-pstore"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid ""
"💣💥🧨💥💥💣 Please note that those configuration files must be available at "
"all times. If /usr/local/ is a separate partition, it may not be available "
"during early boot, and must not be used for configuration."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "/etc/systemd/pstore\\&.conf /etc/systemd/pstore\\&.conf\\&.d/*"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. "
"Initially, the main configuration file in /etc/systemd/ contains commented "
"out entries showing the defaults as a guide to the administrator\\&. Local "
"overrides can be created by editing this file or by creating drop-ins, as "
"described below\\&. Using drop-ins for local configuration is recommended "
"over modifications to the main configuration file\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"In addition to the \"main\" configuration file, drop-in configuration "
"snippets are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/"
"systemd/*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins "
"have higher precedence and override the main configuration file\\&. Files in "
"the *\\&.conf\\&.d/ configuration subdirectories are sorted by their "
"filename in lexicographic order, regardless of in which of the "
"subdirectories they reside\\&. When multiple files specify the same option, "
"for options which accept just a single value, the entry in the file sorted "
"last takes precedence, and for options which accept a list of values, "
"entries are collected as they occur in the sorted files\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-41 mageia-cauldron
msgid "B<systemd-journald.service>(8)"
msgstr ""

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. The main "
"configuration file is either in /usr/lib/systemd/ or /etc/systemd/ and "
"contains commented out entries showing the defaults as a guide to the "
"administrator\\&. Local overrides can be created by creating drop-ins, as "
"described below\\&. The main configuration file can also be edited for this "
"purpose (or a copy in /etc/ if it\\*(Aqs shipped in /usr/) however using "
"drop-ins for local configuration is recommended over modifications to the "
"main configuration file\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&. This also defined a concept "
"of drop-in priority to allow distributions to ship drop-ins within a "
"specific range lower than the range used by users\\&. This should lower the "
"risk of package drop-ins overriding accidentally drop-ins defined by "
"users\\&."
msgstr ""
