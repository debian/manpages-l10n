# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 18:09+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_oom_score_adj"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/oom_score_adj - OOM-killer score adjustment"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</oom_score_adj> (since Linux 2.6.36)"
msgstr ""

#.  Text taken from Linux 3.7 Documentation/filesystems/proc.txt
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file can be used to adjust the badness heuristic used to select which "
"process gets killed in out-of-memory conditions."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The badness heuristic assigns a value to each candidate task ranging from 0 "
"(never kill) to 1000 (always kill) to determine which process is targeted.  "
"The units are roughly a proportion along that range of allowed memory the "
"process may allocate from, based on an estimation of its current memory and "
"swap use.  For example, if a task is using all allowed memory, its badness "
"score will be 1000.  If it is using half of its allowed memory, its score "
"will be 500."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There is an additional factor included in the badness score: root processes "
"are given 3% extra memory over other tasks."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The amount of \"allowed\" memory depends on the context in which the OOM-"
"killer was called.  If it is due to the memory assigned to the allocating "
"task's cpuset being exhausted, the allowed memory represents the set of mems "
"assigned to that cpuset (see B<cpuset>(7)).  If it is due to a mempolicy's "
"node(s) being exhausted, the allowed memory represents the set of mempolicy "
"nodes.  If it is due to a memory limit (or swap limit) being reached, the "
"allowed memory is that configured limit.  Finally, if it is due to the "
"entire system being out of memory, the allowed memory represents all "
"allocatable resources."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value of I<oom_score_adj> is added to the badness score before it is "
"used to determine which task to kill.  Acceptable values range from -1000 "
"(OOM_SCORE_ADJ_MIN) to +1000 (OOM_SCORE_ADJ_MAX).  This allows user space to "
"control the preference for OOM-killing, ranging from always preferring a "
"certain task or completely disabling it from OOM-killing.  The lowest "
"possible value, -1000, is equivalent to disabling OOM-killing entirely for "
"that task, since it will always report a badness score of 0."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Consequently, it is very simple for user space to define the amount of "
"memory to consider for each task.  Setting an I<oom_score_adj> value of "
"+500, for example, is roughly equivalent to allowing the remainder of tasks "
"sharing the same system, cpuset, mempolicy, or memory controller resources "
"to use at least 50% more memory.  A value of -500, on the other hand, would "
"be roughly equivalent to discounting 50% of the task's allowed memory from "
"being considered as scoring against the task."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For backward compatibility with previous kernels, I</proc/>pidI</oom_adj> "
"can still be used to tune the badness score.  Its value is scaled linearly "
"with I<oom_score_adj>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Writing to I</proc/>pidI</oom_score_adj> or I</proc/>pidI</oom_adj> will "
"change the other with its scaled value."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<choom>(1)  program provides a command-line interface for adjusting the "
"I<oom_score_adj> value of a running process or a newly executed command."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</oom_adj> (since Linux 2.6.11)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file can be used to adjust the score used to select which process "
"should be killed in an out-of-memory (OOM) situation.  The kernel uses this "
"value for a bit-shift operation of the process's I<oom_score> value: valid "
"values are in the range -16 to +15, plus the special value -17, which "
"disables OOM-killing altogether for this process.  A positive score "
"increases the likelihood of this process being killed by the OOM-killer; a "
"negative score decreases the likelihood."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The default value for this file is 0; a new process inherits its parent's "
"I<oom_adj> setting.  A process must be privileged (B<CAP_SYS_RESOURCE>)  to "
"update this file, although a process can always increase its own I<oom_adj> "
"setting (since Linux 2.6.20)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since Linux 2.6.36, use of this file is deprecated in favor of I</proc/"
">pidI</oom_score_adj>, and finally removed in Linux 3.7."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5), B<proc_pid_oom_score>(5)"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-11-24"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
